var app = {};

$(document).ready(function () {

    $('#btn-app-connect').on('click', function () {

        $(this).html('<i class="icon-refresh icon-spin"></i> Connecting...');

        var ws = new WebSocket("ws://localhost:8081/service");
        ws.binaryType = "arraybuffer";

        ws.onopen = function () {
            //alert("About to send data");
            ws.send(response2bytes("Hello World", 0)); // I WANT TO SEND THIS MESSAGE TO THE SERVER!!!!!!!!
            //alert("Message sent!");
            $('#ul-app-connect').hide();
            $('#ul-app-connected').show();
        };

        ws.onmessage = function (evt) {
            //alert("About to receive data");
            // var bytes = evt.data;
            // var int32View = new Int32Array( bytes, 0, 2 );
            // var floatView = new Float32Array( bytes, 4, 1 );
            // console.log( "msg type: " + evt.type );
            // console.log( "msg name: " + int32View[0] );
            // console.log( "msg data: " + int32View[1] );
            // window.app.fps = int32View[1];
            // if ( app.remote_peer ) {
            //     app.remote_peer.send( bytes );
            // }


            var bytes = new Uint8Array(evt.data);
            var type = bytes[0];
            var id = ab2int(bytes.subarray(1,5));
            bytes = bytes.subarray(5);
            console.log( "msg type: " + type );
            console.log( "msg id: " + id );

            switch(type){
                case 16:
                var blob = new Blob([ab2str(bytes)],{type: "text/plain"});
                    uploadFile(blob,"bpTest",function(file) {
                                    console.log(file);
                                    GetFile(file.id,function(data){
                                        console.log(data);
                                    });
                                });
                    ws.send(response2bytes("Blueprint Saved?", id));
                break;
            }

        };

        ws.onclose = function () {
            // websocket is closed.
            alert("Connection is closed...");
        };

    });

    $('#btn-sign-in').on('click', function () {

        $(this).hide();
        $('#btn-sign-in-loading').show();

        app.user = $('#input-sign-in').val() || $('#input-sign-in').attr("placeholder");

        var peer = new Peer(app.user, {key: 'qlahkw7gx8aj1yvi', 'iceServers': [
            { 'url': 'stun:stun1.l.google.com:19302' },
            { 'url': 'stun:stun2.l.google.com:19302' },
            { 'url': 'stun:stun3.l.google.com:19302' },
            { 'url': 'stun:stun4.l.google.com:19302' }
        ], debug: true } );

        peer.on('open', function() {
            console.log('Connected to peer-server, user: ' + app.user);
            $('#div-sign-in-user').hide();
            $('#ul-signed-in-user').show();
            $('#a-signed-in-user').html('<i class="icon-user"></i> '+app.user);
        });

        peer.on('error', function(error) {
            console.log(error.type);
            $('#btn-sign-in-loading').hide();
            $('#btn-sign-in').show();
        });

        peer.on('connection', function(conn){
            window.app.remote_peer = conn;
            conn.on('data', function(data) {
                console.log('Got data:', data);
            });
        });

    });
});
