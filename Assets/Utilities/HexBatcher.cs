using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class HexBatcher : MonoBehaviour {
	
	public GameObject root;
	public List<GameObject> childs = new List<GameObject>();
	
	public bool initialBatchFinished = false;
	
	public static HexBatcher instance;
	
	void Awake () {
		instance = this;
	}
	

	void _Update () {
		if(Input.GetKeyDown(KeyCode.B)){
			var pos = new Vector3(Random.Range(-10,10),Random.Range(-10,10),Random.Range(-10,10));
			var newHex = (GameObject)Instantiate(Resources.Load("Bot/SmallHexEarth"),pos,Quaternion.identity);
			newHex.name = "SmallHexEarth"; 
			childs.Add(newHex);
		}
		if(Input.GetKeyDown(KeyCode.S)){
			StaticBatchingUtility.Combine(childs.ToArray(),root);
		}
	}
	
	public void AddHexForBatch(GameObject hex){	
		childs.Add(hex);
	}
	
	public void BatchWorld(){
        StartCoroutine(Batch());
	}

    IEnumerator Batch(){
        yield return null;
        StaticBatchingUtility.Combine(childs.ToArray(),root);
        Bullet.SetForceUpdateAllAabbs(0);
        initialBatchFinished = true;
    }

    public void ResetForceUpdateAllAabbsProperty(){
        StartCoroutine(DelayedSetForceUpdateAllAabbs());
    }
    /* this ugly function sets a property that needs a tick to update everything D: */
    IEnumerator DelayedSetForceUpdateAllAabbs(){

        Bullet.SetForceUpdateAllAabbs(1);
        yield return new WaitForFixedUpdate();
        Bullet.SetForceUpdateAllAabbs(0);
    }

	
	
}
