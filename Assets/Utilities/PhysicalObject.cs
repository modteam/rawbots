using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;
using Oracle;

public class PhysicalObject : MonoBehaviour
{

	public float multiplier = 1;
	bool inRange = true;
	bool locked = false;
	int dustMask;
	bool inWater = false;
	new Transform transform;
	public BulletRigidBody bulletRigidbody;
    float lastFxSpawn = 0;
	private Vector3 currentGravity = Vector3.up;
	private Vector3 positionLastFrame; //z26

	void Awake ()
	{
		dustMask = 1 << LayerMask.NameToLayer ("world");
		transform = gameObject.transform;
	}

	void Start ()
	{
		if (bulletRigidbody != default(BulletRigidBody)) {
			bulletRigidbody.AddOnCollisionDelegate (OnCollision);
		}
	}

	public void OnCollision (Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction)
	{
		if(other.nodeview != default(NodeView)){
		if (relativeVelocity.sqrMagnitude > 300 ) {
			if (!inWater && (((1 << other.gameObject.layer) & dustMask) != 0) && Time.time - lastFxSpawn > 0.2f) {
                if(other.nodeview.proxy is HexEarthProxy){
//                    Debug.Log("Este es " + ((HexEarthProxy)other.nodeview.proxy).fx );
                    //TODO:Implement custom FX HERE
                }
				var fx = Instantiate (Resources.Load ("FX/Dust", typeof(GameObject)),contactPoint,transform.rotation) as GameObject;
				fx.transform.parent = Gaia.Instance ().scratch;
				fx.hideFlags = HideFlags.HideInHierarchy;
				fx.transform.position = contactPoint;
				fx.transform.rotation = transform.rotation;
                lastFxSpawn = Time.time;
				Destroy (fx, 5);
			}
		}
		}
	}

	public Vector3 Up ()
	{
		return Util.FastNormalize(currentGravity)*-1f;
		//return (transform.position - CenterOfGravity ()).normalized;
	}
	
	Vector3 centerOfGravity = Vector3.zero;

	public Vector3 CenterOfGravity ()
	{
		return centerOfGravity;
	}
	
	public Vector3 Gravity ()
	{
		Vector3 gravity = Vector3.zero;
		var newCenterOfGravity = Vector3.zero;
		int gravitiesCounter = 0;
		UnityEngine.Profiling.Profiler.BeginSample("gravity forloop");
		UnityEngine.Profiling.Profiler.BeginSample("count");
		var count = GravityField.fields.Count;
		UnityEngine.Profiling.Profiler.EndSample();
		for (int i = 0; i<count; ++i) {
			var gravityField = GravityField.fields [i];
			UnityEngine.Profiling.Profiler.BeginSample("get data");
			var pos = gravityField.transform.position;
			var radio = gravityField.transform.localScale.x / 2f;
			UnityEngine.Profiling.Profiler.EndSample();
			UnityEngine.Profiling.Profiler.BeginSample("testing");
			var test = (transform.position - pos).sqrMagnitude < radio*radio;
			UnityEngine.Profiling.Profiler.EndSample();
			
			UnityEngine.Profiling.Profiler.BeginSample("if distance");
			if (test) {
				UnityEngine.Profiling.Profiler.BeginSample("compute");
				gravity += gravityField.Compute (transform.position);
				UnityEngine.Profiling.Profiler.EndSample();
				newCenterOfGravity = newCenterOfGravity+ gravityField.GetCenter(transform.position);// GravityField.fields [i].transform.position;
				gravitiesCounter = gravitiesCounter+1;
			}
			UnityEngine.Profiling.Profiler.EndSample();
		}
		UnityEngine.Profiling.Profiler.EndSample();
		UnityEngine.Profiling.Profiler.BeginSample("new center of gravity");
		if(gravitiesCounter > 0){
			centerOfGravity = newCenterOfGravity/gravitiesCounter;
			currentGravity = gravity;
		}
		UnityEngine.Profiling.Profiler.EndSample();
		
		return gravity * multiplier;
	}

	void Lock ()
	{
		var nodeView = GetComponent< NodeView > ();
		if (nodeView == null) {
			return;
		}
		var partProxy = (nodeView.proxy as PartProxy);
		if (partProxy == null) {
			return;
		}
		var phyObjs = partProxy.GridParts ()
            .Where (p => p.nodeView != null)
            .Select (p => p.nodeView.GetComponent< PhysicalObject > ())
            .Where (p => p != null);
		var outOfRange = !phyObjs.Any (d => d.inRange);
		if (outOfRange) {
			//NOT WORKING YET NOT NEEDED
//            foreach ( var phyObj in phyObjs ) {
//                phyObj.rigidbody.isKinematic = true;
//                phyObj.locked = true;
//            }
		}
	}

	void Unlock ()
	{
		var nodeView = GetComponent< NodeView > ();
		if (nodeView == null) {
			return;
		}
		var partProxy = (nodeView.proxy as PartProxy);
		if (partProxy == null) {
			return;
		}
		var phyObjs = partProxy.Parts ()
            .Where (p => p.nodeView != null)
            .Select (p => p.nodeView.GetComponent< PhysicalObject > ())
            .Where (p => p != null);
		var inRange = !phyObjs.Any (d => !d.inRange);
		if (inRange && phyObjs.All (p => p.locked)) {
			foreach (var phyObj in phyObjs) {
				phyObj.GetComponent<Rigidbody>().isKinematic = false;
				phyObj.locked = false;
			}
			var dependants = new HashSet< Entity > ();
			Hyperspace.Entities (GetComponent< NodeView > ().proxy.entity, dependants,new string[]{"input_for","output_for","extends","slot_for","linked_to","consumes"});
			var proxies = dependants.Select (d => d.proxy).Where (p => p != null).ToList ();

			proxies.ForEach (io => io.OnPreBuild ());
			proxies.ForEach (io => io.OnBuild ());
			proxies.ForEach (io => io.OnPostBuild ());
		}
	}

	void FixedUpdate ()
	{

		if (bulletRigidbody != null) {

			//only update strength of gravity if the part moved enough (0.05 = arbitrary treshold).
			//This means changing gravity fields (if we ever add these) wont be handled correctly when the part is still. -z26
			if ( (transform.position - positionLastFrame).sqrMagnitude > 0.05 ) {
			positionLastFrame = transform.position;
			bulletRigidbody.SetGravity (Gravity ());
			}	
		}

	}

	void OnWaterEnter (Water water)
	{
		inWater = true;
	}

	void OnWaterExit (Water water)
	{
		inWater = false;
	}

	

}
