using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class SubPartImporter{

    public static List<Entity> FilterSubparts(List<Entity> entities){
        Entity elbow = default(Entity);
        Entity elbow_base = default(Entity);
        Entity elbow_rotor = default(Entity);
        
        Gaia.Instance().entities.TryGetValue("elbow",out elbow);
        Gaia.Instance().entities.TryGetValue("elbow_base",out elbow_base);
        Gaia.Instance().entities.TryGetValue("elbow_rotor",out elbow_rotor);
        
        if(elbow != default(Entity) && elbow_base != default(Entity) && elbow_rotor != default(Entity)){
            entities = Filter(entities,elbow,elbow_base,elbow_rotor,"elbow_slot_1");
        }

        Entity piston = default(Entity);
        Entity piston_base = default(Entity);
        Entity piston_head = default(Entity);
        
        Gaia.Instance().entities.TryGetValue("piston",out piston);
        Gaia.Instance().entities.TryGetValue("piston_base",out piston_base);
        Gaia.Instance().entities.TryGetValue("piston_head",out piston_head);

        if(piston != default(Entity) && piston_base != default(Entity) && piston_head != default(Entity) ){
            entities = Filter(entities,piston,piston_base,piston_head,"piston_slot_0");
        }

        Entity motor = default(Entity);
        Entity motor_base = default(Entity);
        Entity motor_rotor = default(Entity);
        
        Gaia.Instance().entities.TryGetValue("motor",out motor);
        Gaia.Instance().entities.TryGetValue("motor_base",out motor_base);
        Gaia.Instance().entities.TryGetValue("motor_rotor",out motor_rotor);
        
        if(motor != default(Entity) && motor_base != default(Entity) && motor_rotor != default(Entity) ){
            entities = Filter(entities,motor,motor_base,motor_rotor,"motor_slot_0");
        }

        return entities;
    }


    static List<Entity> Filter(List<Entity> entities, Entity oldBase, Entity newBase, Entity newMobile, string slot_name ){
        var oldParts = entities.Where(e => e.Heads(p => p == "as").FirstOrDefault() == oldBase).ToList();
        var special_slot = Gaia.Instance().entities[slot_name];
        for(int i = 0 ; i < oldParts.Count ; ++i){
            var e = oldParts[i];
            entities.Remove(e);
            Gaia.Instance().entities.Remove(e.id);
            var id = e.id;
            var eBase = Gaia.Instance().GetEntity(id);
            var eRotor = Gaia.Instance().GetEntity("e_" + (uint)eBase.GetHashCode() + "_mobile");
            new Relationship(eRotor,"linked_to",eBase);
            for(int j = 0 ; j < e.properties.Count ; ++j ){
                var p = e.properties[j];
                eBase.SetProperty(p.Name,p.Value);
                eRotor.SetProperty(p.Name,p.Value);
            }
            var relationships = e.Relationships().ToList();
            for(int k = 0 ; k < relationships.Count; ++k ){
                var r = relationships[k];
                var head = r.Head;
                var tail = r.Tail;
                var predicate = r.Predicate;
                r.Break();
                if(predicate == "as"){
                    new Relationship(eBase, predicate,newBase);
                    new Relationship(eRotor, predicate,newMobile);
                }else if(predicate == "local_to"){
                    new Relationship(eBase,predicate,head);
                    new Relationship(eRotor,predicate,head);
                }
                else if(predicate == "slot_for"){
                    if(tail.InmediateParent() == special_slot){
                        tail.RemoveProperty("parent");
                        tail.SetProperty("parent","main");
                        new Relationship(tail,predicate,eRotor);
                    }else{
                        new Relationship(tail,predicate,eBase);
                    }
                    
                }else{
                    if(head == e){
                        new Relationship(tail,predicate,eBase);
                    }else if(tail == e){
                        new Relationship(eBase,predicate,head);
                    }
                }
            }
            
            entities.Add(eBase);
            entities.Add(eRotor);
        }

        return entities;
    }
}
