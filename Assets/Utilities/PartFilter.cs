﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Operants;

public static class PartFilter { //by z26
    
    
    static List<string> allowedWords = new List<string> {
        "all","subpart","nohealth","actuator","extension","weapon","any_wheel","hyper","propeller"};
            

    static PartFilter() {
        //I think this static class will be initialized the first time its called anywhere.
        //the following line's stolen from hexgrid.cs.
        var parts = Gaia.Instance().entities[ "part" ].Tails( p => p == "is" ).Select(e => e.id).ToList();
        allowedWords.AddRange(parts);
        int loops = allowedWords.Count;
        for(int i = 0; i < loops; i++) { //add negative version of all keywords.
            allowedWords.Add("-"+ allowedWords[i]);
        }
        //foreach(string word in allowedWords) {Debug.Log(word);}
    }

    
    
    public static List<string> ProcessInput(string input) {
        var inputParams = input.Split(new Char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        
        List<string> badParams = new List<string>();
        List<string> errorsMsg = new List<string>();
        
        foreach(string param in inputParams) {
            if(!allowedWords.Contains(param)) {
                badParams.Add(param);
                errorsMsg.Add("\"" + param + "\" is not a valid part name or option");
            }
        }

        if(badParams.Count > 0) { Console.PushError("filter", errorsMsg.ToArray()); }
        return inputParams.Except(badParams).ToList();
    }
    
    
    
    public static IEnumerable <PartProxy> Apply(IEnumerable <PartProxy> inputParts, List<string> filter) {
        var output = new List <PartProxy>();
        
        foreach(PartProxy proxy in inputParts) {
            string type = proxy.entity.Heads(p => p == "as").FirstOrDefault().id;
               
            
            if(filter.Contains("-nohealth") && proxy.Energy() < 1) {
                continue;//don't add.
            }
            if(filter.Contains(type) || filter.Contains("all")) {
                output.Add(proxy);
            }
        }
        
        
        if(filter.Contains("-subpart")) {
            output = PartProxy.ExcludeSubparts(output).ToList(); }
            
        return output;
    }
    
    
}
