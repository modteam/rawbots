using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using Oracle;
using Operants;

public class BluePrintLoader : MonoBehaviour {

    public Console console;
    public List<Entity> entities;
    public Vector3 up;
    string bpName;
    AudioClip success;
    AudioClip cancel;
    float displacement = 0;
    private Dictionary<Entity,GameObject> xFormDummies;



    public enum BluePrintLoaderState {
        Init,
        PlaceGhost,
        DestroyGhost,
    }

    public BluePrintLoaderState state = BluePrintLoaderState.Init;

    void Start () {
        xFormDummies = new Dictionary<Entity, GameObject>();
        success = Resources.Load( "SoundFX/EditAttachSuccess" ) as AudioClip;
        cancel = Resources.Load( "SoundFX/EditAttachFailed" ) as AudioClip;
        bpName = gameObject.name;

        var xform = Gaia.Instance().entities[ "xform" ];
        var xForms = entities .Where( e => e.Relationships()
                    .Any( r => e != xform && r.Predicate == "as" && r.Head == xform ) ).ToList();

        var loadDataEntity = entities.Where( e => e.id == "blueprint_load_data" ).FirstOrDefault();

        var dummy = new GameObject( "blueprintGhost" );
        var recordedUp = Vector3.up;
        if ( loadDataEntity == default(Entity) ) {


            var mainXForm = xForms.FirstOrDefault(); // the should be at least one xform in the blueprint.
            if ( mainXForm == default(Entity) ) {
                return;
            }
            mainXForm.ForProperty<string>( "position", position => {
                dummy.transform.position = Util.PropertyToVector3( position );
            } );

            mainXForm.ForProperty<string>( "rotation", rotation => {
                dummy.transform.transform.eulerAngles = Util.PropertyToVector3( rotation );
            } );
            recordedUp = dummy.transform.up;


        }
        else {
            loadDataEntity.ForProperty<string>( "position", position => {
                dummy.transform.position = Util.PropertyToVector3( position );
            } );

            loadDataEntity.ForProperty<string>( "rotation", rotation => {
                dummy.transform.transform.eulerAngles = Util.PropertyToVector3( rotation );
            } );
            loadDataEntity.ForProperty<string>( "up", value => {
                recordedUp = Util.PropertyToVector3( value );
            } );

        }


        dummy.transform.parent = transform;

        xForms.ForEach( xf => {
            var xFormDummy = new GameObject( "xformGhost" );

            xFormDummies.Add( xf, xFormDummy );
            xf.ForProperty<string>( "position", position => {
                xFormDummy.transform.position = Util.PropertyToVector3( position );
            } );
            xf.ForProperty<string>( "rotation", rotation => {
                xFormDummy.transform.eulerAngles = Util.PropertyToVector3( rotation );
            } );
            xFormDummy.transform.parent = dummy.transform;

            xf.SetProperty( "position", Util.Vector3ToProperty( xFormDummy.transform.localPosition ) );
            xf.SetProperty( "rotation", Util.Vector3ToProperty( xFormDummy.transform.localEulerAngles ) );

            var xformEntities = xf.Tails( p => p == "local_to" ).ToList();

            xformEntities.ForEach( xfe => {

                var typeEntity = xfe.Relationships()
                .Where( r => r.Predicate == "as" && r.Tail == xfe )
                    .Select( r => r.Head ).FirstOrDefault();
                if ( typeEntity != null &&
                typeEntity.Heads( p => p == "is" ).FirstOrDefault() == Gaia.Instance().entities[ "part" ] ) {
                    var viewLocation = ( string )typeEntity.GetProperty<string>( "view" );
                    var view = GameObject.Instantiate( Resources.Load( viewLocation ) ) as GameObject;
                    view.SetActive( false );
                    var clone = new GameObject();
                    clone.transform.parent = xFormDummy.transform;
                    clone.AddComponent<GhostPart>();
                    var ePosition = Vector3.zero;
                    var eRotation = Vector3.zero;
                    xfe.ForProperty< string >( "position", value => {
                        ePosition = Util.PropertyToVector3( value );
                    } );
                    xfe.ForProperty< string >( "rotation", value => eRotation = Util.PropertyToVector3( value ) );
                    Util.ClonePiece( clone, view, LayerMask.NameToLayer( "ghostPart" ) );

                    clone.transform.localPosition = ePosition;
                    clone.transform.localEulerAngles = eRotation;
                    var delta = ( Vector3.Dot( up, dummy.transform.InverseTransformPoint( clone.transform.position ) ) * up ).magnitude;
                    var projection = Vector3.Project( ( clone.transform.position - dummy.transform.position ), up );

                    var angle = Vector3.Angle( projection, up );
                    if ( angle > 90 ) {
                        if ( projection.magnitude > displacement ) {
                            displacement = projection.magnitude;
                        }
                    }

                    var highLight = clone.AddComponent<Highlight>();
                    highLight.showAxis = false;
                    highLight.Prepare();
                    highLight.ghostMaterial = view.GetComponent<Highlight>().ghostMaterial;
                    highLight.SetHighlight( true, Highlight.HighlightType.Green );
                    Destroy( view );
                }




            } );


        } );

        if ( loadDataEntity != default(Entity) ) {
            var dummyUp = new GameObject();
            dummyUp.transform.position = dummy.transform.position;
            dummyUp.transform.rotation = dummy.transform.rotation;
            dummyUp.transform.up = recordedUp;
            dummy.transform.parent = dummyUp.transform;
            dummyUp.transform.up = up;
            dummy.transform.parent = transform;


            Destroy( dummyUp );

        }
        else {
            dummy.transform.localRotation = dummy.transform.rotation;

        }

        dummy.transform.localPosition = Vector3.zero;

        if ( transform.childCount > 0 ) {
            state = BluePrintLoaderState.PlaceGhost;
        }
        else {
            state = BluePrintLoaderState.DestroyGhost;
        }
        NextState();
    }
    
    IEnumerator InitState () {
        while ( state == BluePrintLoaderState.Init ) {
            yield return null;
        }
        NextState();
    }

    IEnumerator PlaceGhostState () {
        GameModeManager.ChangeMode( GameModeManager.ModeId.World );
        var z = ( transform.position - console.gameCamera.transform.position ).magnitude;
        transform.position = transform.position + ( up * ( displacement +1 ) );
        var surfacePoint = transform.position;

        while ( state == BluePrintLoaderState.PlaceGhost ) {
            BulletRaycastHitDetails hit;
            var ray = Builder.instance.gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition ); //added structure -z26
            var hitStatus = Bullet.Raycast( ray, out hit, 500, BulletLayers.Camera, ( BulletLayers.World | BulletLayers.HexBlock | BulletLayers.Structure ) );

            if ( hitStatus ) {

                transform.position = hit.point + ( up * ( displacement +1 ) );
            }


            if ( Input.GetKeyUp( KeyCode.Escape ) ) {
                GameModeManager.ChangeMode( GameModeManager.ModeId.World );
                console.gameCamera.GetComponent<AudioSource>().PlayOneShot( cancel );
                state = BluePrintLoaderState.DestroyGhost;
                entities.Where(e => e.Relationships().Where(r => r.Predicate == "as" && r.Tail == e).Any() ).ToList().ForEach( e => {
                    Gaia.Instance().entities.Remove( e.id );
                } );
            }

            if ( Input.GetMouseButtonUp( 0 ) ) {


                foreach ( var keyPair in xFormDummies ) {
                    keyPair.Key.SetProperty( "position", Util.Vector3ToProperty( keyPair.Value.transform.position ) );
                    keyPair.Key.SetProperty( "rotation", Util.Vector3ToProperty( keyPair.Value.transform.eulerAngles ) );
                }

                while ( Gaia.Instance().recentlyAddedEntities.Count > 0 ) {
                    yield return null;
                }
                Gaia.Instance().recentlyAddedEntities.AddRange( SubPartImporter.FilterSubparts( entities ) );

                state = BluePrintLoaderState.DestroyGhost;
            }
            yield return null;
        }
        NextState();
    }

    IEnumerator DestroyGhostState () {


        yield return null;
        Destroy( this.gameObject );
    }

    void NextState () {
        string methodName = state.ToString() + "State";
        System.Reflection.MethodInfo info =
            GetType().GetMethod( methodName,
                                  System.Reflection.BindingFlags.NonPublic |
                                  System.Reflection.BindingFlags.Instance );
        StartCoroutine( ( IEnumerator )info.Invoke( this, null ) );
    }

}
