//Commented out by z26, it seems currently unused.

// using UnityEngine;
// using System.Collections;
// using System.Collections.Generic;
// using System.Linq;
// using System.Net;
// using System.Net.Sockets;
// using System.Security.Cryptography;
// using System.IO;

// public class WebSocketServer : MonoBehaviour {

//     public enum MessageType {
//         json = 0x10,
//     }

//     public int port = 8080;
//     const string guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
//     static SHA1 sha1 = SHA1CryptoServiceProvider.Create();
//     Socket server;
//     Socket client;
//     bool online = false;
//     Queue< byte[] > inQueue = new Queue< byte[] >();
//     Queue< byte[] > outQueue = new Queue< byte[] >();
//     static WebSocketServer instance;
//     public static WebSocketServer Instance { 
//         get { return instance; }
//     }

//     void Awake(){
//         instance = this;
//     }

//     void Start () {
//         Reset();
//     }

//     void Reset () {
//         server = new Socket( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP );
//         server.Bind( new IPEndPoint( IPAddress.Any, port ) );
//         server.Listen( 128 );
//         server.BeginAccept( null, 0, OnAccept, null );
//     }

//     void OnDestroy () {
//         online = false;
//         server.Close();
//     }

//     public void Send ( byte[] bytes, int id ) {
//         if ( client == null ) {
//             return;
//         }
//         Debug.Log("SENDING w ID " + id);
//         lock ( outQueue ) {
//             outQueue.Enqueue( bytes );
//         }
//     }

//     void Update () {

//         byte[] bytes;
//         var queue = new Queue< byte[] >();

//         lock ( inQueue ) {
//             queue = new Queue< byte[] >( inQueue );
//             inQueue.Clear();
//         }

//         while ( queue.Any() ) {
//             bytes = queue.Dequeue();
//             if ( bytes.Length > 8 ) {
//                 var data = Read( bytes );
//                 var id = System.BitConverter.ToInt32( data, 0 );
//                 int lenght = data.Length - 4;
//                 var message = new byte[lenght];
//                 System.Array.Copy( data, 4, message, 0, lenght );
//                 Debug.Log("ID CAME " + id);
//                 Debug.Log( "Payload: " + System.Text.Encoding.UTF8.GetString( message ) );
//             }
//         }

//         lock ( outQueue ) {
//             queue = new Queue< byte[] >( outQueue );
//             outQueue.Clear();
//         }

//         while ( queue.Any() ) {
//             bytes = queue.Dequeue();
//             client.Send( Frame( bytes ) );
//         }

//     }

//     void OnAccept ( System.IAsyncResult result ) {

//         client = server.EndAccept( result );

//         byte[] buffer = new byte[ 1024 ];
//         var size = client.Receive( buffer );
//         var response = ( System.Text.Encoding.UTF8.GetString( buffer ) ).Substring( 0, size );
//         var ch = new Header( response );
//         //Debug.Log( response );

//         string longKey = ch.headerFields[ "Sec-WebSocket-Key" ] + guid;
//         byte[] hashBytes = sha1.ComputeHash( System.Text.Encoding.ASCII.GetBytes( longKey ) );
//         string acceptKey = System.Convert.ToBase64String( hashBytes );

//         var sh = new Header();
//         sh.leadingLine = "HTTP/1.1 101 Switching Protocols";
//         sh.headerFields[ "Upgrade" ] = "websocket";
//         sh.headerFields[ "Connection" ] = "Upgrade";
//         //sh.header_fields[ "Sec-WebSocket-Protocol" ] = "service";
//         sh.headerFields[ "Sec-WebSocket-Accept" ] = acceptKey;

//         response = sh.Response();
//         //Debug.Log( response );
//         client.Send( System.Text.Encoding.UTF8.GetBytes( response ) );

//         online = true;

//         while ( online ) {
//             size = client.Receive( buffer );
//             lock ( inQueue ) {
//                 //Debug.Log( System.Text.Encoding.UTF8.GetString( buffer ) );
//                 inQueue.Enqueue( buffer.Take( size ).ToArray() );
//             }
//         }
//     }

//     byte[] Read ( byte[] buffer ) {

//         var bs = new MemoryStream( buffer );

//         bs.Position += 2;

// //        bool fin = ( buffer[ 0 ] & 0x80 ) == 0x80;

// //        bool rsv1 = ( buffer[ 0 ] & 0x40 ) == 0x40;
// //        bool rsv2 = ( buffer[ 0 ] & 0x20 ) == 0x20;
// //        bool rsv3 = ( buffer[ 0 ] & 0x10 ) == 0x10;

// //        int opcode = ( ( buffer[ 0 ] & 0x8 ) |
// //            ( buffer[ 0 ] & 0x4 ) | ( buffer[ 0 ] & 0x2 ) | ( buffer[ 0 ] & 0x1 ) );

//         bool mask = ( buffer[ 1 ] & 0x80 ) == 0x80;

//         byte payload = ( byte )( ( buffer[ 1 ] & 0x40 ) | ( buffer[ 1 ] & 0x20 ) |
//             ( buffer[ 1 ] & 0x10 ) | ( buffer[ 1 ] & 0x8 ) | ( buffer[ 1 ] & 0x4 ) |
//             ( buffer[ 1 ] & 0x2 ) | ( buffer[ 1 ] & 0x1 ) );

//         ulong length = 0;

//         switch ( payload ) {
//             case 126:
//                 byte[] bytesUShort = new byte[ 2 ];
//                 bs.Read( bytesUShort, 0, 2 );
//                 if ( bytesUShort != null ) {
//                     length = System.BitConverter.ToUInt16( bytesUShort.Reverse().ToArray(), 0 );
//                 }
//                 break;
//             case 127:
//                 byte[] bytesULong = new byte[ 8 ];
//                 bs.Read( bytesULong, 0, 8 );
//                 if ( bytesULong != null ) {
//                     length = System.BitConverter.ToUInt16( bytesULong.Reverse().ToArray(), 0 );
//                 }
//                 break;
//             default:
//                 length = payload;
//                 break;
//         }

//         byte[] maskKeys = new byte[ 4 ];
//         if ( mask ) {
//             bs.Read( maskKeys, 0, 4 );
//         }

//         byte[] data = new byte[ ( int )length ];
//         bs.Read( data, 0, ( int )length );

//         if ( mask ) {
//             for ( int i = 0; i < data.Length; ++i ) {
//                 data[ i ] = ( byte )( data[ i ] ^ maskKeys[ i % 4 ] );
//             }
//         }

//         return data;

// //        ushort closeCode = 0;
// //        if ( opCode == OpCode.Close && data.Length == 2 ) {
// //            closeCode = BitConverter.ToUInt16( ( ( byte[] )data.Clone() ).Reverse().ToArray(), 0 );
// //        }

//     }

//     byte[] Frame ( byte[] binary ) {

//         ulong headerLength = 2;
//         byte[] data = binary;

//         bool mask = false;
//         byte[] maskKeys = null;

//         if ( mask ) {
//             headerLength += 4;
//             data = ( byte[] )data.Clone();

//             System.Random random = new System.Random();
//             maskKeys = new byte[4];
//             for ( int i = 0; i < 4; ++i ) {
//                 maskKeys[ i ] = ( byte )random.Next( byte.MinValue, byte.MaxValue );
//             }

//             for ( int i = 0; i < data.Length; ++i ) {
//                 data[ i ] = ( byte )( data[ i ] ^ maskKeys[ i % 4 ] );
//             }
//         }

//         byte payload;
//         if ( data.Length >= 65536 ) {
//             headerLength += 8;
//             payload = 127;
//         }
//         else if ( data.Length >= 126 ) {
//             headerLength += 2;
//             payload = 126;
//         }
//         else {
//             payload = ( byte )data.Length;
//         }

//         byte[] header = new byte[ headerLength ];

//         header[ 0 ] = 0x80 | 0x2;
//         if ( mask ) {
//             header[ 1 ] = 0x80;
//         }
//         header[ 1 ] = ( byte )( header[ 1 ] | payload & 0x40 | payload & 0x20 |
//                 payload & 0x10 | payload & 0x8 | payload & 0x4 | payload & 0x2 | payload & 0x1 );

//         if ( payload == 126 ) {
//             byte[] lengthBytes = System.BitConverter.GetBytes( ( ushort )data.Length ).Reverse().ToArray();
//             header[ 2 ] = lengthBytes[ 0 ];
//             header[ 3 ] = lengthBytes[ 1 ];
//             if ( mask ) {
//                 for ( int i = 0; i < 4; ++i ) {
//                     header[ i + 4 ] = maskKeys[ i ];
//                 }
//             }
//         }
//         else if ( payload == 127 ) {
//             byte[] lengthBytes = System.BitConverter.GetBytes( ( ulong )data.Length ).Reverse().ToArray();
//             for ( int i = 0; i < 8; ++i ) {
//                 header[ i + 2 ] = lengthBytes[ i ];
//             }
//             if ( mask ) {
//                 for ( int i = 0; i < 4; ++i ) {
//                     header[ i + 10 ] = maskKeys[ i ];
//                 }
//             }
//         }

//         return header.Concat( data ).ToArray();
//     }

//     class Header {

//         public string leadingLine = string.Empty;
//         public Dictionary< string, string > headerFields = new Dictionary< string, string >();

//         public Header () {
//         }

//         public Header ( string text ) {
//             var lines = text.Split( '\n' );
//             leadingLine = lines[ 0 ];
//             foreach ( var line in lines ) {
//                 var keyval = line.Split( ':' );
//                 if ( keyval.Length == 2 ) {
//                     var key = keyval[ 0 ].Trim();
//                     var val = keyval[ 1 ].Trim();
//                     headerFields[ key ] = val;
//                 }
//             }
//         }

//         public string Response () {
//             var newline = "\r\n";
//             var response = leadingLine + newline;
//             foreach ( var headerField in headerFields ) {
//                 response += headerField.Key + ": " + headerField.Value + newline;
//             }
//             response += newline;
//             return response;
//         }
//     }
// }

