using UnityEngine;
using System.Collections;

public class RelativePosition : MonoBehaviour {
    
    new public Camera camera;
    public enum horizontalAlignment {
        right,
        left,
    }
    
    public enum verticalAlignment {
        top,
        bottom,
    }
    
    public enum Type {
        pixels,
        percentage,
    }
    
    public horizontalAlignment hAlign;
    public verticalAlignment vAlign;
    public Type type;
    public float x;
    public float y;
    
    // Use this for initialization
    void Start () {
        var width = Screen.width;
        var height = Screen.height;
        
        var xPos = 0f;
        var yPos = 0f;
        
        x = type == Type.pixels ? ( x / width ) : x;
        y = type == Type.pixels ? ( y / height ) : y;
        
        xPos = hAlign == horizontalAlignment.right ? 1 - x : x;
        yPos = vAlign == verticalAlignment.top ? 1 - y : y;
        
        var position = camera.ViewportToWorldPoint( new Vector3( xPos, yPos, camera.nearClipPlane + 5 ) );
        
        transform.position = position;
        
    }
}
