using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public static class Sec {

    //mask n stuff
    [FlagsAttribute]
     public enum Permission : uint {
        all = connect | disconnect | edit | drag | delete,
        _ = 0x00,
        connect = 0x01,
        disconnect = connect << 1,
        edit = disconnect << 1,
        drag = edit << 1,
        delete = drag << 1,
        _f6 = delete << 1,
        _f7 = _f6 << 1,
        _f8 = _f7 << 1,
        _f9 = _f8 << 1,
        _f10 = _f9 << 1,
        _f11 = _f10 << 1,
        _f12 = _f11 << 1,
        _f13 = _f12 << 1,
        _f14 = _f13 << 1,
        _f15 = _f14 << 1,
        _f16 = _f15 << 1,
        _f17 = _f16 << 1,
        _f18 = _f17 << 1,
        _f19 = _f18 << 1,
        _f20 = _f19 << 1,
        _f21 = _f20 << 1,
        _f22 = _f21 << 1,
        _f23 = _f22 << 1,
        _f24 = _f23 << 1,
        _f25 = _f24 << 1,
        _f26 = _f25 << 1,
        _f27 = _f26 << 1,
        _f28 = _f27 << 1,
        _f29 = _f28 << 1,
        _f30 = _f29 << 1,
        _f31 = _f30 << 1,
        _f32 = _f31 << 1,
    };

    public static void ClearPermissions ( uint permissions ) {
        permissions = ( uint )Permission._;
    }

    public static uint SetPermissionOn ( uint permissions, Permission perm ) {
        return ( permissions |= ( uint )perm );
    }
        
    public static uint SetPermissionOff ( uint permissions, Permission perm ) {
        return ( permissions &= ~( uint )perm );
    }

    public static bool HasPermission ( Entity entity, Permission perm ) {
        uint entityPermissions = 0;
        entity.ForProperty<float>( "permissions", value => entityPermissions = ( uint )value );
        if ( ( entityPermissions & ( uint )perm ) == ( uint )perm ) {
//            || DevTools.IsOptionEnabled( DevTools.Options.CheckPermissionSupressed ) ) {
            return true;
        }
        else {
            return false;
        }
    }

    public static List<String> PermissionList () {
        var nameList = new List<String>();
        var names = System.Enum.GetNames( typeof( Sec.Permission ) );
        for ( int i = 0; i < names.Length; ++i ) {
            if ( !names[ i ].Contains( "_" ) ) {
                nameList.Add( names[ i ] );
            }
        }
        return nameList;
    }

    public static uint SetPermission ( uint permissions, string input ) {
        var mode = input.Substring( 0, 1 );
        var name = input.Substring( 1 );
        var names = System.Enum.GetNames( typeof( Sec.Permission ) ).ToList();
        int index = names.IndexOf( name );
        if ( index != -1 ) {
            var values = System.Enum.GetValues( typeof( Sec.Permission ) ) as uint[];
            var perm = ( Sec.Permission )values[ index ];
            if ( mode == "+" ) {
                permissions = SetPermissionOn( permissions, perm );
            }
            else if ( mode == "-" ) {
                permissions = SetPermissionOff( permissions, perm );
            }
        }
        return permissions;
    }

    public static void ForPermission ( Entity entity,
        Sec.Permission requiredPermissions,
        System.Action success, System.Action fail ) {
        if ( entity == default(Entity) ) {
//            || DevTools.IsOptionEnabled( DevTools.Options.CheckPermissionSupressed ) ) {
            success();
            return;
        }
        uint entityPermissions = 0;
        entity.ForProperty<float>( "permissions", value => entityPermissions = ( uint )value );
//        Debug.Log( "Entity Permissions " + System.Convert.ToString( ( uint )entityPermissions, 2 ) );
//        Debug.Log( "Requiered Permissions " + System.Convert.ToString( ( uint )requiredPermissions, 2 ) );
//        Debug.Log( "Result " + System.Convert.ToString( ( uint )( entityPermissions &= ( uint )requiredPermissions ), 2 ) );
        if ( ( entityPermissions & ( uint )requiredPermissions ) == ( uint )requiredPermissions ) {
            success();
        }
        else {
            fail();
        }
    }

    public static uint editorMask = ( uint )Permission._;
    public static uint editorReadMask = ( uint )Permission._;
    public static string readPassCode;
    public static string setPasscode;

}
