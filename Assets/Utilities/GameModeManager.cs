using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GameModeManager : MonoBehaviour {

    public enum ModeId {
        World,
        Grid,
        PartCloseConnect,
        PartExtendConnect,
        DisconnectSingle,
        DisconnectPart,
        SpawnPart,
        DestroyParts,
        SaveCluster,
        ChangeCameraFocus,
        CreateStructure,
        DestroyStructure,
		CreateAddon
    }

    public enum KeyState {
        KeyDown,
        KeyUp,
        KeyPressed
    }

    public class Transition {
        public KeyCode key;
        public KeyState state;
        public ModeId mode;
    }

    public class Mode {
        public ModeId id;
        public List<Transition> transitions;
    }

    static List<Mode> modes = new List<Mode>();
    static Mode mode = default(Mode);

    public static void RegisterMode ( Mode newMode ) {
        ForMode( newMode.id, m => modes.Remove( m ) );
        modes.Add( newMode );
    }

    public static void ChangeMode ( ModeId id ) {
        ForMode( id, m => mode = m );
    }

    static void ForMode ( ModeId id, System.Action<Mode> action ) {
        var stored = modes.FirstOrDefault( m => m.id == id );
        if ( stored != default(Mode) ) {
            action( stored );
        }
    }

    public static bool InMode ( System.Predicate<ModeId> predicate ) {
        return predicate( mode.id );
    }

    void Awake () {
        var mode = new Mode(){
            id = ModeId.World,
            transitions = new List<Transition>(){
                new Transition(){ key = KeyCode.Escape, state = KeyState.KeyUp , mode = ModeId.Grid },
                new Transition(){ key = KeyCode.Alpha1, state = KeyState.KeyDown, mode = ModeId.PartCloseConnect },
                new Transition(){ key = KeyCode.Alpha2, state = KeyState.KeyDown, mode = ModeId.PartExtendConnect },
                new Transition(){ key = KeyCode.Alpha4, state = KeyState.KeyDown, mode = ModeId.SaveCluster },
                new Transition(){ key = KeyCode.Alpha0, state = KeyState.KeyDown, mode = ModeId.DisconnectPart },
                new Transition(){ key = KeyCode.Alpha6, state = KeyState.KeyDown, mode = ModeId.CreateStructure },
                new Transition(){ key = KeyCode.Alpha7, state = KeyState.KeyDown, mode = ModeId.DestroyStructure },
                new Transition(){ key = KeyCode.Alpha8, state = KeyState.KeyDown, mode = ModeId.CreateAddon },
                new Transition(){ key = KeyCode.Alpha9, state = KeyState.KeyDown, mode = ModeId.DisconnectSingle },     //Disconnect single link
                new Transition(){ key = KeyCode.Tab, state = KeyState.KeyDown, mode = ModeId.ChangeCameraFocus },
                new Transition(){ key = KeyCode.LeftControl, state = KeyState.KeyPressed, mode = ModeId.SpawnPart },
                new Transition(){ key = KeyCode.Backspace, state = KeyState.KeyDown, mode = ModeId.DestroyParts },
            }};
        RegisterMode( mode );
        ChangeMode( ModeId.World );
    }

    void Update () {
        var transitions = mode.transitions;
        foreach ( var t in transitions ) {
            var key = t.key;
            if ( t.state == KeyState.KeyDown && Input.GetKeyDown( key ) ) {
                ChangeMode( t.mode );
                //Console.PushLog("Mode", new string[] { t.mode.ToString() });
                break;
            }
            else if ( t.state == KeyState.KeyUp && Input.GetKeyUp( key ) ) {
                ChangeMode( t.mode );
                break;
            }
            else if ( t.state == KeyState.KeyPressed && Input.GetKey( key ) ) {
                ChangeMode( t.mode );
                break;
            }
        }
    }
}