/*
/*COMMENTING IT OUT CAUSE COHERENT IS APPARENTLY MISSING -z26
/*
/*using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Coherent.UI.Binding;

public class HUD : MonoBehaviour {
    static HUD instance;
    private CoherentUIView view;
    public string map = "none";
    public string blueprint = "no_blueprint";
    public GameObject targetPlane;
    static string userId;
    #if UNITY_STANDALONE_WIN
    string hostdir = "windows";
    #elif UNITY_STANDALONE_OSX
    string hostdir = "macosx";
    #endif      

    List<RemoteTask> requests = new List<RemoteTask>();

    public CoherentUISystem system;

    void Awake () {
        if ( !instance ) {
            instance = this;
            DontDestroyOnLoad( transform.gameObject );
            view = gameObject.AddComponent<CoherentUIView>();
            system.HostDirectory = "StreamingAssets/CoherentUI_Host/"+hostdir;
            view.Page = "http://localhost:8080/rawbotsWWW/dashboard.html?h=" + Time.time.GetHashCode();
            view.IsTransparent = true;
            view.ReceivesInput = true;
            view.DrawAfterPostEffects = true;
            view.Height = 10;
            view.Listener.ReadyForBindings += HandleReadyForBindings;
        }
        else {
            Destroy( gameObject );
        }
    }

    public static HUD Instance {
        get { return instance; }
    }

    void Play (PlayOptions options) {
        Debug.Log( "Play on map: " +options.map + " bot: " + options.blueprint);
        map = options.map;
        blueprint = options.blueprint;
        userId = options.uid;
        Debug.Log(HUD.Instance.map);
        Application.LoadLevel("BulletStardust");
        view.View.TriggerEvent("GoToHUD");

    }
    void BackToMainMenu(){
        Debug.Log("In unity back to main menue");
        Application.LoadLevel("MainMenu");
        view.View.TriggerEvent("GoToMainMenu");
    }

    void SaveGame(){
        Debug.Log("saving "+HUD.instance.map);
        Gaia.Instance().console.commands["save"]("save redshift");
    }

    void Update(){
        if ( Input.GetKey( KeyCode.LeftControl ) && Input.GetKeyUp(KeyCode.A) ) {
            view.Reload( true );
        }
    }

    void HandleReadyForBindings ( int frameId, string path, bool isMainFrame ) {
        if ( isMainFrame ) {
            view.View.BindCall( "Play", ( System.Action<PlayOptions> )this.Play );
            view.View.BindCall( "UploadResponse" , ( System.Action<Response>)this.UploadResponse);
            view.View.BindCall( "BackToMainMenu", ( System.Action )this.BackToMainMenu );
            view.View.BindCall( "SaveGame", ( System.Action )this.SaveGame );
        }
    }

    public void SaveBlueprint(string name, Request bp, Request snapshot){
        StartCoroutine(SaveBlueprintI(name,bp,snapshot));
    }

    public IEnumerator LoadBlueprint(string name, Holder outHolder){
        var header = new Header() {
            name = name,
            contentType = RemoteTask.contenTypeValues[(int)RemoteTask.ContentType.gzip]
        };
        var request = new Request() {
            id = (uint)name.GetHashCode(),
            header = Newtonsoft.Json.JsonConvert.SerializeObject( header ),
            body = ""
        };

        var task = new RemoteTask( request, RemoteTask.RequestMethod.BotDownloadRequest );
        requests.Add( task);
        yield return StartCoroutine(task.GetEnumerator());
        outHolder.botBase64Enc = task.Response.body;
        yield return null;
    }

    IEnumerator SaveBlueprintI(string name, Request bp, Request snapshot){
        RawbotsConsole.PushLog( "Blueprint", new string[]{"saving blueprint..."} );
        var tasks = new List<RemoteTask>();
        tasks.Add( new RemoteTask( bp,RemoteTask.RequestMethod.DriveUploadRequest ) );
        tasks.Add(new RemoteTask( snapshot, RemoteTask.RequestMethod.DriveUploadRequest ));
        requests.AddRange( tasks );
        foreach ( var task in tasks ) {
            yield return StartCoroutine( task.GetEnumerator());
        }
        var metadata = new BlueprintMetadata() {
            name = name,
            bpId = tasks[0].GetResponseId(),
            snapshotId = tasks[1].GetResponseId()
        };
        var json = Newtonsoft.Json.JsonConvert.SerializeObject( metadata );
        var base64 = System.Convert.ToBase64String( System.Text.Encoding.ASCII.GetBytes( json ) );
        var header = new Header() { 
            name = "blueprints/"+userId+"/"+name,
            contentType = RemoteTask.contenTypeValues[(int)RemoteTask.ContentType.json]
        };
        var metadataRequest = new Request() {
            id = (uint)base64.GetHashCode(),
            header = Newtonsoft.Json.JsonConvert.SerializeObject( header ),
            body = base64
        };
        var cloudTask = new RemoteTask(metadataRequest,RemoteTask.RequestMethod.CloudUploadRequest);
        requests.Add( cloudTask );
        yield return StartCoroutine(cloudTask.GetEnumerator());
        Debug.Log( "Blueprint: " + cloudTask.Response.message );
        RawbotsConsole.PushLog( "Blueprint", new string[]{cloudTask.Response.message} );
    }

    void UploadResponse( Response response ){
        Debug.Log("Here " + response.requestId);
        var request = requests.FirstOrDefault( r => r.GetRequestId() == response.requestId );
        request.SetResponse( response );
        requests.Remove( request );
    }

    public IEnumerator UploadRequest(RemoteTask task){
        Debug.Log("Request " + task.Request.id);
        view.View.TriggerEvent(task.MethodName.ToString(), task.Request);
        while ( !task.Done  ) {
            yield return null;
        }
        yield return 0;
    }
}*/
