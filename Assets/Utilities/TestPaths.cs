using UnityEngine;
using System.Collections;

public class TestPaths : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Debug.Log("Application.persistentDataPath "+Application. persistentDataPath);
        Debug.Log("Personal "+ System.Environment.GetFolderPath( System.Environment.SpecialFolder.Personal ));
        Debug.Log("ApplicationData "+ System.Environment.GetFolderPath( System.Environment.SpecialFolder.ApplicationData ));
        Debug.Log("asset path "+ Application.dataPath);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
