using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class BulletCompoundShape : MonoBehaviour {

    public int compoundShapeId = -1;
    public BulletCollisionShape[] collisionShapes;
	public bool calculateCenterOfMass = true;
    public string serializationName = "_";
    public GameObject centerOfMass;
    void Awake(){
        centerOfMass = new GameObject("center_of_mass");
        centerOfMass.transform.parent = transform;
        centerOfMass.transform.localPosition = Vector3.zero;
        centerOfMass.transform.localRotation = Quaternion.identity;
    }

	public Vector3 CalculateInertia(){
		if(!calculateCenterOfMass)return Vector3.zero;
		var inertiaPoint = Vector3.zero;
		for(int i = 0;i<collisionShapes.Length; i++){
			inertiaPoint += collisionShapes[i].transform.localPosition;
		}
		var lenght = collisionShapes.Count();
        centerOfMass.transform.localPosition = inertiaPoint/lenght;
		//return Vector3.zero;
		return centerOfMass.transform.localPosition;
	}

}
