using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletRigidBody : MonoBehaviour {
    public BulletLayers collisionGroup;
    [HideInInspector]
    public BulletLayers mask;
    public btRigidBody btRB;
    public NodeView nodeview;
    public float mass = 1.0f;
    public float restitution = 0f;
    public float friction = 0f;
	public float rollingFriction = 0f;
    public bool isTrigger = false;
    public bool alwaysActive = true;
    public BulletCompoundShape compoundShape;
    public List<System.Action<Vector3 ,BulletRigidBody ,Vector3 ,Vector3 >>  collisionActionsList;
    public List<System.Action<Vector3 ,BulletRigidBody ,Vector3 ,Vector3 >>  triggerActionsList;
    Vector3 velocity;
    Vector3 angularVelocity;

    //TODO: shouldn't this id be an IntPtr to btRigidBody
    int id;

    public int Id {
        get {
            return id;
        }
    }

    public bool IsKinematic () {
        return mass == 0;
    }

    public float linearDamping = 0.2f;
    public float angularDamping = 0.2f;
    public new Transform transform;
    
    public void Awake () {
        transform = gameObject.transform;
        
        collisionActionsList = new List<System.Action<Vector3 ,BulletRigidBody ,Vector3 ,Vector3 >>();
        triggerActionsList = new List<System.Action<Vector3 ,BulletRigidBody ,Vector3 ,Vector3 >>();
        if ( gameObject.activeInHierarchy ) {
            if ( compoundShape == null ) {
                Debug.LogError( "missing compound shape" );
            }
            id = Bullet.AddToBulletBodiesList(this);


            Bullet.instance.AddRigidbodyToSimulation( this );
            if(mass == 0f && HexBatcher.instance.initialBatchFinished){
                HexBatcher.instance.ResetForceUpdateAllAabbsProperty();
            }
        }
    }
    
    public void FixedUpdate () {

        //TODO: why are we looking for the btBody and not have it cached here?
        // FastGetTransform is unnecessary
        if(mass != 0){
            var transformStored = Bullet.instance.GetBulletTransform( btRB );
            //TODO: change bullet transform id field to reflect real use
            //Debug.Log(id + " "+ transformStored.id + " "+ Time.time);
            if ( transformStored.id == 1 ) {// id tells if obj is active

                //TODO: why are we unity-transformin on FixedUpdate? this should happen on Update()
                transform.position = transformStored.position.ToVector3(); //really need to investigate this -z26
                transform.rotation = transformStored.rotation.ToQuaternion();
                SetVelocity (transformStored.velocity);
                SetAngularVelocity (transformStored.angularVelocity);
            }
        }
    }

    public void AddForce ( Vector3 force, Vector3 offset ) {
        Bullet.AddForce( btRB, force.ToBulletVector3(), offset.ToBulletVector3() );
    }

    //TODO: this is wrong, overloaded functions, but fundamentally different effects
    // one is a setter, the other one sets data on the physics world
    // this needs better naming

    public void SetVelocity ( Vector3 velocity ) {
        Bullet.SetVelocity( btRB, velocity.ToBulletVector3() );
    }

    public void SetVelocity ( BulletVector3 newVelocity ) {
        velocity = newVelocity.ToVector3();
    }

    public void SetAngularVelocity ( Vector3 angularVelocity ) {
        Bullet.SetAngularVelocity( btRB, angularVelocity.ToBulletVector3() );
        
    }
    
    public void SetAngularVelocity ( BulletVector3 angularVelocity ) {
       
        this.angularVelocity = angularVelocity.ToVector3();
    }

    public Vector3 GetVelocity () {
        return velocity;
    }

    public Vector3 GetAngularVelocity () {
        return angularVelocity;
    }

    public void SetDamping ( float linearDamping, float angularDamping ) {
        Bullet.SetDamping( btRB, linearDamping, angularDamping );
    }
    
    public void SetBodyMass ( float newMass, Vector3 inertiaPoint ) {
        Bullet.SetBodyMass( btRB, newMass, inertiaPoint.ToBulletVector3() );
    }

    public void ChangeMaskAndGroup ( BulletLayers mask, BulletLayers collisionGroup ) {
        this.mask = mask;
        this.collisionGroup = collisionGroup;
        Bullet.ChangeLayer( btRB, ( short )mask, ( short )collisionGroup );
    }

    public void SetGravity ( Vector3 newGravity ) {
        Bullet.SetGravity( btRB, newGravity.ToBulletVector3() );
    }

    public void SetPosition ( Vector3 newPos ) {
        transform.position = newPos;
        Bullet.SetRigidbodyPosition( btRB, newPos.ToBulletVector3() );
    }

    public void SetRotation ( Quaternion rotation ) {
        transform.rotation = rotation;
        Bullet.SetRigidbodyRotation( btRB, rotation.ToBulletQuaternion() );
    }

    public void DestroyRigidbody () {
        Bullet.RemoveRidigbody( this );
    }

    public void SetCollisionShapeScale ( Vector3 localScale ) {
        Bullet.SetCollisionShapeScale( btRB, ( localScale ).ToBulletVector3() );
    }

    public void AddOnTriggerDelegate ( System.Action<Vector3 ,BulletRigidBody ,Vector3 ,Vector3 > onTriggerDelegate ) {
        triggerActionsList.Add( onTriggerDelegate );
    }

    public void AddOnCollisionDelegate ( System.Action<Vector3 ,BulletRigidBody ,Vector3 ,Vector3 > onCollisionDelegate ) {
        collisionActionsList.Add( onCollisionDelegate );
    }

    public void OnCollision ( Vector3 relativeVelocity, BulletRigidBody other, Vector3 contactPoint, Vector3 lineOfAction ) {
        for ( int i = 0; i< collisionActionsList.Count; ++i ) {
            collisionActionsList[ i ]( relativeVelocity, other, contactPoint, lineOfAction );
        }
    }

    public void OnTrigger ( Vector3 relativeVelocity, BulletRigidBody other, Vector3 contactPoint, Vector3 lineOfAction ) {
        for ( int i = 0; i< triggerActionsList.Count; ++i ) {
            triggerActionsList[ i ]( relativeVelocity, other, contactPoint, lineOfAction );
        }
        
    }

    void OnDestroy () {
        //onCollision = c => {};
        //onTrigger = t => {};
        DestroyRigidbody();
    }

}
