using UnityEngine;
using System.Collections;

public class BulletContactPoint {
    public Vector3 point;
    public Vector3 lineOfAction;

    public BulletContactPoint ( Vector3 point, Vector3 lineOfAction ){
        this.point = point;
        this.lineOfAction = lineOfAction;
    }
}

public class BulletCollision {
    public Vector3 relativeVelocity;
    public BulletRigidBody btRigidBody;
    public BulletContactPoint btContact;

    public BulletCollision (Vector3 relativeVelocity, BulletRigidBody btRigidBody, BulletContactPoint btContact){
        this.relativeVelocity = relativeVelocity;
        this.btRigidBody = btRigidBody;
        this.btContact = btContact;
    }
}
