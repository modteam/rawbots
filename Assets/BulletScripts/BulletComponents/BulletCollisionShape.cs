using UnityEngine;
using System.Collections;

public class BulletCollisionShape : MonoBehaviour {
	
	public CollisionShapeType type;
    public Mesh mesh;
    public int vertexCount = 0;
    public int triangleCount = 0;

    void Awake(){
        if(mesh != default(Mesh)){
            vertexCount = mesh.vertexCount;
            triangleCount = mesh.triangles.Length / 3;
        }
    }

	public enum CollisionShapeType{
		Cuboid = 0,
        Sphere = 1,
        MeshShape = 2,
        Cylinder = 3,
        TriangleMeshShape = 4,
	}
    
	
	
}
