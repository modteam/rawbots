using UnityEngine;
using System.Collections;

public class BulletGeneric6DofConstraint : MonoBehaviour {

    public enum Axis {
        X = 0,
        Y = 1,
        Z = 2
    }

    public BulletRigidBody bulletRbA;
    public BulletRigidBody bulletRbB;
    public Vector3 lowerLimit = Vector3.zero;
    public Vector3 upperLimit = Vector3.zero;
    public Vector3 lowerLinearLimit = Vector3.zero;
    public Vector3 upperLinearLimit = Vector3.zero;
    public Vector3 linearCFMValue;
    public Vector3 angularCFMValue;
    public Vector3 linearERPValue;
    public Vector3 angularERPValue;
    [HideInInspector]
    public Vector3 rbAPos;
    [HideInInspector]
    public Vector3 rbBPos;
    [HideInInspector]
    public Quaternion rbARot;
    [HideInInspector]
    public Quaternion rbBRot;
    public bool useLinearReferenceFrameA = true;//use fixed frame A for linear llimits
    public bool constraintToWorld = false;


    public bool enableSpring;
    public Axis springAxis;
    public float damping;
    public float stiffness;
    public bool disableCollisionBetweenBodies = true;
    public int solverIterations = 20;
    public bool automaticSetUp = false;

    public void Awake () {
        if ( automaticSetUp ) {
            rbAPos = bulletRbA.transform.position;
            rbARot = bulletRbA.transform.rotation;
            
            rbBPos = bulletRbB.transform.position;
            rbBRot = bulletRbB.transform.rotation;

            Bullet.instance.AddConstraintToSimulation( this );
            var x = GetComponent<BulletGenericMotor>();
            if ( x ) {
                x.Initialize();
            }
        }
    }

    public void Initialize () {
        rbAPos = bulletRbA.transform.position;
        rbARot = bulletRbA.transform.rotation;

        if ( !constraintToWorld ) {
            rbBPos = bulletRbB.transform.position;
            rbBRot = bulletRbB.transform.rotation;
        }
        Bullet.instance.AddConstraintToSimulation( this );
        var x = GetComponent<BulletGenericMotor>();
        if ( x ) {
            x.Initialize();
        }

    }

    public void RemoveBulletConstraint () {

        Bullet.RemoveContraint( GetHashCode() );
    }
}
