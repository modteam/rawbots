using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Asteroids : MonoBehaviour {

    public int numOfAsteroids = 100;
    public float radius = 100;
    public Waveform offset;
    public Waveform scale;
    public List< GameObject > asteroidAssets;

    void Start () {
        for ( float i = 0; i < numOfAsteroids; ++i ) {
            var asteroid = Instantiate( asteroidAssets[ Random.Range( 0, asteroidAssets.Count ) ] ) as GameObject;
            asteroid.transform.parent = transform;
            var x = Mathf.Sin( i / numOfAsteroids * 2 * Mathf.PI );
            var y = Mathf.Cos( i / numOfAsteroids * 2 * Mathf.PI );
            var z = Mathf.Sin( i / numOfAsteroids * 2 * Mathf.PI );
            var position = new Vector3( x, y, z );
            x = offset.Sample( i / numOfAsteroids + 0.33f ).sample;
            y = offset.Sample( i / numOfAsteroids + 0.66f ).sample;
            z = offset.Sample( i / numOfAsteroids + 1 ).sample;
            var displacement = new Vector3( x, y, z );
            position = position + position.normalized * radius + displacement;
            asteroid.transform.localPosition = position;
            x = y = z = scale.Sample( i / numOfAsteroids ).sample;
            asteroid.transform.localScale = new Vector3( x, y, z );
            asteroid.transform.rotation = Random.rotation;
        }
    }
}
