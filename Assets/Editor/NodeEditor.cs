using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using System.Reflection;

using Operants;

public class NodeEditor : EditorWindow {
    /*
    GraphBuilder builder;
    string nodeId = string.Empty;
    NodeProxy node;
    
    [ MenuItem( "Window/Node Editor" ) ]
    public static void ShowWindow () {
        EditorWindow.GetWindow( typeof( NodeEditor ) );
    }
    
    void OnGUI () {
        GUILayout.Label ("Node Editor", EditorStyles.boldLabel);
        builder = (GraphBuilder)EditorGUILayout.ObjectField( "Bot", builder, typeof( GraphBuilder ), true );
        nodeId = EditorGUILayout.TextField ("Node Id", nodeId );
        
    
        if( builder ) {
            GUILayout.Label( "Nodes count: " + builder.nodes.Count() );
        
            //var find = GUILayout.Button( "Find node!");
            
            //if( find ) {
                node = builder.nodes.Where( n => {
                    return n.entity.id == nodeId;
                } ).FirstOrDefault();
            //}
        }
        
        if( node != null ) {
            CreateControls( node );
        }
        else {
            GUILayout.Label( "Node not found" );
        }
    }
    
    void CreateControls ( NodeProxy proxy ) {
        
        var fields = proxy.GetType().GetFields().Where( f => f.IsPublic );
        
        foreach( var field in fields ) {
            GenerateControl( field, proxy );
        }
        
//        var props = proxy.GetType().GetProperties().Where( p => p.CanRead && p.CanWrite );
//        foreach( var prop in props ) {
//            GenerateControl( prop, proxy );
//        }
    }
    
    void CreateControls ( string name, AbstractComponent component ) {
        
        var fields = component.GetType().GetFields().Where( f => f.IsPublic );
        
        foreach( var field in fields ) {
            GenerateControl( field, component );
        }
        
//        var props = component.GetType().GetProperties().Where( p => p.CanRead && p.CanWrite );
//        foreach( var prop in props ) {
//            GenerateControl( prop, component );
//        }
    }
    
    void GenerateControl( FieldInfo field, object obj ) {
        
        var val = field.GetValue( obj );
        if( val != null ) {
//                Debug.Log( "Type: " + val.GetType().Name + " val: " + val );
        }
        
        if( val is float ) {
            field.SetValue( obj, EditorGUILayout.FloatField(field.Name, val != null ? float.Parse( val.ToString() ) : 0 ) );
        }
        
        if( val is int ) {
            field.SetValue( obj, EditorGUILayout.IntField(field.Name, val != null ? int.Parse( val.ToString() ) : 0 ) );
        }
        
        if( val is bool ) {
            field.SetValue( obj, EditorGUILayout.Toggle( field.Name, val != null ? bool.Parse( val.ToString() ) : false ) );
        }
        
        if( val is Vector3 ) {
            field.SetValue( obj, EditorGUILayout.Vector3Field( field.Name, val != null ? (Vector3)val : Vector3.zero ) );
        }
        
        if( val is UnityEngine.Object  ) {
            field.SetValue( obj, EditorGUILayout.ObjectField( field.Name, val as UnityEngine.Object, field.GetType(), true ) );
        }
        
        if( val is AbstractComponent ) {
            CreateControls( field.Name, val as AbstractComponent );
        }
        
    }
    
    void GenerateControl( PropertyInfo prop, object obj ) {
        
        var val = prop.GetValue( obj, null );
        if( val != null ) {
//                Debug.Log( "Type: " + val.GetType().Name + " val: " + val );
        }
        
        if( val is float ) {
            prop.SetValue( obj, EditorGUILayout.FloatField( prop.Name, val != null ? float.Parse( val.ToString() ) : 0 ), null );
        }
        
        if( val is int ) {
            prop.SetValue( obj, EditorGUILayout.IntField( prop.Name, val != null ? int.Parse( val.ToString() ) : 0 ), null );
        }
        
        if( val is bool ) {
            prop.SetValue( obj, EditorGUILayout.Toggle( prop.Name, val != null ? bool.Parse( val.ToString() ) : false ), null );
        }
        
        if( val is Vector3 ) {
            prop.SetValue( obj, EditorGUILayout.Vector3Field( prop.Name, val != null ? (Vector3)val : Vector3.zero ), null );
        }
        
        if( val is UnityEngine.Object  ) {
            prop.SetValue( obj, EditorGUILayout.ObjectField( prop.Name, val as UnityEngine.Object, prop.GetType(), true ), null );
        }
        
        if( val is AbstractComponent ) {
            CreateControls( prop.Name, val as AbstractComponent );
        }
        
    }*/
    
}
