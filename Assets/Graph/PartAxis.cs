using UnityEngine;
using System.Collections;

public class PartAxis : MonoBehaviour {

    public GameObject redArrow;
    public GameObject greenArrow;
    public GameObject blueArrow;

    public float xDistance = 2;
    public float yDistance = 2;
    public float zDistance = 2;


	void Start () {

	    redArrow.transform.localPosition = redArrow.transform.localPosition + Vector3.right*xDistance;
        greenArrow.transform.localPosition = greenArrow.transform.localPosition + Vector3.up*yDistance;
        blueArrow.transform.localPosition = blueArrow.transform.localPosition + Vector3.forward*zDistance;
	}
}
