using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class NodeView : MonoBehaviour {

    public NodeProxy proxy;
    public new Transform transform;
    public BulletRigidBody btRigidBody;
    public Highlight highlight;

    void Awake () {
        transform = gameObject.transform;
        highlight = GetComponent<Highlight>();
    }

    void Start () {
    }
    //anything put in these methods will apply to all parts. dunno where these empty functions
    // are defined, since it isnt written "virtual" anywhere. I'm obviously a c# newbie... -z26
    void OnMouseEnterCustom ( TouchData touch ) {
    }

    void OnMouseExitCustom ( TouchData touch ) {
    }

    void OnMouseMoveCustom ( TouchData touch ) {
    }

    void OnMouseDownCustom ( TouchData touch ) {
    }

    void OnMouseUpCustom ( TouchData touch ) {
    }

    void OnMouseDragCustom ( TouchData touch ) {
    }

    void OnMouseStayCustom ( TouchData touch ) {
    }

}

public class TouchData {
    public int touchId;
    public Vector3 position;
    public Vector3 deltaPosition;
    public Camera camera;
    public Vector3 worldPosition;
    public bool dragging = false;
}
