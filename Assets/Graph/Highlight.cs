using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Highlight : MonoBehaviour {

    public Dictionary< Renderer, Material > materials = new Dictionary< Renderer, Material >();
    public List< Renderer > renderers = new List< Renderer >();
    public List< Renderer > subpartRenderers = new List< Renderer >();
    public List< Renderer > hiddenRenderers = new List< Renderer >();
    bool lit = false;
    public bool highlighted = false;
    public PartAxis axis;
    public float xAxisOffset = 2f;
    public float yAxisOffset = 2f;
    public float  zAxisOffset = 2f;
    public bool showAxis = true;
    public Material ghostMaterial;
    public enum HighlightType {
        Normal,
        Blue,
        Green,
        Yellow,
        Bright,
        Red,
        Skyblue
    }

    void Awake () {
        Prepare();
    }

    public void Prepare () {
        if ( renderers.Count == 0 ) {
            foreach ( var r in gameObject.GetComponentsInChildren< Renderer >() ) {
                if ( r.enabled ) {
                    if ( r.castShadows || r.receiveShadows ) {
                        renderers.Add( r );
                    }
                    else {
                        hiddenRenderers.Add( r );
                    }
                }
            }
        }

        foreach ( var renderer in renderers ) {
            Material material = null;
            if ( !materials.TryGetValue( renderer, out material ) ) {
                materials[ renderer ] = renderer.sharedMaterial;
            }
        }

    }

    public void RegisterRenderers ( List<Renderer> newRenderers ) {

        subpartRenderers.AddRange( newRenderers );
        foreach ( var renderer in newRenderers ) {
            Material material = null;
            if ( !materials.TryGetValue( renderer, out material ) ) {
                materials[ renderer ] = renderer.sharedMaterial;
            }
        }
    }

    public void SetAxis ( bool activate ) {

        if ( activate && showAxis ) {
            if ( axis == default(PartAxis) ) {
                var axisGameObject = Instantiate( Resources.Load( "FX/PartAxis" ) ) as GameObject;
                var bulletRigidody = GetComponent<BulletRigidBody>();
                axisGameObject.name = "axis";
                axis = axisGameObject.GetComponent<PartAxis>();
                axis.transform.parent = transform;
                axis.xDistance = xAxisOffset;
                axis.yDistance = yAxisOffset;
                axis.zDistance = zAxisOffset;
                if(bulletRigidody == default(BulletRigidBody)){
                    axis.transform.localPosition =Vector3.zero;
                    axis.transform.localRotation =Quaternion.identity;
                }else{
                    axis.transform.localPosition = bulletRigidody.compoundShape.centerOfMass.transform.localPosition;
                    axis.transform.localRotation =bulletRigidody.compoundShape.centerOfMass.transform.localRotation;
                }

            }
        }
        else {
            if ( axis != default(PartAxis) ) {

                Destroy( axis.gameObject );
                axis = default(PartAxis);
            }

        }
    }

    public void SetHighlight ( bool on, HighlightType highlightType = HighlightType.Normal, bool affectSubpart = true ) {
        highlighted = on;
//        var part = GetComponent<Part>();
        if ( on && !lit ) {

            SetAxis( true );

            lit = true;
            Material selectedHightlight = null;

            var builder = Builder.GetInstance();

            var tintedMaterial = default(Material);

            if(ghostMaterial != default(Material)){

                tintedMaterial = new Material(ghostMaterial);
            }

            switch ( highlightType ) {
                case HighlightType.Blue:
                    selectedHightlight = builder.blueHighlight;
                    //tintedMaterial = default(Material);
                    if ( tintedMaterial != default(Material) ) {
                        tintedMaterial.SetColor( "_Color", new Color( 0 / 255f, 169/ 255f, 255 / 255f ) );
                    }
                    break;
                case HighlightType.Green:
                    selectedHightlight = builder.greenHighlight;
                    //tintedMaterial = default(Material);
                    if ( tintedMaterial != default(Material) ) {

                        tintedMaterial.SetColor( "_Color",new Color( 101 / 255f, 255 / 255f, 0 / 255f ) );
                    }
                    break;
                case HighlightType.Yellow:
                    selectedHightlight = builder.yellowHighlight;
                    //tintedMaterial = default(Material);
                    if ( tintedMaterial != default(Material) ) {
                        tintedMaterial.SetColor( "_Color", new Color( 238 / 255f, 228 / 255f, 66 / 255f ) );
                    }
                    break;
                case HighlightType.Normal:
                    selectedHightlight = builder.highlight;
                    //tintedMaterial = default(Material);
                    if ( tintedMaterial != default(Material) ) {
                        tintedMaterial.SetColor( "_Color",new Color( 255 / 255f, 230 / 255f, 0 / 255f ) );
                    }
                    break;
                case HighlightType.Red:
                    selectedHightlight = builder.redHighlight;
                    //tintedMaterial = default(Material);
                    if ( tintedMaterial != default(Material) ) {
                        tintedMaterial.SetColor( "_Color", new Color( 255 / 255f, 29 / 255f, 29 / 255f ) );
                    }
                    break;
                case HighlightType.Skyblue:
                    selectedHightlight = builder.skyblueHighlight;
                    tintedMaterial = default(Material);
                    if ( tintedMaterial != default(Material) ) {
                        tintedMaterial.SetColor( "_Color", new Color( 0 / 255f, 226 / 255f, 255 / 255f ) );
                    }
                    break;
                case HighlightType.Bright:
                    break;
                default:

                    selectedHightlight = builder.highlight;
                    tintedMaterial = default(Material);
                    if ( tintedMaterial != default(Material) ) {
                        tintedMaterial.SetColor( "_Color", new Color( 0 / 255f, 226 / 255f, 255 / 255f ) );
                    }
                    break;
            }

            if ( ghostMaterial != default(Material) ) {

            }


            foreach ( var renderer in renderers ) {

                if ( highlightType == HighlightType.Bright ) {
                    renderer.material.SetColor( "_Tint", Color.white * 0.75f );
                }
                else {
                    if ( tintedMaterial != default(Material) ) {
                        renderer.material = tintedMaterial;
                    }
                    else {
                        renderer.material = selectedHightlight;
                    }
                }
            }
            if ( affectSubpart ) {
                foreach ( var renderer in subpartRenderers ) {
                    if ( tintedMaterial != default(Material) ) {
                        renderer.material = tintedMaterial;
                    }
                    else {
                        renderer.material = selectedHightlight;
                    }
                }
            }
            foreach ( var renderer in hiddenRenderers ) {
                renderer.enabled = false;
            }
        }
        else if ( !on && lit ) {
            SetAxis( false );
            lit = false;
            foreach ( var renderer in renderers ) {
                Material material = null;
                if ( materials.TryGetValue( renderer, out material ) ) {
                    renderer.material = material;
                }
                else {

                    Debug.Log( "not found :/" );
                }
            }
            //if ( affectSubpart ) {
            foreach ( var renderer in subpartRenderers ) {
                Material material = null;
                if ( materials.TryGetValue( renderer, out material ) ) {
                    renderer.material = material;
                }
            }
            //}
            foreach ( var renderer in hiddenRenderers ) {
                renderer.enabled = true;
            }
            //materials.Clear();
        }
    }
}
