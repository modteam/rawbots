using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GravityField : MonoBehaviour {

    public bool on = true;
    public float gravity;
	public float innerRadio = 0;
    public new Transform transform;
    public static List<GravityField> fields = new List<GravityField>();
	private Bounds[] bounds;
	private Vector3[] cubesUp;
    void Awake () {
        if(fields == null)fields = new List<GravityField>();
        fields.Add(this);
        transform = gameObject.transform;
		bounds =new Bounds[filters.Count()];
		cubesUp = new Vector3[bounds.Length];
		for(int i = 0; i< bounds.Length ;i++){
			var filter = filters[i];
			bounds[i]= filter.GetComponent<Renderer>().bounds;
			cubesUp[i]= filter.transform.up;
		}
		
    }
	public GravityFieldPlane[] filters;
	
	public void SetBounds(float height){
		for(int i = 0; i< bounds.Length ;i++){
			var filter = filters[i];
			filter.bounds = filter.GetComponent<Renderer>().bounds;
			filter.up = filter.transform.up;
			filter.height = height;
			filter.SetPlane();
//			bounds[i]= filter.renderer.bounds;
//			cubesUp[i]= filter.transform.up;
		}
		
	}
    void OnTrigger(BulletCollision collision){
        collision.btRigidBody.gameObject.SendMessage( "OnGravity", this, SendMessageOptions.DontRequireReceiver );

    }
    public Vector3 Compute ( Vector3 position ) {
        var radius = transform.localScale.x/2;
		var distanceVector = transform.position - position;
		var invSqrt = Util.FastInvSqrt( distanceVector.x*distanceVector.x+distanceVector.y*distanceVector.y+distanceVector.z*distanceVector.z);
        var distance = 1.0f/invSqrt;
        if ( distance > 0.01f ) {
            var normal = FilterNormal( distanceVector/distance,position);
            var diff = distance / radius;
            return gravity * normal* ( 1 - ( ( 2 * diff - 1 ) * ( 2 * diff - 1 ) ) );
        }
        return Vector3.up;
    }
	
	
	Vector3 FilterNormal(Vector3 normal, Vector3 testPoint){
		for(int i = 0; i<bounds.Length ; i++){
		var filter = filters[i];
			if( filter.bounds.Contains(testPoint)){
				return filter.GetNormal(normal); 
			}
		}
		return normal;
	}
	
	public Vector3 GetCenter(Vector3 testPoint){
		
		for(int i = 0; i<bounds.Length ; i++){
		var filter = filters[i];
			if(filter.bounds.Contains(testPoint)){
				
				return filter.GetCenter(testPoint);
//				Plane plane = new Plane(cubesUp[i], transform.position +cubesUp[i]*innerRadio );
//				Ray ray = new Ray(testPoint,cubesUp[i]*-1f);
//				float distance = 0f;
//				if(plane.Raycast(ray,out distance)){
//					//Debug.Log("Inside cube "+Time.time);
//					return  ray.GetPoint(distance);
//				}
			}
		}
//Debug.Log("outside cube "+Time.time);
//return transform.position;//transform.position + Util.FastNormalize(testPoint - transform.position)*innerRadio;

        if(bounds.Length > 5)return filters[6].GetCenterFromOutSide(testPoint);
        else return transform.position;
	}
	

    void OnDisable(){
        fields.Clear();
    }

}
