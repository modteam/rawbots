using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class OscilloscopeSignal {

    int currentSample = 0;
    float deltaTime = 0.02f;
    public float max = 1;
    public  float min = 1;
    public float delta = 0;
    public int sample = 4;
    public List<Vector3> samples = new List<Vector3>();
    List<float> tValues = new List<float>();
    List<float> yValues = new List<float>();
 
    public void SetPoints ( float yValue ) {
        
        if ( currentSample != sample ) {
            int iterations = Mathf.FloorToInt( sample / deltaTime );
            tValues.Clear();
            yValues.Clear();
            for ( int i = 0; i <= iterations; ++i ) {
                float tValue = i * deltaTime;
                tValues.Add( tValue );
                yValues.Add( 0 );
            }
            FillSamples();
        }
        currentSample = sample;
        yValues.RemoveAt( 0 );
        yValues.Add( yValue );
        FillSamples();
    }
 
    public void FillSamples () {
        samples.Clear();
        max = yValues.Max();
        min = yValues.Min();
        for ( int i = 0; i < tValues.Count; ++i ) {
            samples.Add( new Vector3( tValues[i], yValues[i], 0 ) );
        }
        delta = min + Mathf.Abs( max - min ) / 2;
    }
 
}
