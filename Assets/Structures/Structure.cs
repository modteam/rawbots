using UnityEngine;
using System.Collections;

public class Structure : MonoBehaviour {

    public Operants.NodeProxy proxy;
    public Oracle.Entity parentEntity;

    public StructureHooks hooks;
	public StructureHooks addOnhooks;
	public NodeView nodeView;
}
