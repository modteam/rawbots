using UnityEngine;
using System.Collections;
using System.Linq;

public class StructureHooks : MonoBehaviour {

    public  HookPoint[] hooks;
    public NodeView nodeView;

  

    public HookPoint GetClosestHookPoint ( Vector3 worldHitPoint ) {

//        var minDistance = hooks.Where(h => h.hookType == type || type == HookPoint.HookType.All )
//                               .Min( h => Vector3.Distance( h.gameObject.transform.position, worldHitPoint ) );
		
		var minDistance = hooks.Min( h => Vector3.Distance( h.gameObject.transform.position, worldHitPoint ) );
        return hooks.Where( h => {
            var distance = Vector3.Distance( h.gameObject.transform.position, worldHitPoint );
            return minDistance == distance;
        } ).FirstOrDefault();
    }




}
