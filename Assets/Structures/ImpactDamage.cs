using UnityEngine;
using System.Collections;
using Operants;

public class ImpactDamage : MonoBehaviour {
    BulletRigidBody body;
    
    
    float speedThreshold = 25;
    float dmgMultiplier = 100f;
    float radiusMultiplier = 0.003f;
    float minDelay = 0.5f;
    float timer = 0;
    
    
    
    void Start(){
        body = GetComponent<BulletRigidBody>();
        body.AddOnCollisionDelegate(OnCollision);
        
    }

    //used for the hex_impact terrain only -z26
     void OnCollision (Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction){
        
        PartProxy partHit;
        var otherNodeview = other.gameObject.GetComponent<NodeView>();
        
        if ( otherNodeview != null && otherNodeview.proxy is PartProxy  ) {
            partHit = otherNodeview.proxy as PartProxy;
        } else {
            return;
        }
        
        if(relativeVelocity.magnitude < speedThreshold) {return;}
        
         
        float maxdamage = (relativeVelocity.magnitude) * dmgMultiplier;
        float maxradius = maxdamage * radiusMultiplier;

        //if treshold is exceeded
        if ( maxdamage > 0 && Time.time - timer > minDelay) {
            
            Debug.Log(maxdamage);
            Debug.Log(maxradius);
        
            GameObject.Instantiate( Resources.Load( "FX/BlastImpact" ), contactPoint, transform.rotation );
            var parts = partHit.PhysicallyLinked();
            
            
            foreach(PartProxy p in parts) {
                
                float distance = Vector3.Magnitude(p.nodeView.transform.position - contactPoint);
                
                float dmgReduction = maxdamage * ( distance / maxradius);
                float damage = maxdamage - dmgReduction;
                
                if (damage > 0){ p.OnHit(damage); }
            }
    
            timer = Time.time; //reset the max delay
        }
    }
    
    
}
