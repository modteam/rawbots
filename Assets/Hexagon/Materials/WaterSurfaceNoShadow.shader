Shader "Rozgo/WaterSurfaceNoShadows" {

	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	
	SubShader {
	 
		Tags {
			"Queue"="Geometry"
			"RenderType"="Transparent"
		}
		

		
	CGPROGRAM
		#pragma surface surf BlinnPhong alpha fullforwardshadows
		
		float4 _Color;
		
		struct v2f {
			float2 uv : TEXCOORD1;
		};
	
		struct Input {
			float2 uv_MainTex;
		};
	
		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color.rgb;
			o.Alpha = _Color.a;
		}
	ENDCG
	}

}


