using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Offset_texture : MonoBehaviour {

    public float flowSpeed = 5;
    public float flowSpeedy = 5;
    public new Renderer renderer;

 
    void Update () {

        var offset = renderer.material.mainTextureOffset;
        offset.x += Time.deltaTime * flowSpeed;
        offset.y += Time.deltaTime * flowSpeedy;
        renderer.material.mainTextureOffset = offset;
    }
}
