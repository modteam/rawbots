#include Assembler/Assembly
#include Assembler/Touch
#include Assembler/KeyBoard
#include Bot/Bot

################################################
##midi001
################################################
midi001 . position [0,0,0]
midi001 . rotation [0,0,0]
midi001 as midi_input

midi001_hip_map output_for midi001
midi001_hip_map . label control_10
midi001_hip_map . property control_10
midi001_hip_map as midi_input_sample

midi001_right_shouder_motor_map output_for midi001
midi001_right_shouder_motor_map . label control_11
midi001_right_shouder_motor_map . property control_11
midi001_right_shouder_motor_map as midi_input_sample

midi001_left_shouder_motor_map output_for midi001
midi001_left_shouder_motor_map . label control_12
midi001_left_shouder_motor_map . property control_12
midi001_left_shouder_motor_map as midi_input_sample

midi001_right_shouder_elbow_map output_for midi001
midi001_right_shouder_elbow_map . label control_13
midi001_right_shouder_elbow_map . property control_13
midi001_right_shouder_elbow_map as midi_input_sample

midi001_left_shouder_elbow_map output_for midi001
midi001_left_shouder_elbow_map . label control_14
midi001_left_shouder_elbow_map . property control_14
midi001_left_shouder_elbow_map as midi_input_sample

midi001_right_elbow_motor_map output_for midi001
midi001_right_elbow_motor_map . label control_15
midi001_right_elbow_motor_map . property control_15
midi001_right_elbow_motor_map as midi_input_sample

midi001_left_elbow_motor_map output_for midi001
midi001_left_elbow_motor_map . label control_16
midi001_left_elbow_motor_map . property control_16
midi001_left_elbow_motor_map as midi_input_sample

midi001_right_elbow_elbow_map output_for midi001
midi001_right_elbow_elbow_map . label control_17
midi001_right_elbow_elbow_map . property control_17
midi001_right_elbow_elbow_map as midi_input_sample

midi001_left_elbow_elbow_map output_for midi001
midi001_left_elbow_elbow_map . label control_18
midi001_left_elbow_elbow_map . property control_18
midi001_left_elbow_elbow_map as midi_input_sample

midi001_right_thigh_motor_map output_for midi001
midi001_right_thigh_motor_map . label control_21
midi001_right_thigh_motor_map . property control_21
midi001_right_thigh_motor_map as midi_input_sample

midi001_left_thigh_motor_map output_for midi001
midi001_left_thigh_motor_map . label control_22
midi001_left_thigh_motor_map . property control_22
midi001_left_thigh_motor_map as midi_input_sample

midi001_right_knee_elbow_map output_for midi001
midi001_right_knee_elbow_map . label control_23
midi001_right_knee_elbow_map . property control_23
midi001_right_knee_elbow_map as midi_input_sample

midi001_left_knee_elbow_map output_for midi001
midi001_left_knee_elbow_map . label control_24
midi001_left_knee_elbow_map . property control_24
midi001_left_knee_elbow_map as midi_input_sample

midi001_hip_map01 output_for midi001
midi001_hip_map01 . label control_30
midi001_hip_map01 . property control_30
midi001_hip_map01 as midi_input_sample

midi001_right_shouder_motor_map01 output_for midi001
midi001_right_shouder_motor_map01 . label control_31
midi001_right_shouder_motor_map01 . property control_31
midi001_right_shouder_motor_map01 as midi_input_sample

midi001_left_shouder_motor_map01 output_for midi001
midi001_left_shouder_motor_map01 . label control_32
midi001_left_shouder_motor_map01 . property control_32
midi001_left_shouder_motor_map01 as midi_input_sample

midi001_right_shouder_elbow_map01 output_for midi001
midi001_right_shouder_elbow_map01 . label control_33
midi001_right_shouder_elbow_map01 . property control_33
midi001_right_shouder_elbow_map01 as midi_input_sample

midi001_left_shouder_elbow_map01 output_for midi001
midi001_left_shouder_elbow_map01 . label control_34
midi001_left_shouder_elbow_map01 . property control_34
midi001_left_shouder_elbow_map01 as midi_input_sample

midi001_right_elbow_motor_map01 output_for midi001
midi001_right_elbow_motor_map01 . label control_35
midi001_right_elbow_motor_map01 . property control_35
midi001_right_elbow_motor_map01 as midi_input_sample

midi001_left_elbow_motor_map01 output_for midi001
midi001_left_elbow_motor_map01 . label control_36
midi001_left_elbow_motor_map01 . property control_36
midi001_left_elbow_motor_map01 as midi_input_sample

midi001_right_elbow_elbow_map01 output_for midi001
midi001_right_elbow_elbow_map01 . label control_37
midi001_right_elbow_elbow_map01 . property control_37
midi001_right_elbow_elbow_map01 as midi_input_sample

midi001_left_elbow_elbow_map01 output_for midi001
midi001_left_elbow_elbow_map01 . label control_38
midi001_left_elbow_elbow_map01 . property control_38
midi001_left_elbow_elbow_map01 as midi_input_sample

midi001_right_thigh_motor_map01 output_for midi001
midi001_right_thigh_motor_map01 . label control_41
midi001_right_thigh_motor_map01 . property control_41
midi001_right_thigh_motor_map01 as midi_input_sample

midi001_left_thigh_motor_map01 output_for midi001
midi001_left_thigh_motor_map01 . label control_42
midi001_left_thigh_motor_map01 . property control_42
midi001_left_thigh_motor_map01 as midi_input_sample

midi001_right_knee_elbow_map01 output_for midi001
midi001_right_knee_elbow_map01 . label control_43
midi001_right_knee_elbow_map01 . property control_43
midi001_right_knee_elbow_map01 as midi_input_sample

midi001_left_knee_elbow_map01 output_for midi001
midi001_left_knee_elbow_map01 . label control_44
midi001_left_knee_elbow_map01 . property control_44
midi001_left_knee_elbow_map01 as midi_input_sample