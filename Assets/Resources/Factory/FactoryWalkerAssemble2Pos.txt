#################################################
###core00
#################################################
core00 . position [0,8,4]
core00 . rotation [0,0,0]

lcannon . position [2,5,-8]
lcannon . rotation [0,180,0]

rcannon . position [-2,5,-8]
rcannon . rotation [0,180,0]

#left_shield_a . position [-19,2,12]
#left_shield_a . rotation [0,0,270]

back_left_leg_box . position [5,-2,4]
back_left_leg_box . rotation [0,0,0]

parker_third_middle_box . position [5,-4,4]
parker_third_middle_box . rotation [0,0,0]

back_base_left_elbow . position [4,4,4]
back_base_left_elbow . rotation [90,0,180]

back_left_elbow . position [5,0,4]
back_left_elbow . rotation [0,270,90]

back_foot . position [5,-6,4]
back_foot . rotation [0,0,0]

lshotgun . position [0,12,6]
lshotgun . rotation [0,0,0]

#################################################

#right_shield_a . position [-19,2,-12]
#right_shield_a . rotation [0,0,270]

back_right_leg_box . position [-5,-2,4]
back_right_leg_box . rotation [0,0,0]

parker_second_middle_box . position [-5,-4,4]
parker_second_middle_box . rotation [0,0,0]

back_base_right_elbow . position [-4,4,4]
back_base_right_elbow . rotation [90,0,0]

back_right_elbow . position [-5,0,4]
back_right_elbow . rotation [0,270,270]

back_foot2 . position [-5,-6,4]
back_foot2 . rotation [0,0,0]

#################################################
###mt02
#################################################
mt02 . position [0,6,4]
mt02 . rotation [180,0,0]

#################################################
###top00
#################################################
top00 . position [0,2,-2]
top00 . rotation [0,0,0]

#################################################
###top01
#################################################
top01 . position [0,2,-4]
top01 . rotation [0,0,0]

#################################################
###top02
#################################################
top02 . position [0,2,-6]
top02 . rotation [0,0,0]

#################################################
###mt00
#################################################
#mt00 . position [8,-4,8]
#mt00 . rotation [0,0,-90]

#################################################
###mt01
#################################################
#mt01 . position [-8,-4,8]
#mt01 . rotation [0,0,90]

#################################################
###lgun
#################################################
#lgun . position [10,-4,9]
#lgun . rotation [0,0,-90]

#################################################
###rgun
#################################################
#rgun . position [-10,-4,9]
#rgun . rotation [0,0,90]

#################################################
###base00
#################################################
base00 . position [0,4,4]
base00 . rotation [0,0,0]

#################################################
###base01
#################################################
base01 . position [0,-4,-10]
base01 . rotation [0,0,0]

#################################################
###base02
#################################################
base02 . position [2,2,-6]
base02 . rotation [0,0,0]

#################################################
###base03
#################################################
base03 . position [-2,2,-6]
base03 . rotation [0,0,0]

#################################################
###base04
#################################################
base04 . position [-4,2,-6]
base04 . rotation [0,0,0]

#################################################
###wheelbase00
#################################################
#wheelbase00 . position [0,-4,8]
#wheelbase00 . rotation [0,0,0]

#################################################
###wheelbase01
#################################################
wheelbase01 . position [4,2,-6]
wheelbase01 . rotation [0,0,0]

#################################################
###wheelbase02
#################################################
#wheelbase02 . position [0,0,6]
#wheelbase02 . rotation [0,0,0]

#################################################
###wheelbase03
#################################################
#wheelbase03 . position [-2,-4,-8]
#wheelbase03 . rotation [0,0,0]

#################################################
###wheelmotor02
#################################################
wheelmotor02 . position [-2,4,4]
wheelmotor02 . rotation [0,0,-90]

#################################################
###wheelmotor00
#################################################
wheelmotor00 . position [2,4,4]
wheelmotor00 . rotation [0,0,-90]

#################################################
###wheelmotor01
#################################################
wheelmotor01 . position [2,-4,-10]
wheelmotor01 . rotation [0,0,-90]

#################################################
###wheelmotor05
#################################################
#wheelmotor05 . position [-4,-4,0]
#wheelmotor05 . rotation [0,0,90]

#################################################
###wheelmotor03
#################################################
#wheelmotor03 . position [-2,-4,8]
#wheelmotor03 . rotation [0,0,90]

#################################################
###wheelmotor04
#################################################
wheelmotor04 . position [-2,-4,-10]
wheelmotor04 . rotation [0,0,90]

#################################################
###wheel00
#################################################
#wheel00 . position [5,-4,8]
#wheel00 . rotation [0,-90,0]

#################################################
###wheel01
#################################################
wheel01 . position [3,2,-2]
wheel01 . rotation [0,-90,0]

#################################################
###wheel02
#################################################
wheel02 . position [5,-4,-10]
wheel02 . rotation [0,-90,0]

#################################################
###wheel03
#################################################
#wheel03 . position [-5,-4,8]
#wheel03 . rotation [0,90,0]

#################################################
###wheel04
#################################################
wheel04 . position [-3,2,-2]
wheel04 . rotation [0,90,0]

#################################################
###wheel05
#################################################
wheel05 . position [-5,-4,-10]
wheel05 . rotation [0,90,0]


#################################################
###front_shield
#################################################
#front_shield . position [0,-2,11]
#front_shield . rotation [270,0,90]

#################################################
###back_shield
#################################################
back_shield . position [0,9,6]
back_shield . rotation [90,0,180]

#################################################
###front_right_shield
#################################################
front_right_shield . position [2,8,4]
front_right_shield . rotation [0,90,90]

#################################################
###front_left_shield
#################################################
front_left_shield . position [-2,8,4]
front_left_shield . rotation [0,270,270]

#################################################
###inner_right_shield
#################################################
inner_right_shield . position [5,2,-6]
inner_right_shield . rotation [0,90,180]

#################################################
###inner_left_shield
#################################################
inner_left_shield . position [-5,2,-6]
inner_left_shield . rotation [0,270,0]
