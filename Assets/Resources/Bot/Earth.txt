######################################################
# planet
######################################################

planet . proxy Operants.PlanetProxy
planet . view Planets/Planet
planet . tile_name planet
planet is celestial_body

planet_type input_for planet
planet_type . property type
planet_type . value Body
planet_type . enum Oracle.PlanetType
planet_type is input_list

planet_size input_for planet
planet_size . property size
planet_size . value 1
planet_size is input_amount

planet_gravity input_for planet
planet_gravity . property gravity
planet_gravity . value 100
planet_gravity is input_amount

planet_roots input_for planet
planet_roots . property roots
planet_roots . value 0
planet_roots is input_amount

######################################################
# structure
######################################################

structure . contact_fx 0

#structure_structure output_for structure
#structure_structure . property structure
#structure_structure is output_structure

######################################################
# hex_earth
######################################################

hex_earth . proxy Operants.HexEarthProxy
hex_earth . view Bot/HexEarth
hex_earth . root True
hex_earth . contact_fx 1
hex_earth . tile_name hex_earth
hex_earth is structure

######################################################
# small_hex_earth
######################################################

small_hex_earth . proxy Operants.HexEarthProxy
small_hex_earth . view Bot/SmallHexEarth
small_hex_earth . tile_name small_hex_earth
small_hex_earth . root True
small_hex_earth . contact_fx 1
small_hex_earth is structure

######################################################
# small_hex_ice
######################################################

small_hex_ice . proxy Operants.HexEarthProxy
small_hex_ice . view Bot/SmallHexIce
small_hex_ice . tile_name small_hex_ice
small_hex_ice . contact_fx 3
small_hex_ice is structure

######################################################
# small_hex_volcanic_rock
######################################################

small_hex_volcanic_rock . proxy Operants.HexEarthProxy
small_hex_volcanic_rock . view Bot/SmallHexVolcanicRock
small_hex_volcanic_rock . tile_name small_hex_volcanic_rock
small_hex_volcanic_rock . contact_fx 2
small_hex_volcanic_rock is structure

######################################################
# small_hex_metal
######################################################

small_hex_metal . proxy Operants.HexEarthProxy
small_hex_metal . view Bot/SmallHexMetal
small_hex_metal . tile_name small_hex_metal
small_hex_metal . contact_fx 4
small_hex_metal is structure

######################################################
# small_hex_grass
######################################################

small_hex_grass . proxy Operants.HexEarthProxy
small_hex_grass . view Bot/SmallHexGrass
small_hex_grass . tile_name small_hex_grass
small_hex_grass . contact_fx 5
small_hex_grass is structure

######################################################
# hex_earth_ramp
######################################################

hex_earth_ramp . proxy Operants.HexEarthProxy
hex_earth_ramp . view Bot/HexEarthRamp
hex_earth_ramp . tile_name hex_earth_ramp
hex_earth_ramp . contact_fx 1
hex_earth_ramp is structure

######################################################
# small_hex_ramp
######################################################

small_hex_ramp . proxy Operants.HexEarthProxy
small_hex_ramp . view Bot/SmallHexRamp
small_hex_ramp . tile_name small_hex_ramp
small_hex_ramp . contact_fx 1
small_hex_ramp is structure

######################################################
# small_hex_ice_ramp
######################################################

small_hex_ice_ramp . proxy Operants.HexEarthProxy
small_hex_ice_ramp . view Bot/SmallHexIceRamp
small_hex_ice_ramp . tile_name small_hex_ice_ramp
small_hex_ice_ramp . contact_fx 3
small_hex_ice_ramp is structure

######################################################
# small_hex_metal_ramp
######################################################

small_hex_metal_ramp . proxy Operants.HexEarthProxy
small_hex_metal_ramp . view Bot/SmallHexMetalRamp
small_hex_metal_ramp . tile_name small_hex_metal_ramp
small_hex_metal_ramp . contact_fx 4
small_hex_metal_ramp is structure

######################################################
# small_hex_volcanic_ramp
######################################################

small_hex_volcanic_ramp . proxy Operants.HexEarthProxy
small_hex_volcanic_ramp . view Bot/SmallHexVolcanicRamp
small_hex_volcanic_ramp . tile_name small_hex_volcanic_ramp
small_hex_volcanic_ramp . contact_fx 2
small_hex_volcanic_ramp is structure

######################################################
# small_hex_truncated_ramp
######################################################

small_hex_truncated_ramp . proxy Operants.HexEarthProxy
small_hex_truncated_ramp . view Bot/SmallHexTruncatedRamp
small_hex_truncated_ramp . tile_name small_hex_truncated_ramp
small_hex_truncated_ramp . contact_fx 1
small_hex_truncated_ramp is structure

######################################################
# small_hex_truncated_ice_ramp
######################################################

small_hex_truncated_ice_ramp . proxy Operants.HexEarthProxy
small_hex_truncated_ice_ramp . view Bot/SmallHexTruncatedIceRamp
small_hex_truncated_ice_ramp . tile_name small_hex_truncated_ice_ramp
small_hex_truncated_ice_ramp . contact_fx 3
small_hex_truncated_ice_ramp is structure

######################################################
# small_hex_truncated_metal_ramp
######################################################

small_hex_truncated_metal_ramp . proxy Operants.HexEarthProxy
small_hex_truncated_metal_ramp . view Bot/SmallHexTruncatedMetalRamp
small_hex_truncated_metal_ramp . tile_name small_hex_truncated_metal_ramp
small_hex_truncated_metal_ramp . contact_fx 4
small_hex_truncated_metal_ramp is structure

######################################################
# small_hex_truncated_volcanic_ramp
######################################################

small_hex_truncated_volcanic_ramp . proxy Operants.HexEarthProxy
small_hex_truncated_volcanic_ramp . view Bot/SmallHexTruncatedVolcanicRamp
small_hex_truncated_volcanic_ramp . tile_name small_hex_truncated_volcanic_ramp
small_hex_truncated_volcanic_ramp . contact_fx 2
small_hex_truncated_volcanic_ramp is structure

######################################################
# small_hex_half_ramp
######################################################

small_hex_half_ramp . proxy Operants.HexEarthProxy
small_hex_half_ramp . view Bot/SmallHexHalfRamp
small_hex_half_ramp . tile_name small_hex_half_ramp
small_hex_half_ramp . contact_fx 1
small_hex_half_ramp is structure

######################################################
# small_hex_half_ice_ramp
######################################################

small_hex_half_ice_ramp . proxy Operants.HexEarthProxy
small_hex_half_ice_ramp . view Bot/SmallHexHalfIceRamp
small_hex_half_ice_ramp . tile_name small_hex_half_ice_ramp
small_hex_half_ice_ramp . contact_fx 3
small_hex_half_ice_ramp is structure

######################################################
# small_hex_half_metal_ramp
######################################################

small_hex_half_metal_ramp . proxy Operants.HexEarthProxy
small_hex_half_metal_ramp . view Bot/SmallHexHalfMetalRamp
small_hex_half_metal_ramp . tile_name small_hex_half_metal_ramp
small_hex_half_metal_ramp . contact_fx 4
small_hex_half_metal_ramp is structure

######################################################
# small_hex_half_volcanic_ramp
######################################################

small_hex_half_volcanic_ramp . proxy Operants.HexEarthProxy
small_hex_half_volcanic_ramp . view Bot/SmallHexHalfVolcanicRamp
small_hex_half_volcanic_ramp . tile_name small_hex_half_volcanic_ramp
small_hex_half_volcanic_ramp . contact_fx 2
small_hex_half_volcanic_ramp is structure

######################################################
# hex_water
######################################################

hex_water . proxy Operants.HexEarthProxy
hex_water . view Bot/HexWater
hex_water . tile_name small_hex_half_volcanic_ramp
hex_water is structure

######################################################
# small_hex_water
######################################################

small_hex_water . proxy Operants.HexEarthProxy
small_hex_water . view Bot/SmallHexWater
small_hex_water . tile_name small_hex_water
small_hex_water is structure

######################################################
# hex_plat
######################################################

hex_plat . proxy Operants.HexPlatProxy
hex_plat . view Bot/HexPlat
hex_plat . tile_name hex_plat
hex_plat is structure

######################################################
# hex_column
######################################################

hex_column . proxy Operants.HexColumnProxy
hex_column . view Bot/HexColumn
hex_column . tile_name hex_column
hex_column is structure

######################################################
# energy_bridge
######################################################

energy_bridge . proxy Operants.EnergyBridgeProxy
energy_bridge . view Bot/EnergyBridge
energy_bridge . tile_name energy_bridge
energy_bridge is structure

energy_bridge_power input_for energy_bridge
energy_bridge_power . property power
energy_bridge_power . value 0
energy_bridge_power is input_amount

energy_bridge_length input_for energy_bridge
energy_bridge_length . property length
energy_bridge_length . value 0
energy_bridge_length is input_amount

energy_bridge_width input_for energy_bridge
energy_bridge_width . property width
energy_bridge_width . value 0
energy_bridge_width is input_amount

energy_bridge . scale [1,1,1]

######################################################
# hex_impact
######################################################

hex_impact . proxy Operants.HexEarthProxy
hex_impact . view Bot/HexImpact
hex_impact . root True
hex_impact . contact_fx 1
hex_impact . tile_name hex_impact
hex_impact is structure
