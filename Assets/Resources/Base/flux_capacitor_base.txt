flux_xform as xform
flux_xform local_to gaia

################################################
# flux_capacitor001
################################################
flux_capacitor001 . position [0,0,0]
flux_capacitor001 . rotation [0,0,0]
flux_capacitor001 . permissions 31
flux_capacitor001 as flux_capacitor
flux_capacitor001 local_to flux_xform