pressureplate_xform as xform
pressureplate_xform local_to gaia

################################################
# pressure_plate_001
################################################
pressure_plate_001 . position [0,0,0]
pressure_plate_001 . rotation [0,0,0]
pressure_plate_001 . permissions 31
pressure_plate_001 as pressure_plate
pressure_plate_001 local_to pressureplate_xform