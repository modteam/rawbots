using UnityEngine;
using System.Collections;

public class GrabPoint : MonoBehaviour {

    public GameObject constraintHolder;
    public BulletRigidBody bulletRigidbody;
	private BulletRigidBody other;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void DisconnectGrabPoint () {
		other.AddForce(other.GetComponent<PhysicalObject>().Up(),Vector3.zero);
	}


    public void PlaceGrabPoint(BulletRigidBody other){

		this.other = other;
        var newConstraintPos = (other.transform.position + transform.position)/2f;
        constraintHolder.transform.position = newConstraintPos;
        var constraint = constraintHolder.AddComponent<BulletGeneric6DofConstraint>();

        constraint.bulletRbA = bulletRigidbody;
        constraint.bulletRbB = other;

        constraint.solverIterations = 50;


        constraint.rbAPos = transform.position;
        constraint.rbBPos = other.transform.position;
        constraint.rbBRot = other.transform.rotation;
        constraint.rbARot = transform.rotation;

        //constraint.solverIterations = 20;
        constraint.useLinearReferenceFrameA = false;
        constraint.disableCollisionBetweenBodies = true;

        constraint.angularERPValue = Vector3.one*0.8f;
        constraint.linearERPValue = Vector3.one*0.8f;

        constraint.angularCFMValue = Vector3.zero;
        constraint.linearCFMValue = Vector3.zero;

        constraint.Initialize();

    }
}
