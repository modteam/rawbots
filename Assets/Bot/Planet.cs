using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class Planet : MonoBehaviour {

    public Water water;
    PlanetType type;
    public GravityField gravityField;
    public float size = 1;
    public int roots;
    public PlanetProxy proxy;
    public Material[] bodyTypes;
    public BulletCompoundShape shape;
    int[] tilingRatios = new [] { 10, 8 , 5 };
    new public Renderer renderer;


    void Start () {
        CreateRoots( roots );
    }

    public void SetType ( PlanetType type ) {
        this.type = type;
        if ( type.ToString().Contains( "Body" ) ) {
            water.gameObject.SetActive( false );
            renderer.material = bodyTypes[(int)type];
        }
    }

    public void SetSize ( float size ) {
        this.size = size;
        if ( type == PlanetType.Water ) {
            water.transform.localScale = Vector3.one * 2000 * size;
            var waterBulletRigidbody =  water.GetComponent<BulletRigidBody>();
            waterBulletRigidbody.SetCollisionShapeScale(water.shape.transform.localScale);
        }
        else {
            shape.transform.localScale = Vector3.one * 2000 * size;
			GetComponent<CameraObstacle>().radio = shape.transform.localScale.x; 
            var tiling = new  Vector2 (size * tilingRatios[(int)type] , size * tilingRatios[(int)type]);
            renderer.material.SetTextureScale("_MainTex", tiling);
            renderer.material.SetTextureScale("_BumpMap", tiling);
            renderer.material.SetTextureScale("_SpecularTex", tiling);
//            renderer.material.mainTextureScale = new  Vector2 (shape.transform.localScale.x/133.3333f,shape.transform.localScale.x/133.3333f);
            var bulletRigidbody =  GetComponent<BulletRigidBody>();
            bulletRigidbody.SetCollisionShapeScale(shape.transform.localScale);
        }
        gravityField.transform.localScale = Vector3.one * 4000 * size;
    }

    public void SetGravity ( float gravity ) {
        gravityField.gravity = gravity;
    }

    public void CreateRoots ( int count ) {

        var placer = new GameObject();

        int n = count;
        float inc = Mathf.PI * ( 3 - Mathf.Sqrt( 5 ) );
        float off = 2f / n;
        float x;
        float y;
        float z;
        float r;
        float phi;

        var hex_earth = Gaia.Instance().entities[ "hex_earth" ];
        var proxy = gameObject.GetComponent< NodeView >().proxy as PlanetProxy;
        var xform = proxy.entity.Heads( p => p == "local_to" ).First();
        var earths = FindObjectsOfType( typeof( HexEarth ) ) as HexEarth[];

        for ( int k = 0; k < n; k++ ) {

            y = k * off - 1 + ( off / 2 );
            r = Mathf.Sqrt( 1 - y * y );
            phi = k * inc;
            x = Mathf.Cos( phi ) * r;
            z = Mathf.Sin( phi ) * r;
            var up = new Vector3( x, y, z );
            placer.transform.up = up;
            var localPosition = up.normalized * 1000 * size;
            var position = transform.TransformPoint( localPosition );

            if ( earths.Any( h => Vector3.Distance( h.transform.position, position ) < 10 ) ) {
                continue;
            }

            var earth = new Entity();
            earth.id = "earth_block@" + earth.id;
            earth.SetProperty( "position", Util.Vector3ToProperty( localPosition ) );
            earth.SetProperty( "rotation", Util.Vector3ToProperty( placer.transform.eulerAngles ) );
            earth.SetProperty( "permissions", 15 );
            new Relationship( earth, "as", hex_earth );
            new Relationship( earth, "local_to", xform );

            Gaia.Instance().recentlyAddedEntities.Add( earth );
        }

        Destroy( placer );
    }

}
