using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Jet : MonoBehaviour {

    public GameObject propulsion;
    public float thrust = 0;
    public float yaw = 0;
    public float pitch = 0;

    public float fxWeight = 0;
    float internalWeight = 1;
    public new Light light;
    public ParticleSystem[] particles;
    List< ParticlesData > particlesData;
    public BulletRigidBody body;
    float lowPassFactor = 100;
    public float safeTimer = 0;
    struct ParticlesData {
        public float startSpeed;
        public float startSize;
    }

    void Start () {
        particlesData = new List< ParticlesData >();
        for ( var i = 0; i < particles.Length; ++i ) {
            var p = new ParticlesData();
            p.startSpeed = particles[ i ].startSpeed;
            p.startSize = particles[ i ].startSize;
            particlesData.Add( p );
        }
        safeTimer = Time.time;

    }

    void FixedUpdate () {
        if ( ( Time.time - safeTimer ) > 0.2f ) {
            var force = thrust * transform.up * internalWeight * 0.8f;
            //Debug.DrawLine(transform.position,transform.position + force,Color.blue);
            if (force.magnitude > 0.1f)
            {
                body.AddForce(force, propulsion.transform.rotation * (Vector3.up * -1f));
            }
        }
    }

    void Update () {
        GetComponent<AudioSource>().volume = Mathf.Lerp( GetComponent<AudioSource>().volume, fxWeight * 100, Time.deltaTime * lowPassFactor ) * internalWeight;
        if ( GetComponent<AudioSource>().volume > 0.01f ) {
            if ( !GetComponent<AudioSource>().isPlaying ) {
                GetComponent<AudioSource>().Play();
                GetComponent<AudioSource>().time = Time.time % GetComponent<AudioSource>().clip.length;
            }
        }
        else if ( GetComponent<AudioSource>().isPlaying ) {
            GetComponent<AudioSource>().Stop();
        }
        GetComponent<AudioSource>().pitch = 0.9f + fxWeight * internalWeight * 1;
        light.intensity = fxWeight * internalWeight;



        for ( var i = 0; i < particles.Length; ++i ) {
            if ( fxWeight < 0.001f ) {
                if ( particles[ i ].isPlaying ) {
                    particles[ i ].Stop();
                    particles[ i ].enableEmission = false;
                }
            }
            else {
                if ( !particles[ i ].isPlaying ) {
                    particles[ i ].Play();
                    particles[ i ].enableEmission = true;
                }
                else {
                    particles[ i ].startSpeed = fxWeight * internalWeight * particlesData[ i ].startSpeed;
                    particles[ i ].startSize = fxWeight * internalWeight * particlesData[ i ].startSize * 2;
                }
            }
        }
    }

    public void UpdateGimbal() {
        if (propulsion != default(Propulsion))
        {
            var gimbal = new Quaternion();
            gimbal.eulerAngles = new Vector3(yaw,0,pitch);
            propulsion.transform.localRotation = gimbal;
        }
    }

    void OnWaterEnter () {
        internalWeight = 0;
    }

    void OnWaterExit () {
        internalWeight = 1;
    }

}
