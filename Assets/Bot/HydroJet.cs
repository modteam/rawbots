using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class HydroJet : MonoBehaviour {

    public float thrust = 0;
    public float fxWeight = 0;
    public Tweaker blades;
    public Tweaker fire;
    public ParticleSystem rings;
    public BulletRigidBody bulletRigidBody;
    Vector3 fireScale;
    float internalWeight = 0;
    float lowPassFactor = 2;

    void Awake () {
        fireScale = fire.transform.localScale;
    }

    void FixedUpdate () {
        var force = thrust * transform.up * internalWeight*0.25f;
        if(force.magnitude >0.1f)bulletRigidBody.AddForce(force,Vector3.zero );
    }

    void Update () {
        GetComponent<AudioSource>().volume = Mathf.Lerp( GetComponent<AudioSource>().volume, fxWeight * fxWeight, Time.deltaTime * lowPassFactor ) * internalWeight;
        if ( GetComponent<AudioSource>().volume > 0.01f ) {
            if ( !GetComponent<AudioSource>().isPlaying ) {
                GetComponent<AudioSource>().Play();
            }
        }
        else if ( GetComponent<AudioSource>().isPlaying ) {
            GetComponent<AudioSource>().Stop();
        }
        GetComponent<AudioSource>().pitch = 0.8f + fxWeight * 2;
        blades.angularWave.frequency = 2 * fxWeight * internalWeight;
        fire.angularWave.frequency = 2 * fxWeight * internalWeight;
        fire.scaleWave.amplitude = 0.1f * fxWeight * internalWeight;
        rings.emissionRate = 3 * fxWeight * internalWeight;
        fire.initialScale = fireScale * Mathf.Max( fxWeight * internalWeight, 0.01f );
    }

    void OnWaterEnter () {
        internalWeight = 1;
    }

    void OnWaterExit () {
        internalWeight = 0;
    }

}
