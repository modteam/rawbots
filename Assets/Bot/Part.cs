using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class Part : MonoBehaviour {

    public bool isProtected = false;
    public NodeView nodeView;
    public PartProxy proxy;
    public List< Part > linkedParts;//to my surprise, it seems like linkedparts only is for jointed parts like elbows.
    public new Transform transform;//normal volt arc connexions don't seem to count -z26
    BulletLayers partMask = 0;
    public List< Material > partMaterials = new List< Material >();
    public BulletRigidBody bulletRigidBody;
    private static int partCount = 0;
    float relativeSpeedThreshold = 100; //increased from 40 to 100 to make the collision visual effect occur less -z26
    public bool inWater = false;
    public bool inWaterState = false;
    public Water water = null;
    public Vector3 waterEntryPoint = Vector3.zero;
    public Vector3 waterLineOfAction = Vector3.zero;
    float lastFXInstantiated = 0;

    public void SetAffectingWater ( Water water, Vector3 waterEntryPoint, Vector3 waterLineOfAction ) {
        //Debug.Log("set affecting water "+ Time.time);
        if ( this.water == null ) {
            this.water = water;
        }
        if ( this.water == water ) {
            inWater = true;
            this.waterEntryPoint = waterEntryPoint;
            this.waterLineOfAction = waterLineOfAction;
        }
    }

    void OnHit ( float damage ) {
        proxy.OnHit( damage );//damage both halves of a part if applicable -z26
        var otherHalfPart = linkedParts.FirstOrDefault();
        if(otherHalfPart != default(Part)){
            otherHalfPart.proxy.OnHit( damage );
        }
    }

    public void OnCollision ( Vector3 relativeVelocity, BulletRigidBody other, Vector3 contactPoint, Vector3 lineOfAction ) {
        var velocity = relativeVelocity.x * relativeVelocity.x
         + relativeVelocity.y * relativeVelocity.y
         + relativeVelocity.z * relativeVelocity.z;
     
        var relativeSpeed = 1.0f / Util.FastInvSqrt( velocity );// collision.relativeVelocity.magnitude;
        if ( relativeSpeed > relativeSpeedThreshold ) {
            if ( Time.time - lastFXInstantiated > 0.2f ) {
                var fx = Instantiate( Resources.Load( "FX/HitFeedback", typeof( GameObject ) ), contactPoint, Quaternion.identity ) as GameObject;
                fx.transform.parent = Gaia.Instance().scratch;
                fx.hideFlags = HideFlags.HideInHierarchy;
                fx.transform.localScale *= relativeSpeed * 0.05f;
                fx.transform.forward = Util.FastNormalize( lineOfAction );
                var volume = Mathf.Clamp01( relativeSpeed / 100f );
                fx.GetComponent<AudioSource>().volume = volume * volume * volume;
                fx.GetComponent<AudioSource>().pitch = Random.Range( 0.8f, 3f );
                lastFXInstantiated = Time.time;
            }
            //OnHit( relativeSpeed * 0.05f ); removed collision damage -z26
        }
    }

    void Awake () {
        partCount++;
        transform = gameObject.transform;
        partMask = BulletLayers.Part;
//        foreach ( Transform child in gameObject.transform ) {
//            if ( child.renderer != null && child.renderer.material.shader.name == "Rozgo/Part" ) {
//                partMaterials.Add( child.renderer.material );
//            }
//        }
        FindPartMaterialInChildren( transform );
//       foreach (var subpart in subparts) {
//           FindPartMaterialInChildren (subpart.transform);
//       }
    }

    void FindPartMaterialInChildren ( Transform parent ) {
        if ( parent.GetComponent<Renderer>() != null && ( parent.GetComponent<Renderer>().material.shader.name == "Rozgo/Part" ) ) {
            partMaterials.Add( parent.GetComponent<Renderer>().material );
        }
        foreach ( Transform child in parent ) {
            FindPartMaterialInChildren( child );
        }
    }

    void Start () {
        bulletRigidBody.AddOnCollisionDelegate( OnCollision );
    }

    void OnCreateView ( PartProxy proxy ) {
        this.proxy = proxy;
    }

    public Slot GetClosestFreeSlot ( Vector3 point ) {

        var slots = proxy.entity.Tails( p => p == "slot_for" ).
            Select( e => e.proxy )
                .Where( sp => !sp.entity.Heads( p => p == "extends" ).Any() )
                .Where( sp => !sp.entity.Tails( p => p == "extends" ).Any() );
        var ordererList = slots.Select( s => {
            return new {proxy = s,distance = Vector3.Distance( s.nodeView.transform.position, point )};
        }
        ).OrderBy( obj => obj.distance ).Select( p => p.proxy.nodeView.GetComponent<Slot>() );

        return ordererList.FirstOrDefault();

    }

    public List< Part > GetNearByParts () { //I noticed when you build stuff if you put for instance 8 continuums in a cube, the game
        var nearByParts = new List< Part >();//will automatically notice and connect adjacents continuums without the player needing to
        var partBSlots = ( ( Operants.PartProxy )nodeView.proxy ).GetAvailableSlots();//do so.  Maybe this is used for that? -z26
        BulletRaycastHitDetails hit;
        for ( int i = 0; i< partBSlots.Count; i++ ) {
            var slotView = partBSlots[ i ].nodeView;
            var origin = slotView.transform.position - slotView.transform.forward * 0.2f;
            Ray ray = new Ray( origin, slotView.transform.forward );
            
            var status = Bullet.Raycast( ray, out hit, 5, partMask, partMask );
            if ( status ) {
                Part otherPart = default(Part);
                otherPart = hit.bulletRigidbody.GetComponent<Part>();// Part.FindOwner( hit.collider.transform, -1 );
                var subpart = hit.bulletRigidbody.GetComponent<Subpart>();
                if ( subpart != null ) {
                    otherPart = subpart.part;
                }
                if ( !nearByParts.Contains( otherPart ) && otherPart.nodeView != nodeView ) {
                    nearByParts.Add( otherPart );
                }
            }
        }
        return nearByParts;
    }

    void FixedUpdate () {
     
//this is extremily dangerous, things dont stop moving, they get locked in one velocity.
//       if (bulletRigidBody.GetVelocity ().sqrMagnitude > 100 * 100) {
//           var clampedVelocity = Vector3.ClampMagnitude (bulletRigidBody.GetVelocity (), 100);
//           bulletRigidBody.SetVelocity (clampedVelocity);
//       }
        if ( inWater && !inWaterState ) {
            inWaterState = true;
            water.TriggerEnter( bulletRigidBody, waterEntryPoint, waterLineOfAction );
         
        }
        if ( !inWater && inWaterState ) {
            inWaterState = false;
            water.TriggerExit( bulletRigidBody, waterEntryPoint, waterLineOfAction );
            this.water = null;
        }
        inWater = false;
    }

    public static Part FindOwner ( Transform ownee, LayerMask mask ) {
        var owneedLayerMask = ( 1 << ownee.gameObject.layer );//investigate this -z26
        if ( ( owneedLayerMask & mask.value ) == 0 ) {
            return default( Part );
        }
        var part = ownee.GetComponent< Part >();
        if ( part == default( Part ) ) {
            var subpart = ownee.GetComponent< Subpart >();
            if ( subpart != default( Part ) ) {
                return subpart.part;
            }
        }
        else {
            return part;
        }
        if ( ownee.parent == default( Transform ) ) {
            return default( Part );
        }
        return FindOwner( ownee.parent, mask );
    }

    public void Disconnect () {
        
    }

    public void ApplyChannel ( string channel, Color color ) {
        HSBColor hsb = new HSBColor( color );
        hsb.b = Mathf.Clamp( hsb.b, 0, 0.59f );
        color = hsb.ToColor();
        for ( var i = 0; i < partMaterials.Count; ++i ) {
            partMaterials[ i ].SetColor( channel, color );
        }
    }

    public GhostPart CreateGhostPiece ( bool showAxis ) {
        string name = "";
        var mainClone = new GameObject();
        var ghost = mainClone.AddComponent<GhostPart>();
        mainClone.transform.position = nodeView.transform.position;
        mainClone.transform.rotation = nodeView.transform.rotation;
        mainClone.transform.localScale = nodeView.transform.localScale;


        Util.ClonePiece( mainClone, nodeView.gameObject, LayerMask.NameToLayer( "ghostPart" ) );

        var ghostMaterial = GetComponent<Highlight>().ghostMaterial;

            var mainhighLight = mainClone.AddComponent<Highlight>();
            mainhighLight.showAxis = showAxis;
            mainhighLight.Prepare();
            mainhighLight.ghostMaterial = ghostMaterial;
            mainhighLight.SetHighlight( true, Highlight.HighlightType.Green );

//        if ( ghostMaterial != default(Material) )  {
//            var tintedMaterial = ghostMaterial;
//            tintedMaterial.SetColor( "_Color", new Color( 79 / 255f, 225 / 255f, 236 / 255f ) );
//            foreach ( var r in mainClone.GetComponentsInChildren< Renderer >() ) {
//                if ( r.castShadows || r.receiveShadows ) {
//                    r.material = tintedMaterial;
//                }
//            }
//        }

        ghost.partComponent.Add( nodeView.gameObject.GetHashCode(), mainClone );


        return ghost;
    }

}
