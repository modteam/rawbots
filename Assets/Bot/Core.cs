using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;
using Oracle;

public class Core : MonoBehaviour {

    public GravityField gravityField;
    public Renderer[] faces;
	public Transform[] gCubes;
	public GameObject coreCube;
    CoreProxy proxy;
    int size = 0;
    List<Entity> newHex = new List<Entity>();

	void Awake () {
        for(int i = 0 ; i < faces.Length ; ++i){
            faces[i].material.SetFloat("_IllumPower", 0);
        }
	}

    void OnCreateView(CoreProxy proxy){
        this.proxy = proxy;
    }
	
    public void SetSize(float value){
        size = (int)value;
        gravityField.transform.localScale = Vector3.one * 50 * size*7f;
		gravityField.innerRadio = 50f * size;
		coreCube.transform.localScale = new Vector3(50f * size*2f,50f * size*2f,50f * size*2f);
		
		for(int i = 0 ; i < faces.Length ; ++i){
        	gCubes[i].localScale = new Vector3(50f * size*2f,50f * size*2f,50f * size*2f);
        	gCubes[i].localPosition = gCubes[i].up * 50f * size * 2;
        }
		gravityField.SetBounds(50f * size * 2);		
		
    }
 
    IEnumerator WaitThenAdd(){
        while(Gaia.Instance().recentlyAddedEntities.Count > 0){
            yield return null;
        }
        Gaia.Instance().recentlyAddedEntities.AddRange(newHex);
    }

    public void SetFaceState(int faceId, float value){
        var placer = new GameObject("DUMMY " + faceId);
        placer.transform.parent = transform;
        var small_hex_earth = Gaia.Instance().entities[ "small_hex_earth" ];
        var xform = proxy.entity.Heads( p => p == "local_to" ).First();
        var earths = FindObjectsOfType( typeof( HexEarth ) ) as HexEarth[];

        var position = transform.position + gCubes[faceId].up * 50 * size;

        placer.transform.rotation = gCubes[faceId].rotation;
        placer.transform.position = position;

        var localPosition = transform.parent.InverseTransformPoint(position);
        var localRotation = Quaternion.Inverse( transform.parent.rotation) * placer.transform.rotation;

        var inGameHex =  earths.FirstOrDefault( h => Vector3.Distance( h.transform.position, placer.transform.position ) < 5 );

//        gCubes[faceId].localScale = new Vector3(50f * size*2f,50f * size*2f,50f * size*2f);
//        gCubes[faceId].localPosition = gCubes[faceId].up * 50f * size * 2;

        faces[faceId].material.SetFloat("_IllumPower", value * 30);

        //The following creates a platform automatically. I commented it out cause I don't care for it, but youre free
        //to uncomment this back if you want. -z26

        // if(value > 0 && inGameHex == default(HexEarth)){
        //     var id = (uint)System.Guid.NewGuid().GetHashCode();
        //     var earth = new Entity();
        //     earth.id = "earth_block@" + id;
        //     earth.SetProperty( "position", Util.Vector3ToProperty( localPosition ) );
        //     earth.SetProperty( "rotation", Util.Vector3ToProperty( localRotation.eulerAngles ) );
        //     earth.SetProperty( "permissions", 15 );
        //     new Relationship( earth, "as", small_hex_earth );
        //     new Relationship( earth, "local_to", xform );
        //     newHex.Add(earth);
        //     if(newHex.Count == 1) {
        //         StartCoroutine(WaitThenAdd());
        //     }
        // }

//        if(value <= 0 && inGameHex != default(HexEarth)){
//            inGameHex.DestroyView();
//        }

        Destroy( placer );

    }

}
