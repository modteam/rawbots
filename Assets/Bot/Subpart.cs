using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Subpart : MonoBehaviour {

    public Part part;

    public  Vector3 localPosition;
    public Quaternion localRotation;

    public void SetBaseTransform(Vector3 localPosition,Quaternion localRotation){
        this.localPosition = localPosition;
        this.localRotation = localRotation;
    }



}
