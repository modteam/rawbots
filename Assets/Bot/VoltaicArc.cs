using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class VoltaicArc : MonoBehaviour {

    public float flowSpeed = 5;
    float length;
    public EdgeView edgeView;
    public new LineRenderer renderer;
    public new Transform transform;

 
		
	
	void Start () {
        length = Vector3.Distance(edgeView.head.transform.position, edgeView.tail.transform.position);
    }
    void Update () {

        if(length < 0.1f) {
            return;
        }

        renderer.positionCount = 2;
        renderer.SetPosition( 0, edgeView.head.transform.position - edgeView.head.transform.forward * 0.2f );
        renderer.SetPosition( 1, edgeView.tail.transform.position - edgeView.tail.transform.forward * 0.2f );
        var offset = renderer.material.mainTextureOffset;
        offset.x += Time.deltaTime * flowSpeed;
        renderer.material.mainTextureOffset = offset;

    }
	
}
