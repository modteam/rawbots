using UnityEngine;
using System.Collections;
using Operants;

public class Grenade : MonoBehaviour { //Modified by z26.

    public BulletRigidBody btRigidbody;
    public PhysicalObject physicalObject;
    public Renderer chargeFeedback;
    GrenadeProxy proxy;
    float maxIllum = 20;
    float illum = 0;
    float tStep = 0.5f;
    float nextStep;

    void Start(){
        nextStep = Time.time;
    }

    void OnCreateView ( GrenadeProxy proxy ) {
        this.proxy = proxy;
    }

    public void Explode () {
            var explosion = (GameObject.Instantiate( Resources.Load( "FX/GammaExplosion" ), transform.position, Quaternion.identity ) as GameObject).GetComponent<GammaExplosion>();
            explosion.transform.up = physicalObject.Up();
            explosion.btRB = btRigidbody.btRB;
    }

    //This indirectly triggers an explosion, cause DrainEnergy() is overriden in GrenadeProxy.cs
    public void SelfDestruct() {
        proxy.DrainEnergy(proxy.MaxEnergy());
    }


    void Update() {//visual effect.
        if ( Time.time > nextStep) {
            nextStep = Time.time + tStep;
            chargeFeedback.material.SetFloat( "_IllumPower", illum == 0 ? 0 : maxIllum);
            ++illum;
            illum = illum > 1 ? 0 : illum;
        }
    }
}
