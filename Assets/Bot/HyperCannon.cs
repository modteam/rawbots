using UnityEngine;
using System.Collections;
using System.Linq;
using Operants;

public class HyperCannon : MonoBehaviour {

    public HyperCannonProxy proxy;
    public Transform cannonExit;
    public PhysicalObject physicalObject;
    public float force = 200;
    public System.Action<float> produceShootForce = f => {};
    
    void OnCreateView ( HyperCannonProxy proxy ) {
        this.proxy = proxy;
    }

    public void ShootParts ( PartProxy partProxy ) {
        Gaia.Instance().ScheduleAction( () => {

            var entity = partProxy.entity.Heads( p => p == "local_to" ).FirstOrDefault();

            var inHyperSpace = entity.Relationships().Any( p => p.Predicate == "contained_in" );

            if ( !inHyperSpace ) {
                return;
            }
            Hyperspace.RecoverPartsView( entity, cannonExit );

            var recoil = -cannonExit.up * force;
            physicalObject.bulletRigidbody.AddForce( recoil, Vector3.zero);
            var effect = ( GameObject )Instantiate( Resources.Load( "FX/Spit", typeof( GameObject ) ),
                    transform.position, transform.rotation );
            effect.transform.parent = gameObject.transform;
            var parts = entity.Tails( p => p == "local_to" ).Select( e => e.proxy ).Cast<PartProxy>();
            foreach ( var part in parts ) {
                if ( part.nodeView == default(NodeView) ) {
                    part.CreateView();
                    continue;
                }
                if ( !part.nodeView.btRigidBody.IsKinematic()) {
                    StartCoroutine( Shoot( part.nodeView.btRigidBody ) );
                }
            }

        } );
    }

    IEnumerator Shoot ( BulletRigidBody projectile ) {
        projectile.SetVelocity(physicalObject.bulletRigidbody.GetVelocity());
        var t = Time.time + 1;
        var sForce = cannonExit.up * force;
        produceShootForce(sForce.magnitude);
        while ( Time.time <= t ) {
            if ( projectile != default(BulletRigidBody) ) {
                projectile.AddForce( sForce, Vector3.zero );
            }
            yield return null;
        }
    }

}
