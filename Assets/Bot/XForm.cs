using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class XForm : MonoBehaviour {



    public void MoveXForm ( Vector3 newPosition, Quaternion newRotation ) {

        var childs = new List<Transform>();
        foreach ( Transform child in transform ) {
            childs.Add( child );
        }

        foreach ( Transform child in childs ) {
            child.parent = null;
        }

        transform.position = newPosition;
        transform.rotation = newRotation;

        foreach ( Transform child in childs ) {
            child.parent = transform;
        }

    }
}
