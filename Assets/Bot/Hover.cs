using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Hover : MonoBehaviour {

    public float lift = 0;
    public float fxWeight = 0;
    public Tweaker blades;
    public Tweaker fire;
    public ParticleSystem rings;
    Vector3 fireScale;
    float internalWeight = 1;
    public PhysicalObject physicalObject;
    public Transform fan;
    float lowPassFactor = 2;
    public float safeTimer = 0;
    private BulletRigidBody body;

    void Awake () {
        fireScale = fire.transform.localScale;
        body = gameObject.GetComponent<BulletRigidBody>();
        safeTimer = Time.time;
    }
	
	

    void FixedUpdate () {

        if ( ( Time.time - safeTimer ) > 0.2f ) {
            var force = Mathf.Max( 0, Vector3.Dot( physicalObject.Up() * lift, transform.up ) ) * physicalObject.Up();
            force = force* internalWeight*0.25f;
            if(force.magnitude >0.1f )body.AddForce(force,Vector3.zero );
            fxWeight = Mathf.Clamp( force.magnitude / 500, 0, 1 );
        }


        var gravityUp = physicalObject.Up();
        var current = fan.up;
        var dot = Vector3.Dot( gravityUp, transform.up );
        if ( dot > 0.9f ) {
            fan.up = Vector3.Slerp( current, gravityUp, Time.deltaTime * 10 );
        }

        GetComponent<AudioSource>().volume = Mathf.Lerp( GetComponent<AudioSource>().volume, fxWeight * 100, Time.deltaTime * lowPassFactor ) * internalWeight;
        if ( GetComponent<AudioSource>().volume > 0.01f ) {
            if ( !GetComponent<AudioSource>().isPlaying ) {
                GetComponent<AudioSource>().Play();
                GetComponent<AudioSource>().time = Time.time % GetComponent<AudioSource>().clip.length;
            }
        }
        else if ( GetComponent<AudioSource>().isPlaying ) {
            GetComponent<AudioSource>().Stop();
        }
        GetComponent<AudioSource>().pitch = Mathf.Lerp( GetComponent<AudioSource>().pitch, 0.9f + fxWeight * 1, Time.deltaTime * lowPassFactor );
        blades.angularWave.frequency = 2 * fxWeight * internalWeight;
        fire.angularWave.frequency = 2 * fxWeight * internalWeight;
        fire.scaleWave.amplitude = 0.1f * fxWeight * internalWeight;
		var module = rings.emission;
        module.rateOverTime = 3 * fxWeight * internalWeight;
        fire.initialScale = fireScale * Mathf.Max( fxWeight * internalWeight, 0.01f );
    }

    void OnWaterEnter () {
        internalWeight = 0;
    }

    void OnWaterExit () {
        internalWeight = 1;
    }

}
