using UnityEngine;
using System.Collections;

public class PlanetWater : MonoBehaviour
{

	void Start ()
	{
		GetComponent<Water> ().getSurfacePoint = GetSurfacePoint;
	}

	
	public Vector3 GetSurfacePoint (Vector3 partPosition)
	{
		var radio = (transform.localScale.x) / 2;
		var up = (partPosition - transform.position).normalized;
		return transform.position + up * (radio);

	}
}
