using UnityEngine;
using System.Collections;
using Operants;

public class FluxCapacitor : MonoBehaviour {//Modified by z26

    public Renderer coil;
    public FluxCapacitorProxy proxy;

    float maxIllum = 25;
    float illum = 0;
    float tStep = 0.15f;
    float nextStep;

    int capacitance;
    int maxCapacitance = 20;
    float chargeAtMaxCapacitance = 1000;
    float chargeRate = 100;
    float lastRecharge;

    public float charge { get; private set; }
    public float maxCharge { get; private set; }


    void OnCreateView ( FluxCapacitorProxy proxy ) {
        this.proxy = proxy;
    }

    void OnBuild () {
        proxy = gameObject.GetComponent< NodeView >().proxy as FluxCapacitorProxy;
        lastRecharge = Time.time;
        SetLevel(maxCapacitance);
        charge = maxCharge;
    }


        //Generic stuff that all parts that stores charge will need. I'll make this class generic if we add more.
        public float IncreaseCharge(float amount) {
            float spaceFree = maxCharge - charge;
            float addedCharge = Mathf.Clamp(amount, 0, spaceFree);

            charge += addedCharge;
            return addedCharge;
        }

        public float DecreaseCharge(float amount) {
            float available = charge;
            float removedCharge = Mathf.Clamp(amount, 0, available);

            charge -= removedCharge;
            return removedCharge;
        }

        public void TransferChargeTo(FluxCapacitor other, float amount) {
            FluxCapacitor giver = this;
            FluxCapacitor recipient = other;
            if (amount < 0) {
                giver = other;
                recipient = this;
            }

            float availableToGive = giver.DecreaseCharge(amount);
            float actuallyGiven = recipient.IncreaseCharge(availableToGive);

            float notGiven = availableToGive - actuallyGiven;
            giver.IncreaseCharge(notGiven);
        }





    //Stuff specific to the flux_capacitor.
    public void SetLevel ( int input ) {
        capacitance = Mathf.Clamp( input, 1, maxCapacitance );
        float ratio = (float)capacitance / (float)maxCapacitance;
        
        maxCharge = chargeAtMaxCapacitance * ratio;
        charge = Mathf.Clamp(charge, 0, maxCharge);
    }

    void Update (){
        float timeSince = Time.time - lastRecharge;
        IncreaseCharge(timeSince * chargeRate);
        lastRecharge = Time.time;

        //mostly copied visual effect from grenade.
        if(Mathf.Approximately(charge,maxCharge)) {
            coil.material.SetFloat( "_IllumPower",maxIllum);
        }
        else if ( Time.time > nextStep) {
            nextStep = Time.time + tStep;
            coil.material.SetFloat( "_IllumPower", illum == 0 ? 0 : maxIllum);
            ++illum;
            illum = illum > 1 ? 0 : illum;
        }

        
    }
}
