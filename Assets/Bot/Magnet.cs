using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

using Operants;
public class Magnet : MonoBehaviour
{

    public Transform reachFeedback;
    public BulletRigidBody btRigidbody;
    public LayerMask mask;
    public PID movementPID;
    float maxDistance = 10;
    float radius = 5;
    //RaycastHit hit;
    //Vector3 localHitPoint;
    BulletRigidBody selected;
    BulletRigidBody selectedLast;
    private BulletRaycastHitDetails hitDetails;
    private Ray ray;
    bool armed = false;
    public float fire = 0.0f;
    private float reachAngle = 0.0f;

    public void Start()
    {
        //Vector3 scale = reachFeedback.localScale;
        //scale.z = 0;
        //reachFeedback.localScale = scale;
    }

    //public void Fire(float fire)
    //{
    //    //fire = Mathf.Clamp01(fire);
    //    if (fire >= 0.5f && selected != default(BulletRigidBody))
    //    {
    //        movementPID.Reset(selected.transform.position);
    //        armed = true;
    //    }
    //    else if (fire < 0.5f)
    //    {
    //        armed = false;
    //        selected = default(BulletRigidBody);
    //    }
    //}

    void Update()
    {/*
        ray = new Ray(transform.position, transform.forward);
        var hit = Bullet.Raycast(ray, out hitDetails, maxDistance, BulletLayers.Part, BulletLayers.Part);
        if(hit)
        {
            selected = hitDetails.bulletRigidbody;
            var distance = selected.transform.position - transform.position;
        }
        else
        {
            armed = false;
            selected = default(BulletRigidBody);
        }*/
        if (fire >= 0.5f)
        {
            reachFeedback.GetComponent<Renderer>().enabled = true;
            reachFeedback.Rotate(reachFeedback.transform.up, 1.0f);
            var offset = Vector3.zero;// new Vector3(0.0f, 0.0f, 4.0f);
            var centre = transform.position + 4.0f * transform.up;
            var reaction = Vector3.zero;
            var baseVel = btRigidbody.GetVelocity();
            var bodies = Bullet.OverlapSphere(btRigidbody.btRB, BulletLayers.Part, BulletLayers.Part, Vector3Extensions.ToBulletVector3(centre), 2.0f);  //Get list of affected bodies.
            float scale = 1.0f;
            if (bodies.Count >= 5)
            {
                scale = 1.0f / ((float)bodies.Count - 3.0f);
            }
            var maxP = 200 * scale;
            var maxD = 50 * scale;
            bodies.ForEach(r =>     //Iterate through list
            {
                var rPos = centre - r.transform.position;   //Part position        
                var rVel = r.GetVelocity() - baseVel;       //delta V
                var forceMagnitudeP = rPos.magnitude * 30.0f * scale;            //Calculate proportional
                var forceMagnitudeD = rVel.magnitude * -3.0f * scale;            //Calculate differential
                forceMagnitudeP = Mathf.Clamp(forceMagnitudeP, -maxP, maxP);
                forceMagnitudeD = Mathf.Clamp(forceMagnitudeD, -maxD, maxD);
                var force = (rPos.normalized * forceMagnitudeP) + (rVel.normalized * forceMagnitudeD);           //Give force a direction
                r.AddForce(force, Vector3.zero);                       //Apply force to object
                reaction -= force;                                      //Calculate reaction force
            });
            btRigidbody.AddForce(reaction, offset);           //Apply reaction force to magnet
        }
        else
        {
            reachFeedback.GetComponent<Renderer>().enabled = false;
        }
    }

    void FixedUpdate()
    {
        /*if ((selected != default(BulletRigidBody)) && (fire >= 0.5f))
        {
            if(selected != selectedLast)
            {
                movementPID.Reset(selected.transform.position);
            }
            selectedLast = selected;
            movementPID.setpoint = transform.position + 4 * (transform.forward);
            var force = movementPID.Compute(selected.transform.position, Time.deltaTime);
            selected.AddForce(force, Vector3.zero);
            btRigidbody.AddForce(-force, Vector3.zero);
        }*/


    }
}

//public class Magnet : MonoBehaviour
//{

//    //public BulletRigidBody btRigidbody;
//    //public PhysicalObject physicalObject;
//    //public Renderer chargeFeedback;
//    MagnetProxy proxy;
//    //float requiredEnergy = 700;
//    //float maxIllum = 20;
//    //float illum = 0;
//    //bool charged = false;
//    //float tStep = 0.25f;
//    //float nextStep;
//    //float energy;
//    public float field = 0.0f;
//    float rangeScale = 1.0f;
//    float forceScale = 1.0f;
//    public BulletRigidBody btRigidbody;


//    //void Start()
//    //{
//    //    nextStep = Time.time;
//    //}

//    void OnCreateView(MagnetProxy proxy)
//    {
//        this.proxy = proxy;
//    }

//    //public void TryExplode(float value)
//    //{
//    //    if (value > 0.99f && charged)
//    //    {
//    //        proxy.RequestEnergy(energy);
//    //        var explosion = (GameObject.Instantiate(Resources.Load("FX/MagPull"), transform.position, Quaternion.identity) as GameObject).GetComponent<MagPull>();
//    //        //explosion.transform.up = physicalObject.Up();
//    //        //explosion.btRB = btRigidbody.btRB;

//    //    }
//    //}


//    void Update()
//    {
//        Vector3 reaction = Vector3.zero;    //Reset reaction sum
//        Vector3 direction;
//        var bodies = Bullet.OverlapSphere(btRigidbody.btRB, BulletLayers.Part, BulletLayers.Part, transform.position.ToBulletVector3(), (Mathf.Abs(field * rangeScale)));  //Get list of affected bodies. Range scales linearly
//        bodies.ForEach(r =>     //Iterate through list
//        {

//            var rPos = transform.position - r.transform.position;   //Part position
//            direction = Vector3.Cross(rPos, transform.up);          //Orient coordinate systems
//            if (direction.z > 0)                                    //Limit to one hemishere
//            {
//                var forceMagnitude = (Mathf.Max(0, (Mathf.Abs(field * rangeScale)) - rPos.magnitude)) * forceScale * field;   //Force decreases linearly, clamped to prevent polarity flip at edge of field
//                var force = rPos.normalized * forceMagnitude;           //Give force a direction
//                r.AddForce(force, Vector3.zero);                       //Apply force to object
//                reaction -= force;                                      //Calculate reaction force
//            }
//        });
//        btRigidbody.AddForce(reaction, Vector3.zero);           //Apply reaction force to magnet
//        }

//}