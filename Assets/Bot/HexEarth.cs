using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class HexEarth : MonoBehaviour {


    public List< Light > lights;
    public HexEarthProxy proxy;
    public BulletLayers rayTestMask;
    Collider pickedCollider;
    LibNoise.Unity.Generator.Perlin perlin;
    System.Random propGen;
    public bool noProps = false;
    public int index = 0;
    public float angle = 0f;
    BulletRaycastHitDetails details;
    public bool done =false;
    public StructureHooks hooks;

    private class EarthOption {
        public string earthType = "hexEarth";
        public float angle = 0;

    }
    List<EarthOption> options = new List<EarthOption>();

    void Awake () {
        var option1 = new EarthOption();
        option1.earthType = "hex_earth";
        option1.angle = 0;
        var option2 = new EarthOption();
        option2.earthType = "hex_earth_ramp";
        option2.angle = 0;
        var option3 = new EarthOption();
        option3.earthType = "hex_earth_ramp";
        option3.angle = 60;
        var option4 = new EarthOption();
        option4.earthType = "hex_earth_ramp";
        option4.angle = 120;
        var option5 = new EarthOption();
        option5.earthType = "hex_earth_ramp";
        option5.angle = 180;
        var option6 = new EarthOption();
        option6.earthType = "hex_earth_ramp";
        option6.angle = 240;
        var option7 = new EarthOption();
        option7.earthType = "hex_earth_ramp";
        option7.angle = 300;

        options.Add( option1 );
        options.Add( option2 );
        options.Add( option3 );
        options.Add( option4 );
        options.Add( option5 );
        options.Add( option6 );
        options.Add( option7 );

        //rayTestMask = 1 << LayerMask.NameToLayer( "structure" );
    }

    void OnCreateView ( HexEarthProxy proxy ) {
        this.proxy = proxy;
		//HexBatcher.instance.AddHexForBatch(gameObject);
    }

    public void DestroyView(){
        var locals = proxy.entity.Relationships().Where( r => r.Predicate == "local_to" ).ToList();
        proxy.entity.deprecated = true;
        foreach ( var local in locals ) {
            local.Break();
        }
        Destroy( gameObject );
    }

    public void CheckForNeighbors () {
        //Util.LogStack();
        UnityEngine.Profiling.Profiler.BeginSample( "HexEarth.CheckForNeighrbors" );
        Ray ray;

        for ( var i = 0; i < 360; i += 60 ) {
            var available = true;
            var front = Quaternion.AngleAxis( i, transform.up ) * transform.forward;
            ray = new Ray( transform.position + front * 38, front );

//            Debug.DrawRay( ray.origin, ray.direction, Color.red );
//            Debug.DrawLine( ray.origin, ray.origin + ray.direction * 6f );
//            Debug.DrawLine( ray.origin, ray.origin + transform.up * 2f );


            if ( Bullet.Raycast( ray,out details, 6,rayTestMask, rayTestMask ) ) {
                available = false;
            }
            else {
            }

            proxy.availableFaces[ Mathf.FloorToInt( i / 60 ) ] = available;
        }

        ray = new Ray( transform.position - transform.up * 18, -transform.up );

        if ( Bullet.Raycast( ray, out details, 6,rayTestMask, rayTestMask ) ) {
            proxy.availableFaces[ ( int )HexEarthProxy.Faces.BottomFace ] = false;
        }
        else {
            proxy.availableFaces[ ( int )HexEarthProxy.Faces.BottomFace ] = true;
        }

        ray = new Ray( transform.position + transform.up * 18, transform.up );

        if ( Bullet.Raycast( ray, out details, 6,rayTestMask, rayTestMask ) ) {
            proxy.availableFaces[ ( int )HexEarthProxy.Faces.TopFace ] = false;
        }
        else {
            proxy.availableFaces[ ( int )HexEarthProxy.Faces.TopFace ] = true;
        }
        UnityEngine.Profiling.Profiler.EndSample();
    }

    void Start () {
		//HexBatcher.instance.AddHexForBatch(gameObject);
        if ( !noProps ) {
            Sprout();
        }
//        Builder.Filler.Add(this);
    }

    float NextRandom ( System.Random rnd ) {
        return ( float )rnd.NextDouble();
    }

    float NextRandomRange ( System.Random rnd, float start, float end ) {
        return start + NextRandom( rnd ) * ( end - start );
    }

    int NextRandomRangeInt ( System.Random rnd, int start, int end ) {
        return rnd.Next( start, end );
    }

    public void Sprout () {
        var island = GameObject.Find( "IslandProps" ).GetComponent< Island >();
        if ( island == default(Island) ) {
            return;
        }
        var seed = Random.Range( 0, 1000 );
        proxy.entity.ForProperty< float >( "seed", value => seed = ( int )value );
        proxy.entity.SetProperty( "seed", seed );
        perlin = new LibNoise.Unity.Generator.Perlin();
        perlin.Seed = seed;
        propGen = new System.Random( seed );

        CheckForNeighbors();

        foreach ( var prop in GetComponentsInChildren< Prop >( true ) ) {
            Destroy( prop.gameObject );
        }

        float propScale = 1;
        if ( proxy.availableFaces[ ( int )HexEarthProxy.Faces.BottomFace ] ) {

            if ( NextRandom( propGen ) > 0.8f ) {
            }
            else if ( NextRandom( propGen ) > 0.8f ) {
                // rocks
                propScale = NextRandomRange( propGen, 2f, 15f );
                for ( var x = -1f; x <= 1f; x += propScale / 15f ) {
                    for ( var z = -1f; z <= 1f; z += propScale / 15f ) {
                        var y = ( float )perlin.GetValue( x, 0, z );
                        var s = Mathf.Abs( ( float )perlin.GetValue( x, z, 0 ) );
                        if ( y > 0.5f ) {
                            var rockAsset = island.GetRocksAsset( propGen );
                            if ( rockAsset != null ) {
                                var propObj = Instantiate( rockAsset ) as GameObject;
                                propObj.transform.parent = transform;
                                propObj.transform.localRotation = Quaternion.Euler( 0, NextRandomRangeInt( propGen, 0, 180 ), 0 );
                                propObj.transform.localPosition = new Vector3( x * 20, 20.5f, z * 20 );
                                propObj.transform.localScale = new Vector3( propScale * y, propScale * y + s * 5, propScale * y );
                            }
                        }
                    }
                }
            }
            else if ( NextRandom( propGen ) > 0.8f ) {
                propScale = NextRandomRange( propGen, 1f, 1.5f );
                var craterAsset = island.GetCraterAsset( propGen );
                if ( craterAsset != null ) {
                    var propObj = Instantiate( craterAsset ) as GameObject;
                    propObj.transform.parent = transform;
                    propObj.transform.localRotation = Quaternion.Euler( 0, NextRandomRangeInt( propGen, 0, 180 ), 0 );
                    propObj.transform.localPosition = new Vector3( 0, 20.5f, 0 );
                    propObj.transform.localScale = Vector3.one * propScale;
                }
            }
            else if ( NextRandom( propGen ) > 0.9f ) {
                propScale = NextRandomRange( propGen, 10f, 15f );
                var crystalAsset = island.GetCrystalAsset( propGen );
                if ( crystalAsset != null ) {
                    var propObj = Instantiate( crystalAsset ) as GameObject;
                    propObj.transform.parent = transform;
                    propObj.transform.localRotation = Quaternion.Euler( 0, NextRandomRangeInt( propGen, 0, 180 ), 0 );
                    propObj.transform.localPosition = new Vector3( 0, 20.5f, 0 );
                    propObj.transform.localScale = new Vector3( propScale, propScale + NextRandomRangeInt( propGen, 1, 3 ), propScale );
                }
            }
        }

        if ( proxy.availableFaces[ ( int )HexEarthProxy.Faces.TopFace ] ) {
            if ( NextRandomRange( propGen, 0, 10 ) > 5 ) {
                // stalagmites
                propScale = 1;
                for ( var x = -1f; x <= 1f; x += 0.2f ) {
                    for ( var z = -1f; z <= 1f; z += 0.3f ) {
                        var y = ( float )perlin.GetValue( x, 0, z );
                        if ( y > 0.5f ) {
                            var stalagmiteAsset = island.GetStalagmiteAsset( propGen );
                            if ( stalagmiteAsset != null ) {
                                var propObj = Instantiate( stalagmiteAsset ) as GameObject;
                                propObj.transform.parent = transform;
                                propObj.transform.localRotation = Quaternion.Euler( 180, NextRandomRangeInt( propGen, 0, 180 ), 0 );
                                propObj.transform.localPosition = new Vector3( x * 30, -20.5f, z * 30 );
                                propObj.transform.localScale = new Vector3( propScale * y, propScale * y, propScale * y );
                            }
                        }
                    }
                }
            }
        }

    }

    void OnDrawGizmosSelected () {

        Gizmos.color = Color.white;
        Gizmos.matrix = Matrix4x4.identity;

        Ray ray;

        for ( var i = 0; i < 360; i += 60 ) {
            var front = Quaternion.AngleAxis( i, transform.up ) * transform.forward;
            ray = new Ray( transform.position + front * 38, front );
            if ( Bullet.Raycast( ray, out details, 6,rayTestMask, rayTestMask ) ) {
                Gizmos.color = Color.red;
            }
            else {
                Gizmos.color = Color.blue;
            }
            Gizmos.DrawRay( ray.origin, ray.direction * 6 );
        }

        ray = new Ray( transform.position + transform.up * 18, transform.up );
        if ( Bullet.Raycast( ray, out details, 6,rayTestMask, rayTestMask ) ) {
            Gizmos.color = Color.red;
        }
        else {
            Gizmos.color = Color.blue;
        }
        Gizmos.DrawRay( ray.origin, ray.direction * 6 );

        ray = new Ray( transform.position - transform.up * 18, -transform.up );
        if ( Bullet.Raycast( ray, out details, 6,rayTestMask, rayTestMask ) ) {
            Gizmos.color = Color.red;
        }
        else {
            Gizmos.color = Color.blue;
        }
        Gizmos.DrawRay( ray.origin, ray.direction * 6 );
    }

}
