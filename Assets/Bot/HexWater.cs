using UnityEngine;
using System.Collections;

public class HexWater : MonoBehaviour {
	public float height = 21f;
	// Use this for initialization
	void Start () {
		GetComponent<Water>().getSurfacePoint = GetSurfacePoint;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public Vector3 GetSurfacePoint(Vector3 partPosition ){
		var surfacePoint = transform.position+transform.up*height;

		Plane surface = new Plane(transform.up,surfacePoint);
		float distance = 0f;
		Ray ray = new Ray(partPosition,transform.up);
		
		if(surface.Raycast(ray,out distance)){
			return ray.GetPoint(distance);
		}
		
		return partPosition;
	}
	
}
