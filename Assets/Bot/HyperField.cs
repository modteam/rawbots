using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;
using Oracle;

public class HyperField : MonoBehaviour {

    HyperFieldProxy proxy;
    public LayerMask triggerMask;
    public bool activeField = false;
    public List< PartProxy > partsToTransport = new List< PartProxy >();
    public Transform hotspot;
    float size = 0.5f;
    float glowTime = 0;
    public Part part;

    void OnCreateView ( HyperFieldProxy proxy ) {
        this.proxy = proxy;
    }

    void OnTriggerEnter ( Collider collider ) {
        var triggeredPart = Part.FindOwner( collider.transform, triggerMask );
        if ( triggeredPart == default(Part) ) {
            return;
        }
//        if ( !activeField || proxy.fieldType == HyperFieldType.White ) {
//            if ( !triggeredPart.proxy.IsPasscodeSet() ) {
//                triggeredPart.proxy.SetPasscode( part.proxy.Passcode );
//            }
//            return;
//        }

        glowTime = 0.1f;
        hotspot.GetComponent<Renderer>().enabled = true;
        var color = hotspot.GetComponent<Renderer>().material.GetColor( "_TintColor" );
        color.a = 1f;
        hotspot.GetComponent<Renderer>().material.SetColor( "_TintColor", color );
        partsToTransport.Add( triggeredPart.proxy );
        
    }

    void OnTriggerExit ( Collider collider ) {
        if ( !activeField || proxy.fieldType == HyperFieldType.Black ) {
            return;
        }
        var triggeredPart = Part.FindOwner( collider.transform, triggerMask );
        if ( triggeredPart == default(Part) ) {
            return;
        }
        
        if ( triggeredPart.proxy.Passcode == part.proxy.Passcode ) {
            glowTime = 0.1f;
            hotspot.GetComponent<Renderer>().enabled = true;
            var color = hotspot.GetComponent<Renderer>().material.GetColor( "_TintColor" );
            color.a = 1f;
            hotspot.GetComponent<Renderer>().material.SetColor( "_TintColor", color );
            partsToTransport.Add( triggeredPart.proxy );
        }
    }

    void OnGridTileEnter () {
        glowTime = 0;
        hotspot.GetComponent<Renderer>().enabled = true;
        var color = hotspot.GetComponent<Renderer>().material.GetColor( "_TintColor" );
        color.a = 0.1f;
        hotspot.GetComponent<Renderer>().material.SetColor( "_TintColor", color );
    }

    void OnGridTileExit () {
        hotspot.GetComponent<Renderer>().enabled = false;
        var color = hotspot.GetComponent<Renderer>().material.GetColor( "_TintColor" );
        color.a = 0;
        hotspot.GetComponent<Renderer>().material.SetColor( "_TintColor", color );
    }

    public void SetSize ( float size ) {
        size = Mathf.Clamp( size, 1, 20000 );
        if ( !Mathf.Approximately( this.size, size ) ) {
            this.size = size;
            hotspot.localScale = Vector3.one * this.size;
        }
    }

    void Update () {
        if ( glowTime > 0 ) {
            glowTime -= Time.deltaTime;
            var color = hotspot.GetComponent<Renderer>().material.GetColor( "_TintColor" );
            color.a -= Time.deltaTime * 10;
            hotspot.GetComponent<Renderer>().material.SetColor( "_TintColor", color );
            if ( glowTime <= 0 ) {
                hotspot.GetComponent<Renderer>().enabled = false;
            }
        }
    }

}
