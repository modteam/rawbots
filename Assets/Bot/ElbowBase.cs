using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class ElbowBase : MonoBehaviour {

    
    public BulletGenericMotor motor;
    public BulletGeneric6DofConstraint constraint;
    public GameObject constraintHolder;
    public GameObject rotor;
    public System.Action<float> producePartAngle = (notAForce) => {};
    public float safeTimer = 0f;

    public void Start(){
        safeTimer = Time.time;
    }

    public void SetForce ( float force ) {
        //this.force = force;
    }

    public void SetElbowAngle ( float value ) {
            motor.SetTarget( value * Mathf.PI / 180f );

    }

    public void SetElbowVelocity ( float value ) {
            motor.SetVelocity( value );
    }

    void Update () {
        var rotorCurrentRotation = Quaternion.Inverse( transform.rotation ) * rotor.transform.rotation;
        var reportedAngle = rotorCurrentRotation.eulerAngles.y;
        var testVector = new Vector3( Mathf.Cos( reportedAngle * Mathf.PI / 180f ), Mathf.Sin( reportedAngle * Mathf.PI / 180f ), 0 );
        var referenceVector = new Vector3( Mathf.Cos( 0 ), Mathf.Sin( 0 ), 0 );
        var sign = Mathf.Sign( Vector3.Cross( referenceVector, testVector ).z );
        producePartAngle( Quaternion.Angle( transform.rotation, rotor.transform.rotation ) * sign );
    }

}
