using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;

public class HyperCubeHotSpot : MonoBehaviour {

    public BulletRigidBody btRigidBody;
    public HyperCube hyperCube;
    float spotYOffset = 5.5f;
    
    void Start(){
        btRigidBody.SetPosition(transform.position);
        btRigidBody.SetRotation(hyperCube.transform.rotation);
        btRigidBody.AddOnTriggerDelegate( OnTrigger );
        //transform.parent = hyperCube.transform.parent;

    }

    void Update(){
        transform.position =hyperCube.transform.position + hyperCube.transform.up * spotYOffset;
        btRigidBody.SetPosition(transform.position);
        btRigidBody.SetRotation(hyperCube.transform.rotation);
    }
    

     //I rewrote this method, use git history to see the original -z26    
    public void OnTrigger( Vector3 relativeVelocity,BulletRigidBody other,Vector3 contactPoint,Vector3 lineOfAction ) {
   
        //don't hypercube yourself. This can still happen if the absorbed part is connected to the hypercube, fix this!
        if(other == hyperCube.btRigidBody){ 
            return;
        }
        var part = other.GetComponent<Part>();
        

        
        if(part != default(Part)){
            
            //selectedLastFrame ensures the part that has been in the hotspot the longest will always stay the one selected until it leaves the hotspot. Not sure this matters, it's just my personal preference.
            if (hyperCube.selectedPart == default(PartProxy) || hyperCube.selectedLastFrame == part.proxy) {
                hyperCube.selectedPart = part.proxy;
            }
        }
    }
}

