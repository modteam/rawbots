using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;
using Oracle;

public class Hyperspace {

    public static bool DestroyPartsView ( Part root, PartProxy container ) {
        var cancel = false;

        var entities = new HashSet<Entity>();

        Entities( root.proxy.entity, entities, new string[]{"input_for","output_for","extends","slot_for","linked_to","consumes"} );
        var connectedParts = entities.Where( e => e.nodeView != default(NodeView) ).Where( e => e.proxy is PartProxy ).Select( e => e.proxy ).Cast<PartProxy>();


        foreach ( var part in connectedParts ) {
            if ( part.nodeView.GetComponent<Part>().isProtected ) {
                cancel = true;
            }
        }
        var partConnectedPhysicaly = new HashSet<Entity>();
        Hyperspace.Entities( root.proxy.entity, partConnectedPhysicaly, new string[]{"extends","slot_for","linked_to"} );

        var connectedPhysicalyParts = partConnectedPhysicaly.Where( e => e.nodeView != default(NodeView) ).Where( e => e.proxy is PartProxy ).Select( e => e.proxy ).Cast<PartProxy>();

        if ( connectedPhysicalyParts.Count() != connectedParts.Count() ) {
            cancel = true;
        }
        if ( cancel ) {
            return false;
        }

        var skipList = new List<PartProxy>();
        connectedPhysicalyParts.ToList().ForEach( p => {
            if ( !skipList.Contains( p ) ) {
                p.RecalculateClusters();
                skipList.AddRange( p.Parts().ToList() );
            }
        } );




        var xForms = new List<Entity>();
        partConnectedPhysicaly.ToList().ForEach( entity => {
            var xFormEntity = entity.Heads( p => p == "local_to" ).FirstOrDefault();
            if ( xFormEntity != default(Entity) ) {
                if ( !xForms.Contains( xFormEntity ) ) {
                    xForms.Add( xFormEntity );
                }
            }
        } );


        xForms.ForEach( xf => {
            new Relationship( xf, "contained_in", container.entity );
        } );
        var tempList = new List<PartProxy>();

        foreach ( var part in connectedParts ) {
            part.entity.SetProperty( "hyper_space", 1f );
            tempList.Add( part );
            part.RemoveJointsAndExtends();
            part.OnViewDestroyed();
        }

         foreach ( var part in tempList ) {//without this the part will be forgotten (wont survive a save and reload), but will still exist -z26
             UnityEngine.Object.Destroy( part.nodeView.gameObject );
             part.nodeView = default(NodeView);
         }
        return true;
    }

    public static void RecoverPartsView ( Entity master, Transform spawnPoint ) {

        //TODO: Integrate with Gaia action list
        var entities = new HashSet<Entity>();

        var parts = master.Tails( p => p == "local_to" ).Select( e => e.proxy ).Cast<PartProxy>();

        Entities( parts.FirstOrDefault().entity, entities, new string[]{"input_for","output_for","extends","slot_for","linked_to","consumes"} );

        var connectedParts = entities.Where( e => e.proxy is PartProxy ).Select( e => e.proxy ).Cast<PartProxy>();


        List<XForm> xforms = new List<XForm>();

        foreach ( var partProxy in connectedParts ) {
            partProxy.entity.SetProperty( "hyper_space", 0f );
            var localToEntity = partProxy.entity.Relationships().Where( r => r.Predicate == "local_to" ).Select( r => r.Head ).FirstOrDefault();

            var xform = Gaia.Instance().GetXForm( localToEntity );

            //xform.transform.position = spawnPoint.transform.position;
            if ( !xforms.Contains( xform ) ) {
                var relationShip = localToEntity.Relationships().Where(r => r.Predicate == "contained_in").FirstOrDefault();
                relationShip.Break();
                xforms.Add( xform );
            }

            partProxy.xform = xform;
        }

        Vector3 centerPos = Vector3.zero;
        for(int i = 0; i< xforms.Count; i++){
            centerPos+= xforms[i].transform.position;
        }
        centerPos = centerPos/xforms.Count;

        var centerGameObject = new GameObject();
        centerGameObject.transform.position = centerPos;

        var lastParent = xforms[0].transform.parent;

        for(int i = 0; i< xforms.Count; i++){
            xforms[i].transform.parent = centerGameObject.transform;
        }
        centerGameObject.transform.position = spawnPoint.position;

        for(int i = 0; i< xforms.Count; i++){
            xforms[i].transform.parent = lastParent;
        }

        var visited = new HashSet< Entity >();

        UnityEngine.Profiling.Profiler.BeginSample( "Hyperspace: Entities" );
        UnityEngine.Profiling.Profiler.EndSample();
        var proxies = connectedParts.Where( p => p != default( NodeProxy ) ).ToList();
        UnityEngine.Profiling.Profiler.BeginSample( "Hyperspace: OnPrebuild" );
        proxies.ForEach( p => p.OnPreBuild() );
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.BeginSample( "Hyperspace: OnBuild" );
        proxies.ForEach( p => p.OnBuild() );
        UnityEngine.Profiling.Profiler.EndSample();

        UnityEngine.Profiling.Profiler.BeginSample( "Hyperspace: OnPostbuild" );
        proxies.ForEach( p => p.OnPostBuild() );
        UnityEngine.Profiling.Profiler.EndSample();
    }

    public static void Entities ( Entity entity, HashSet< Entity > visited, string[] predicates ) {
        AccumulateEntities( entity, visited, predicates );
    }

    static void AccumulateEntities ( Entity entity, HashSet< Entity > visited, string[] predicates ) {
        if ( visited.Contains( entity ) ) {
            return;
        }
        visited.Add( entity );
        foreach ( var r in entity.Relationships().Where( r =>{
            foreach(var predicate in predicates){
                if(r.Predicate == predicate)return true;
            }
            return false;
        }

            ) ) {
            var e = r.Head == entity ? r.Tail : r.Head;
            AccumulateEntities( e, visited, predicates );
        }
    }

    public static IEnumerator AddEntitiesToGaiaFromHyperSpace ( List<Oracle.Entity> entities ) {
        while ( Gaia.Instance().recentlyAddedEntities.Count > 0 ) {
            yield return null;
        }
        Gaia.Instance().recentlyAddedEntities.AddRange( SubPartImporter.FilterSubparts( entities ) );
        yield return null;
    }


}
