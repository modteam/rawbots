using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class LiveBug : MonoBehaviour {

    static LiveBug liveBug;

    public static LiveBug Instance () {
        if ( liveBug == null ) {
            var livebugObj = Instantiate( Resources.Load( "LiveBug/LiveBug", typeof( GameObject ) ) ) as GameObject;
            liveBug = livebugObj.GetComponent< LiveBug >();
        }
        return liveBug;
    }

    public List< string > channels = new List< string >();
    public bool showDebug = false;
    bool isVisible = true;
    public GameObject container;
    List< LiveStatus > statuses = new List< LiveStatus >();
    public Transform statusesContainer;
    List< LiveBar > bars = new List< LiveBar >();
    public Transform barsContainer;
//    GraphBuilder graph;
    float stride = 0.4f;
    int touches = 0;

    void Awake () {
        if ( liveBug != default( LiveBug ) && liveBug != this ) {
            Debug.LogError( "Livebug singleton violated!" );
        }
        liveBug = this;
    }

    void Start () {

        LiveBar bar;

        bar = AddBar();
        bar.label = "meter_1";
        bar.color = Color.red;
        bar.channel = "a";
        bar.test = true;

        bar = AddBar();
        bar.label = "meter_2";
        bar.color = Color.green;
        bar.channel = "b";
        bar.min = 20;
        bar.max = 40;
        bar.value = 30;

        bar = AddBar();
        bar.label = "meter_3";
        bar.color = Color.blue;
        bar.channel = "c";
        bar.min = -20;
        bar.max = 20;
        bar.value = -10;

        LiveStatus status;

        status = AddStatus();
        status.label = "active_object";
        status.value = "none";
        status.color = Color.magenta;
        status.channel = "a";

        status = AddStatus();
        status.label = "event";
        status.value = "will auto destroy";
        status.color = Color.cyan;
        status.channel = "b";
        status.time = Time.time + 10;

        status = AddStatus();
        status.label = "this";
        status.value = this.ToString();
        status.color = Color.yellow;
        status.channel = "c";
    }

    void Update () {
        for ( int t = 0; t < Input.touchCount; ++t ) {
            Touch evt = Input.GetTouch( t );
            if ( evt.phase == TouchPhase.Began ) {
                ++touches;
            }
            else if ( evt.phase == TouchPhase.Moved ) {
            }
            else if ( evt.phase == TouchPhase.Ended || evt.phase == TouchPhase.Canceled ) {
                if ( touches == 3 ) {
                    showDebug = !showDebug;
                }
                --touches;
            }
        }

        if ( Input.GetKeyUp( "`" ) ) {
            showDebug = !showDebug;
        }

        if ( showDebug && !isVisible ) {
            isVisible = true;
            container.SetActive( true );
        }
        else if ( !showDebug && isVisible ) {
            isVisible = false;
            container.SetActive( false );
        }

        if ( !isVisible ) {
            return;
        }

        UpdateWidgets< LiveBar >( bars, barsContainer );
        UpdateWidgets< LiveStatus >( statuses, statusesContainer );
    }

    void UpdateWidgets< T > ( List< T > widgets, Transform container ) where T : LiveWidget {
        bool cleanList = false;
        foreach ( var widget in widgets ) {
            if ( widget == default( T ) ) {
                cleanList = true;
            }
            else if ( channels.Count == 0 ) {
                widget.showDebug = true;
            }
            else if ( channels.Any( c => c == widget.channel ) ) {
                widget.showDebug = true;
            }
            else {
                widget.showDebug = false;
            }
        }
        if ( cleanList ) {
            widgets = widgets.Where( m => m != default( T ) ).ToList();
        }
        int count = 0;
        foreach ( var widget in widgets ) {
            if ( widget != default( T ) && widget.showDebug ) {
                widget.transform.position = container.position - new Vector3( 0, stride * count, 0 );
                ++count;
            }
        }
    }

    public LiveBar AddBar () {
        var obj = Instantiate( Resources.Load( "LiveBug/LiveBar", typeof( GameObject ) ) ) as GameObject;
        var widget = obj.GetComponent< LiveBar >();
        bars.Add( widget );
        widget.transform.parent = barsContainer.transform;
        widget.showDebug = showDebug;
        return widget;
    }

    public void RemoveBar ( LiveBar bar ) {
        Destroy( bar.gameObject );
        bars = bars.Where( b => b != bar ).ToList();
    }

    public LiveBar FindBar ( string label ) {
        return bars.FirstOrDefault( b => b.label == label );
    }

    public LiveStatus AddStatus () {
        var obj = Instantiate( Resources.Load( "LiveBug/LiveStatus", typeof( GameObject ) ) ) as GameObject;
        var widget = obj.GetComponent< LiveStatus >();
        statuses.Add( widget );
        widget.transform.parent = statusesContainer.transform;
        widget.showDebug = showDebug;
        return widget;
    }

    public void RemoveStatus ( LiveStatus status ) {
        Destroy( status.gameObject );
        statuses = statuses.Where( s => s != status ).ToList();
    }
 
    public LiveStatus FindStatus ( string label ) {
        return statuses.FirstOrDefault( s => s.label == label );
    }

//    public LiveGraph GetGraph () {
//        return this.graph;
//    }

}

public partial class LiveWidget : MonoBehaviour {

    internal protected bool showDebug = false;
}


