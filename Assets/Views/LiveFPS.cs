using UnityEngine;
using System.Collections;

public class LiveFPS : MonoBehaviour {

    public TextMesh textMesh;
    int frames = 0;
    float timer = 0;
    static int fps = 0;

    void Start () {
    }
 
    void Update () {
        ++frames;
        timer -= Time.deltaTime;
        if ( timer < 0 ) {
            int roundedTimer = ( int )timer;
            timer = 1.0f + timer - roundedTimer;
            fps = frames;
            textMesh.text = "fps: " + fps.ToString();
            frames = 0;
        }
    }
}
