using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public enum MathFunctionType {
        ABS,
        ACOS,
        ASIN,
        ATAN,
        ADD,
        CEIL,
        COS,
        DIV,
        EXP,
        FLOOR,
		LIMIT,
        LOG,
        MAX,
        MIN,
        MOD,
        MULT,
        POW,
        ROUND,
        SIGN,
        SIN,
		SLEW,
        SQR,
        SUBS,
        TAN,
    }

    public class MathFunctionProxy : NodeProxy {
        public IOComponent io;
        MathFunctionType funcType = MathFunctionType.ADD;
        float a;
        float b;
		float lastOutput = 0.0f;

		private float SlewLimit(float inA, float rate, float dt)	//Slew rate limiting function, assumes rate is non-negative
		{
			var w = 0.0f;
			w = Mathf.Min (inA, (lastOutput + (rate * dt)));
			w = Mathf.Max (w, (lastOutput - (rate * dt)));
			lastOutput = w;
			return w;
		}
        
        public MathFunctionProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }
        
        public override void OnPreBuild () {
            base.OnPreBuild();
        }
        
        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "in_a", ( outbox, value ) => a =  Util.Sum( value ));
                inbox.ForQueued< float >( property == "in_b", ( outbox, value ) => b =  Util.Sum ( value ));
                inbox.For< string >( property == "type", ( outbox, value ) => {
                    funcType = ( MathFunctionType )System.Enum.Parse( typeof( MathFunctionType ), value.Split( '.' ).Last() );
                } );
            } );
        }
        public override void OnUpdate () {
            base.OnUpdate();
            
            var r = 0.0f;
            
            switch ( funcType ) {
                
                case MathFunctionType.ADD:
                    r = a + b;
                    break;
                    
                case MathFunctionType.ABS:
                    r = Mathf.Abs(a);
                    break;

                case MathFunctionType.ACOS:
                    r = Mathf.Rad2Deg * Mathf.Acos(a);
                    break;

                case MathFunctionType.ASIN:
                    r = Mathf.Rad2Deg * Mathf.Asin(a);
                    break;

                case MathFunctionType.ATAN:
                    r = Mathf.Rad2Deg * Mathf.Atan(a);
                    break;

                case MathFunctionType.CEIL:
                    r = (float)Mathf.CeilToInt(a);
                    break;
                    
                case MathFunctionType.COS:
                    r = Mathf.Cos(a * Mathf.Deg2Rad);
                    break;
                    
                case MathFunctionType.DIV:
                    if(b != 0) {
                        r = a / b;
                    }
                    break;
                    
                case MathFunctionType.EXP:
                    r = Mathf.Exp(a);
                    break;
                    
                case MathFunctionType.FLOOR:
                    r = (float) Mathf.FloorToInt(a);
                    break;

				case MathFunctionType.LIMIT:			//Combined min/max, limits a to +/-b
					r = Mathf.Max(a,-Mathf.Abs(b));
					r = Mathf.Min(r,Mathf.Abs(b));
					break;
                
                case MathFunctionType.LOG:
                    if(a > 0){
                        r = Mathf.Log(a);
                    }
                    break;
                
                case MathFunctionType.MAX:
                    r = Mathf.Max(a,b);
                    break;

                case MathFunctionType.MIN:
                    r = Mathf.Min(a,b);
                    break;

                case MathFunctionType.MOD:
                    if(b != 0){
                        r = (a % b);
                    }
                    break;

                case MathFunctionType.MULT:
                    r = a * b;
                    break;

                case MathFunctionType.POW:
                    r = Mathf.Pow(a,b);
                    break;

                case MathFunctionType.ROUND:
                    r = (float) Mathf.RoundToInt(a);
                    break;

                case MathFunctionType.SIGN:
                    r = Mathf.Sign(a);
                    break;

                case MathFunctionType.SIN:
                    r = Mathf.Sin(a * Mathf.Deg2Rad);
                    break;

				case MathFunctionType.SLEW:
					r = SlewLimit(a, Mathf.Abs(b), Time.deltaTime);	//Limits rate of change to +/-b
					break;

                case MathFunctionType.SQR:
                    if(a >= 0){
                        r = Mathf.Sqrt(a);
                    }
                    break;

                case MathFunctionType.SUBS:
                    r = a - b;
                    break;

                case MathFunctionType.TAN:
                    r = Mathf.Tan(a * Mathf.Deg2Rad);
                    break;

            }
            
            io.Produce< float >( "out", r );
            io.Produce< string >( "string", r.ToString() );
        }
    }

}
