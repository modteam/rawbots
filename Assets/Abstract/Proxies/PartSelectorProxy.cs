using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class PartSelectorProxy : NodeProxy {

        public IOComponent io;

        PartProxy[] parts = new PartProxy[16];
        int selector = 0;

        public PartSelectorProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            selector = 0;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "selector", ( outbox, value ) => selector = Mathf.RoundToInt( Mathf.Clamp( Util.Sum(value), 0f, 15f ) ) );
                inbox.For< PartProxy >( property == "part_0", ( outbox, value ) => parts[0] = value );
                inbox.For< PartProxy >( property == "part_1", ( outbox, value ) => parts[1] = value );
                inbox.For< PartProxy >( property == "part_2", ( outbox, value ) => parts[2] = value );
                inbox.For< PartProxy >( property == "part_3", ( outbox, value ) => parts[3] = value );
                inbox.For< PartProxy >( property == "part_4", ( outbox, value ) => parts[4] = value );
                inbox.For< PartProxy >( property == "part_5", ( outbox, value ) => parts[5] = value );
                inbox.For< PartProxy >( property == "part_6", ( outbox, value ) => parts[6] = value );
                inbox.For< PartProxy >( property == "part_7", ( outbox, value ) => parts[7] = value );
                inbox.For< PartProxy >( property == "part_8", ( outbox, value ) => parts[8] = value );
                inbox.For< PartProxy >( property == "part_9", ( outbox, value ) => parts[9] = value );
                inbox.For< PartProxy >( property == "part_10", ( outbox, value ) => parts[10] = value );
                inbox.For< PartProxy >( property == "part_11", ( outbox, value ) => parts[11] = value );
                inbox.For< PartProxy >( property == "part_12", ( outbox, value ) => parts[12] = value );
                inbox.For< PartProxy >( property == "part_13", ( outbox, value ) => parts[13] = value );
                inbox.For< PartProxy >( property == "part_14", ( outbox, value ) => parts[14] = value );
                inbox.For< PartProxy >( property == "part_15", ( outbox, value ) => parts[15] = value );
            } );
        }

        public override void OnUpdate () {
            io.Produce< PartProxy >( "part", parts[selector]);
        }

    }
}
