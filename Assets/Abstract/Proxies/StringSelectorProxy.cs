using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class StringSelectorProxy : NodeProxy {

        public IOComponent io;

        string[] strings = new string[16];
        int selector = 0;

        public StringSelectorProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            selector = 0;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "selector", ( outbox, value ) => selector = Mathf.RoundToInt( Mathf.Clamp( Util.Sum(value), 0f, 15f ) ) );
                inbox.For< string >( property == "string_0", ( outbox, value ) => strings[0] = value );
                inbox.For< string >( property == "string_1", ( outbox, value ) => strings[1] = value );
                inbox.For< string >( property == "string_2", ( outbox, value ) => strings[2] = value );
                inbox.For< string >( property == "string_3", ( outbox, value ) => strings[3] = value );
                inbox.For< string >( property == "string_4", ( outbox, value ) => strings[4] = value );
                inbox.For< string >( property == "string_5", ( outbox, value ) => strings[5] = value );
                inbox.For< string >( property == "string_6", ( outbox, value ) => strings[6] = value );
                inbox.For< string >( property == "string_7", ( outbox, value ) => strings[7] = value );
                inbox.For< string >( property == "string_8", ( outbox, value ) => strings[8] = value );
                inbox.For< string >( property == "string_9", ( outbox, value ) => strings[9] = value );
                inbox.For< string >( property == "string_10", ( outbox, value ) => strings[10] = value );
                inbox.For< string >( property == "string_11", ( outbox, value ) => strings[11] = value );
                inbox.For< string >( property == "string_12", ( outbox, value ) => strings[12] = value );
                inbox.For< string >( property == "string_13", ( outbox, value ) => strings[13] = value );
                inbox.For< string >( property == "string_14", ( outbox, value ) => strings[14] = value );
                inbox.For< string >( property == "string_15", ( outbox, value ) => strings[15] = value );
            } );
        }

        public override void OnUpdate () {
            io.Produce< string >( "string", strings[selector]);
            //io.Produce< string >( "all", System.String.Join( "", strings) );
        }

    }
}
