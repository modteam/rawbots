using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class PhysicsSleepProxy : NodeProxy {

        IOComponent io;
        PartProxy proxyPartA;
        bool alwaysActive = false;
        bool aalastValue = false;
        bool freeze = false;
        bool fLastValue = false;
        

        public PhysicsSleepProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => proxyPartA = value );
                inbox.ForQueued< float >( property == "always_active", ( outbox, value ) => alwaysActive = Convert.ToBoolean(Util.Sum(value)) );
                inbox.ForQueued< float >( property == "freeze", ( outbox, value ) => freeze = Convert.ToBoolean(Util.Sum(value)) );

            } );
        }

        public override void OnUpdate () {
            
            
            if(alwaysActive != aalastValue) { //only update the status if the "always_active" input changed"
                aalastValue = alwaysActive;

                var assembly_a = (proxyPartA != null)? proxyPartA.PhysicallyLinked() : null;
                if(assembly_a == null) { return; }
                
                foreach(PartProxy p in assembly_a) {
                    if(p.nodeView != null) {
                        p.nodeView.GetComponent<PhysicalObject>().bulletRigidbody.alwaysActive = alwaysActive;
                    }
                }
            }

            if(freeze != fLastValue) {
                fLastValue = freeze;
                
                var assembly_a = (proxyPartA != null)? proxyPartA.PhysicallyLinked() : null;
                if(assembly_a == null) { return; }
                
                
                foreach(PartProxy p in assembly_a) { //issue: continuums with their mass handset (but not program-set) will not reinstate the modified mass once unfrozen.
                    if(p.nodeView != null) {
                        var body = p.nodeView.GetComponent<PhysicalObject>().bulletRigidbody;
                        var mass = freeze ? 0:body.mass;
                        body.SetBodyMass(mass, body.compoundShape.CalculateInertia());
                    }
                }
            }
            
        }
        
    }
}
