using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class AltimeterProxy : NodeProxy {

        PartProxy part;
        IOComponent io;
        float gravity = 0;

        public AltimeterProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< PartProxy >( property == "part_a", ( output, value ) => part = value );
            } );
        }

        public override void OnUpdate () {
            var altitude = 0f;
            if ( part != default( PartProxy ) ) {
                var nv = part.nodeView;
                if ( nv != default( NodeView ) ) {
                    var po = nv.GetComponent< PhysicalObject >();
                    if ( po != default( PhysicalObject ) ) {
                        altitude = Vector3.Distance( po.transform.position, po.CenterOfGravity() );
                        gravity = po.Gravity().magnitude;
                    }

                    
                }
                part = default( PartProxy );
            }
            io.Produce< float >( "altitude", altitude );
            io.Produce<float>( "gravity", gravity );
        }

    }
}
