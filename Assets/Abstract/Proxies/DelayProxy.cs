using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class DelayProxy : NodeProxy {

        IOComponent io;
        Queue history = new Queue();
        float lastAdded;
        int delay;

        public DelayProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
            inbox.ForQueued< float >( property == "in_a", ( outbox, value ) => lastAdded = Util.Sum( value ));
            inbox.ForQueued< float >( property == "delay", ( outbox, value ) => delay = (int)Mathf.Max(Util.Sum( value ),0));

            } );
        }

        void DequeueExcess() { //Needed when the delay is lowered.
            if(history.Count > delay + 1) {
                history.Dequeue();
                DequeueExcess();
            }
        }
        

        public override void OnUpdate () {
            history.Enqueue(lastAdded);
            DequeueExcess();
            if(history.Count > delay) {
                io.Produce< float >( "sample", (float)history.Dequeue() );
            }
        }
        
    }
}