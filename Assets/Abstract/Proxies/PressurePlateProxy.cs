using UnityEngine;
using System.Collections;
using System.Linq;

namespace Operants {

    public class PressurePlateProxy : PartProxy {

        float position = 0;
        PressurePlate pressurePlate;

        public PressurePlateProxy () {
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            position = 0;
        }

        public override void OnPostBuild () {
            base.OnPostBuild();
            ForView< PressurePlate >( ref pressurePlate, () => {
                pressurePlate.press.transform.parent = pressurePlate.gameObject.transform.parent;
            } );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "force", ( outbox, value ) => {
                    ForView< PressurePlate >( ref pressurePlate, () => {
                        //var force = Mathf.Clamp( Util.Sum( value ), 0, 1000 );
                        //pressurePlate.force = force;
                    } );
                } );
            } );
        }

        public override void OnUpdate () {
            base.OnUpdate();
            ForView< PressurePlate >( ref pressurePlate, () => {
                position = pressurePlate.GetPosition();
            } );
            io.Produce< float >( "position", position );
        }

    }
}
