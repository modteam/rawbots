using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class CounterProxy : NodeProxy {

        public IOComponent io;
        int min;
        int max;
        int count;
        bool save = false;
        bool wrap = false;
        float trigger = 0;
        float invTrigger = 0;
        Pulse pulse = new Pulse();
        Pulse invPulse = new Pulse();
        

        public CounterProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            pulse = new Pulse();
            invPulse = new Pulse();
            min = 0;
            max = 0;
            trigger = 0f;
            invTrigger = 0f;

            entity.ForInferredProperty< float >( "saved", Util.AsIs, value => count = Mathf.RoundToInt(value) );//remember values from previous save.
        }
        

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "trigger", ( outbox, value ) => { trigger = Util.Sum(value); invTrigger = -Util.Sum(value); } );
                inbox.ForQueued< float >( property == "min", ( outbox, value ) => min = Mathf.RoundToInt(Util.Sum(value)) );
                inbox.ForQueued< float >( property == "max", ( outbox, value ) => max = Mathf.RoundToInt(Util.Sum(value)) );
                inbox.ForQueued< float >( property == "wrap", ( outbox, value ) => wrap = Convert.ToBoolean(Util.Sum(value)) );
                inbox.ForQueued< float >( property == "save", ( outbox, value ) => save = Convert.ToBoolean(Util.Sum(value)) );

            } );
        }

        public override void OnUpdate () {
        
            pulse.ForPulse( trigger, () => count += 1);
            invPulse.ForPulse( invTrigger, () => count -= 1);
    
            if(wrap) {
                if(count < min) {
                    count = max;
                } else if(count > max) {
                    count = min;
                }
            } else {
                count = Mathf.Clamp(count,min,max);
            }
            io.Produce< float >( "count",count );
            if(save) {
                entity.SetProperty("saved",count);
            }
        }

    }
}
