using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class SubstringProxy : NodeProxy {

        public IOComponent io;

        string string_in;
        int index = 0;
        int length = 0;

        public SubstringProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "index", ( outbox, value ) => index = Mathf.RoundToInt(Util.Sum(value)) - 1 );
                inbox.ForQueued< float >( property == "length", ( outbox, value ) => length = Mathf.RoundToInt(Util.Sum(value)) );
                inbox.For< string >( property == "string_in", ( outbox, value ) => string_in = value );
            } );
        }

        public override void OnUpdate () {
            io.Produce< string >( "string_out", SafeSubstring(string_in, index, length));
        }
        
        
        
        
        static string SafeSubstring( string value, int startIndex, int length) {
            
            if (!string.IsNullOrWhiteSpace(value) && startIndex >= 0) {
            
                if (value.Length >= (startIndex + length)) {
                    return value.Substring(startIndex, length);
                } else if (value.Length >= startIndex) {
                    return value.Substring(startIndex);
                }
            }
            return string.Empty;
        }
        

    }
}
