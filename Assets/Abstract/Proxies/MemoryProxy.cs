using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class MemoryProxy : NodeProxy {

        public IOComponent io;
            float sample_in;
            string string_in;
            float sample_out;
            string string_out;
            bool write = false;
        

        public MemoryProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            entity.ForInferredProperty< float >( "sample", Util.AsIs, value => sample_out = value );//remember values from previous save.
            entity.ForInferredProperty< string >( "string", Util.AsIs, value => string_out = value );
        }
        

        
        
        
        

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "sample_in", ( outbox, value ) => sample_in = Util.Sum(value) );
                inbox.For< string >( property == "string_in", ( outbox, value ) => string_in = value );
                inbox.ForQueued< float >( property == "write", ( outbox, value ) => write = Convert.ToBoolean(Util.Sum(value)) );
            } );
        }

        public override void OnUpdate () {
            if(write) {
                sample_out = sample_in;
                string_out = string_in;
                
                //if(string_out != null) { Debug.Log(string_out); } //interesting: if "write" is saved as 1 in the save file,
                //for a few frames when starting the world the game will accept it even theres an output that sets write to 0.
                
                //store the current values in the savefile. -z26
                entity.SetProperty("sample",sample_out);
                if(string_out != default(string)) { 
                    entity.SetProperty("string",string_out);
                }
            }
            
            
            io.Produce< float >( "sample",sample_out );
            io.Produce< string >( "string",string_out );

        }

    }
}
