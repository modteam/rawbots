using UnityEngine;
using System.Collections;

namespace Operants {

    public class DefaultOutputProxy : NodeProxy {
        
        public DefaultOutputProxy () {
            components.Add( new Outbox( this ) );
            updates = false;
        }
        
    }
    
}
