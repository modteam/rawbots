using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class SampleSelectorProxy : NodeProxy {

        public IOComponent io;

        float[] samples = new float[16];
        int selector = 0;

        public SampleSelectorProxy () {
            io = new IOComponent( this );
            components.Add( io );
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            selector = 0;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "selector", ( outbox, value ) => selector = Mathf.RoundToInt( Mathf.Clamp( Util.Sum(value), 0f, 15f ) ) );
                inbox.ForQueued< float >( property == "sample_0", ( outbox, value ) => samples[0] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_1", ( outbox, value ) => samples[1] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_2", ( outbox, value ) => samples[2] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_3", ( outbox, value ) => samples[3] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_4", ( outbox, value ) => samples[4] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_5", ( outbox, value ) => samples[5] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_6", ( outbox, value ) => samples[6] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_7", ( outbox, value ) => samples[7] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_8", ( outbox, value ) => samples[8] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_9", ( outbox, value ) => samples[9] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_10", ( outbox, value ) => samples[10] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_11", ( outbox, value ) => samples[11] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_12", ( outbox, value ) => samples[12] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_13", ( outbox, value ) => samples[13] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_14", ( outbox, value ) => samples[14] = Util.Sum(value) );
                inbox.ForQueued< float >( property == "sample_15", ( outbox, value ) => samples[15] = Util.Sum(value) );
            } );
        }

        public override void OnUpdate () {
            io.Produce< float >( "sample", samples[selector]);
        }

    }
}
