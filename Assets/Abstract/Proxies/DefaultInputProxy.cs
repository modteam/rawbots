using UnityEngine;
using System.Collections;

namespace Operants {

    public class DefaultInputProxy : NodeProxy {
        
        public DefaultInputProxy () {
            components.Add( new Inbox( this ) );
        }
        
    }
    
}
