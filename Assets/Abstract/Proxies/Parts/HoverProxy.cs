using UnityEngine;
using System.Collections;
using System.Linq;

namespace Operants {

    public class HoverProxy : PartProxy {

        Hover hover;

        public HoverProxy () {
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            ForView<Hover>( ref hover, () => hover.safeTimer = Time.time );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued<float>( property == "lift", ( outbox, value ) =>
                    ForView<Hover>( ref hover, () =>
                    hover.lift = Mathf.Max( 0, Util.Sum( value ) ) )
                );
            } );
        }

    }
}
