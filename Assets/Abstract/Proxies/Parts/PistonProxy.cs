using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class PistonProxy : PartProxy {

        Piston piston;

        public PistonProxy () {

        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            if ( nodeView != null ) {
                piston = nodeView.GetComponent<Piston>();
                piston.pistonObject.transform.parent = piston.gameObject.transform.parent;
				 piston.producePartLocalPosition = position => io.Produce( "position", position );
                auxiliar = piston.pistonObject;
            }
        }



        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }




        public override void StorePosition () {
            ForView< Piston >( ref piston, () => {
                        piston.StorePistonStatus();
                    } );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "position", ( outbox, value ) => {
                    ForView< Piston >( ref piston, () => {
                        piston.ChangePosition(Util.Sum(value));
                    } );
                } );
            } );
        }

        public override void ResetMovablePart () {

        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                Util.ForPredicate( property == "position", () => {
                    ForView< Piston >( ref piston, () => {
                        piston.motor.Stop();
                    } );
                } );

            } );
        }
    }
 
}
