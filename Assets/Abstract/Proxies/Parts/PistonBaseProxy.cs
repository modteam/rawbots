using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class PistonBaseProxy : PartProxy {

        PistonBase piston;

        public PistonBaseProxy () {

        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "position", ( outbox, value ) => {
                    ForView< PistonBase >( ref piston, () => {
                        piston.ChangePosition(Util.Sum(value));
                    } );
                } );
            } );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                Util.ForPredicate( property == "position", () => {
                    ForView< PistonBase >( ref piston, () => {
                        piston.motor.Stop();
                    } );
                } );

            } );
        }
		
		public override void OnPostBuild ()
		{
			var rotorProxy = this.entity.Tails (e => e == "linked_to").Select (e => e.proxy).Cast<PistonHeadProxy> ().First ();

             if(rotorProxy.nodeView == default(NodeView)){
                base.OnPostBuild ();
                return;
            }


			GameObject constraintHolder =null;
			ForView< PistonBase > (ref piston, () => {
				constraintHolder = piston.constraintHolder;
                piston.pistonHead = rotorProxy.nodeView.gameObject;
			});

            ForView< PistonBase >( ref piston, () => {
                rotorProxy.nodeView.btRigidBody.nodeview = nodeView;

                var partHighLight = nodeView.GetComponent<Highlight>();
                var subpartHighLight = rotorProxy.nodeView.GetComponent<Highlight>();

                partHighLight.RegisterRenderers(subpartHighLight.renderers);
                var part = nodeView.GetComponent<Part>();
                var subpart =rotorProxy.nodeView.GetComponent<Part>();
                part.partMaterials.AddRange(subpart.partMaterials);
                part.linkedParts.Add( subpart );
                subpart.linkedParts.Add(part);

            } );

			var constraint = constraintHolder.AddComponent<BulletGeneric6DofConstraint> ();

            ForView< PistonBase > (ref piston, () => {
                piston.motor.constraint = constraint;
                piston.producePartLocalPosition = position => io.Produce( "position", position );
            });

			constraint.bulletRbA = nodeView.GetComponent<BulletRigidBody> ();
			constraint.bulletRbB = rotorProxy.nodeView.GetComponent<BulletRigidBody> ();

			var objectA = nodeView.gameObject;
			var objectB = rotorProxy.nodeView.gameObject;
				
			GameObject dummyRotor = new GameObject ();
			dummyRotor.transform.parent = objectA.transform;
			dummyRotor.transform.localPosition = new Vector3(0f, 0f, 0f);
			dummyRotor.transform.localRotation = Quaternion.identity;


			constraint.rbAPos = objectA.transform.position;
			constraint.rbBPos = dummyRotor.transform.position;// objectB.transform.position;
			constraint.rbBRot = dummyRotor.transform.rotation;//objectB.transform.rotation;
			constraint.rbARot = objectA.transform.rotation;

            var objectBCurrentPos =objectB.transform.position;


			objectB.GetComponent<BulletRigidBody> ().SetPosition (dummyRotor.transform.position);
			objectB.GetComponent<BulletRigidBody> ().SetRotation (dummyRotor.transform.rotation);

			dummyRotor.transform.position = objectBCurrentPos;
            var currenPos = dummyRotor.transform.localPosition.y;

			//constraint.solverIterations = 20;
			constraint.useLinearReferenceFrameA = false;
			constraint.disableCollisionBetweenBodies = true;

			constraint.angularERPValue = Vector3.one * 0.8f;
			constraint.linearERPValue = Vector3.one * 0.8f;

			constraint.angularCFMValue = Vector3.zero; //Vector3.one*0.01f;
			constraint.linearCFMValue = Vector3.zero;//Vector3.one*0.01f;
			
			//constraint.lowerLimit.z = -1.57f;
			constraint.upperLinearLimit.z = 6f;     //Max length
			constraint.Initialize ();
			
			
			dummyRotor.transform.localPosition = Vector3.up*currenPos;
			objectB.GetComponent<BulletRigidBody> ().SetPosition (dummyRotor.transform.position);
			objectB.GetComponent<BulletRigidBody> ().SetRotation (dummyRotor.transform.rotation);
			GameObject.Destroy (dummyRotor);
			base.OnPostBuild ();
		}
    }
 
}
