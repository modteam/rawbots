using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class PivotProxy : PartProxy {

        public Pivot pivot;

        public PivotProxy () {
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            if ( nodeView != null ) {
                pivot = nodeView.GetComponent<Pivot>();
                pivot.rotorObject.transform.parent = pivot.gameObject.transform.parent;

                pivot.producePartAngle = angle => io.Produce("angle_front", angle );
                auxiliar = pivot.rotorObject;
            }
        }



        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }

        public override void StorePosition () {
            ForView< Pivot >( ref pivot, () => {
                pivot.StorePivotStatus();
            } );
        }



        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued<float>( property == "angle_front", ( outbox, value ) => {

                    ForView< Pivot >( ref pivot, () => {
                        pivot.SetPivotAngle(Util.Sum(value) );
                    } );
                } );
                inbox.ForQueued<float>( property == "force_front", ( outbox, value ) => {

                } );
                inbox.ForQueued<float>( property == "velocity_front", ( outbox, value ) => {
                    ForView< Pivot >( ref pivot, () => {
                        pivot.SetPivotVelocity(Util.Sum(value) );
                    } );
                } );
            } );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                Util.ForPredicate( property == "angle_front", () => {
                    ForView< Pivot >( ref pivot, () => {
                        pivot.motor.Stop();
                    } );
                } );
                Util.ForPredicate( property == "velocity_front", () => {
                    ForView< Pivot >( ref pivot, () => {
                        pivot.motor.Stop();
                    } );
                } );
            } );
        }

        public override void ResetMovablePart () {
            ForView< Pivot >( ref pivot, () => {
                pivot.ResetPivotJoint();
            } );
        }
    }
    
}
