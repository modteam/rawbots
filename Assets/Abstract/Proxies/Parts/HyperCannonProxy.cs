using UnityEngine;
using System.Collections;

namespace Operants {

    public class HyperCannonProxy : PartProxy {

        public HyperCannon cannon;


        public override void OnPreBuild () {
            base.OnPreBuild();
            ForView< HyperCannon > ( ref cannon, () =>  cannon.produceShootForce = f => io.Produce<float> ( "shoot_force" , f ));
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {

                inbox.For< PartProxy >( property == "shoot", ( outbox, value ) => {
                    if ( value != default( PartProxy ) ) {
                        ForView< HyperCannon >( ref cannon, () => cannon.ShootParts( value ) );
                    }
                } );

                inbox.ForQueued<float>( property == "force", (Outbox, value) => {
                    ForView<HyperCannon>( ref cannon, () => cannon.force = Util.Sum( value ) );
                } );

            } );
        }

    }

}
