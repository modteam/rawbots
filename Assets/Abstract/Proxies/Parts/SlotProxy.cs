using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class SlotProxy : NodeProxy {

        public GameObject ownerGameObject;
        public PartProxy ownerPartProxy;
        public bool isConnected = false;
        
        public SlotProxy () {
        }
		
		public override void OnPreBuild () {
            base.OnPreBuild();
           isConnected = entity.Relationships().Any( p => p.Predicate == "extends" );
        }

        public void OnSlot () {
            if ( nodeView == null ) {

                var slotParent = this.entity.Heads( e => e == "slot_for" ).Select( e => e.proxy ).Cast<PartProxy>().First();

                this.entity.ForInferredProperty< string >( "parent", Util.AsIs, parent => {
                    Transform parentView = default(Transform);
                    if ( parent == "main" ) {
                        parentView = slotParent.nodeView.transform;
                    }
                    else {
                        parentView = slotParent.auxiliar.transform;
                    }
                    ownerPartProxy = slotParent;
                    ownerGameObject = parentView.gameObject;
                    this.entity.ForInferredProperty< string >( "view", Util.AsIs, viewAsset => {
                        var asset = Resources.Load( viewAsset, typeof( GameObject ) );
                        if ( asset != default( Object ) ) {
                            nodeView = ( GameObject.Instantiate( asset ) as GameObject ).GetComponent< NodeView >();
                            nodeView.name = nodeView.name.Replace( "(Clone)", "(" + entity.id + ")" );
                            nodeView.proxy = this;
                            nodeView.transform.parent = parentView;
                            entity.nodeView = nodeView;
                        }
                    } );

                    entity.ForInferredProperty< string >( "position", Util.AsIs, position => {
                        nodeView.transform.localPosition = Util.PropertyToVector3( position );
                    } );

                    entity.ForInferredProperty< string >( "rotation", Util.AsIs, rotation => {
                        nodeView.transform.localEulerAngles = Util.PropertyToVector3( rotation );
                    } );
                    nodeView.gameObject.GetComponent<Slot>().slotView.enabled = true;
                } );
                isConnected = true;
            }
            if ( nodeView != default(NodeView) ) {
                nodeView.gameObject.GetComponent<Slot>().slotView.enabled = true;
            }
        }

        public void RemoveUnUsedSlot () {
            
            isConnected = entity.Relationships().Any( p => p.Predicate == "extends" );
            if ( nodeView != default(NodeView) ) {
                nodeView.gameObject.GetComponent<Slot>().slotView.enabled = false;
            }
        }

    }

}
