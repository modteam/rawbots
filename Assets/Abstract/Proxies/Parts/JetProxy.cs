using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class JetProxy : PartProxy {

        Jet jet;

        public JetProxy () {
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {

                inbox.ForQueued< float >( property == "thrust", ( outbox, value ) => {
                    ForView< Jet >( ref jet, () => {
                        jet.thrust = Mathf.Max( 0, Util.Sum( value ) );
                        jet.fxWeight = Mathf.Clamp01( Util.MapValue0X( jet.thrust, 500, 0, 1 ) );
                    } );
                } );

                inbox.ForQueued<float>(property == "yaw", (outbox, value) => {
                    ForView<Jet>(ref jet, () => {
                        jet.yaw = Mathf.Clamp(Util.Sum(value),-45,45);
                        jet.UpdateGimbal();
                    });
                });

                inbox.ForQueued<float>(property == "pitch", (outbox, value) => {
                    ForView<Jet>(ref jet, () => {
                        jet.pitch = Mathf.Clamp(Util.Sum(value),-45,45);
                        jet.UpdateGimbal();
                    });
                });
            });
        }

    }

}
