using UnityEngine;
using System.Collections;
using System.Linq;
using Oracle;

namespace Operants {

    public enum AudioFormat{
        wav,
        ogg,
    }

    public class SpeakerProxy : PartProxy {

        Speaker speaker;

        public SpeakerProxy () {

        }
        
        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.For< string >( property == "source", 
                    ( outbox, value ) => ForView<Speaker>(ref speaker, () => speaker.LoadSource(value) ) );
                inbox.For< string >( property == "format",
                                    ( outbox, value ) => ForView<Speaker>(ref speaker,
                () => speaker.SetAudioFormat( (AudioFormat ) System.Enum.Parse( typeof( AudioFormat ), value.Split( '.' ).Last() ) ) ) );

                inbox.For < float > (property == "play" , ( outbox, value ) => ForView<Speaker>( ref speaker, () => speaker.Play((int)value) ) ) ;

                inbox.For <float> (property == "volume" , ( outbox, value ) =>
                   ForView<Speaker>(ref speaker, () => speaker.SetVolume( value ) ) );
            } );
        }
        
    }
}