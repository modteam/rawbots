using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Operants {

    public class MotorBaseProxy : PartProxy {
        
     
        MotorBase motor;
     
        public MotorBaseProxy () {
           // updates = false;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            if ( nodeView != null ) {

            }
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "force", ( outbox, value ) => {
                    ForView< MotorBase >( ref motor, () => {
                    } );
                } );
                inbox.ForQueued<float>(property == "idle", (outbox, value) => {
                    ForView<MotorBase>(ref motor, () => {
                        motor.SetIdle(Util.Sum(value));
                    });
                });
                inbox.ForQueued< float >( property == "angle", ( outbox, value ) => {
                    ForView< MotorBase >( ref motor, () => {
                        motor.SetMotorAngle( Util.Sum( value ) );
                    } );
                } );
                inbox.ForQueued< float >( property == "velocity", ( outbox, value ) => {
                    ForView< MotorBase >( ref motor, () => {
                        motor.SetMotorVelocity( Util.Sum( value ) );
                    } );
                } );
            } );
        }

        public override void OnInboxDestroy ( Inbox inbox ) {
            base.OnInboxDestroy( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                Util.ForPredicate( property == "angle", () => {
                    ForView< MotorBase >( ref motor, () => {
                        motor.motor.Stop();

                    } );
                } );
                Util.ForPredicate( property == "velocity", () => {
                    ForView< MotorBase >( ref motor, () => {
                        motor.motor.Stop();
                    } );
                } );
            } );
        }

        public override void OnPostBuild () {
            //base.OnPostBuild();
            var rotorProxy = this.entity.Tails( e => e == "linked_to" ).Select( e => e.proxy ).Cast<MotorRotorProxy>().First();
            if(rotorProxy.nodeView == default(NodeView)){
                base.OnPostBuild ();
                return;
            }

        ForView< MotorBase >( ref motor, () => {

                if(rotorProxy.nodeView != default(NodeView)){
                    motor.rotor = rotorProxy.nodeView.gameObject;
                    motor.producePartAngle = ( angle ) => io.Produce( "angle", angle );
                }
            } );



            GameObject constraintHolder = null;
            ForView< MotorBase >( ref motor, () => {
                constraintHolder = motor.constraintHolder;
            } );

            ForView< MotorBase >( ref motor, () => {
                rotorProxy.nodeView.btRigidBody.nodeview = nodeView;
                var partHighLight = nodeView.GetComponent<Highlight>();
                var subpartHighLight = rotorProxy.nodeView.GetComponent<Highlight>();

                partHighLight.RegisterRenderers(subpartHighLight.renderers);
                var part = nodeView.GetComponent<Part>();
                var subpart =rotorProxy.nodeView.GetComponent<Part>();
                part.partMaterials.AddRange(subpart.partMaterials);
                part.linkedParts.Add( subpart );
                subpart.linkedParts.Add(part);
            } );

            var constraint = constraintHolder.AddComponent<BulletGeneric6DofConstraint>();

            ForView< MotorBase >( ref motor, () => {
                motor.motor.constraint = constraint;
            } );


            constraint.bulletRbB = nodeView.GetComponent<BulletRigidBody>();
            constraint.bulletRbA = rotorProxy.nodeView.GetComponent<BulletRigidBody>();

            var objectA = nodeView.gameObject;
            var objectB = rotorProxy.nodeView.gameObject;
             
            GameObject dummyRotor = new GameObject("dummyRotor");
            dummyRotor.transform.parent = objectA.transform;
            dummyRotor.transform.localPosition = new Vector3( 0, 0, 0 );
            dummyRotor.transform.localRotation = Quaternion.identity;


            constraint.rbAPos = objectA.transform.position;
            constraint.rbBPos = dummyRotor.transform.position;// objectB.transform.position;
            constraint.rbBRot = dummyRotor.transform.rotation;//objectB.transform.rotation;
            constraint.rbARot = objectA.transform.rotation;



            //var currentAngle = Vector3.Angle( objectA.transform.right, objectB.transform.right );
            var rotorRotation = Quaternion.Inverse( objectA.transform.rotation ) * objectB.transform.rotation;
        var currentAngle = rotorRotation.eulerAngles.y ;



            objectB.GetComponent<BulletRigidBody>().SetPosition( dummyRotor.transform.position );
            objectB.GetComponent<BulletRigidBody>().SetRotation( dummyRotor.transform.rotation );
         
            //constraint.solverIterations = 20;
            constraint.useLinearReferenceFrameA = false;
            constraint.disableCollisionBetweenBodies = true;

            constraint.angularERPValue = Vector3.one * 0.8f;
            constraint.linearERPValue = Vector3.one * 0.8f;

            constraint.angularCFMValue = Vector3.zero; //Vector3.one*0.01f;
            constraint.linearCFMValue = Vector3.zero;//Vector3.one*0.01f;
         
            constraint.lowerLimit.z = 1;


            constraint.Initialize();

            var newRotation = Quaternion.AngleAxis( currentAngle, Vector3.up );
            dummyRotor.transform.localRotation = newRotation;

            objectB.GetComponent<BulletRigidBody>().SetPosition( dummyRotor.transform.position );
            objectB.GetComponent<BulletRigidBody>().SetRotation( dummyRotor.transform.rotation );

            GameObject.Destroy( dummyRotor );
           base.OnPostBuild ();

        }

        public override void OnViewDestroyed () {
            base.OnViewDestroyed();
        }



  
    }
    
}
