using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class MagnetProxy : PartProxy
    {

        Magnet magnet;
        public System.Action<float> fire = (fireParameter) => { };

        public MagnetProxy()
        {
        }

        public override void OnPreBuild()
        {
            base.OnPreBuild();
            //if (nodeView != default(NodeView))
            //{
            //    var magnet = nodeView.GetComponent<Magnet>();
            //    fire = magnet.Fire;
            //}
            //magnet.reachFeedback = new Transform(;
            
            ForView<Magnet>(ref magnet, () =>
            {
                magnet.Start();
                //fire = magnet.Fire;
                //magnet.fire = fire;
            });
        }

        public override void OnViewDestroyed()
        {
            base.OnViewDestroyed();
            fire = (fireParameter) => { };
        }

        public override void OnInbox(Inbox inbox)
        {
            base.OnInbox(inbox);
            inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property =>
            {
                inbox.ForQueued<float>(property == "fire", (outbox, value) =>
                {
                    ForView<Magnet>(ref magnet, () =>
                    {
                        magnet.fire = Mathf.Clamp(Util.Sum(value), -1000f, 1000f);
                    });
                });
            });
        }
    }
}

//{

//    public class MagnetProxy : PartProxy
//    {

//        Magnet magnet;
//        //System.Action<float> tryExplode = (v) => { };

//        public MagnetProxy()
//        {
//        }

//        //public override void OnPreBuild()
//        //{
//        //    base.OnPreBuild();
//        //    ForView<Magnet>(ref magnet, () => {
//        //        //tryExplode = magnet.TryExplode;
//        //    });
//        //}

//        //public override void OnViewDestroyed()
//        //{
//        //    base.OnViewDestroyed();
//        //    //tryExplode = (v) => { };
//        //}

//        //public override void EnergyRecharge(float amount)
//        //{

//        //}

//        public override void OnInbox(Inbox inbox)
//        {
//            base.OnInbox(inbox);
//            inbox.Proxy.entity.ForInferredProperty<string>("property", Util.AsIs, property => {
//                inbox.ForQueued<float>(property == "detonate", (outbox, value) => {
//                    ForView<Magnet>(ref magnet, () => {
//                        magnet.field = Mathf.Clamp(Util.Sum(value), -1000f, 1000f);
//                    });
//                });
//            });
//        }

//    }
//}
