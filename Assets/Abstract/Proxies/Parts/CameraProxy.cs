using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Operants {

    public class CameraProxy : PartProxy {

        CameraView camera;

        public CameraProxy () {
        }


        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "fov", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetFOV( Util.Sum(value) ) ) );
                inbox.ForQueued< float >( property == "size", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetSize( Util.Sum(value) ) ) );
                inbox.ForQueued< float >( property == "near", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetNearPlane( Util.Sum(value) ) ) );
                inbox.ForQueued< float >( property == "far", ( outbox, value ) =>
                    ForView< CameraView >( ref camera, () => camera.SetFarPlane( Util.Sum(value) ) ) );
            } );
        }
        
        
        
        public override void OnUpdate () {
            
            
            ForView< CameraView >( ref camera, () => {
                io.Produce< float >( "activity", camera.trigger.activity );
                io.Produce< PartProxy >( "detected", camera.trigger.detected );
            } );

        }
        
        
        

    }
 
}
