using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class ProgramProxy : NodeProxy {

        IOComponent io;

        public ProgramProxy () {
            Debug.Log( "PROGRAM STARTED" );
            io = new IOComponent( this );
            components.Add( io );
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "kp", ( output, value ) => {} );
                inbox.ForQueued< float >( property == "ki", ( output, value ) => {} );
                inbox.ForQueued< float >( property == "kd", ( output, value ) => {} );
            } );
        }

    }
}
