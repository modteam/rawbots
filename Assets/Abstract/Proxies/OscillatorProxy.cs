using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;

namespace Operants {

    public class OscillatorProxy : NodeProxy {
        
        public IOComponent io;
        public Waveform oscillator;
        public Sample sample;
        float timeScale = 1;
        bool timeIsDriven = false;
        
        public OscillatorProxy () {
            io = new IOComponent( this );
            components.Add( io );
            oscillator = new Waveform();
            updates = true;
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
            oscillator = new Waveform();
            timeScale = 1;
            timeIsDriven = false;
        }

        public override void OnInbox ( Inbox inbox ) {
            base.OnInbox( inbox );
            inbox.Proxy.entity.ForInferredProperty< string >( "property", Util.AsIs, property => {
                inbox.ForQueued< float >( property == "time", ( outbox, value ) => {
                    timeIsDriven = true;
                    oscillator.time = Util.Sum( value );
                } );
                inbox.ForQueued< float >( property == "time_scale", ( outbox, value ) => timeScale = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "amplitude", ( outbox, value ) => oscillator.amplitude = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "frequency", ( outbox, value ) => oscillator.frequency = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "offset", ( outbox, value ) => oscillator.offset = Util.Sum( value ) );
                inbox.ForQueued< float >( property == "phase", ( outbox, value ) => oscillator.phase = Util.Sum( value ) );
                inbox.For< string >( property == "type", ( outbox, value ) => {
                    oscillator.type = ( WaveType )System.Enum.Parse( typeof( WaveType ), value.Split( '.' ).Last() );
                } );
                inbox.For< string >( property == "invert", ( outbox, value ) => {
                    oscillator.invert = ( WaveInvert )System.Enum.Parse( typeof( WaveInvert ), value.Split( '.' ).Last() );
                } );
            } );
        }
        
        public override void OnUpdate () {
            base.OnUpdate();
            if ( timeIsDriven ) {
                sample = oscillator.Sample();
                timeIsDriven = false;
            }
            else {
                sample = oscillator.Sample( Time.deltaTime * timeScale );
            }
            io.Produce( "sample", sample.sample );
            io.Produce( "inverted", sample.inverted );
        }
        
    }
 
}
