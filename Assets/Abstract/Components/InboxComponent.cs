using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

namespace Operants {

    public class Inbox : AbstractComponent {

        List< System.Delegate > actions = new List< System.Delegate >();
        List< System.Delegate > queuedActions = new List< System.Delegate >();
        Dictionary< System.Type, object > valueLists = new Dictionary< System.Type, object >();
        object recorded;

        public object Recorded {
            get {
                return recorded;
            }
        }

        public Inbox ( NodeProxy proxy ) : base( proxy ) {
        }

        public void RemoveAction< T > ( System.Action< Outbox, T > action ) {
            actions.Remove( action );
            queuedActions.Remove( action );
        }

        public void ClearActions () {
            actions.Clear();
            queuedActions.Clear();
            valueLists.Clear();
        }

        public override void OnPreBuild () {
            base.OnPreBuild();
        }

        int lastFrame = 0;

        public void Consume< T > ( Outbox outbox, T value ) {
            recorded = value;
//            foreach ( var action in actions.Cast< System.Action< Outbox, T > >() ) {
//                action( outbox, value );
//            }
//            foreach ( var action in actions ) {
//                var castedAction = ( System.Action< Outbox, T >)action;
//                castedAction( outbox, value );
//            }
            for(int i = 0; i< actions.Count; ++i){
                var castedAction = ( System.Action< Outbox, T >)actions[i];
                castedAction( outbox, value );
            }


            if ( queuedActions.Count > 0 ) {
                var values = GetValueList< T >();
                if ( lastFrame == Time.frameCount ) {
                    values.Add( value );
                }
                else {
                    lastFrame = Time.frameCount;
                    values.Add( value );
                    for(int i = 0; i< queuedActions.Count; ++i){
                        var castedqueuedAction = (System.Action< Outbox, List< T > >)queuedActions[i];

                        castedqueuedAction( outbox, values );

                    }

//                    foreach ( var queuedAction in queuedActions ) {
//                        var castedqueuedAction = (System.Action< Outbox, List< T > >)queuedAction;
//
//                        castedqueuedAction( outbox, values );
//                    }
                    values.Clear();
                }
            }
        }
    
        public void For< T > ( bool predicate, System.Action< Outbox, T > action ) {
            if ( predicate ) {
                actions.Add( action );
                Proxy.entity.ForInferredProperty< T >( "value", Util.AsIs, value => {
                    action( null, value );
                } );
            }
        }

        List< T > GetValueList< T > () {
            object valueListObject;
            if ( valueLists.TryGetValue( typeof( T ), out valueListObject ) ) {
                return valueListObject as List< T >;
            }
            else {
                var valueList = new List< T >();
                valueLists[ typeof( T ) ] = valueList;
                return valueList;
            }
        }

        public void ForQueued< T > ( bool predicate, System.Action< Outbox, List< T > > action ) {
            if ( predicate ) {
                queuedActions.Add( action );
                Proxy.entity.ForInferredProperty< T >( "value", Util.AsIs, value => {
                    action( null, new List< T >() { value } );
                } );
            }
        }

    }

}
