using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;
using System.Text.RegularExpressions;

public class HexPartTile : HexGridTileTypeHandler {

    HexGridLine partOfLine;

    public override void Init (Entity etype) {
        tile.tileType = HexGridTileType.Part;
        io = tile.entity.proxy.GetComponent< IOComponent >();
        if ( tile.entity.nodeView != null ) {
            partOfLine = ( Instantiate( tile.partLineAsset ) as GameObject ).GetComponent< HexGridLine >();
            partOfLine.line.enabled = false;
            partOfLine.grid = tile.grid;
            partOfLine.transform.parent = transform;
            partOfLine.tail = this.transform;
            partOfLine.head = tile.entity.nodeView.transform;
            partOfLine.headInWorld = true;
        }
        InitializeIO();
        SetMaterial();
    }

    public override void SetMaterial () {
        tile.renderer.sharedMaterial = tile.grid.partMaterial;
    }

    public override void DeleteStep (bool step) {
        DestroyIOs();
    }

    public override void OnMouseEnterCustom (TouchData touch) {
        base.OnMouseEnterCustom(touch);
        if ( partOfLine != null && tile.entity.nodeView != null ) {
            partOfLine.line.enabled = true;
            tile.entity.nodeView.gameObject.GetComponent< Highlight >().SetHighlight( true, Highlight.HighlightType.Green );
            tile.entity.nodeView.SendMessage( "OnGridTileEnter", SendMessageOptions.DontRequireReceiver );
        }
    }

    public override void OnMouseExitCustom (TouchData touch) {
        base.OnMouseExitCustom(touch);
        if ( partOfLine != null && tile.entity.nodeView != null ) {
            partOfLine.line.enabled = false;
            tile.entity.nodeView.gameObject.GetComponent< Highlight >().SetHighlight( false );
            tile.entity.nodeView.SendMessage( "OnGridTileExit", SendMessageOptions.DontRequireReceiver );
        }
    }

    public override void RequestOnConsole () {
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        var leftAlt = Input.GetKey( KeyCode.LeftAlt );
        var leftShift = Input.GetKey( KeyCode.LeftShift );
        
        if(Gaia.scene > 0){
            Sec.ForPermission( tile.entity, Sec.Permission.edit, () => {
                if(!leftAlt && !leftShift){
                    if(leftControl){
                        tile.grid.GenerateTilesIO( tile );
                    }
                    onConsole();
                }
            }, () => {
                Console.PushError("tile", new string[]{  "access denied - check permissions" } );
            } );
        }
    }

}
