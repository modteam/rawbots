using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class DevTools : MonoBehaviour {

    static DevTools instance;
    static string optionsKeyBase = "/DevTools/Options/";

    public enum Options {
        HitDamageSuppressed,
        CameraFollowDistanceUnlimited,
        CameraCycleFPS,
        MusicSuppressed,
        DayCycleSuppressed,
        PressurePlateClick,
        HexEarthEdit,
        AskForPasscodeSupressed,
        CheckPermissionSupressed,
        DisruptiveDelete,
        GodMouse,
        BotGenerator
    }

    static Dictionary< Options, bool > options = new Dictionary< Options, bool >();

    void Awake () {
        foreach ( var optionName in System.Enum.GetNames( typeof( Options ) ) ) {
            var prefKey = optionsKeyBase + optionName;
            var value = ( Options )System.Enum.Parse( typeof( Options ), optionName );
            if ( PlayerPrefs.HasKey( prefKey ) ) {
                options[ value ] = true;
                Debug.Log( prefKey + " enabled" );
                continue;
            }
            options[ value ] = false;
        }
    }

    public static bool IsOptionEnabled ( Options option ) {
        bool enabled = false;
        if ( options.TryGetValue( option, out enabled ) ) {
            return enabled;
        }
        return enabled;
    }

    public static void ToggleOption ( Options option ) {
        string prefKey;
        bool enabled;
        if ( options.TryGetValue( option, out enabled ) && enabled ) {
            options[ option ] = false;
            prefKey = optionsKeyBase + option;
            Debug.Log( prefKey + " disabled" );
            PlayerPrefs.DeleteKey( prefKey );
            return;
        }
        options[ option ] = true;
        var names = System.Enum.GetNames( typeof( Options ) );
        var values = System.Enum.GetValues( typeof( Options ) ).Cast< Options >().ToList();
        prefKey = optionsKeyBase + names[ values.IndexOf( option ) ];
        PlayerPrefs.SetInt( prefKey, 1 );
        Debug.Log( prefKey + " enabled" );
    }

}
