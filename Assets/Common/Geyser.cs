using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Geyser : MonoBehaviour {

    public List< GameObject > idles;
    public List< GameObject > eruptions;

    void Start () {
        foreach ( var idle in idles ) {
            idle.SetActive( false );
        }
        foreach ( var eruption in eruptions ) {
            eruption.SetActive( false );
        }
        idles[ Random.Range( 0, idles.Count ) ].SetActive( true );
    }
}
