using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class Island : MonoBehaviour {

    public bool useRockyRocks;
    public bool useFireRocks;
    public bool useIceRocks;
    bool useRockyRocksState;
    bool useFireRocksState;
    bool useIceRocksState;
    public bool useRockyCraters;
    public bool useFireCraters;
    public bool useIceCraters;
    bool useRockyCratersState;
    bool useFireCratersState;
    bool useIceCratersState;
    public bool useRockyEarth;
    public bool useLayerEarth;
    public bool useFireEarth;
    public bool useIceEarth;
    public bool useToxicEarth;
    bool useRockyEarthState;
    bool useLayerEarthState;
    bool useFireEarthState;
    bool useIceEarthState;
    bool useToxicEarthState;
    public List< Prop > rockyRocks;
    public List< Prop > fireRocks;
    public List< Prop > iceRocks;
    public List< Prop > rockyCraters;
    public List< Prop > fireCraters;
    public List< Prop > iceCraters;
    public List< Prop > stalagmites;
    public List< Prop > crystals;
    public List< Material > rockyEarth;
    public List< Material > layerEarth;
    public List< Material > fireEarth;
    public List< Material > iceEarth;
    public List< Material > toxicEarth;

    void Awake () {
        FlipBits();
    }

    void FlipBits () {
        useRockyRocksState = useRockyRocks;
        useFireRocksState = useFireRocks;
        useIceRocksState = useIceRocks;
        useRockyCratersState = useRockyCraters;
        useFireCratersState = useFireCraters;
        useIceCratersState = useIceCraters;
        useRockyEarthState = useRockyEarth;
        useLayerEarthState = useLayerEarth;
        useFireEarthState = useFireEarth;
        useIceEarthState = useIceEarth;
        useToxicEarthState = useToxicEarth;
    }

    void Update () {
        var bitFlipped = false;
        bitFlipped |= useRockyRocksState ^ useRockyRocks;
        bitFlipped |= useFireRocksState ^ useFireRocks;
        bitFlipped |= useIceRocksState ^ useIceRocks;
        bitFlipped |= useRockyCratersState ^ useRockyCraters;
        bitFlipped |= useFireCratersState ^ useFireCraters;
        bitFlipped |= useIceCratersState ^ useIceCraters;
        bitFlipped |= useRockyEarthState ^ useRockyEarth;
        bitFlipped |= useLayerEarthState ^ useLayerEarth;
        bitFlipped |= useFireEarthState ^ useFireEarth;
        bitFlipped |= useIceEarthState ^ useIceEarth;
        bitFlipped |= useToxicEarthState ^ useToxicEarth;
        if ( bitFlipped ) {
            foreach ( var earth in GetComponentsInChildren< HexEarth >() ) {
                earth.Sprout();
            }
            FlipBits();
        }
    }

    public GameObject GetRocksAsset ( System.Random rnd ) {
        var props = new List< Prop >();
        if ( useRockyRocks ) {
            props.AddRange( rockyRocks );
        }
        if ( useFireRocks ) {
            props.AddRange( fireRocks );
        }
        if ( useIceRocks ) {
            props.AddRange( iceRocks );
        }
        if ( props.Count > 0 ) {
            return props[ rnd.Next( 0, props.Count ) ].gameObject;
        }
        else {
            return null;
        }
    }

    public GameObject GetCraterAsset ( System.Random rnd ) {
        var props = new List< Prop >();
        if ( useRockyCraters ) {
            props.AddRange( rockyCraters );
        }
        if ( useFireCraters ) {
            props.AddRange( fireCraters );
        }
        if ( useIceCraters ) {
            props.AddRange( iceCraters );
        }
        if ( props.Count > 0 ) {
            return props[ rnd.Next( 0, props.Count ) ].gameObject;
        }
        else {
            return null;
        }
    }

    public GameObject GetCrystalAsset ( System.Random rnd ) {
        if ( crystals.Count > 0 ) {
            return crystals[ rnd.Next( 0, crystals.Count ) ].gameObject;
        }
        else {
            return null;
        }
    }

    public GameObject GetStalagmiteAsset ( System.Random rnd ) {
        if ( stalagmites.Count > 0 ) {
            return stalagmites[ rnd.Next( 0, stalagmites.Count ) ].gameObject;
        }
        else {
            return null;
        }
    }

    public List< Material > GetEarthMaterial ( System.Random rnd ) {
        var materialSets = new List< List< Material > >();
        if ( useRockyEarth ) {
            materialSets.Add( rockyEarth );
        }
        if ( useLayerEarth ) {
            materialSets.Add( layerEarth );
        }
        if ( useFireEarth ) {
            materialSets.Add( fireEarth );
        }
        if ( useIceEarth ) {
            materialSets.Add( iceEarth );
        }
        if ( useToxicEarth ) {
            materialSets.Add( toxicEarth );
        }
        if ( materialSets.Count > 0 ) {
            return materialSets[ rnd.Next( 0, materialSets.Count ) ];
        }
        else {
            return new List< Material >();
        }
    }

}
