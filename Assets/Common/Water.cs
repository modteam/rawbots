using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Water : MonoBehaviour
{

	public GameObject splashAsset;
	public GameObject bubblesAsset;
	public BulletRigidBody bulletRigidBody;
	float underWaterLinearDamping = 0.9f;
	float underWaterAngulerDamping = 0.2f;
	public BulletCompoundShape shape;

	public System.Func<Vector3,Vector3> getSurfacePoint;
	public float GetRadius ()
	{
		return (transform.localScale.x) / 2;
	}
	
//	public Vector3 GetSurfacePoint(Vector3 partPosition ){
//		var surfacePoint = transform.position+transform.up*20;
//
//		Plane surface = new Plane(transform.up,surfacePoint);
//		float distance = 0f;
//		Ray ray = new Ray(partPosition,transform.up);
//		
//		if(surface.Raycast(ray,out distance)){
//			return ray.GetPoint(distance);
//		}
//		
//		return partPosition;
//	}
	

	static float fxThresholdTimer = 0;

	void Start ()
	{
		bulletRigidBody.AddOnTriggerDelegate (OnTrigger);
	}

	public void OnTrigger (Vector3 relativeVelocity, BulletRigidBody other, Vector3 contactPoint, Vector3 lineOfAction)
	{
		if(other.collisionGroup == BulletLayers.Part){
			var part = other.GetComponent<Part> ();
			var subPart = other.GetComponent<Subpart> ();
			if (subPart != null) {
				part = subPart.part;
			}
			part.SetAffectingWater (this, contactPoint, lineOfAction);
		}
	}


//    void _FixedUpdate(){
//
//        for ( int i = 0; i < recentRigidBodies.Count; ++i ) {
//            var recentBody = recentRigidBodies.ElementAt(i);
//            if(!rigidbodies.Contains(recentBody)){
//                TriggerEnter(recentBody);
//            }
//        }
//
//        for ( int i = 0; i < rigidbodies.Count; ) {
//            var storedBody = rigidbodies.ElementAt(i);
//            if(!recentRigidBodies.Contains(storedBody)){
//                TriggerExit( storedBody );
//            }else{
//                ++i;
//            }
//        }
//
//        recentRigidBodies.Clear();
//    }

	public void TriggerEnter (BulletRigidBody btRigidbody, Vector3 contactPoint, Vector3 lineOfAction)
	{
		var velocity = btRigidbody.GetVelocity ();
		var speed = velocity.magnitude;
//		if (btRigidbody.mass != 0) {
//			velocity *= 0.1f;
//			btRigidbody.SetVelocity (velocity);
//		}
		btRigidbody.SetDamping (underWaterLinearDamping, underWaterAngulerDamping);
		if (fxThresholdTimer < Time.time) {
			fxThresholdTimer = Time.time + 0.1f;
			var splashObject = Instantiate (splashAsset) as GameObject;
			splashObject.transform.parent = Gaia.Instance ().scratch;
			splashObject.hideFlags = HideFlags.HideInHierarchy;

			splashObject.transform.up = lineOfAction.normalized;
			splashObject.transform.position = contactPoint;
			var splash = splashObject.GetComponent< WaterSplash > ();
			splash.GetComponent<AudioSource>().volume = speed / 50;
			splash.GetComponent<AudioSource>().pitch = Random.Range (0.9f, 1.2f);
			splash.GetComponent<AudioSource>().Play ();
		}
		var bubblesObject = Instantiate (bubblesAsset) as GameObject;
		bubblesObject.transform.parent = btRigidbody.transform;
		bubblesObject.transform.localPosition = Vector3.zero;
		btRigidbody.SendMessage ("OnWaterEnter", this, SendMessageOptions.DontRequireReceiver);
	}

	public void TriggerExit (BulletRigidBody btRigidbody, Vector3 contactPoint, Vector3 lineOfAction)
	{

		foreach (var bubbles in btRigidbody.GetComponentsInChildren< Bubbles >()) {
			Destroy (bubbles.gameObject);
		}
		if (fxThresholdTimer < Time.time) {
			var velocity = btRigidbody.GetVelocity ();
			var speed = velocity.magnitude;
			fxThresholdTimer = Time.time + 0.1f;
			var splashObject = Instantiate (splashAsset) as GameObject;
           
			splashObject.transform.up = lineOfAction.normalized;
			splashObject.transform.position = contactPoint;
			var splash = splashObject.GetComponent< WaterSplash > ();
			splash.GetComponent<AudioSource>().volume = speed / 50;
			splash.GetComponent<AudioSource>().pitch = Random.Range (0.9f, 1.2f);
			splash.GetComponent<AudioSource>().Play ();
		}
		btRigidbody.SetDamping (bulletRigidBody.linearDamping, bulletRigidBody.angularDamping);
		btRigidbody.SendMessage ("OnWaterExit", this, SendMessageOptions.DontRequireReceiver);
	}
}
