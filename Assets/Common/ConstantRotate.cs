using UnityEngine;
using System.Collections;

public class ConstantRotate : MonoBehaviour {

	public enum Space {
		World,
		Local,
	}
	
	public Vector3 velocity = Vector3.up;	
	new Transform transform;
	public float randomize = 1;
	public Space space = Space.World;
	
	void Awake () {
		transform = gameObject.transform;
	}

	void Update () {
		var r = 1f;
		if ( randomize > 1f ) {
			r = Random.Range( 1f, randomize );
		}
		if ( space == Space.World ) {
			transform.rotation *= Quaternion.Euler( velocity * Time.deltaTime * r );
		}
		else if ( space == Space.Local ) {
			transform.localRotation *= Quaternion.Euler( velocity * Time.deltaTime * r );
		}
	}
}

