using UnityEngine;
using System.Collections;

public class Prop : MonoBehaviour {

    public float hp = 200f;
    public string effect = "CrystalsDestroyed";

    void OnHit ( float damage ) {
        hp -= damage;
        if ( hp < 0 ) {
            var effectInstance = ( GameObject )Instantiate( Resources.Load( "FX/" + effect, typeof( GameObject ) ),
            transform.position, transform.rotation );
            effectInstance.GetComponentInChildren<ParticleSystem>();
            Destroy( gameObject );
        }
    }


}
