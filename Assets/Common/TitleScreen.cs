using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour {

    bool fading = false;
    Color logoColor;
    Color auraColor;
    float musicVolume = 1;
    public Renderer aura;
    public Renderer logo;

    void Start () {

    }

    public bool Fading {
        set {
            fading = value;
            Destroy( gameObject, 4 );
        }
        get { return fading; }
    }

    void Update () {

        // TODO: don't do anythin with this, if you are not making a music system
        if ( Gaia.scene > 0 ) {
            GetComponent<AudioSource>().volume = 0;
        }
        else {
            GetComponent<AudioSource>().volume = musicVolume;
        }
        if ( !fading ) {
            logoColor = logo.material.GetColor( "_TintColor" );
            auraColor = aura.material.GetColor( "_TintColor" );
        }

        if ( fading ) {
            logoColor.a -= Time.deltaTime / 2;
            auraColor.a -= Time.deltaTime / 2;

            musicVolume -= Time.deltaTime / 3;

            logo.material.SetColor( "_TintColor", logoColor );
            aura.material.SetColor( "_TintColor", auraColor );
        }
 
    }
}
