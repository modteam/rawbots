using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;


public class BlastImpact : MonoBehaviour {

	public new Light light;
	public List< AudioClip > clips;

	void Start () {
		Destroy( light.gameObject, 0.1f );
		Destroy( gameObject, 1.5f );
		GetComponent<AudioSource>().clip = clips[ Random.Range( 0, clips.Count ) ];
		GetComponent<AudioSource>().pitch = Random.Range( 0.5f, 1.5f );
		GetComponent<AudioSource>().Play();
	}

}
