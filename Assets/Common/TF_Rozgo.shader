Shader "Rozgo/Part" {

    Properties {
        _RimColor ("Rim Color", Color) = (0.97,0.88,1,0.75)
        _RimPower ("RimPower", Range(0.1,3) ) = 3
        _MainTex ("Diffuse (RGB) Alpha (A)", 2D) = "white" {}
        _ChannelA ("ChannelA", Color) = (1,1,1,1)
        _ChannelB ("ChannelB", Color) = (1,1,1,1)
        _ChannelC ("ChannelC", Color) = (1,1,1,1)
        _BumpMap ("Normal (Normal)", 2D) = "bump" {}
        _SpecularTex ("Specular Level (R) Gloss (G) Rim Mask (B) Illum (A)", 2D) = "black" {}
        _IllumTint ("IllumTint", Color) = (1,1,1,1)
        _IllumPower ("IllumPower", Range(0,25) ) = 0
    }

    SubShader{
        Tags { "RenderType" = "Opaque" }
        
        CGPROGRAM
            #pragma surface surf Custom
            #pragma target 3.0

            struct Input
            {
                float2 uv_MainTex;
                float3 viewDir;
            };
            
            sampler2D _MainTex, _SpecularTex, _BumpMap;
            float4 _RimColor;
            float _RimPower;
            float4 _IllumTint;
            float _IllumPower;
            float4 _ChannelA;
            float4 _ChannelB;
            float4 _ChannelC;

            inline fixed4 LightingCustom_PrePass (SurfaceOutput s, half4 light)
            {
				fixed spec = light.a * s.Gloss;
				fixed4 c;
				c.rgb = ( s.Albedo * light.rgb + light.rgb * spec );
				c.a = s.Alpha + spec * s.Specular;
				return c;
            }

			inline fixed4 LightingCustom (SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
			{
			    half3 h = normalize ( lightDir + viewDir );
			    fixed diff = max ( 0, dot ( s.Normal, lightDir ) );
			    fixed4 c;
			    c.rgb = (s.Albedo * _LightColor0.rgb * diff) * (atten * 2);
			    c.a = s.Alpha + _LightColor0.a * atten;
			    return c;
			}
			
            void surf (Input IN, inout SurfaceOutput o)
            {
				float4 ma = tex2D( _MainTex, IN.uv_MainTex.xy );
				float4 ca = _ChannelA * ma.r;
				float4 cb = _ChannelB * ma.g;
				float4 cc = _ChannelC * ma.b;
				float4 mc = ca * ma.a + cb * ma.a + cc * ma.a;
				mc.rgb = mc.rgb + ma.rgb * ( 1 - ma.a );
				mc.a = 1;
				o.Albedo = mc;

                o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
                
                float4 specGloss = tex2D(_SpecularTex, IN.uv_MainTex);
                o.Specular = specGloss.r;
                o.Gloss = specGloss.g;

                half rim = 1.0 - saturate( dot ( normalize( IN.viewDir ), o.Normal ) );

                o.Emission = pow( rim, _RimPower ) 
                    * _RimColor.rgb * _RimColor.a * specGloss.b + mc.rgb * specGloss.a * _IllumPower * _IllumTint;
            }
        ENDCG
    }
    Fallback "VertexLit"
}