using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class HexStructureTile : HexGridTileTypeHandler {

    static List<HexStructureTile> structureTiles = new List<HexStructureTile>();

    public override void Init (Entity etype) {
        if ( tile.tileType == HexGridTileType.StructureType ) {
            structureTiles.Add(this);
            onConsole = () => {
                tile.ForGrid(grid => {
                    StructureAddonsManager.addOnType = tile.label.text;
                    for(int i = 0 ; i < structureTiles.Count ; ++i){
                        structureTiles[i].SetMaterial();
                    }
                });
            };
        }
        SetMaterial();
    }

    public override void OnDestroyEventHandler () {
        base.OnDestroyEventHandler();
        structureTiles.Remove(this);
    }

    public override void SetMaterial( ) {
        if( tile.label.text == StructureAddonsManager.addOnType ){
            tile.renderer.sharedMaterial = tile.grid.outputMaterial;
        }else{
            tile.renderer.sharedMaterial = tile.grid.inputMaterial;
        }
    }
    
    public override void DeleteStep (bool step) {
        return;
    }
    
	public override void OnMouseEnterCustom (TouchData touch)
	{
		base.OnMouseEnterCustom (touch);
		Builder.instance.selectingElement = true;
	}
	public override void OnMouseExitCustom (TouchData touch)
	{
		base.OnMouseExitCustom (touch);
		Builder.instance.selectingElement = false;
	}
	
    public override void RequestOnConsole () {
        var num8 = Input.GetKey( KeyCode.Alpha8 );
        if(num8 && Gaia.scene > 0){
            onConsole();
        }
    }

    public override void RequestContextualMenu () {
        return;
    }
    
}
