using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class HexGridEmptyTile : HexGridTileTypeHandler {

    public override void Init (Entity etype) {
        tile.tileType = HexGridTileType.Empty;
        onConsole = () => {
            tile.grid.GenerateTilesOperants( tile.tiling );
            var options = Gaia.Instance().entities[ "operant" ].Tails( p => p == "is" )
            .Where( e => !e.Heads( p => p == "hidden_from" ).Any() ).Select( e => e.id ).ToList();
            tile.grid.console.ForInput( "add", "operant", options, input => {
                var availableGrid = FindObjectOfType( typeof( HexGrid ) ) as HexGrid;
                if ( availableGrid != default( HexGrid ) ) {
                    availableGrid.CreateOperant( input, tile.tiling, Vector3.zero );
                }
            } );
        };

        SetMaterial();
    }

    public override void SetMaterial () {
        tile.renderer.sharedMaterial = tile.originalMaterial;
    }

    public override void OnMouseDragCustom (TouchData touch) {
        return;
    }

    public override void OnMouseUpCustom (TouchData touch) {
        base.OnMouseUpCustom(touch);
    }

    public override void RequestContextualMenu () {
        return;
    }

    public override void RequestOnConsole () {
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        if(leftControl && Gaia.scene > 0){
            onConsole();
        }
    }

}
