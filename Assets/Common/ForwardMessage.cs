using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Oracle;
using Operants;

public class ForwardMessage : MonoBehaviour {

    public GameObject receiver;

    void OnTriggerEnter ( Collider collider ) {
        receiver.SendMessage( "OnTriggerEnter", collider, SendMessageOptions.DontRequireReceiver );
    }

    void OnTriggerExit ( Collider collider ) {
        receiver.SendMessage( "OnTriggerExit", collider, SendMessageOptions.DontRequireReceiver );
    }

    void OnHit ( float damage ) {
        receiver.SendMessage( "OnHit", damage, SendMessageOptions.DontRequireReceiver );
    }


    void OnGravityEnter ( GravityField field ) {
        receiver.SendMessage( "OnGravityEnter", field, SendMessageOptions.DontRequireReceiver );
    }

    void OnGravityExit ( GravityField field ) {
        receiver.SendMessage( "OnGravityExit", field, SendMessageOptions.DontRequireReceiver );
    }

}
