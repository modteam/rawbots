using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class WaterSplash : MonoBehaviour {

    public Waveform wave;
    public Transform splash;
    public float amplitude;
    public float duration;
    float timer;
    float seed;

    void Start () {
        timer = Time.time + duration;
        Destroy( gameObject, duration );
        seed = Random.Range( 0f, 1f );
    }
 
    void Update () {
        var time = ( timer - Time.time ) / duration;
        var sample = wave.Sample( Time.deltaTime );
        var side = Mathfx.Sinerp( seed, 4, 1 - time );
        var scale = new Vector3( side, side, sample.sample * time );
        splash.localScale = scale;
//       var color = splash.renderer.material.GetColor( "_TintColor" );
//       color.a = time;
//       splash.renderer.material.SetColor( "_TintColor", color );
    }
}
