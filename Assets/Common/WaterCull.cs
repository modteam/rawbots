using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//By z26

/*This class has two roles:
1.Warning neighbours of the presence of a nearby terrain or water hex.
2.Removing shared faces of water hexes to make the inner walls in water volumes invisible.

While #2 is only relevant for water hexagons, #1 means the class must be asigned to non-water
hexes because they need to warn nearby water hexagons of their presence when they are added.
Otherwise, water/terrain boundaries cause ugly z-fighting. */

public class WaterCull : MonoBehaviour {
	public float height = 10.5f;
    public float apothem = 10f;//https://www.mathsisfun.com/geometry/regular-polygons.html
    public MeshFilter waterMeshFilter = null;//only used by water hexes, but unity seems to
    public bool isWaterHex = false;      //hate missing fields so I always assign it anyway
    public bool warnOthers = true;

    
    //readonly array of all polygons, including those in hidden faces. only used by water hexes.
    private int[] trianglesArray = new int[60];
    //same idea but for the position of vertices.
    private Vector3[] verticesArray = new Vector3[36];

    
    // void Awake () {
    //     //This isnt needed to prevent recursion anymore, maybe a slight optimization.
    //     warnOthers = HexBatcher.instance.initialBatchFinished;
    // }

	void Start () {
        if (isWaterHex) { 
            waterMeshFilter.mesh.triangles.CopyTo(trianglesArray,0);
            waterMeshFilter.mesh.vertices.CopyTo(verticesArray,0);
        }
        //100ms delay to ensure everything is properly initialized before doing raycasts.
        Invoke("CheckNeighbours",0.1f);
	}

	
	void OnDestroy () {
        //Tells the surrounding hexes to check again for your presence once you're gone.
        warnOthers = true;
        CheckNeighbours();
    }
	
	
	private List<int> CheckDirection(int index) {
        //First, select the direction to check with a raycast.
        //For water hexes, also choose what polygons to hide if a neighbour is detected.
        
        var face = new List<int>();
        var direction = Vector3.zero;
        var rayLength = height/2 + 1f; //extend the ray 1 unit past the boundary.
        //dunno if the length is ideal...
            
    
        if (index == 0) {
            direction = transform.up;
            //top face: first 12 vertices (4 triangles)
            if (isWaterHex) { face = trianglesArray.Take(12).ToList();}
        }
        
        if (index == 1) {
            direction = transform.up * -1f;
            //bottom face: 13th to 24th vertices
            if (isWaterHex) { face = trianglesArray.Skip(12).Take(12).ToList(); }
        }
        
        if (index > 1) {
            rayLength = apothem + 1;
            var sideIndex = index - 2;
            
            direction = Quaternion.Euler(60 + sideIndex*60,0,0) * transform.forward;
            //side faces 0 to 5: 6 vertices each
            if (isWaterHex) { face = trianglesArray.Skip(24 + (sideIndex) * 6).Take(6).ToList(); }

        }
        
        
        
        
        //Raycast to check for the presence of another nearby terrain hex (including non-water ones, to avoid z-fighting)
        var ray = new Ray (transform.position, direction);
        BulletRaycastHitDetails hitDetails;
        var hit = Bullet.Raycast(ray, out hitDetails, rayLength, BulletLayers.Camera , BulletLayers.Water | BulletLayers.HexBlock);
        
        
        
        
        
        if (hit) { //delete the vertices of the selected face if you don't want it to stay visible.
            if (isWaterHex) { face.Clear();}
            
            
            if (warnOthers) { //If tellNeighbour = true, make adjacent terrain update its own mesh.
                hitDetails.bulletRigidbody.gameObject.SendMessage( "CheckYourself", SendMessageOptions.DontRequireReceiver);
            }
        }
        
        return face;
    }
     
    void CheckYourself() {
    //Finally fixed recursion problem. HexBatcher.instance.initialBatchFinished didn't fix it completely, cause
    //spawning blueprints (unlike manual terraincrafting) can introduce several hexes at the same time that recurse.
    warnOthers = false;
    CheckNeighbours();
    }

    void CheckNeighbours() { //If tellNeighbours = true, tell adjacent terrain to refresh.
               
        var triangleList = new List<int>();//fill a list with all the triangles that should stay visible.
        for(int i = 0; i <= 7; i++) {
            triangleList.AddRange(CheckDirection(i));
            }
        
        if(isWaterHex) {
            waterMeshFilter.mesh.vertices = verticesArray; //setting the position of vertices is necessary to avoid an annoying issue.
            waterMeshFilter.mesh.triangles = triangleList.ToArray();
        }

        warnOthers = false;    
    }   
	
   
   
   

}

