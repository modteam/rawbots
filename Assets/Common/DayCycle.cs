using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;
using Operants;

public class DayCycle : MonoBehaviour {

    [System.Serializable]
    public class DayCycleDef {
        public string name;
        public float duration;
        public Color directColor;
        public float directIntensity;
        public float IntensityMultiplier;
        public Color ambientColor;
        public Color fogColor;
        public Color auroraColor;
    }

    public List< DayCycleDef > cycles = new List< DayCycleDef >();
    public static float globalTime = 0;
    public Light directLight;
    public Renderer aurora;
    public float timeScale = 1;
    public Color directColor;
    public float directIntensity;
    public float IntensityMultiplier = 1;
    public Color ambientColor;
    public Color fogColor;
    public Color auroraColor;
    public string status;

    int IndexOfCycle ( float time ) {
        var dayDuration = 0f;
        for ( var i = 0; i < cycles.Count; ++i ) {
            dayDuration += cycles[ i ].duration;
            if ( time < dayDuration ) {
                return i;
            }
        }
        return -1;
    }

    float DayDuration () {
        var dayDuration = 0f;
        for ( var i = 0; i < cycles.Count; ++i ) {
            dayDuration += cycles[ i ].duration;
        }
        return dayDuration;
    }

    float StartOfCycle ( int idx ) {
        var startTime = 0f;
        for ( var i = 0; i < idx; ++i ) {
            startTime += cycles[ i ].duration;
        }
        return startTime;
    }

    void Start () {
    }

    void Update () {

        // if ( !DevTools.IsOptionEnabled( DevTools.Options.DayCycleSuppressed ) ) {
        //     globalTime += Time.deltaTime * timeScale;
        // }

        // prevents !IsNormalized
        // TODO: figure out why
        if ( Mathf.Approximately( globalTime, 0 ) ) {
            globalTime = 0.001f;
        }

        var dayDuration = DayDuration();
        var dayTime = globalTime % dayDuration;
        var cycleIdx = IndexOfCycle( dayTime );
        var lhs = cycleIdx;
        var rhs = ( cycleIdx + 1 ) >= cycles.Count ? 0 : cycleIdx + 1;
        var start = StartOfCycle( lhs );
        var interpolated = ( dayTime - start ) / cycles[ lhs ].duration;

        directColor = HSBColor.Lerp( HSBColor.FromColor( cycles[ lhs ].directColor ),
            HSBColor.FromColor( cycles[ rhs ].directColor ), interpolated ).ToColor();
        ambientColor = HSBColor.Lerp( HSBColor.FromColor( cycles[ lhs ].ambientColor ),
            HSBColor.FromColor( cycles[ rhs ].ambientColor ), interpolated ).ToColor();
        fogColor = HSBColor.Lerp( HSBColor.FromColor( cycles[ lhs ].fogColor ),
            HSBColor.FromColor( cycles[ rhs ].fogColor ), interpolated ).ToColor();
        auroraColor = HSBColor.Lerp( HSBColor.FromColor( cycles[ lhs ].auroraColor ),
            HSBColor.FromColor( cycles[ rhs ].auroraColor ), interpolated ).ToColor();

        directIntensity = Mathf.Lerp( cycles[ lhs ].directIntensity, cycles[ rhs ].directIntensity, interpolated );
        directLight.color = directColor;
        directLight.intensity = directIntensity * IntensityMultiplier;
        RenderSettings.ambientLight = ambientColor;
        RenderSettings.fogColor = fogColor;
        aurora.material.SetColor( "_TintColor", auroraColor );

        directLight.transform.localRotation = Quaternion.Euler( 360 * dayTime / dayDuration, 15, 15 );

        status = cycles[ lhs ].name + " > " + cycles[ rhs ].name + "  t:" + interpolated;
    }

}
