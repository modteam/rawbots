﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*by z26
This script displays a description when the mouse is hovering on top of an operand.
I want to make visual programming more powerful in the future but more powerful oftens equals more complicated.
Hopefully this feature can allow for newbies to only need a simple tutorial to get into rawbots, despite that

This version is incomplete cause I switched to a 3d version of the script. I kept this version just in case I change my mind.*/

public class TileDescript2D : MonoBehaviour {


	RectTransform rectangle;
	Camera overlayCam;
	public Font font;
	public int fontSize;
	GUIStyle style = new GUIStyle();

	void Start () {
	// Texture2D background = new Texture2D(1, 1);
	// background.SetPixel
	//overlayCam = GetComponentInParent<Camera>();
	style.font = font;
	style.fontSize = fontSize;
	style.normal.textColor = Color.white;
	}
	
	void OnGUI () {
		//the usual input.mousePosition = screen coordinates, while Event.current.mousePosition = GUI space coordinates
		Vector3 pos = Event.current.mousePosition;
		GUI.Label(new Rect(pos.x, pos.y, pos.x+50, pos.y+50),"This is an sized label", style);
	}
}