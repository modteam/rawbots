using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oracle;

public class HexAdderTile : HexGridTileTypeHandler {

    public override void Init (Entity etype) {
        if ( tile.tileType == HexGridTileType.OperantAdder ) {
            tile.originalIconColor = ApplyAlphaFactor(tile.iconRenderer,0.5f);
                onConsole = () => {
                    tile.ForGrid(grid => {
                        var newTiling = tile.targetTiling;
                        newTiling.z = 0;
                        newTiling = grid.FindAvailableSpaceAround( newTiling, true );
                        grid.CreateOperant( tile.entity.id, newTiling, tile.tiling );
                    });
                };
            }
        else if ( tile.tileType == HexGridTileType.InputAdder ) {
                tile.entity.ForProperty< string >( "property", value => tile.label.text = value );
                onConsole = () => {
                    tile.ForGrid(grid => {
                        var newTiling = tile.tiling;
                        newTiling.z = 0;
                        newTiling = grid.FindAvailableSpaceAround( newTiling, true );
                        grid.CreateInput( tile.entity, tile.ioTarget.entity, newTiling, tile.ioTarget.tiling );
                        tile.Hide();
                    });
                };
            }
        else if ( tile.tileType == HexGridTileType.OutputAdder ) {
                tile.entity.ForProperty< string >( "property", value => tile.label.text = value );
                onConsole = () => {
                    tile.ForGrid(grid => {
                        var newTiling = tile.tiling;
                        newTiling.z = 0;
                        newTiling = grid.FindAvailableSpaceAround( newTiling, true );
                        grid.CreateOutput( tile.entity, tile.ioTarget.entity, newTiling, tile.ioTarget.tiling );
                        tile.Hide();
                    });
                };
            }
        else if ( tile.tileType == HexGridTileType.PartAdder ) {
                onConsole = () => {
                    tile.ForGrid(grid => {
                        tile.grid.console.SpawnPart( tile.entity.id, tile.spawnPosition, tile.spawnRotation );
                    });
                };
            }
        SetMaterial();
    }

    public override void SetMaterial( ) {
        if ( tile.tileType == HexGridTileType.PartAdder ) {
            tile.renderer.sharedMaterial = tile.grid.partMaterial;
        }
        else if (tile.tileType == HexGridTileType.OperantAdder ) {
            tile.renderer.sharedMaterial = tile.grid.operantMaterial;
        }
        else if (tile.tileType == HexGridTileType.InputAdder ) {
            tile.renderer.sharedMaterial = tile.grid.inputMaterial;
        }
        else if (tile.tileType == HexGridTileType.OutputAdder ) {
            tile.renderer.sharedMaterial = tile.grid.outputMaterial;
        }
    }

    public override void DeleteStep (bool step) {
        return;
    }

    public override void RequestContextualMenu () {
        return;
    }

    public override void RequestOnConsole () {
        var leftControl = Input.GetKey( KeyCode.LeftControl );
        if(leftControl && Gaia.scene > 0){
            onConsole();
        }
    }

}
