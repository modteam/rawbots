using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Oracle {

    public class QuickLGL {
        /*
        Regex subPreObj = new Regex( @"^\+\ [a-z][a-z0-9_]+\ [a-z][a-z0-9_]+\ [a-z][a-z0-9_]+" );
        Regex subProVal = new Regex( @"^\+\ [a-z][a-z0-9_]+\ \.\ [a-z][a-z0-9_]+\ .*" );
        public Dictionary< string, Entity > entities = new Dictionary< string, Entity >();

        static int IndexOfValue ( string input, char chr, int start, int count ) {
            int index = input.IndexOf( chr, start );
            if ( count > 0 ) {
                return IndexOfValue( input, chr, ++index, --count );
            }
            return index + 1;
        }

        Entity GetEntity ( Graph graph, string id ) {
            Entity entity;
            if ( !entities.TryGetValue( id, out entity ) ) {
                entity = new Entity( id );
                graph.AddEntity_Fast( entity );
                entities[ id ] = entity;
            }
            return entity;
        }

        public IEnumerable< string > Execute ( Graph graph, string expression ) {
            if ( !expression.StartsWith( "+ " ) ) {
                expression = "+ " + expression;
            }
            expression = expression.Trim();
            if ( subPreObj.IsMatch( expression ) ) {
                var parts = expression.Split( ' ' );
                var sub = GetEntity( graph, parts[1] );
                var obj = GetEntity( graph, parts[3] );
                var pre = parts[2];
                if ( !sub.Heads( p => p == pre ).Where( h => h == obj ).Any() ) {
                    var relationship = new Relationship( sub, pre, obj );
                    graph.relationships.Add( relationship );
					graph.AddEdge(relationship, relationship.Tail, relationship.Head);
                }
                
                yield return expression;
            }
            else if ( subProVal.IsMatch( expression ) ) {
                var parts = expression.Split( ' ' );
                var value = expression.Substring( IndexOfValue( expression, ' ', 0, 3 ) );
                var entity = GetEntity( graph, parts[1] );
                float number;
                if ( float.TryParse( value, out number ) ) {
                    entity.SetProperty( parts[3], number );
                }
                else {
                    entity.SetProperty( parts[3], value );
                }
                yield return expression;
            }
        }*/

    }
}
