using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace Oracle {

    public partial class Relationship : IProperties {

        public string id;
        Entity tail;

        public Entity Tail {
            get { return tail; }
        }

        Entity head;

        public Entity Head {
            get { return head; }
        }

        public IEnumerable< Entity > Entities () {
            yield return Tail;
            yield return Head;
        }

        string predicate;

        public string Predicate {
            get { return predicate; }
        }

        public List< Property > properties = new List< Property >();

        object GetProperty< T > ( string name ) {
            return properties
                .Where( p => p.Name == name && p.Value.GetType() == typeof( T ) )
                .Select( p => p.Value )
                .FirstOrDefault();
        }

        public void ForProperty< T > ( string name, System.Action< T > action ) {
            var property = GetProperty< T >( name );
            if ( property != default( object ) ) {
                action( ( T )property );
            }
        }

        public void SetProperty ( string name, object value ) {
            var property = properties.Where( p => p.Name == name ).FirstOrDefault();
            if ( property == default( Property ) ) {
                property = new Property( name, value );
                properties.Add( property );
            }
            else {
                property.Name = name;
                property.Value = value;
            }
        }

        public Property RemoveProperty ( string name ) {
            var property = properties.Where( p => p.Name == name ).FirstOrDefault();
            if ( property != default( Property ) ) {
                properties.Remove( property );
            }
            return property;
        }

        public static string GenerateId () {
            return "r_" + ( uint )Guid.NewGuid().GetHashCode();
        }

        public Relationship () {
            id = GenerateId();
        }

        public Relationship ( Entity tail, string predicate, Entity head ) : this() {
            this.predicate = predicate;
            Connect( tail, head );
        }
        
        public override string ToString (){
            return string.Format ("{0} {1} {2}", Tail.id, Predicate, Head.id);
        }

    }
}

