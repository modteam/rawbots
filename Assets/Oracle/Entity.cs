using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

//lots of interesting stuff in here, will take some time for me to grasp how it all works -z26

namespace Oracle {
//Oracle is a brand of databases that has something called the Entity Framework. The Proxies/Entities concepts used
//in rawbots is inspired from that. In Rawbots, proxies holds some of the logic/code that pertains parts and
//all the code used by operants, While entities are used to store and manipulate game data.
//(blueprints, vp programs and maps.)

    [System.Serializable]
    public partial class  Entity : IProperties {
    //"partial" allows to spread the definition of a single class into several files (go look at Relationship.cs)
    //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/partial-classes-and-methods
        
    //a Relationship is an association composed of two entities and a predicate (a string) that describes its nature.
    //In the line "camera01 local_to xform01, "local_to" is the predicate while counterintuively, the first entity
    //is the tail, not the head. (probably because hiearchically, the head is above the tail.)

    //"camera01 as camera" also is a relationship, since "camera" is just a normal entity that's loaded in memory
    //from bot.txt, then is used as a template to define all cameras in the world.
        
    //lines with points in them are properties. (example: camera01 . position [0,0,0])
    //I wonder if all lines in map/bp files are either properties or relationships.
    //(I think even "camera01 was unlocked" is a relationship between camera01 and an entity called "unlocked") 
        

        public string id;
        public uint instance = 0;
        public NodeView nodeView;
        public Operants.NodeProxy proxy;
        public bool deprecated = false;
        internal List< Relationship > relationships = new List< Relationship >();
        
        public IEnumerable< Relationship > Relationships () {
            foreach ( var relationship in relationships ) {
                yield return relationship;
            }
        }

        IEnumerable< Entity > Neighbors ( System.Predicate<string> filter ) {
            foreach ( var r in relationships
                .Where( r => filter(r.Predicate) && r.Head == this).Select( r => r.Tail ) ) {
                yield return r;
            }
            foreach ( var r in relationships
                .Where( r => filter(r.Predicate) && r.Tail == this).Select( r => r.Head ) ) {
                yield return r;
            }
        }

        public List< Property > properties = new List< Property >();
     
        
        
        public IEnumerable< Entity > Heads ( System.Predicate< string > predicate ) {
            return relationships
                .Where( r => r.Tail == this && predicate( r.Predicate ) )
                .Select( r => r.Head );
        }

        public IEnumerable< Entity > Tails ( System.Predicate< string > predicate ) {
            return relationships
                .Where( r => r.Head == this && predicate( r.Predicate ) )
                .Select( r => r.Tail );
        }

        IEnumerable< Entity > This () {
            yield return this;
        }
  
        public Entity InmediateParent () {
            return Relationships().Where( r => r.Predicate == "as" && r.Tail == this ).Select( r => r.Head ).FirstOrDefault();
        }

        public void ForInferredProperty< T > ( string name, System.Predicate< string > predicate, System.Action< T > action ) {
            var visited = new List< Entity >();
            bool recurse = true;
            var heads = This();
            while ( recurse ) {
                recurse = false;
                foreach ( var head in heads ) {
                    if ( visited.Any( e => e == head ) ) {
                        continue;
                    }
                    visited.Add( head );
                    var p = head.GetProperty< T >( name );
                    if ( p != default( object ) ) {
                        action( ( T )p );
                        return;
                    }
                    var parents = head.Relationships()
                        .Where( r => r.Tail == head && predicate( r.Predicate ) )
                        .Select( r => r.Head );
                    if ( parents.Count() > 0 ) {
                        heads = parents;
                        recurse = true;
                    }
                }
            }
        }

        public void ForInferredHead ( string name, System.Predicate< string > predicate, System.Action< Entity > action ) {
            var visited = new List< Entity >();
            bool recurse = true;
            var heads = This();
            while ( recurse ) {
                recurse = false;
                foreach ( var head in heads ) {
                    if ( visited.Any( e => e == head ) ) {
                        continue;
                    }
                    visited.Add( head );
                    if ( head.id == name ) {
                        action( head );
                        return;
                    }
                    else {
                        var parents = head.Relationships()
                        .Where( r => r.Tail == head && predicate( r.Predicate ) )
                        .Select( r => r.Head );
                        if ( parents.Count() > 0 ) {
                            heads = parents;
                            recurse = true;
                        }
                    }
                }
            }
        }

        public void ForHeads ( System.Predicate< string > predicate, System.Action< Entity > action ) {
            var visited = new List< Entity >();
            bool recurse = true;
            var heads = This();
            while ( recurse ) {
                recurse = false;
                foreach ( var head in heads ) {
                    if ( visited.Any( e => e == head ) ) {
                        continue;
                    }
                    visited.Add( head );
                    action( head );
                    var parents = head.Relationships()
                    .Where( r => r.Tail == head && predicate( r.Predicate ) )
                    .Select( r => r.Head );
                    if ( parents.Count() > 0 ) {
                        heads = parents;
                        recurse = true;
                    }
                }
            }
        }

        public IEnumerable<Entity> Entities ( System.Predicate<string> filter ) {
            var entities = new List< Entity >();
            foreach ( var e in Entities( entities,filter ) ) {
                yield return e;
            }
        }

        IEnumerable<Entity> Entities ( List<Entity> entities, System.Predicate<string> filter ) {
            if ( !entities.Contains( this ) ) {
                entities.Add( this );
                yield return this;
                foreach ( var n in Neighbors(filter) ) {
                    foreach ( var e in n.Entities(entities,filter) ) {
                        yield return e;
                    }
                }
            }
        }
        
        public IEnumerable<Entity> EntitiesToSave ( System.Predicate<string> filter ) {
            var entities = new List< Entity >();
            foreach ( var e in EntitiesToSave( entities, filter ) ) {
                yield return e;
            }
        }

        // TODO: functions here should not know about "slot_for" or "extends", breaks arquiteture
        // all these functions should be fully independent of graph purpose
        IEnumerable<Entity> EntitiesToSave ( List<Entity> entities, System.Predicate<string> filter ) {
            if ( !entities.Contains( this ) ) {
                entities.Add( this );
                if ( Relationships().Any( r => r.Predicate == "slot_for" && r.Tail == this ) ) {
                    if ( Relationships().Any( r => r.Predicate == "extends" ) ) {
                        yield return this;
                    }
                }
                else {
                    yield return this;
                }
                foreach ( var n in Neighbors(filter) ) {
                    foreach ( var e in n.Entities(entities,filter) ) {
                        if ( e.Relationships().Any( r => r.Predicate == "slot_for" && r.Tail == e ) ) {
                            if ( e.Relationships().Any( r => r.Predicate == "extends" ) ) {
                                yield return e;
                            }
                        }
                        else {
                            yield return e;
                        }
                    }
                }
            }
        }

        public object GetProperty< T > ( string name ) {
            if ( typeof( T ) == typeof( Color ) ) {
                var stringValue = properties
                    .Where( p => p.Name == name && p.Value.GetType() == typeof( string ) )
                    .Select( p => p.Value )
                    .FirstOrDefault();
                if ( stringValue == null ) {
                    return default( T );
                }
                else {
                    return Util.PropertyToColor( stringValue as string );
                }
            }
            else {
                return properties
                    .Where( p => p.Name == name && p.Value.GetType() == typeof( T ) )
                    .Select( p => p.Value )
                    .FirstOrDefault();
            }
        }

        public void ForProperty< T > ( string name, System.Action< T > action ) {
            var property = GetProperty< T >( name );
            if ( property != default( object ) ) {
                action( ( T )property );
            }
        }

        public void SetProperty ( string name, object value ) {
            var property = properties.Where( p => p.Name == name ).FirstOrDefault();
            if ( property == default( Property ) ) {
                property = new Property( name, value );
                properties.Add( property );
            }
            else {
                property.Name = name;
                property.Value = value;
            }
        }

        public Property RemoveProperty ( string name ) {
            var property = properties.Where( p => p.Name == name ).FirstOrDefault();
            if ( property != default( Property ) ) {
                properties.Remove( property );
            }
            return property;
        }

        public static string GenerateId () {
            return "e_" + ( uint )System.Guid.NewGuid().GetHashCode();
        }

        public string Serialize ( System.Predicate<string> filter ) {
            
            var str = "################################################\n";
            
            properties.ForEach( p => str += id + " " + p.ToString() + "\n" );
            
            foreach ( var r in relationships.Where(r => filter(r.Predicate) && r.Tail == this) ) {
                str += r.ToString() + "\n";
            }
            
            return str;
        }


        public List<string> MangledSerialize ( System.Predicate<string> filter ) {
            //Changed the formatting to match Serialize() better -z26

            var payload = new List<string>();
            payload.Add("################################################");

            foreach(var p in properties) {
            payload.Add(Remangle(id) + " " + p.ToString());
            }
            
            foreach ( var r in relationships.Where(r => filter(r.Predicate) && r.Tail == this) ) {
                payload.Add(string.Format ("{0} {1} {2}", Remangle(r.Tail.id), r.Predicate, Remangle(r.Head.id)) );
            }

            payload.Add("");
            
            return payload;
        }

        string Remangle(string id){
            var index = id.IndexOf( '@' );
            if ( index != -1 ) {
                var mangle = id.Substring(index);
                id = id.Remove( index );
                mangle = ((uint) mangle.GetHashCode()).ToString();
                id = id + "@" + mangle;
            }
            return id;
        }

        public override string ToString () {
            
            var str = id + "\n";
            
            foreach ( var p in properties ) {
                str += id + " " + p.ToString() + "\n";
            }
            
            foreach ( var r in relationships ) {
                str += r.ToString() + "\n";
            }
            
            return str;
        }
        
        public Entity () {
            id = GenerateId();
        }

        public Entity ( string id ) {
            this.id = id;
        }

        public void MangleId () {
            var index = id.IndexOf( '@' );
            if ( index != -1 ) {
                id = id.Remove( index );
            }
            instance = ( uint )GetHashCode();
            id = id + "@" + instance;
        }

    }

    public partial class Relationship {

        public void Connect ( Entity tail, Entity head ) {
            this.tail = tail;
            this.head = head;
            tail.relationships.Add( this );
            head.relationships.Add( this );
        }

        public void Break () {
            tail.relationships.Remove( this );
            head.relationships.Remove( this );
            tail = default( Entity );
            head = default( Entity );
        }

    }
}




