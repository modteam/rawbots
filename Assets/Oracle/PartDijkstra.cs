using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Oracle;
using Operants;

public class PartDijkstra {
 
    private static int infinity = 1000; ////Parametrizable
    
    public static IEnumerator GetPathFor ( 
        List< PartProxy > entities, PartProxy origin, PartProxy destination, Action< List< PartProxy > > callback ) {

        Dictionary< PartProxy, int > distances = new Dictionary< PartProxy, int >();
        Dictionary< PartProxy, PartProxy > previous = new Dictionary< PartProxy, PartProxy >();
        List< PartProxy > entitySet = new List<PartProxy>();
        int alt;
        
        foreach ( var entity in entities ) {
            distances.Add( entity, infinity );
            previous.Add( entity, default(PartProxy) );
            entitySet.Add( entity );
        }
        
        yield return 0;
        
        distances[ origin ] = 0;
        
        while ( entitySet.Count > 0 ) {
            var u = GetWSmallestDistance( entitySet, distances );
            if ( distances[ u ] == infinity ) {
                break;
            }
            entitySet.Remove( u );
            
            var neighbors = u.Neighbors().ToList();
            
            foreach ( var neighbor in neighbors ) {
                alt = distances[ u ] + GetNeighborWeight( neighbor, u );
                if ( !distances.ContainsKey( neighbor ) ) {
                    yield return 0;
                    continue;
                }
                if ( alt < distances[ neighbor ] ) {
                    distances[ neighbor ] = alt;
                    previous[ neighbor ] = u;
                }
                
                yield return 0;
            }
            
            yield return 0;
        }
        
        var path = new List< PartProxy >(); 
        
        while ( previous[ destination ] != default( PartProxy ) ) {
            path.Add( destination );
            destination = previous[ destination ];
            
            yield return 0;
        }

        path.Add( origin );
        path.Reverse();
        
        callback( path );
        
        yield return 0;
    }
    
    private static int GetNeighborWeight ( PartProxy fromPart, PartProxy toPart ) {
        //TODO: Implement weight for neighbours
        return 1;
    }
    
    private static PartProxy GetWSmallestDistance ( List< PartProxy > entitySet, Dictionary< PartProxy, int > distances ) {
        
        int smallest = distances[ entitySet.First() ];
        PartProxy closets = default(PartProxy);
        
        foreach ( var entity in entitySet ) {
            if ( smallest > distances[ entity ] ) {
                smallest = distances[ entity ];
            }
        }
        
        foreach ( var key in distances.Keys ) {
            if ( distances[ key ] == smallest && entitySet.Any( e => e == key ) ) {
                closets = key;
            }
        }
        
        return closets;
    }
    
    
    
}