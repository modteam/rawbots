﻿using System.Diagnostics;
using UnityEngine;

namespace Oracle {

    public enum WaveType {

        Sine,
        Square,
        Triangle,
        Sawtooth,
        Pulse,

        // random between -1 and 1
        WhiteNoise,

        // random between -1 and 1 with normal distribution
        GaussNoise,

        // random 0 or 1
        DigitalNoise,
    }

    public enum WaveInvert {
        False,
        True,
    }

    public struct Sample {
        public float sample;
        public float inverted;
    }

    [System.Serializable]
    public class Waveform {

        public WaveType type = WaveType.Sine;
        public float frequency = 1f;
        public float phase = 0f;
        public float amplitude = 0f;
        public float offset = 0f;
        public WaveInvert invert = WaveInvert.False;
        System.Random random = new System.Random();
        public float time = 0;

        public Sample Sample () {

            Sample output;
            output.sample = 0;
            output.inverted = 0;
            float value = 0;
            float t = frequency * time + phase;

            // http://en.wikipedia.org/wiki/Waveform
            switch ( type ) {

            // sin( 2 * pi * t )
                case WaveType.Sine:
                    value = Mathf.Sin( 2f * Mathf.PI * t );
                    break;

            // sign( sin( 2 * pi * t ) )
                case WaveType.Square:
                    value = Mathf.Sign( Mathf.Sin( 2f * Mathf.PI * t ) );
                    break;

            // 2 * abs( t - 2 * floor( t / 2 ) - 1 ) - 1
                case WaveType.Triangle:
                    value = 1f - 4f * Mathf.Abs( Mathf.Round( t - 0.25f ) - ( t - 0.25f ) );
                    break;

            // 2 * ( t/a - floor( t/a + 1/2 ) )
                case WaveType.Sawtooth:
                    value = 2f * ( t - Mathf.Floor( t + 0.5f ) );
                    break;

            // http://en.wikipedia.org/wiki/Pulse_wave
                case WaveType.Pulse:
                    value = ( Mathf.Abs( Mathf.Sin( 2 * Mathf.PI * t ) ) < 1.0 - 10E-3 ) ? ( 0 ) : ( 1 );
                    break;

            // http://en.wikipedia.org/wiki/White_noise
                case WaveType.WhiteNoise:
                    value = 2f * ( float )random.Next( int.MaxValue ) / int.MaxValue - 1f;
                    break;

            // http://en.wikipedia.org/wiki/Gaussian_noise
                case WaveType.GaussNoise:
                    value = StatisticFunction.NORMINV( ( float )random.Next( int.MaxValue ) / int.MaxValue, 0.0f, 0.4f );
                    break;

            // Binary Bit Generators
                case WaveType.DigitalNoise:
                    value = random.Next( 2 );
                    break;
            }

            output.sample = ( invert == WaveInvert.True ? -1 : 1 ) * amplitude * value + offset;
            output.inverted = ( invert == WaveInvert.True ? 1 : -1 ) * amplitude * value + offset;

            return output;
        }

        public Sample Sample ( float dt ) {
            Sample output;
            output = Sample();
            time += dt;
            return output;
        }

        public void Reset () {
            time = 0;
        }

        public void Synchronize ( Waveform waveform ) {
            time = waveform.time;
        }

        public static Waveform CloneWave ( Waveform origin ) {

            var newWaveform = new Waveform();
            newWaveform.amplitude = origin.amplitude;
            newWaveform.frequency = origin.frequency;
            newWaveform.invert = origin.invert;
            newWaveform.offset = origin.offset;
            newWaveform.phase = origin.phase;
            newWaveform.type = origin.type;

            return newWaveform;

        }

    }
 
    public class StatisticFunction {
        // http://geeks.netindonesia.net/blogs/anwarminarso/archive/2008/01/13/normsinv-function-in-c-inverse-cumulative-standard-normal-distribution-function.aspx
        // http://home.online.no/~pjacklam/notes/invnorm/impl/misra/normsinv.html
     
        public static float Mean ( float[] values ) {
            float tot = 0;
            foreach ( float val in values )
                tot += val;

            return ( tot / values.Length );
        }
     
        public static float StandardDeviation ( float[] values ) {
            return Mathf.Sqrt( Variance( values ) );
        }

        public static float Variance ( float[] values ) {
            float m = Mean( values );
            float result = 0;
            foreach ( float d in values )
                result += Mathf.Pow( ( d - m ), 2 );

            return ( result / values.Length );
        }
     
        //
        // Lower tail quantile for standard normal distribution function.
        //
        // This function returns an approximation of the inverse cumulative
        // standard normal distribution function.  I.e., given P, it returns
        // an approximation to the X satisfying P = Pr{Z <= X} where Z is a
        // random variable from the standard normal distribution.
        //
        // The algorithm uses a minimax approximation by rational functions
        // and the result has a relative error whose absolute value is less
        // than 1.15e-9.
        //
        // Author:      Peter J. Acklam
        // (Javascript version by Alankar Misra @ Digital Sutras (alankar@digitalsutras.com))
        // Time-stamp:  2003-05-05 05:15:14
        // E-mail:      pjacklam@online.no
        // WWW URL:     http://home.online.no/~pjacklam
     
        // An algorithm with a relative error less than 1.15*10-9 in the entire region.
     
        public static float NORMSINV ( float p ) {
            // Coefficients in rational approximations
            float[] a = { -39.696830f, 220.946098f, -275.928510f, 138.357751f, -30.664798f, 2.506628f };
         
            float[] b = { -54.476098f, 161.585836f, -155.698979f, 66.801311f, -13.280681f };

            float[] c = { -0.007784894002f, -0.32239645f, -2.400758f, -2.549732f, 4.374664f, 2.938163f };

            float[] d = { 0.007784695709f, 0.32246712f, 2.445134f, 3.754408f };
         
            // Define break-points.
            float plow = 0.02425f;
            float phigh = 1 - plow;
         
            // Rational approximation for lower region:
            if ( p < plow ) {
                float q = Mathf.Sqrt( -2 * Mathf.Log( p ) );
                return ( ( ( ( ( c[ 0 ] * q + c[ 1 ] ) * q + c[ 2 ] ) * q + c[ 3 ] ) * q + c[ 4 ] ) * q + c[ 5 ] ) /
                 ( ( ( ( d[ 0 ] * q + d[ 1 ] ) * q + d[ 2 ] ) * q + d[ 3 ] ) * q + 1 );
            }
         
            // Rational approximation for upper region:
            if ( phigh < p ) {
                float q = Mathf.Sqrt( -2 * Mathf.Log( 1 - p ) );
                return -( ( ( ( ( c[ 0 ] * q + c[ 1 ] ) * q + c[ 2 ] ) * q + c[ 3 ] ) * q + c[ 4 ] ) * q + c[ 5 ] ) /
                 ( ( ( ( d[ 0 ] * q + d[ 1 ] ) * q + d[ 2 ] ) * q + d[ 3 ] ) * q + 1 );
            }
         
            // Rational approximation for central region:
            {
                float q = p - 0.5f;
                float r = q * q;
                return ( ( ( ( ( a[ 0 ] * r + a[ 1 ] ) * r + a[ 2 ] ) * r + a[ 3 ] ) * r + a[ 4 ] ) * r + a[ 5 ] ) * q /
                 ( ( ( ( ( b[ 0 ] * r + b[ 1 ] ) * r + b[ 2 ] ) * r + b[ 3 ] ) * r + b[ 4 ] ) * r + 1 );
            }
        }

        public static float NORMINV ( float probability, float mean, float standard_deviation ) {
            return ( NORMSINV( probability ) * standard_deviation + mean );
        }

        public static float NORMINV ( float probability, float[] values ) {
            return NORMINV( probability, Mean( values ), StandardDeviation( values ) );
        }





     
    }
 
}
