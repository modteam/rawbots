using UnityEngine;
using System.Collections;

public class GravityFieldPlane : MonoBehaviour {
	
	public Bounds bounds;
	
	public Vector3 up;
	public float height;
	
	private Plane[] plane;
	private Vector3[] planePos;
	public enum GravityType{
		
		Center,
		Up,
		Side,
		Corner
		
	}
	public GravityType type = GravityType.Up;
	Plane upPlane;
	Ray upRay;
	public Vector3 GetNormal(Vector3 normal){
		
		if(type == GravityType.Up){
			
			return up*-1f;
		}
		return normal;
		
	}
	
	public void SetPlane(){
		plane = new Plane[6];
		planePos = new Vector3[6];
		plane[0] = new Plane(transform.up, transform.parent.position +transform.up*height/2.0f);
		planePos[0] = transform.parent.position +transform.up*height/2.0f; 
		
		plane[1] = new Plane(transform.up*-1f, transform.parent.position +transform.up*-1f*height/2.0f);
		planePos[1] = transform.parent.position +transform.up*-1f*height/2.0f;
		
		plane[2] = new Plane(transform.right, transform.parent.position +transform.right*height/2.0f);
		planePos[2] = transform.parent.position +transform.right*height/2.0f;
		
		plane[3] = new Plane(transform.right*-1f, transform.parent.position +transform.right*-1f*height/2.0f);
		planePos[3] = transform.parent.position +transform.right*-1f*height/2.0f;
		
		plane[4] = new Plane(transform.forward, transform.parent.position +transform.forward*height/2.0f);
		planePos[4] = transform.parent.position +transform.forward*height/2.0f;
		
		plane[5] = new Plane(transform.forward*-1f, transform.parent.position +transform.forward*-1f*height/2.0f);
		planePos[5] = transform.parent.position +transform.forward*-1f*height/2.0f;

        upPlane = new Plane(up, transform.parent.position /*+up*height/2.0f*/ );
		upRay = new Ray(Vector3.zero,up*-1f);
	}
	
	public Vector3 GetCenterFromOutSide(Vector3 testPoint){
		int closest = 0;
			float closestDistance = 0;
			for(int i = 0; i< 6; i++){
				if(i == 0){
					closestDistance = Vector3.SqrMagnitude(planePos[i]-testPoint);
				}else{
					var testDistance = Vector3.SqrMagnitude(planePos[i]-testPoint);
					if(testDistance < closestDistance){
						closestDistance = testDistance;
						closest = i;
					}
				}
			}
			
			Ray ray = new Ray(testPoint,Util.FastNormalize(transform.parent.position - testPoint));
			float distance = 0;
			Vector3 innerCubeSurfacePoint = Vector3.zero;
			if(plane[closest].Raycast(ray,out distance)){
				innerCubeSurfacePoint = ray.GetPoint(distance);
				Debug.DrawLine(testPoint,innerCubeSurfacePoint,Color.red);
				var surfaceToCenterDistance = Vector3.Distance(innerCubeSurfacePoint , transform.parent.position);
				var testPointToCenterDistance = Vector3.Distance(testPoint , transform.parent.position);
				
				var fakeDistance = (testPointToCenterDistance/surfaceToCenterDistance) *(height/2.0f);
				var fakeCenter = testPoint + ray.direction*-1*fakeDistance;
				//Debug.Log("fake center "+fakeCenter + " "+ Time.time + " "+height + " "+ (testPointToCenterDistance/surfaceToCenterDistance) );
				return fakeCenter;
			}
			return transform.parent.position;
		
	}
	
	public Vector3 GetCenter(Vector3 testPoint){
		
		if(type == GravityType.Up){
			//Plane plane = new Plane(up, transform.parent.position /*+up*height/2.0f*/ );
			//Ray ray = new Ray(testPoint,up*-1f);
            upRay.origin = testPoint;
			float distance = 0f;
			if(upPlane.Raycast(upRay,out distance)){
				return  upRay.GetPoint(distance);
			}
		}
		else if(type == GravityType.Center){
			int closest = 0;
			float closestDistance = 0;
			for(int i = 0; i< 6; ++i){
				if(i == 0){
					closestDistance = Vector3.SqrMagnitude(planePos[i]-testPoint);
				}else{
					var testDistance = Vector3.SqrMagnitude(planePos[i]-testPoint);
					if(testDistance < closestDistance){
						closestDistance = testDistance;
						closest = i;
					}
				}
			}
			
			Ray ray = new Ray(testPoint,Util.FastNormalize(testPoint - transform.parent.position));
			float distance = 0;
			Vector3 innerCubeSurfacePoint = Vector3.zero;
			if(plane[closest].Raycast(ray,out distance)){
				innerCubeSurfacePoint = ray.GetPoint(distance);
				Debug.DrawLine(testPoint,innerCubeSurfacePoint,Color.yellow);
				var surfaceToCenterDistance = Vector3.Distance(innerCubeSurfacePoint , transform.parent.position);
				var testPointToCenterDistance = Vector3.Distance(testPoint , transform.parent.position);
				
				var fakeDistance = (testPointToCenterDistance/surfaceToCenterDistance) *(height/2.0f);
				var fakeCenter = testPoint + ray.direction*-1*fakeDistance;
				//Debug.Log("fake center "+fakeCenter + " "+ Time.time + " "+height + " "+ (testPointToCenterDistance/surfaceToCenterDistance) );
				return fakeCenter;
			}
			return transform.parent.position;
			
		}
		
		//return transform.parent.position + Util.FastNormalize(testPoint - transform.parent.position)*height/2.0f;
		return transform.parent.position;
		
	}
}
