using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Oracle;
using Operants;

public class StructureOption {
 
    public StructureOption ( string type, HookPoint.AllowedHex allowedHook, float angle ) {
        this.entityName = type;
        this.angle = angle;
        hookType = allowedHook;
        this.element = HookElement.All;
     
    }
 
    public StructureOption ( string type, HookElement element, float angle ) {
        this.entityName = type;
        this.angle = angle;
        hookType = HookPoint.AllowedHex.HexNotRamp;
        this.element = element;
     
    }

	public StructureOption (string type, float angle){
		this.entityName = type;
		this.angle = angle;
		hookType = HookPoint.AllowedHex.HexNotRamp;
		this.element = HookElement.All;
	}

    public HookPoint.AllowedHex hookType = HookPoint.AllowedHex.HexNotRamp;
    public string entityName = "hexEarth";
    public float angle = 0;
    public HookElement element;

}

public enum HexElement {
    Earth,
    Water,
    Volcanic,
    Grass,
    Metal,
    Ice
}

public enum HookElement {
    Earth,
    Water,
    Volcanic,
    Grass,
    Metal,
    Ice,
    All
}

public enum Structures{
    energy_bridge,
    hook,
}

public class Builder : MonoBehaviour {

    public BulletLayers partMask;
    public GameCamera gameCamera;
    private GameObject ghost;
    public bool godMode = false;
    public Material highlight;
    public Material blueHighlight;
    public Material greenHighlight;
    public Material yellowHighlight;
    public Material redHighlight;
    public Material skyblueHighlight;
    public static Builder instance;
    Dictionary<Oracle.Entity,int> visited = new Dictionary<Oracle.Entity, int>();
    public GameObject soundDetachAsset;
    public AudioClip soundSelectSlot;
    public StructureOption currentOption;
    public static HexElement activeElement = HexElement.Earth;
    bool inStructMode = false;
    public bool selectingElement = false;
    List<StructureOption> earthOptions = new List<StructureOption>();
    List<StructureOption> volcanicOptions = new List<StructureOption>();
    List<StructureOption> waterOptions = new List<StructureOption>();
    List<StructureOption> grassOptions = new List<StructureOption>();
    List<StructureOption> metalOptions = new List<StructureOption>();
    List<StructureOption> iceOptions = new List<StructureOption>();
    List<StructureOption>[] options = new List<StructureOption>[6];
    int index = 0;

    void Awake () {
        var destroyStructureMode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.DestroyStructure,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha7, state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( destroyStructureMode );

        var createStructureMode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.CreateStructure,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha6, state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( createStructureMode );

        var diconnectPartMode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.DisconnectPart,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha0, state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( diconnectPartMode );

        var destroyPartMode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.DestroyParts,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Backspace, state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( destroyPartMode );

        var saveClusterMode = new GameModeManager.Mode(){
            id = GameModeManager.ModeId.SaveCluster,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha4, state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }};
        GameModeManager.RegisterMode( saveClusterMode );

        earthOptions.Add( new StructureOption( "small_hex_earth", 0 ) );
        earthOptions.Add( new StructureOption( "small_hex_ramp", 0 ) );
        earthOptions.Add( new StructureOption( "small_hex_ramp", 60 ) );
        earthOptions.Add( new StructureOption( "small_hex_ramp", 120 ) );
        earthOptions.Add( new StructureOption( "small_hex_ramp", 180 ) );
        earthOptions.Add( new StructureOption( "small_hex_ramp", 240 ) );
        earthOptions.Add( new StructureOption( "small_hex_ramp", 300 ) );

     
        earthOptions.Add( new StructureOption( "small_hex_truncated_ramp", 0 ) );
        earthOptions.Add( new StructureOption( "small_hex_truncated_ramp", 60 ) );
        earthOptions.Add( new StructureOption( "small_hex_truncated_ramp", 120 ) );
        earthOptions.Add( new StructureOption( "small_hex_truncated_ramp", 180 ) );
        earthOptions.Add( new StructureOption( "small_hex_truncated_ramp", 240 ) );
        earthOptions.Add( new StructureOption( "small_hex_truncated_ramp", 300 ) );
     
     
        earthOptions.Add( new StructureOption( "small_hex_half_ramp", HookPoint.AllowedHex.HalfRamp, 0 ) );
        earthOptions.Add( new StructureOption( "small_hex_half_ramp", HookPoint.AllowedHex.HalfRamp, 60 ) );
        earthOptions.Add( new StructureOption( "small_hex_half_ramp", HookPoint.AllowedHex.HalfRamp, 120 ) );
        earthOptions.Add( new StructureOption( "small_hex_half_ramp", HookPoint.AllowedHex.HalfRamp, 180 ) );
        earthOptions.Add( new StructureOption( "small_hex_half_ramp", HookPoint.AllowedHex.HalfRamp, 240 ) );
        earthOptions.Add( new StructureOption( "small_hex_half_ramp", HookPoint.AllowedHex.HalfRamp, 300 ) ); 
     

        volcanicOptions.Add( new StructureOption( "small_hex_volcanic_rock", 0 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_volcanic_ramp", 0 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_volcanic_ramp", 60 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_volcanic_ramp", 120 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_volcanic_ramp", 180 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_volcanic_ramp", 240 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_volcanic_ramp", 300 ) );

     
        volcanicOptions.Add( new StructureOption( "small_hex_truncated_volcanic_ramp", 0 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_truncated_volcanic_ramp", 60 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_truncated_volcanic_ramp", 120 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_truncated_volcanic_ramp", 180 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_truncated_volcanic_ramp", 240 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_truncated_volcanic_ramp", 300 ) );
     
     
        volcanicOptions.Add( new StructureOption( "small_hex_half_volcanic_ramp", HookPoint.AllowedHex.HalfRamp, 0 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_half_volcanic_ramp", HookPoint.AllowedHex.HalfRamp, 60 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_half_volcanic_ramp", HookPoint.AllowedHex.HalfRamp, 120 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_half_volcanic_ramp", HookPoint.AllowedHex.HalfRamp, 180 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_half_volcanic_ramp", HookPoint.AllowedHex.HalfRamp, 240 ) );
        volcanicOptions.Add( new StructureOption( "small_hex_half_volcanic_ramp", HookPoint.AllowedHex.HalfRamp, 300 ) );     
     
     
        metalOptions.Add( new StructureOption( "small_hex_metal", 0 ) );
        metalOptions.Add( new StructureOption( "small_hex_metal_ramp", 0 ) );
        metalOptions.Add( new StructureOption( "small_hex_metal_ramp", 60 ) );
        metalOptions.Add( new StructureOption( "small_hex_metal_ramp", 120 ) );
        metalOptions.Add( new StructureOption( "small_hex_metal_ramp", 180 ) );
        metalOptions.Add( new StructureOption( "small_hex_metal_ramp", 240 ) );
        metalOptions.Add( new StructureOption( "small_hex_metal_ramp", 300 ) );

     
        metalOptions.Add( new StructureOption( "small_hex_truncated_metal_ramp", 0 ) );
        metalOptions.Add( new StructureOption( "small_hex_truncated_metal_ramp", 60 ) );
        metalOptions.Add( new StructureOption( "small_hex_truncated_metal_ramp", 120 ) );
        metalOptions.Add( new StructureOption( "small_hex_truncated_metal_ramp", 180 ) );
        metalOptions.Add( new StructureOption( "small_hex_truncated_metal_ramp", 240 ) );
        metalOptions.Add( new StructureOption( "small_hex_truncated_metal_ramp", 300 ) );
     
     
        metalOptions.Add( new StructureOption( "small_hex_half_metal_ramp", HookPoint.AllowedHex.HalfRamp, 0 ) );
        metalOptions.Add( new StructureOption( "small_hex_half_metal_ramp", HookPoint.AllowedHex.HalfRamp, 60 ) );
        metalOptions.Add( new StructureOption( "small_hex_half_metal_ramp", HookPoint.AllowedHex.HalfRamp, 120 ) );
        metalOptions.Add( new StructureOption( "small_hex_half_metal_ramp", HookPoint.AllowedHex.HalfRamp, 180 ) );
        metalOptions.Add( new StructureOption( "small_hex_half_metal_ramp", HookPoint.AllowedHex.HalfRamp, 240 ) );
        metalOptions.Add( new StructureOption( "small_hex_half_metal_ramp", HookPoint.AllowedHex.HalfRamp, 300 ) );
     
        waterOptions.Add( new StructureOption( "small_hex_water", HookElement.Water, 0 ) );
     
        grassOptions.Add( new StructureOption( "small_hex_grass", 0 ) );
//       grassOptions.Add (new StructureOption ("small_hex_grass_ramp", 0));
//       grassOptions.Add (new StructureOption ("small_hex_grass_ramp", 60));
//       grassOptions.Add (new StructureOption ("small_hex_grass_ramp", 120));
//       grassOptions.Add (new StructureOption ("small_hex_grass_ramp", 240));
//       grassOptions.Add (new StructureOption ("small_hex_grass_ramp", 300));
//
//       
//       grassOptions.Add (new StructureOption ("small_hex_truncated_grass_ramp", 0));
//       grassOptions.Add (new StructureOption ("small_hex_truncated_grass_ramp", 60));
//       grassOptions.Add (new StructureOption ("small_hex_truncated_grass_ramp", 120));
//       grassOptions.Add (new StructureOption ("small_hex_truncated_grass_ramp", 240));
//       grassOptions.Add (new StructureOption ("small_hex_truncated_grass_ramp", 300));
//       
//       
//       grassOptions.Add (new StructureOption ("small_hex_half_grass_ramp", 0));
//       grassOptions.Add (new StructureOption ("small_hex_half_grass_ramp", 60));
//       grassOptions.Add (new StructureOption ("small_hex_half_grass_ramp", 120));
//       grassOptions.Add (new StructureOption ("small_hex_half_grass_ramp", 240));
//       grassOptions.Add (new StructureOption ("small_hex_half_grass_ramp", 300));  

     
        iceOptions.Add( new StructureOption( "small_hex_ice", 0 ) );
        iceOptions.Add( new StructureOption( "small_hex_ice_ramp", 0 ) );
        iceOptions.Add( new StructureOption( "small_hex_ice_ramp", 60 ) );
        iceOptions.Add( new StructureOption( "small_hex_ice_ramp", 120 ) );
        iceOptions.Add( new StructureOption( "small_hex_ice_ramp", 180 ) );
        iceOptions.Add( new StructureOption( "small_hex_ice_ramp", 240 ) );
        iceOptions.Add( new StructureOption( "small_hex_ice_ramp", 300 ) );

     
        iceOptions.Add( new StructureOption( "small_hex_truncated_ice_ramp", 0 ) );
        iceOptions.Add( new StructureOption( "small_hex_truncated_ice_ramp", 60 ) );
        iceOptions.Add( new StructureOption( "small_hex_truncated_ice_ramp", 120 ) );
        iceOptions.Add( new StructureOption( "small_hex_truncated_ice_ramp", 180 ) );
        iceOptions.Add( new StructureOption( "small_hex_truncated_ice_ramp", 240 ) );
        iceOptions.Add( new StructureOption( "small_hex_truncated_ice_ramp", 300 ) );
     
     
        iceOptions.Add( new StructureOption( "small_hex_half_ice_ramp", HookPoint.AllowedHex.HalfRamp, 0 ) );
        iceOptions.Add( new StructureOption( "small_hex_half_ice_ramp", HookPoint.AllowedHex.HalfRamp, 60 ) );
        iceOptions.Add( new StructureOption( "small_hex_half_ice_ramp", HookPoint.AllowedHex.HalfRamp, 120 ) );
        iceOptions.Add( new StructureOption( "small_hex_half_ice_ramp", HookPoint.AllowedHex.HalfRamp, 180 ) );
        iceOptions.Add( new StructureOption( "small_hex_half_ice_ramp", HookPoint.AllowedHex.HalfRamp, 240 ) );
        iceOptions.Add( new StructureOption( "small_hex_half_ice_ramp", HookPoint.AllowedHex.HalfRamp, 300 ) );
     
     
        options[ 0 ] = earthOptions;
        options[ 1 ] = volcanicOptions;
        options[ 2 ] = metalOptions;
        options[ 3 ] = grassOptions;
        options[ 4 ] = waterOptions;
        options[ 5 ] = iceOptions;
     
     
     
        currentOption = earthOptions[ 0 ];

        instance = this;

        
    }
    public class TemporalRelationShip {
        public Oracle.Entity head, tail;
        public string predicate;

        public TemporalRelationShip ( Oracle.Entity tail, string predicate, Oracle.Entity head ) {
            this.head = head;
            this.tail = tail;
            this.predicate = predicate;
        }
    }

    public class SlotsMarkerWrapper {
        public List< NodeView > slots = new List< NodeView >();
        public int slotIndex = 0;
    }


    Part hoverPart = default(Part);
    Part hoverPartToDelete = default(Part);
    Part hoverPartToSave = default(Part);
    Structure hoverEarth = default(Structure);

    public static Builder GetInstance () {
        return instance;
    }

    void Update () {
        DisconnectPartMode();
        DestroyEarthMode();
        DeleteClustersMode();
        SaveClusterMode();
        CreateStructureMode();
    }

    void DeleteClustersMode () {
        BulletRaycastHitDetails partHit;

        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.DestroyParts ) ) {
            var clickUp = Input.GetMouseButtonUp( 0 );
            var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
            var partHitStatus = Bullet.Raycast( ray, out partHit, 1500, partMask, partMask );
            if ( partHitStatus ) {
                var part = partHit.bulletRigidbody.GetComponent<Part>(); //Part.FindOwner( partHit.collider.gameObject.transform, partMask );
//                var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
//                if ( subPart != null ) {
//                    part = subPart.part;
//                }
                if ( part != default(Part) ) {

                    if ( hoverPartToDelete != part ) {
                        if ( hoverPartToDelete != default(Part) ) {
                            SetHighLightToPart( hoverPartToDelete.gameObject, false );
                        }

                        hoverPartToDelete = part.bulletRigidBody.nodeview.GetComponent<Part>();
                        SetHighLightToPart( hoverPartToDelete.gameObject, true, Highlight.HighlightType.Red );
                    }

                    if ( clickUp ) {//Changed delete function, but kept old one if right ctrl is held, at plines request -z26
                        if(Input.GetKey(KeyCode.RightShift)) {
                            DeleteCluster( part.proxy.entity,true,true );
                        } else {
                            DeleteCluster( part.proxy.entity,true,false );
                        }
                    }
                }
            }
            else if ( hoverPartToDelete != default(Part) ) {
                hoverPartToDelete.gameObject.GetComponent<Highlight>().SetHighlight( false );
                hoverPartToDelete = default(Part);
            }


        }
        else if ( hoverPartToDelete != default(Part) ) {
            SetHighLightToPart( hoverPartToDelete.gameObject, false );
            hoverPartToDelete = default(Part);
        }

    }

    void SaveClusterMode () {
        BulletRaycastHitDetails partHit;

        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.SaveCluster ) ) {
            var clickUp = Input.GetMouseButtonUp( 0 );
            var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
            var partHitStatus = Bullet.Raycast( ray, out partHit, 1500, partMask, partMask );
            if ( partHitStatus ) {
                var part = partHit.bulletRigidbody.GetComponent<Part>();
//                var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
//                if ( subPart != null ) {
//                    part = subPart.part;
//                }
                if ( part != default(Part) ) {
                    if ( hoverPartToSave != part ) {
                        if ( hoverPartToSave != default(Part) ) {
                            SetHighLightToPart( hoverPartToSave.gameObject, false );
                        }

                        hoverPartToSave = part.bulletRigidBody.nodeview.GetComponent<Part>();
                        SetHighLightToPart( hoverPartToSave.gameObject, true, Highlight.HighlightType.Skyblue );
                    }

                    if ( clickUp ) {
                        var accumulated = new HashSet<Entity>();
                        Hyperspace.Entities( part.proxy.entity, accumulated ,new string[]{"input_for","output_for","extends","slot_for","linked_to","consumes"} );
                        var connectedParts = accumulated.Where(e => e.nodeView != default(NodeView)).Where(e => e.proxy is PartProxy).Select(e => e.proxy).Cast<PartProxy>();



                        var partConnectedPhysicaly = new HashSet<Entity>();
                        Hyperspace.Entities( part.proxy.entity, partConnectedPhysicaly ,new string[]{"extends","slot_for","linked_to"} );

                        var connectedPhysicalyParts = partConnectedPhysicaly.Where(e => e.nodeView != default(NodeView)).Where(e => e.proxy is PartProxy).Select(e => e.proxy).Cast<PartProxy>();

                        //Debug.Log(connectedParts.Count() + "  "+ connectedPhysicalyParts.Count());

                        var skipList = new List<PartProxy>();
                        connectedParts.ToList().ForEach( p =>  {
                            if(!skipList.Contains(p)){
                                p.RecalculateClusters();
                                skipList.AddRange(p.Parts().ToList());
                            }
                        });


                        GameModeManager.ChangeMode( GameModeManager.ModeId.Grid );
                        if ( connectedParts.Count() == connectedPhysicalyParts.Count() ) {
                            var up = part.nodeView.GetComponent<PhysicalObject>().Up();

                            Entity blueprintLoadData = new Entity("blueprint_load_data");
                            blueprintLoadData.SetProperty("position",Util.Vector3ToProperty(part.transform.position));
                            blueprintLoadData.SetProperty("rotation",Util.Vector3ToProperty(part.transform.rotation.eulerAngles));
                            blueprintLoadData.SetProperty("up",Util.Vector3ToProperty(up));
                            blueprintLoadData.SetProperty("version",Application.version); //z26

                            accumulated.Add(blueprintLoadData);
                            //var dummy = new GameObject( "dummy" );
                            //dummy.transform.position = part.nodeView.transform.position;
                            //dummy.transform.up = up;
                            //var localRotation = Quaternion.Inverse( dummy.transform.rotation ) * centerPart.nodeView.transform.rotation;
                            gameCamera.gridCamera.console.SaveBlueprint( accumulated );
//                            Destroy( dummy );
                        }
                        else {
                            Console.PushError("blueprint", new string[]{"cannot save blueprint, all parts must be connected"} );
                        }
                    }
                }
            }
            else if ( hoverPartToSave != default(Part) ) {
                hoverPartToSave.gameObject.GetComponent<Highlight>().SetHighlight( false );
                hoverPartToSave = default(Part);
            }


        }
        else if ( hoverPartToSave != default(Part) ) {
            SetHighLightToPart( hoverPartToSave.gameObject, false );
            hoverPartToSave = default(Part);
        }

    }

    void DisconnectPartMode () {
        BulletRaycastHitDetails partHit;
        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.DisconnectPart ) ) {
            var clickUp = Input.GetMouseButtonUp( 0 );
            var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
            var partHitStatus = Bullet.Raycast( ray, out partHit, 1500, partMask, partMask );
            if ( partHitStatus ) {
                var part = partHit.bulletRigidbody.GetComponent<Part>(); //Part.FindOwner( partHit.collider.gameObject.transform, partMask );
//               var subPart = partHit.bulletRigidbody.GetComponent<Subpart> ();
//               if (subPart != null)
//                   part = subPart.part;
                if ( part != default(Part) ) {

                    if ( hoverPart != part ) {
                        if ( hoverPart != default(Part) ) {
                            SetHighLightToPart( hoverPart.gameObject, false );
                        }

                        hoverPart = part.bulletRigidBody.nodeview.GetComponent<Part>();
                        SetHighLightToPart( hoverPart.gameObject, true, Highlight.HighlightType.Red );
                    }

                    if ( clickUp ) {
                        Sec.ForPermission( part.proxy.entity, Sec.Permission.disconnect, () => {
                            Disconnect( part, true );
                        }, () => {
                            Console.PushError("", new string[]{  "access denied - check permissions" } );
                            Debug.Log( "Fail FX" );
                        } );
                    }
                }
            }
            else if ( hoverPart != default(Part) ) {
                hoverPart.nodeView.highlight.SetHighlight( false, Highlight.HighlightType.Normal, true );
                hoverPart = default(Part);
            }


        }
        else if ( hoverPart != default(Part) ) {
            SetHighLightToPart( hoverPart.gameObject, false, Highlight.HighlightType.Normal, true );
            hoverPart = default(Part);
        }

    }

    void DestroyEarthMode () {
        BulletRaycastHitDetails partHit;
        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.DestroyStructure ) ) {
            var clickUp = Input.GetMouseButtonUp( 0 );
            var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
            var structureHitStatus = Bullet.Raycast( ray, out partHit, 1500, BulletLayers.Camera, ( BulletLayers.HexBlock | BulletLayers.Structure | BulletLayers.Water ) );
            if ( structureHitStatus ) {
                var structureCollider = partHit.bulletRigidbody.GetComponent<Structure>();
//               Debug.Log (structureCollider + "  " + partHit.bulletRigidbody.name + " " + structureCollider.nodeView.proxy);
                var allowedToDestroyEarth = false;
                Sec.ForPermission( structureCollider.nodeView.proxy.entity, Sec.Permission.delete,
                    () => {
                    allowedToDestroyEarth = true; },
                    () => {
                    Debug.Log( "Fail FX" );} );
                if ( allowedToDestroyEarth ) {
                    if ( hoverEarth != structureCollider.nodeView.GetComponent<Structure>() ) {
                        if ( hoverEarth != default(Part) ) {

                            SetHighLightToPart( hoverEarth.gameObject, false );
                        }
                        SetHighLightToPart( structureCollider.nodeView.gameObject, true, Highlight.HighlightType.Normal );
                        hoverEarth = structureCollider.nodeView.GetComponent<Structure>();
                    }

                    if ( clickUp ) {
                        structureCollider.nodeView.proxy.OnDestroy();
                    }
                }

            }
            else if ( hoverEarth != default(Structure) ) {
                hoverEarth.gameObject.GetComponent<Highlight>().SetHighlight( false );
                hoverEarth = default(Structure);
            }


        }
        else if ( hoverEarth != default(Structure) ) {
            SetHighLightToPart( hoverEarth.gameObject, false );
            hoverEarth = default(Structure);
        }

    }

    HookPoint hookPoint;

    void CreateStructureMode () {
        BulletRaycastHitDetails partHit;
     
        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.CreateStructure ) ) {
            var clickUp = Input.GetMouseButtonUp( 0 );
            var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
            var partHitStatus = Bullet.Raycast( ray, out partHit, 1500, BulletLayers.Camera, ( BulletLayers.HexBlock | BulletLayers.Structure | BulletLayers.Water ) );

            if ( !inStructMode ) {
                gameCamera.EnableGridCamera( null, GridCamera.GridType.HexCreation, false );
                inStructMode = true;
                var grid = gameCamera.gridCamera.grid;
                grid.transform.position = gameCamera.transform.position;
                grid.camera.gridPosition = Vector3.zero;
                grid.transform.rotation = gameCamera.transform.rotation;
                grid.GenerateHexElementOperants( new Vector3( -9, 4, 0 ) );
            }
            var optionList = options[ 0 ];
            switch ( Builder.activeElement ) {
                case HexElement.Earth:
                    optionList = options[ 0 ];
                    break;
                case HexElement.Grass:
                    optionList = options[ 3 ];
                    break;
                case HexElement.Metal:
                    optionList = options[ 2 ];
                    break;
                case HexElement.Volcanic:
                    optionList = options[ 1 ];
                    break;
                case HexElement.Water:
                    optionList = options[ 4 ];
                    break;
                case HexElement.Ice:
                    optionList = options[ 5 ];
                    break;      
            }

            if ( partHitStatus && !selectingElement ) {
             

                var structureHooks = partHit.bulletRigidbody.GetComponent<Structure>().hooks;
             
                if ( structureHooks == default(StructureHooks) ) {
                    return;
                }
                if ( structureHooks.hooks.Count() == 0 ) {
                    return;
                }
                var structureNodeView = partHit.bulletRigidbody.GetComponent<NodeView>();
                var structureProxy = structureNodeView.proxy;

                var newhookPoint = structureHooks.GetClosestHookPoint( partHit.point );
                if ( Input.GetMouseButtonDown( 1 ) ) {
                    RotateBetweenEarthOptions( optionList, newhookPoint.allowedHex );
                }
                if ( newhookPoint != hookPoint ) {
                    index = 0;
                    currentOption = GetFilteredEarthOption( optionList, newhookPoint.allowedHex, newhookPoint.allowedElement );
                }
                //Debug.Log(partHit.bulletRigidbody.name + " "+currentOption+" "+ Time.time);
                //Debug.Log("currentOption "+currentOption + " "+ Time.time);
                if ( currentOption != default(StructureOption) ) {
             
                    if ( newhookPoint != hookPoint || Input.GetMouseButtonDown( 1 ) ) {
 
                        if ( hookPoint != default(HookPoint) ) {
                            hookPoint.ShowGhost( false );
                        }
                     
                        hookPoint = newhookPoint;
                        ghost = hookPoint.ShowGhost( true );
 
                        if ( ghost != default(GameObject) ) {
                            switch ( hookPoint.allowedHex ) {
                                case HookPoint.AllowedHex.HexNotRamp:
                                    ghost.transform.rotation = ghost.transform.rotation * Quaternion.Euler( 0, currentOption.angle, 0 );
                                    break;
                            }
                        }
                    }
 
                    var structurePrefab = "hex_earth";
                    structurePrefab = currentOption.entityName;
 
                    if ( clickUp && ghost != default(GameObject) ) {
                        var xform = structureNodeView.transform.parent.GetComponent< XForm >();
                        var expresion = CreateExpressionForStructure( xform.transform, structureProxy, ghost.transform, structurePrefab );
                        Gaia.Instance().Eval( xform, expresion );
                        hookPoint.ShowGhost( false );
                        index = 0;
                        currentOption = optionList[ index ];
                    }
                }
            }
            else {
                if ( hookPoint ) {
                    hookPoint.ShowGhost( false );
                }
                index = 0;
                currentOption = optionList[ index ];
                hookPoint = null;
            }
        }
        else {
            if ( hookPoint ) {
                hookPoint.ShowGhost( false );
            }
            hookPoint = null;

            if ( inStructMode ) {
                inStructMode = false;
                gameCamera.DisableGridCamera( GridCamera.GridType.Programming, false );
            }
        }
    }

    public void Disconnect ( Part part, bool addForce = true ) {
        Instantiate( soundDetachAsset, part.transform.position, part.transform.rotation );
        part.proxy.Disconnect();

        var subpartEntity = part.proxy.entity.Tails( e => e == "linked_to" ).FirstOrDefault();
        if ( subpartEntity != default(Entity) ) {
            var subpartProxy = subpartEntity.proxy as PartProxy;
            subpartProxy.Disconnect();
            if ( addForce ) {
                float mass = subpartProxy.nodeView.btRigidBody.mass;
                subpartProxy.nodeView.btRigidBody.AddForce( -gameCamera.transform.forward * 40 * mass, Vector3.zero );
            }
        }

        var masterPartEntity = part.proxy.entity.Heads( e => e == "linked_to" ).FirstOrDefault();
        if ( masterPartEntity != default(Entity) ) {
            var masterPartProxy = masterPartEntity.proxy as PartProxy;
            masterPartProxy.Disconnect();
            if ( addForce ) {
                float mass = masterPartProxy.nodeView.btRigidBody.mass;
                masterPartProxy.nodeView.btRigidBody.AddForce( -gameCamera.transform.forward * 40 * mass, Vector3.zero );
            }
        }
        //.Select( e => e.proxy ).Cast<MotorRotorProxy>().First();




        if ( addForce ) {
            float mass = part.bulletRigidBody.mass;
            part.bulletRigidBody.AddForce( -gameCamera.transform.forward * 40 * mass, Vector3.zero );
        }
    }

    public void DeleteCluster ( Oracle.Entity entity ) {
        DeleteCluster( entity, true );
    }

    //Changed this so that it can delete bots without removing parts only linked by VP code -z26
    public void DeleteCluster ( Oracle.Entity entity, bool checkPermissions, bool followsVPcode = true ) {
        visited = new Dictionary<Oracle.Entity, int>();

        //Only this chunk is different.
        if(followsVPcode) {
        RemoveEntities( entity );
        } else {
        RestrictedRemoveEntities( entity );
        }

        var proceed = visited.Keys.Where( e => e.proxy is Operants.PartProxy )
            .All( e => Sec.HasPermission( e, Sec.Permission.delete ) );

        if ( !checkPermissions || proceed ) {
            visited.Keys.ToList().ForEach( visitedEntity => {
                Gaia.Instance().entities.Remove( visitedEntity.id );
                visitedEntity.deprecated = true;
            } );

            var visitedParts = visited.Keys
                            .Where( e => e.proxy is Operants.PartProxy)
                            .Select( e => e.proxy )
                            .Cast<Operants.PartProxy>()
                            .ToList();

            foreach ( var partToRemove in visitedParts ) {
                partToRemove.RemoveJointsAndExtends();
                partToRemove.OnViewDestroyed();
            }
            foreach ( var partToRemove in visitedParts ) {
//                foreach ( var subpart in partToRemove.nodeView.GetComponent< Part >().subparts.ToList() ) {
//                    Destroy( subpart.gameObject );
//                }
                if (partToRemove.nodeView != null) { //Quick and dirty fix to a map corruption issue -z26
                    var obj = partToRemove.nodeView.gameObject;
                    Destroy( obj );
                    partToRemove.unList();
                }
            }
            foreach ( var partToRemove in visitedParts ) {
                var locals = partToRemove.entity.Relationships().Where( r => r.Predicate == "local_to" ).ToList();
                foreach ( var local in locals ) {
                    local.Break();
                }
            }


        }
        else {
            Console.PushError("", new string[] { "access denied - require delete permission for all" } );
        }
    }

    void RemoveEntities ( Oracle.Entity entity ) {
        var num = 0;
        if ( visited.TryGetValue( entity, out num ) ) {
            return ;
        }

        visited.Add( entity, 0 );

        foreach ( var r in entity.Relationships().Where( r =>
            r.Predicate == "input_for" ||
            r.Predicate == "output_for" ||
            r.Predicate == "extends" ||
            r.Predicate == "slot_for" ||
            r.Predicate == "linked_to" ||
            r.Predicate == "consumes" ) ) {
            var e = r.Head == entity ? r.Tail : r.Head;
            RemoveEntities( e );
        }
    }

    void RestrictedRemoveEntities ( Oracle.Entity entity ) { //by z26.
        var num = 0;
        if ( visited.TryGetValue( entity, out num ) ) {
            return ;
        }

        visited.Add( entity, 0 );

        foreach ( var r in entity.Relationships().Where( r =>
        //Don't follow links from input tiles to output tiles. (consumes)
            r.Predicate == "input_for" ||
            r.Predicate == "output_for" ||
            r.Predicate == "extends" ||
            r.Predicate == "slot_for" ||
            r.Predicate == "linked_to" ) ) {
            var e = r.Head == entity ? r.Tail : r.Head;
            RestrictedRemoveEntities( e );
        }
    }


    void SaveCluster ( Oracle.Entity entity ) {
        visited = new Dictionary<Oracle.Entity, int>();
        SaveEntities( entity );
    }

    void SaveEntities ( Oracle.Entity entity ) {
        var num = 0;
        if ( visited.TryGetValue( entity, out num ) ) {
            return ;
        }

        visited.Add( entity, 0 );

        foreach ( var r in entity.Relationships().Where( r =>
            r.Predicate == "input_for" ||
            r.Predicate == "output_for" ||
            r.Predicate == "extends" ||
            r.Predicate == "slot_for" ||
            r.Predicate == "consumes" ) ) {
            var e = r.Head == entity ? r.Tail : r.Head;
            SaveEntities( e );
        }
    }

    public void SwitchSlot ( int delta, SlotsMarkerWrapper slotList ) {
        if ( slotList.slots.Count == 0 ) {
            return;
        }
        var lastSlotIndex = slotList.slotIndex;
        slotList.slotIndex += delta;
        if ( slotList.slotIndex >= slotList.slots.Count ) {
            slotList.slotIndex = 0;
        }
        if ( slotList.slotIndex < 0 ) {
            slotList.slotIndex = slotList.slots.Count - 1;
        }
        var lastSlot = slotList.slots[ lastSlotIndex ].transform.Find( "SlotMarker" );

        var currentSlot = slotList.slots[ slotList.slotIndex ].transform.Find( "SlotMarker" );

        lastSlot.GetComponent<MeshRenderer>().enabled = false;
        currentSlot.GetComponent<MeshRenderer>().enabled = true;
        gameCamera.GetComponent<AudioSource>().PlayOneShot( soundSelectSlot );
    }

    public static void SetHighLightToPart (
        GameObject part,
        bool on,
        Highlight.HighlightType highlight = Highlight.HighlightType.Normal,
        bool affectSubpart = true
        ) {
        part.GetComponent<Highlight>().SetHighlight( on, highlight, affectSubpart );
    }

    public static void SetHighLightToPartAndSubpart (
        GameObject part,
        bool on,
        Highlight.HighlightType highlight = Highlight.HighlightType.Normal,
        bool affectSubpart = false
        ) {
        part.GetComponent<Highlight>().SetHighlight( on, highlight, affectSubpart );
    }

    public static void CleanSlotsMarker ( List<NodeView> slotList ) {
        for ( int i=0; i< slotList.Count; i++ ) {
            var slotMarker = slotList[ i ].transform.Find( "SlotMarker" );
            if ( slotMarker != default(Transform) ) {
                slotMarker.GetComponent<Renderer>().enabled = false;
            }
        }
    }
 
    public static void RemoveCurrentXFormsBySlot ( Oracle.Entity slotEntity ) {
        var slotProxy = slotEntity.proxy as Operants.SlotProxy;
        Part part = slotProxy.ownerPartProxy.nodeView.GetComponent<Part>();
        var parts = part.proxy.Parts();
        foreach ( var connectedPart in parts ) {
            var relationShip = connectedPart.entity.Relationships().Where( r => r.Predicate == "local_to" ).FirstOrDefault();
            if ( relationShip != default(Oracle.Relationship) ) {
                relationShip.Break();
            }
        }
    }

    public static void SetPartEntityData ( Oracle.Entity newMasterPartSlotEntity, Oracle.Entity newPartSlotEntity ) {


		var masterSlotProxy = newMasterPartSlotEntity.proxy as Operants.SlotProxy;
		var slaveSlotProxy = newPartSlotEntity.proxy as Operants.SlotProxy;

        Part newMasterPart = masterSlotProxy.ownerPartProxy.nodeView.GetComponent<Part>();
        Part newPart = slaveSlotProxy.ownerPartProxy.nodeView.GetComponent<Part>();
     
        newMasterPart.proxy.RecalculateClusters();// this uses the next one.
        newMasterPart.proxy.SetAsGroupPositionReference();// this function must be changed
     
        newPart.proxy.RecalculateClusters();
        newPart.proxy.SetAsGroupPositionReference();

        //slot localPos;
        var masterSlotPos = Vector3.zero;
        var masterSlotRot = Quaternion.identity;

        masterSlotProxy.entity.ForInferredProperty< string >( "position", Util.AsIs, position => {
            masterSlotPos = Util.PropertyToVector3( position );
        } );
        masterSlotProxy.entity.ForInferredProperty< string >( "rotation", Util.AsIs, rotation => {
            masterSlotRot = Quaternion.Euler( Util.PropertyToVector3( rotation ) );
        } );

        var dummyPart = new GameObject( "part" );
        var dummySlot = new GameObject( "slot" );
        dummyPart.transform.position = Vector3.zero;
        dummyPart.transform.rotation = Quaternion.identity;
        dummySlot.transform.parent = dummyPart.transform;
        dummySlot.transform.localPosition = masterSlotPos;
        dummySlot.transform.localRotation = masterSlotRot;

        dummySlot.transform.parent = null;
        dummyPart.transform.parent = dummySlot.transform;

        dummySlot.transform.position = masterSlotProxy.nodeView.transform.position;
        dummySlot.transform.rotation = masterSlotProxy.nodeView.transform.rotation;


        // dirty procedure D:

        //other slot localPos;
        var slaveSlotPos = Vector3.zero;
        var slaveSlotRot = Quaternion.identity;

        slaveSlotProxy.entity.ForInferredProperty< string >( "position", Util.AsIs, position => {
            slaveSlotPos = Util.PropertyToVector3( position );
        } );
        slaveSlotProxy.entity.ForInferredProperty< string >( "rotation", Util.AsIs, rotation => {
            slaveSlotRot = Quaternion.Euler( Util.PropertyToVector3( rotation ) );
        } );



        var dummySlavePart = new GameObject( "slavePart" );
        var dummySlaveSlot = new GameObject( "slaveSlot" );
        dummySlavePart.transform.position = Vector3.zero;
        dummySlavePart.transform.rotation = Quaternion.identity;

        dummySlaveSlot.transform.parent = dummySlavePart.transform;
        dummySlaveSlot.transform.localPosition = slaveSlotPos;
        dummySlaveSlot.transform.localRotation = slaveSlotRot;

        dummySlaveSlot.transform.parent = null;
        dummySlavePart.transform.parent = dummySlaveSlot.transform;

        dummySlaveSlot.transform.position = slaveSlotProxy.nodeView.transform.position;
        dummySlaveSlot.transform.rotation = slaveSlotProxy.nodeView.transform.rotation;


        // dirty procedure D:


        var localToEntity = newMasterPart.proxy.entity.Relationships()
            .Where( r => r.Predicate == "local_to" )
            .Select( r => r.Head ).FirstOrDefault();

        var newSlavePos = dummyPart.transform.InverseTransformPoint( dummySlavePart.transform.position );
        var newSlaveRot = Quaternion.Inverse( dummyPart.transform.rotation ) * dummySlavePart.transform.rotation;

        newPart.proxy.entity.SetProperty( "position", Util.Vector3ToProperty( newSlavePos ) );
        newPart.proxy.entity.SetProperty( "rotation", Util.Vector3ToProperty( newSlaveRot.eulerAngles ) );


        
        var parts = newPart.proxy.Parts();

        var dummy = new GameObject();
        dummy.transform.position = newSlavePos;
        dummy.transform.rotation = newSlaveRot;

        newPart.proxy.entity.Relationships()
            .Where( r => r.Predicate == "local_to" )
            .FirstOrDefault().Break();

        new Oracle.Relationship( newPart.proxy.entity, "local_to", localToEntity );

        

        foreach ( var part in parts ) {

            if ( part == newPart.proxy ) {
                continue;
            }
            var slavePos = Vector3.zero;
            var slaveRot = Quaternion.identity;


            part.Proxy.entity.ForProperty< string >( "position", position => {
                slavePos = Util.PropertyToVector3( position );
            } );

            part.Proxy.entity.ForProperty< string >( "rotation", rotation => {
                slaveRot = Quaternion.Euler( Util.PropertyToVector3( rotation ) );
            } );



            var dummySlave = new GameObject();
            dummySlave.transform.parent = dummy.transform;
            dummySlave.transform.localPosition = slavePos;
            dummySlave.transform.localRotation = slaveRot;

            part.entity.SetProperty( "position", Util.Vector3ToProperty( dummySlave.transform.position ) );
            part.entity.SetProperty( "rotation", Util.Vector3ToProperty( dummySlave.transform.rotation.eulerAngles ) );



            part.entity.Relationships().Where( r => r.Predicate == "local_to" ).FirstOrDefault().Break();

            new Oracle.Relationship( part.entity, "local_to", localToEntity );

            Destroy( dummySlave );
        }

        Destroy( dummy );
        Destroy( dummyPart );
        Destroy( dummySlavePart );
        Destroy( dummySlaveSlot );
        Destroy( dummySlot );

    }

    public static string  CreateExpressionForStructure ( Transform xformTransform, Operants.NodeProxy proxy, Transform newStructureGhost, string type ) {
        var count = newStructureGhost.GetHashCode();
        var passcode = string.Empty;
        uint permissions = ( uint )Sec.Permission.all;

        proxy.entity.ForProperty<string>( "passcode", value => passcode = value );
//        proxy.entity.ForProperty<float>( "permissions", value => permissions = ( uint )value );

        var eId = "earth_block_" + Mathf.Abs( count );
     
        newStructureGhost.parent = xformTransform;
     
        var spawn_position = newStructureGhost.localPosition;// xformTransform.InverseTransformPoint (newStructureGhost.position);
        var spawn_rotation = newStructureGhost.localRotation;// Quaternion.Inverse( xformTransform.rotation)* newStructureGhost.rotation;
     
        var expression = eId + " . position " + Util.Vector3ToProperty( spawn_position ) + "\n";
        expression += eId + " . rotation " + Util.Vector3ToProperty( spawn_rotation.eulerAngles ) + "\n";
     
        expression += eId + " as " + type + "\n";

		if (passcode != string.Empty) {
			expression += eId + " . passcode " + passcode + "\n";
		}

        expression += eId + " . permissions " + permissions + "\n";

        expression += eId + " local_to " + proxy.parentEntity.id;


        return expression;
    }

    public void RotateBetweenEarthOptions ( List<StructureOption> options, HookPoint.AllowedHex type ) {
        var filteredOptions = options.Where( o => o.hookType == type ).ToList();

        if ( index >= filteredOptions.Count ) {
            index = -1;
        }
        index++;
        if ( index >= filteredOptions.Count ) {
            index = 0;
        }
        currentOption = filteredOptions[ index ];
    }
 
    public StructureOption GetFilteredEarthOption ( List<StructureOption> options, HookPoint.AllowedHex type, HookElement allowedElement ) {
        if ( allowedElement == HookElement.All ) {
            //var list = options.Where (o => o.hookType == type ).ToList ();
            return options.Where( o => o.hookType == type ).ToList().FirstOrDefault();
        }
        else {
            return options.Where( o => o.hookType == type && o.element == allowedElement ).ToList().FirstOrDefault();
        }
    }



}
