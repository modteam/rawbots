using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Operants;
using Oracle;

public class DisconnectSingleMode : MonoBehaviour
{


    private BulletLayers partMask;
    public GameCamera gameCamera;
    Slot lastTargetSlot = null;
    bool click = false;
    private Part firstPart;
    private Part secondPart;
    BuilderState currentState = BuilderState.None;
    private GhostPart ghostPart;
    private Part posiblePart;
    public GameObject soundDetachAsset;
    private List<Part> parts = new List<Part>();
    private List<Part> partsToClean = new List<Part>();
    public List<Builder.TemporalRelationShip> temporal = new List<Builder.TemporalRelationShip>();
    BulletLayers partLayers = (BulletLayers.Part | BulletLayers.World | BulletLayers.Structure | BulletLayers.Water | BulletLayers.Sensor | BulletLayers.HexBlock);

    public enum BuilderState
    {
        None,
        OnePart,
        Disconnecting
    }

    void Awake()
    {
        var mode = new GameModeManager.Mode()
        {
            id = GameModeManager.ModeId.DisconnectSingle,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha9 , state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }
        };
        GameModeManager.RegisterMode(mode);
    }

    // Use this for initialization
    void Start()
    {
        partMask = BulletLayers.Part;
    }

    // Update is called once per frame
    void Update()
    {
        DisconnectPartsMachine();
    }

    void DisconnectPartsMachine()
    {

        var builderActive = GameModeManager.InMode(m => m == GameModeManager.ModeId.DisconnectSingle);

        BulletRaycastHitDetails partHit;

        var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        var clickUp = Input.GetMouseButtonDown(0);
        click = Input.GetMouseButton(0);

        var partHitStatus = Bullet.Raycast(ray, out partHit, 1500, partMask, partMask);

        switch (currentState)
        {
            case BuilderState.None:
                if (builderActive)
                {
                    if (partHitStatus)
                    {
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }


                        HighlightElegiblePartFirstMode(part, Highlight.HighlightType.Normal);
                    }
                    else
                    {
                        DisableHighlightToElegiblePartFirstMode();
                    }
                    if (partHitStatus && clickUp)
                    {
                        DisableHighlightToElegiblePartFirstMode();

                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }

                        var validPart = SelectFirstPart(part);
                        if (validPart)
                        {
                            Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, true, Highlight.HighlightType.Red);

                            Sec.ForPermission(firstPart.proxy.entity, Sec.Permission.disconnect, () => {

                                currentState = BuilderState.OnePart;
                            }, () => {
                                Console.PushError("", new string[] { "access denied - check permissions" });
                                firstPart.isProtected = false;
                                currentState = BuilderState.None;
                                Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                                Destroy(ghostPart.gameObject);
                            });
                            //Debug.Break();
                        }
                    }
                }
                else
                {
                    DisableHighlightToElegiblePartFirstMode();
                }

                break;
            case BuilderState.OnePart:
                if (builderActive)
                {

                    if (partHitStatus)
                    {
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }

                        if (part != firstPart)
                        {
                            HighlightElegiblePartFirstMode(part, Highlight.HighlightType.Yellow);
                        }
                        else
                        {
                            DisableHighlightToElegiblePartFirstMode();
                        }
                    }
                    else
                    {
                        DisableHighlightToElegiblePartFirstMode();
                    }


                    if (click && partHitStatus && clickUp)
                    {
                        DisableHighlightToElegiblePartFirstMode();
                        var part = partHit.bulletRigidbody.GetComponent<Part>();
                        var subPart = partHit.bulletRigidbody.GetComponent<Subpart>();
                        if (subPart != null)
                        {
                            part = subPart.part;
                        }

                        var validPart = SelectSecondPart(part, true);

                        if (validPart)
                        {
                            Sec.ForPermission(secondPart.proxy.entity, Sec.Permission.disconnect, () => {
                                currentState = BuilderState.Disconnecting;
                            }, () => {
                                Console.PushError("", new string[] { "access denied - check permissions" });
                                firstPart.isProtected = false;
                                secondPart.isProtected = false;
                                currentState = BuilderState.None;
                                Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                                Builder.SetHighLightToPartAndSubpart(secondPart.gameObject, false);
                                Destroy(ghostPart.gameObject);
                            });

                        }
                    }
                }
                else
                {
                    firstPart.isProtected = false;
                    currentState = BuilderState.None;
                    Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                }
                break;

            case BuilderState.Disconnecting:
                Console.PushLog("Disconnecting", new string[] { "Done"});
                Builder.SetHighLightToPartAndSubpart(firstPart.gameObject, false);
                Builder.SetHighLightToPartAndSubpart(secondPart.gameObject, false);
                Instantiate(soundDetachAsset, firstPart.transform.position, firstPart.transform.rotation);
                Instantiate(soundDetachAsset, secondPart.transform.position, secondPart.transform.rotation);

                firstPart.proxy.DisconnectSingle(secondPart);

                var subpartEntity = firstPart.proxy.entity.Tails(e => e == "linked_to").FirstOrDefault();
                if (subpartEntity != default(Entity))
                {
                    var subpartProxy = subpartEntity.proxy as PartProxy;
                    subpartProxy.DisconnectSingle(secondPart);
                }

                var masterPartEntity = firstPart.proxy.entity.Heads(e => e == "linked_to").FirstOrDefault();
                if (masterPartEntity != default(Entity))
                {
                    var masterPartProxy = masterPartEntity.proxy as PartProxy;
                    masterPartProxy.DisconnectSingle(secondPart);
                }

                currentState = BuilderState.None;
                break;
            default:
                break;

        }
    }

    public void HighlightElegiblePartFirstMode(Part part, Highlight.HighlightType type)
    {

        if (part == posiblePart)
        {
            return;
        }
        if (posiblePart != null)
        {
            Builder.SetHighLightToPartAndSubpart(posiblePart.gameObject, false);
        }
        Builder.SetHighLightToPartAndSubpart(part.gameObject, true, type);
        posiblePart = part;
    }

    public void DisableHighlightToElegiblePartFirstMode()
    {
        if (posiblePart != null)
        {
            Builder.SetHighLightToPartAndSubpart(posiblePart.gameObject, false);
            posiblePart = default(Part);
        }
    }

    public bool SelectFirstPart(Part part)
    {
        firstPart = part;
        firstPart.isProtected = true;
        return true;

    }

    public bool SelectSecondPart(Part part, bool allowSameBot = false)
    {
        var master = firstPart.proxy.entity.Heads(p => p == "local_to").FirstOrDefault();
        var secondMaster = part.proxy.entity.Heads(p => p == "local_to").FirstOrDefault();
        if (part == firstPart || (master == secondMaster && !allowSameBot))
        {
            return false;
        }
        
        secondPart = part;
        secondPart.isProtected = true;
        return true;
    }

    
    private void AddParts(Part part)
    {
        if (parts.Contains(part))
        {
            return;
        }
        parts.Add(part);
        foreach (var connectedPart in part.proxy.Neighbors())
        {
            AddParts(connectedPart.nodeView.GetComponent<Part>());
        }
        foreach (var r in part.proxy.entity.Relationships().Where(r =>
          r.Predicate == "linked_to"))
        {
            var e = r.Head == part.proxy.entity ? r.Tail : r.Head;
            AddParts(e.nodeView.GetComponent<Part>());
        }
    }
}
