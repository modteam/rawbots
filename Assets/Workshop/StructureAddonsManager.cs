using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;


public class StructureAddonsManager : MonoBehaviour {


    class RelationshipData{
        public string tailId;
        public string predicate;
        public string headId;
    }

    class EntityData{
        public string id;
        public Dictionary<string,string> properties;
        public List<RelationshipData> relationships;
    }
    public static string addOnType = "energy_bridge";
    public GameCamera gameCamera;
    public BulletLayers structureMask;
    private GameObject ghost;
    Dictionary< string, List<StructureOption > > options = new Dictionary<string, List<StructureOption>>();
    public StructureOption currentOption;
    bool inMode = false;

    void Start () {
        var eBridgeOptions = new List<StructureOption>();
        eBridgeOptions.Add( new StructureOption("energy_bridge",0) );
        eBridgeOptions.Add( new StructureOption("energy_bridge",45) );
        eBridgeOptions.Add( new StructureOption("energy_bridge",90) );
        eBridgeOptions.Add( new StructureOption("energy_bridge",135) );
        options.Add( "energy_bridge", eBridgeOptions );
        var hookOptions = new List<StructureOption>();
        hookOptions.Add( new StructureOption("hook",0) );
        hookOptions.Add( new StructureOption("hook",45) );
        hookOptions.Add( new StructureOption("hook",90) );
        hookOptions.Add( new StructureOption("hook",135) );
        hookOptions.Add( new StructureOption("hook",180) );
        hookOptions.Add( new StructureOption("hook",225) );
        hookOptions.Add( new StructureOption("hook",270) );
        hookOptions.Add( new StructureOption("hook",315) );
        options.Add( "hook", hookOptions );
        var createAddonMode = new GameModeManager.Mode() {
            id = GameModeManager.ModeId.CreateAddon,
            transitions = new List<GameModeManager.Transition>(){
                new GameModeManager.Transition(){ key = KeyCode.Alpha8, state = GameModeManager.KeyState.KeyUp , mode = GameModeManager.ModeId.World},
            }
        };
        GameModeManager.RegisterMode( createAddonMode );
    }

    void Update () {
        CreateAddonMode();
    }

    HookPoint hookPoint;

    void CreateAddonMode () {
        BulletRaycastHitDetails partHit;
        if ( GameModeManager.InMode( m => m == GameModeManager.ModeId.CreateAddon ) ) {
            var clickUp = Input.GetMouseButtonUp( 0 );
            var ray = gameCamera.GetComponent<Camera>().ScreenPointToRay( Input.mousePosition );
            var partHitStatus = Bullet.Raycast( ray, out partHit, 1500, BulletLayers.Camera, ( BulletLayers.HexBlock | BulletLayers.Structure ) );

            if ( !inMode ) {
                gameCamera.EnableGridCamera( null, GridCamera.GridType.StructureCreation, false );
                inMode = true;
                var grid = gameCamera.gridCamera.grid;
                grid.transform.position = gameCamera.transform.position;
                grid.camera.gridPosition = Vector3.zero;
                grid.transform.rotation = gameCamera.transform.rotation;
                grid.GenerateStructureTiles( new Vector3( -9, 4, 0 ) );
            }

            if ( partHitStatus && !Builder.instance.selectingElement) {
                var structure = partHit.bulletRigidbody.GetComponent<Structure>();
                if ( structure == default(Structure) )
                    return;
                var structureHooks = partHit.bulletRigidbody.GetComponent<Structure>().addOnhooks;
				
                if ( structureHooks == default(StructureHooks) )
                    return;
                if ( structureHooks.hooks.Count() == 0 )
                    return;
                var structureNodeView = partHit.bulletRigidbody.GetComponent<NodeView>();
                var structureProxy = structureNodeView.proxy;

                var newhookPoint = structureHooks.GetClosestHookPoint( partHit.point );

                if ( Input.GetMouseButtonDown( 1 ) ) {
                    RotateBetweenAddons();
                }
                if ( newhookPoint != hookPoint ) {
                    currentOption = options[ addOnType ][0];
                }
                if ( newhookPoint != hookPoint || Input.GetMouseButtonDown( 1 ) ) {

                    if ( hookPoint != default(HookPoint) ) {
                        hookPoint.ShowGhost( false );
                    }
					
                    hookPoint = newhookPoint;
                    ghost = hookPoint.ShowGhost( true );

                    if ( ghost != default(GameObject) ) {
                        ghost.transform.rotation = ghost.transform.rotation * Quaternion.Euler( 0, currentOption.angle, 0 );
                    }
                }

                var structureType = currentOption.entityName;

                if ( clickUp && ghost != default(GameObject) ) {
                    var xform = structureNodeView.transform.parent.GetComponent< XForm >();
                    var data = new List<EntityData>();
                    var common = new EntityData();
                    common.id = currentOption.entityName + ((uint)ghost.transform.GetHashCode()).ToString();
                    var properties = new Dictionary<string,string>();
                    var passcode = string.Empty;
                    uint permissions = (uint)Sec.Permission.all;
                    structureProxy.entity.ForProperty<string> ("passcode", value => passcode = value);
                    ghost.transform.parent = xform.transform;
                    var spawn_position = ghost.transform.localPosition;
                    var spawn_rotation = ghost.transform.localRotation;
                    properties.Add( "position", Util.Vector3ToProperty( spawn_position ) );
                    properties.Add( "rotation", Util.Vector3ToProperty( spawn_rotation.eulerAngles ) );
                    properties.Add( "permissions", permissions.ToString() );
                    if ( passcode != string.Empty ) {
                        properties.Add( "passcode", passcode );
                    }
                    var relationships = new List<RelationshipData>();
                    relationships.Add( new RelationshipData(){tailId = common.id, predicate = "as", headId = structureType});
                    relationships.Add( new RelationshipData(){tailId = common.id, predicate = "local_to", headId = structureProxy.parentEntity.id } );
                    common.properties = properties;
                    common.relationships = relationships;
                    data.Add( common );
                    if ( currentOption.entityName == "hook" ) {
                        var position = spawn_position + ghost.transform.up * 1.1f;
                        data[0].properties["position"] = Util.Vector3ToProperty( position );
                        data[0].properties.Add( "hook_point", Util.Vector3ToProperty( position ) );
                        data[0].properties.Add( "hook_rotation", Util.Vector3ToProperty( spawn_rotation.eulerAngles ) );
                        data[0].properties.Add( "attached", "1" );

                        var hookAttach = new EntityData();
                        hookAttach.id = common.id + "_attach";
                        var hookAttachProperties = new Dictionary<string,string>();
                        hookAttachProperties.Add("value","1");
                        hookAttachProperties.Add( "permissions", permissions.ToString() );
                        if ( passcode != string.Empty ) {
                            hookAttachProperties.Add( "passcode", passcode );
                        }
                        var hookAttachRelationships = new List<RelationshipData>();
                        hookAttachRelationships.Add( new RelationshipData(){tailId = hookAttach.id, predicate = "input_for", headId = common.id});
                        hookAttachRelationships.Add( new RelationshipData(){tailId = hookAttach.id, predicate = "as", headId = "hook_attach"} );
                        hookAttach.properties = hookAttachProperties;
                        hookAttach.relationships = hookAttachRelationships;
                        data.Add(hookAttach);
                    }

                    var expresion = CreateExpression( data );
                    Gaia.Instance().Eval( xform, expresion );
                    hookPoint.ShowGhost( false );

                }
            } else {
                if ( hookPoint ) {
                    hookPoint.ShowGhost( false );
                }
                hookPoint = null;

            }
        } else {
            if ( hookPoint ) {
                hookPoint.ShowGhost( false );
            }
            hookPoint = null;

            if ( inMode ) {
                inMode = false;
                gameCamera.DisableGridCamera( GridCamera.GridType.Programming, false );
            }

        }
    }

    public void RotateBetweenAddons () {
        var currentList = options[ addOnType ];
        var index = currentList.IndexOf( currentOption );
        index++;
        if ( index >= currentList.Count ) {
            index = 0;
        }
        currentOption = currentList[ index ];
    }


    string  CreateExpression (List<EntityData> data) {
        var expression = string.Empty;
        for(int i = 0 ; i < data.Count ; ++i ){
            var e = data[ i ];
            foreach(var k in e.properties.Keys){
                expression += e.id + " . " +  k + " " + e.properties[k] + "\n";
            }
            for ( int j = 0; j < e.relationships.Count; ++j ) {
                expression += e.relationships[j].tailId + " " + e.relationships[j].predicate + " " + e.relationships[j].headId + "\n";
            }
        }

        return expression;
    }

}
