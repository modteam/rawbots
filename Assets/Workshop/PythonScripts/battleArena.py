import library

with open('../../BotDefinitions/battleArena.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	################### First Hexagon ########################

	#Creating platforms
	obj.addHexPlat("platform000","0,0,0","0,0,0")

	#Creating columns
	obj.addHexColumn("column000","47,0,0","270,0,0")
	obj.addHexColumn("column001","-47,0,0","270,180,0")
	obj.addHexColumn("column002","-23.4,0,-40.7","270,120,0")
	obj.addHexColumn("column003","23.4,0,-40.7","270,60,0")
	obj.addHexColumn("column004","23.4,0,40.7","270,300,0")
	obj.addHexColumn("column005","-23.4,0,40.7","270,240,0")

	#Creating Walls
	obj.addWall("wall000","0,20,-40","0,0,0")
	obj.addWall("wall001","34.3,20,-19.8","0,120,0")
	obj.addWall("wall002","34.3,20,19.8","0,60,0")
	obj.addWall("wall003","0,20,40","0,0,0")
	obj.addWall("wall004","-34.3,20,19.8","0,120,0")
	obj.addWall("wall005","-34.3,20,-19.8","0,60,0")

	#Connecting platform000 to columns
	obj.extend("platform000","01","column000","01")
	obj.extend("platform000","02","column001","01")
	obj.extend("platform000","03","column002","01")
	obj.extend("platform000","04","column003","01")
	obj.extend("platform000","05","column004","01")
	obj.extend("platform000","06","column005","01")

	#Connecting wall000 to columns
	obj.extend("wall000","00","column002","04")
	obj.extend("wall000","01","column002","05")
	obj.extend("wall000","02","column003","02")
	obj.extend("wall000","03","column003","03")
	
	#Connecting wall000 to platforms
	obj.extend("wall000","06","platform000","27")
	obj.extend("wall000","07","platform000","26")

	#Connecting wall001 to columns
	obj.extend("wall001","02","column003","04")
	obj.extend("wall001","03","column003","05")
	obj.extend("wall001","00","column000","02")
	obj.extend("wall001","01","column000","03")
	
	#Connecting wall001 to platforms
	obj.extend("wall001","06","platform000","28")
	obj.extend("wall001","07","platform000","29")

	#Connecting wall002 to columns
	obj.extend("wall002","02","column000","04")
	obj.extend("wall002","03","column000","05")
	obj.extend("wall002","00","column004","02")
	obj.extend("wall002","01","column004","03")
	
	#Connecting wall002 to platforms
	obj.extend("wall002","06","platform000","30")
	obj.extend("wall002","07","platform000","31")

	#Connecting wall003 to columns
	obj.extend("wall003","02","column004","04")
	obj.extend("wall003","03","column004","05")
	obj.extend("wall003","00","column005","02")
	obj.extend("wall003","01","column005","03")
	
	#Connecting wall003 to platforms
	obj.extend("wall003","06","platform000","32")
	obj.extend("wall003","07","platform000","33")

	#Connecting wall004 to columns
	obj.extend("wall004","02","column001","02")
	obj.extend("wall004","03","column001","03")
	obj.extend("wall004","00","column005","04")
	obj.extend("wall004","01","column005","05")
	
	#Connecting wall004 to platforms
	obj.extend("wall004","06","platform000","35")
	obj.extend("wall004","07","platform000","34")

	#Connecting wall005 to columns
	obj.extend("wall005","00","column001","04")
	obj.extend("wall005","01","column001","05")
	obj.extend("wall005","02","column002","02")
	obj.extend("wall005","03","column002","03")

	#Connecting wall005 to platforms
	obj.extend("wall005","06","platform000","37")
	obj.extend("wall005","07","platform000","36")

	################### Second Hexagon #####################

	#Creating platforms
	obj.addHexPlat("platform000","0,0,0","0,0,0")

	#Creating columns
	obj.addHexColumn("column000","47,0,0","270,0,0")
	obj.addHexColumn("column001","-47,0,0","270,180,0")
	obj.addHexColumn("column002","-23.4,0,-40.7","270,120,0")
	obj.addHexColumn("column003","23.4,0,-40.7","270,60,0")
	obj.addHexColumn("column004","23.4,0,40.7","270,300,0")
	obj.addHexColumn("column005","-23.4,0,40.7","270,240,0")

	#Creating Walls
	obj.addWall("wall000","0,20,-40","0,0,0")
	obj.addWall("wall001","34.3,20,-19.8","0,120,0")
	obj.addWall("wall002","34.3,20,19.8","0,60,0")
	obj.addWall("wall003","0,20,40","0,0,0")
	obj.addWall("wall004","-34.3,20,19.8","0,120,0")
	obj.addWall("wall005","-34.3,20,-19.8","0,60,0")

	#Connecting platform000 to columns
	obj.extend("platform000","01","column000","01")
	obj.extend("platform000","02","column001","01")
	obj.extend("platform000","03","column002","01")
	obj.extend("platform000","04","column003","01")
	obj.extend("platform000","05","column004","01")
	obj.extend("platform000","06","column005","01")

	#Connecting wall000 to columns
	obj.extend("wall000","00","column002","04")
	obj.extend("wall000","01","column002","05")
	obj.extend("wall000","02","column003","02")
	obj.extend("wall000","03","column003","03")
	
	#Connecting wall000 to platforms
	obj.extend("wall000","06","platform000","27")
	obj.extend("wall000","07","platform000","26")

	#Connecting wall001 to columns
	obj.extend("wall001","02","column003","04")
	obj.extend("wall001","03","column003","05")
	obj.extend("wall001","00","column000","02")
	obj.extend("wall001","01","column000","03")
	
	#Connecting wall001 to platforms
	obj.extend("wall001","06","platform000","28")
	obj.extend("wall001","07","platform000","29")

	#Connecting wall002 to columns
	obj.extend("wall002","02","column000","04")
	obj.extend("wall002","03","column000","05")
	obj.extend("wall002","00","column004","02")
	obj.extend("wall002","01","column004","03")
	
	#Connecting wall002 to platforms
	obj.extend("wall002","06","platform000","30")
	obj.extend("wall002","07","platform000","31")

	#Connecting wall003 to columns
	obj.extend("wall003","02","column004","04")
	obj.extend("wall003","03","column004","05")
	obj.extend("wall003","00","column005","02")
	obj.extend("wall003","01","column005","03")
	
	#Connecting wall003 to platforms
	obj.extend("wall003","06","platform000","32")
	obj.extend("wall003","07","platform000","33")

	#Connecting wall004 to columns
	obj.extend("wall004","02","column001","02")
	obj.extend("wall004","03","column001","03")
	obj.extend("wall004","00","column005","04")
	obj.extend("wall004","01","column005","05")
	
	#Connecting wall004 to platforms
	obj.extend("wall004","06","platform000","35")
	obj.extend("wall004","07","platform000","34")

	#Connecting wall005 to columns
	obj.extend("wall005","00","column001","04")
	obj.extend("wall005","01","column001","05")
	obj.extend("wall005","02","column002","02")
	obj.extend("wall005","03","column002","03")

	#Connecting wall005 to platforms
	obj.extend("wall005","06","platform000","37")
	obj.extend("wall005","07","platform000","36")



	################### Text Hexagon ########################

	##Creating platforms
	#obj.addHexPlat("platform000","0,0,0","0,0,0")
#
	##Creating columns
	#obj.addHexColumn("column000","47,0,0","270,0,0")
	#obj.addHexColumn("column001","-47,0,0","270,180,0")
	#obj.addHexColumn("column002","-23.4,0,-40.7","270,120,0")
	#obj.addHexColumn("column003","23.4,0,-40.7","270,60,0")
	#obj.addHexColumn("column004","23.4,0,40.7","270,300,0")
	#obj.addHexColumn("column005","-23.4,0,40.7","270,240,0")
#
	##Creating Walls
	#obj.addWall("wall000","0,20,-40","0,0,0")
	#obj.addWall("wall001","34.3,20,-19.8","0,120,0")
	#obj.addWall("wall002","34.3,20,19.8","0,60,0")
	#obj.addWall("wall003","0,20,40","0,0,0")
	#obj.addWall("wall004","-34.3,20,19.8","0,120,0")
	#obj.addWall("wall005","-34.3,20,-19.8","0,60,0")
#
	##Connecting platform000 to columns
	#obj.extend("platform000","01","column000","01")
	#obj.extend("platform000","02","column001","01")
	#obj.extend("platform000","03","column002","01")
	#obj.extend("platform000","04","column003","01")
	#obj.extend("platform000","05","column004","01")
	#obj.extend("platform000","06","column005","01")
#
	##Connecting wall000 to columns
	#obj.extend("wall000","00","column002","04")
	#obj.extend("wall000","01","column002","05")
	#obj.extend("wall000","02","column003","02")
	#obj.extend("wall000","03","column003","03")
	#
	##Connecting wall000 to platforms
	#obj.extend("wall000","06","platform000","27")
	#obj.extend("wall000","07","platform000","26")
#
	##Connecting wall001 to columns
	#obj.extend("wall001","02","column003","04")
	#obj.extend("wall001","03","column003","05")
	#obj.extend("wall001","00","column000","02")
	#obj.extend("wall001","01","column000","03")
	#
	##Connecting wall001 to platforms
	#obj.extend("wall001","06","platform000","28")
	#obj.extend("wall001","07","platform000","29")
#
	##Connecting wall002 to columns
	#obj.extend("wall002","02","column000","04")
	#obj.extend("wall002","03","column000","05")
	#obj.extend("wall002","00","column004","02")
	#obj.extend("wall002","01","column004","03")
	#
	##Connecting wall002 to platforms
	#obj.extend("wall002","06","platform000","30")
	#obj.extend("wall002","07","platform000","31")
#
	##Connecting wall003 to columns
	#obj.extend("wall003","02","column004","04")
	#obj.extend("wall003","03","column004","05")
	#obj.extend("wall003","00","column005","02")
	#obj.extend("wall003","01","column005","03")
	#
	##Connecting wall003 to platforms
	#obj.extend("wall003","06","platform000","32")
	#obj.extend("wall003","07","platform000","33")
#
	##Connecting wall004 to columns
	#obj.extend("wall004","02","column001","02")
	#obj.extend("wall004","03","column001","03")
	#obj.extend("wall004","00","column005","04")
	#obj.extend("wall004","01","column005","05")
	#
	##Connecting wall004 to platforms
	#obj.extend("wall004","06","platform000","35")
	#obj.extend("wall004","07","platform000","34")
#
	##Connecting wall005 to columns
	#obj.extend("wall005","00","column001","04")
	#obj.extend("wall005","01","column001","05")
	#obj.extend("wall005","02","column002","02")
	#obj.extend("wall005","03","column002","03")
#
	##Connecting wall005 to platforms
	#obj.extend("wall005","06","platform000","37")
	#obj.extend("wall005","07","platform000","36")