import library

with open('../../BotDefinitions/Cavern2.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	#Creating terrain
	obj.addHexEarth("terrain000","140,0,-81","0,0,0")
	obj.addHexEarth("terrain001","210,0,-122","0,0,0")
	obj.addHexEarth("terrain002","281,0,1","0,0,0")
	obj.addHexEarth("terrain003","210,0,41","0,0,0")
	obj.addHexEarth("terrain004","70,0,121","0,0,0")
	obj.addHexEarth("terrain005","140,0,80","0,0,0")
	obj.addHexEarth("terrain006","70,0,-40","0,0,0")
	obj.addHexEarth("terrain007","0,0,0","0,0,0")

	#Creating terrain
	obj.addHexPlat("platform000","210,20,-40","0,0,0")
	obj.addHexPlat("platform001","140,20,0","0,0,0")
	obj.addHexPlat("platform002","70,20,40","0,0,0")

	#Connecting platform000
	obj.extend("platform000","18","terrain000","06")
	obj.extend("platform000","17","terrain000","07")
	obj.extend("platform000","09","terrain001","04")
	obj.extend("platform000","10","terrain001","05")
	obj.extend("platform000","16","terrain002","03")
	obj.extend("platform000","15","terrain002","02")
	obj.extend("platform000","08","terrain003","00")
	obj.extend("platform000","07","terrain003","01")
	obj.extend("platform000","13","platform001","11")
	obj.extend("platform000","14","platform001","12")

	#Connecting platform001
	obj.extend("platform001","18","terrain006","06")
	obj.extend("platform001","17","terrain006","07")
	obj.extend("platform001","09","terrain000","04")
	obj.extend("platform001","10","terrain000","05")
	obj.extend("platform001","16","terrain003","03")
	obj.extend("platform001","15","terrain003","02")
	obj.extend("platform001","08","terrain005","00")
	obj.extend("platform001","07","terrain005","01")
	obj.extend("platform001","13","platform002","11")
	obj.extend("platform001","14","platform002","12")

	#Connecting platform002
	obj.extend("platform002","18","terrain007","06")
	obj.extend("platform002","17","terrain007","07")
	obj.extend("platform002","09","terrain006","04")
	obj.extend("platform002","10","terrain006","05")
	obj.extend("platform002","16","terrain005","03")
	obj.extend("platform002","15","terrain005","02")
	obj.extend("platform002","08","terrain004","00")
	obj.extend("platform002","07","terrain004","01")

	#Connecting terrains
	obj.extend("terrain004","09","terrain005","10")
	obj.extend("terrain004","08","terrain005","11")
	obj.extend("terrain004","21","terrain005","22")
	obj.extend("terrain004","20","terrain005","23")
	obj.extend("terrain005","09","terrain003","10")
	obj.extend("terrain005","08","terrain003","11")
	obj.extend("terrain005","21","terrain003","22")
	obj.extend("terrain005","20","terrain003","23")
	obj.extend("terrain003","09","terrain002","10")
	obj.extend("terrain003","08","terrain002","11")
	obj.extend("terrain003","21","terrain002","22")
	obj.extend("terrain003","20","terrain002","23")
	obj.extend("terrain007","09","terrain006","10")
	obj.extend("terrain007","08","terrain006","11")
	obj.extend("terrain007","21","terrain006","22")
	obj.extend("terrain007","20","terrain006","23")
	obj.extend("terrain006","09","terrain000","10")
	obj.extend("terrain006","08","terrain000","11")
	obj.extend("terrain006","21","terrain000","22")
	obj.extend("terrain006","20","terrain000","23")
	obj.extend("terrain000","09","terrain001","10")
	obj.extend("terrain000","08","terrain001","11")
	obj.extend("terrain000","21","terrain001","22")
	obj.extend("terrain000","20","terrain001","23")

