import library

with open('../../BotDefinitions/ant001.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addContinuum("parker_third_middle_box","12,0,0","0,0,0")
	obj.addContinuum("parker_second_middle_box","4,0,0","0,0,0")

	obj.addContinuum("parker_third_back_box","-12,0,0","0,0,0")	
	obj.addContinuum("parker_second_back_box","-4,0,0","0,0,0")

	#obj.addContinuum("second_back_box","-7,0,0","0,0,0")

	obj.addContinuum("middle_box","19,0,0","0,0,0")

	obj.addElbow("front_base_right_elbow",pos= "19,0,-6",rot="90,0,-90",initial_values = [["angle","-40"]])
	obj.extend("front_base_right_elbow","1","middle_box","5")
	obj.addElbow("front_right_elbow",pos= "19,0,-9",rot="0,90,0",initial_values = [["angle","60"]])
	obj.extend("front_right_elbow","1","front_base_right_elbow","0")
	obj.addContinuum("front_right_leg_box","19,0,-11","0,0,0")
	obj.extend("front_right_leg_box","2","front_right_elbow","0")

	obj.addElbow("front_base_left_elbow",pos= "19,0,6",rot="90,90,0",initial_values = [["angle","-40"]])
	obj.extend("front_base_left_elbow","0","middle_box","2")
	obj.addElbow("front_left_elbow",pos= "19,0,9",rot="0,90,0",initial_values = [["angle","60"]])
	obj.extend("front_left_elbow","0","front_base_left_elbow","1")
	obj.addContinuum("front_left_leg_box","19,0,11","0,0,0")
	obj.extend("front_left_leg_box","5","front_left_elbow","1")

	obj.addFoot("front_foot","19,0,14","-90,0,0")
	obj.extend("front_foot","0","front_left_leg_box","2")
	obj.addFoot("front_foot2","19,0,-14","-90,180,0");
	obj.extend("front_foot2","0","front_right_leg_box","5")


	obj.addCore("master","0,0,0","0,0,0")
	obj.addElbow("middle_base_right_elbow",pos= "0,0,-6",rot="90,0,-90",initial_values = [["angle","0"]])
	obj.extend("middle_base_right_elbow","1","master","5")
	obj.addElbow("middle_right_elbow",pos= "0,0,-9",rot="0,90,0",initial_values = [["angle","60"]])
	obj.extend("middle_right_elbow","1","middle_base_right_elbow","0")
	obj.addContinuum("middle_right_leg_box","0,0,-11","0,0,0")
	obj.extend("middle_right_leg_box","2","middle_right_elbow","0")

	obj.addElbow("middle_base_left_elbow",pos= "0,0,6",rot="90,90,0",initial_values = [["angle","0"]])
	obj.extend("middle_base_left_elbow","0","master","2")
	obj.addElbow("middle_left_elbow",pos= "0,0,9",rot="0,90,0",initial_values = [["angle","60"]])
	obj.extend("middle_left_elbow","0","middle_base_left_elbow","1")
	obj.addContinuum("middle_left_leg_box","0,0,11","0,0,0")
	obj.extend("middle_left_leg_box","5","middle_left_elbow","1")


	obj.addFoot("test_foot","0,0,14","-90,0,0")
	obj.extend("test_foot","0","middle_left_leg_box","2")
	obj.addFoot("test_foot2","0,0,-14","-90,180,0");
	obj.extend("test_foot2","0","middle_right_leg_box","5")

	obj.addContinuum("back_box","-19,0,0","0,0,0")
	obj.addElbow("back_base_right_elbow",pos= "-19,0,-6",rot="90,0,-90",initial_values = [["angle","40"]])
	obj.extend("back_base_right_elbow","1","back_box","5")
	obj.addElbow("back_right_elbow",pos= "-19,0,-9",rot="0,90,0",initial_values = [["angle","60"]])
	obj.extend("back_right_elbow","1","back_base_right_elbow","0")
	obj.addContinuum("back_right_leg_box","-19,0,-11","0,0,0")
	obj.extend("back_right_leg_box","2","back_right_elbow","0")


	obj.addElbow("back_base_left_elbow",pos= "-19,0,6",rot="90,90,0",initial_values = [["angle","40"]])
	obj.extend("back_base_left_elbow","0","back_box","2")
	obj.addElbow("back_left_elbow",pos= "-19,0,9",rot="0,90,0",initial_values = [["angle","60"]])
	obj.extend("back_left_elbow","0","back_base_left_elbow","1")
	obj.addContinuum("back_left_leg_box","-19,0,11","0,0,0")
	obj.extend("back_left_leg_box","5","back_left_elbow","1")



	obj.addFoot("back_foot","-19,0,14","-90,0,0")
	obj.extend("back_foot","0","back_left_leg_box","2")
	obj.addFoot("back_foot2","-19,0,-14","-90,180,0");
	obj.extend("back_foot2","0","back_right_leg_box","5")



	obj.extend("middle_box","3","parker_third_middle_box","0")
	obj.extend("parker_third_middle_box","3","parker_second_middle_box","0")	
	obj.extend("parker_second_middle_box","3","master","0")

	obj.extend("back_box","0","parker_third_back_box","3")
	obj.extend("parker_third_back_box","0","parker_second_back_box","3")	
	obj.extend("parker_second_back_box","0","master","3")

	obj.addTimeLine("front_base_right_elbow_timeline",asset = "AntSequences/front_base_right_elbow", scale = "12")
#	obj.addTimeLine("front_base_left_elbow_timeline",asset = "AntSequences/front_base_left_elbow")

	obj.addTimeLine("front_right_elbow_timeline",asset = "AntSequences/front_right_elbow", scale = "12")
	obj.addTimeLine("middle_right_elbow_timeline",asset = "AntSequences/middle_right_elbow", scale = "12")
	obj.addTimeLine("middle_base_right_elbow_timeline",asset = "AntSequences/middle_base_right_elbow", scale = "12")
#	obj.addOscilloscope("osc1",pos = "0,5,0",initial_values = [["scale","0.01"]])
#	obj.addOscilloscope("osc2",pos = "0,8,0",initial_values = [["scale","0.01"]])


	obj.consume("front_right_elbow","angle","front_right_elbow_timeline","sample")
	obj.consume("front_base_right_elbow","angle","front_base_right_elbow_timeline","sample")

	obj.consume("back_right_elbow","angle","front_right_elbow_timeline","sample")
	obj.consume("back_base_right_elbow","angle","front_base_right_elbow_timeline","sample")

	obj.consume("middle_left_elbow","angle","front_right_elbow_timeline","sample")
	obj.consume("middle_base_left_elbow","angle","front_base_right_elbow_timeline","sample")
#
	obj.consume("middle_right_elbow","angle","middle_right_elbow_timeline","sample")
	obj.consume("middle_base_right_elbow","angle","middle_base_right_elbow_timeline","sample")
#
	obj.consume("front_left_elbow","angle","middle_right_elbow_timeline","sample")
	obj.consume("front_base_left_elbow","angle","middle_base_right_elbow_timeline","sample")
#
	obj.consume("back_left_elbow","angle","middle_right_elbow_timeline","sample")
	obj.consume("back_base_left_elbow","angle","middle_base_right_elbow_timeline","sample")



	#obj.consume("osc1","sample","middle_right_elbow_timeline","sample")
	#obj.consume("osc2","sample","middle_right_elbow_timeline","sample")

	obj.addBody_gen03_5("front_shield","21,2,0","-90,0,0")
	obj.extend("front_shield","1","middle_box","0")

	obj.addBody_gen03_4("shield_a","12,1,0","0,90,-90")
	obj.extend("shield_a","0","parker_third_middle_box","1")
	obj.addBody_gen03_4("shield_b","-4,1,0","0,-90,-90")
	obj.extend("shield_b","0","parker_second_back_box","1")
	obj.addBody_gen03_4("shield_c","4,1,0","0,90,-90")
	obj.extend("shield_c","0","parker_second_middle_box","1")
	obj.addBody_gen03_4("shield_d","-12,1,0","0,-90,-90")
	obj.extend("shield_d","0","parker_third_back_box","1")


	obj.addBody_gen03_2("left_shield_a","-19,2,12","-90,90,0")
	obj.extend("left_shield_a","0","back_left_leg_box","1")
	obj.addBody_gen03_2("left_shield_b","0,2,12","-90,90,0")
	obj.extend("left_shield_b","0","middle_left_leg_box","1")
	obj.addBody_gen03_2("left_shield_c","19,2,12","-90,90,0")
	obj.extend("left_shield_c","0","front_left_leg_box","1")

	obj.addBody_gen03_2("right_shield_a","-19,2,-12","-90,-90,0")
	obj.extend("right_shield_a","0","back_right_leg_box","1")
	obj.addBody_gen03_2("right_shield_b","0,2,-12","-90,-90,0")
	obj.extend("right_shield_b","0","middle_right_leg_box","1")
	obj.addBody_gen03_2("right_shield_c","19,2,-12","-90,-90,0")
	obj.extend("right_shield_c","0","front_right_leg_box","1")




	obj.addOscillator(name = "jaw_osc",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "30",
		offset_range= "-1500,1500,1",
		offset_value = "30",
		phase_range = "0,10,0.1")


	obj.addOscillator(name = "jaw_left_osc",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "1",
		amplitude_range = "0,10,1",
		amplitude_value = "-30",
		offset_range= "-1500,1500,1",
		offset_value = "-30",
		phase_range = "0,10,0.1")

	obj.addBody_gen03_2("right_clip","-23,0,4","0,0,180")
	obj.extend("right_clip","0","right_clip_elbow","0")
	obj.addBody_gen03_2("left_clip","-23,0,-4","0,180,0")
	obj.extend("left_clip","0","left_clip_elbow","0")


	obj.addElbow("right_clip_elbow","-24,0,2","0,-90,0")
	obj.extend("right_clip_elbow","1","jaw","2")

	obj.addElbow("left_clip_elbow","-24,0,-2","0,90,0")
	obj.extend("left_clip_elbow","1","jaw","5")

	obj.addContinuum("jaw","-24,0,0","0,0,0")
	obj.extend("jaw","0","back_box","3")


	obj.consume("right_clip_elbow","angle","jaw_osc","sample")
	obj.consume("left_clip_elbow","angle","jaw_left_osc","sample")

