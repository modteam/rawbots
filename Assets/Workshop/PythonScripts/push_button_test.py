import library

with open('../push_button.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("master","0,0,0","0,0,0")


	obj.extend("box001","3","master","0")
	obj.addContinuum("box001","2,0,0","0,0,0")

	obj.addMotor("motor001","4,0,0","0,0,-90")
	obj.extend("motor001","1","box001","0")

	obj.extend("box002","0","master","3")
	obj.addContinuum("box002","-2,0,0","0,0,0")

	obj.addMotor("motor002","-4,0,0","0,0,90")
	obj.extend("motor002","1","box002","3")

	obj.addTimeLineOperant("timeline001","0,0,0","0,0,0", control_keys = ["angle1","angle2"]);

	obj.consume("timeline001","angle1_input","motor001","current_angle")
	obj.consume("motor001","angle_editor_input","timeline001","angle1_data_out")

	obj.consume("timeline001","angle2_input","motor002","current_angle")
	obj.consume("motor002","angle_editor_input","timeline001","angle2_data_out")


	obj.addPushButton("button001","0,4,0","0,0,0")
	obj.extend("button001","0","master","1")

	obj.consume("timeline001","trigger","button001","trigger")


	obj.addMagnet("magnet001","0,-10,0","0,0,0")

	
