import library

with open('../../BotDefinitions/lilfish8.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addContinuum("continuum001","0,0,0","0,0,0")
	obj.addContinuum("continuum002","4,0,0","0,0,0")
	obj.addMotor("motor001","2,0,0","0,0,270", initial_values=[["velocity","500"]])

	obj.addFin("fin001","0,0,2","0,270,0")
	obj.addFin("fin002","0,0,-2","0,90,0")

	obj.addFin("fin003","4,2,0","0,45,90")
	obj.addFin("fin004","4,-2,0","0,-45,270")
	obj.addFin("fin005","4,0,2","60,90,180")
	obj.addFin("fin006","4,0,-2","-45,90,0")

	obj.extend("continuum001","2","fin001","0")
	obj.extend("continuum001","5","fin002","0")
	obj.extend("continuum001","0","motor001","1")

	obj.extend("continuum002","1","fin003","0")
	obj.extend("continuum002","4","fin004","0")
	obj.extend("continuum002","2","fin005","0")
	obj.extend("continuum002","5","fin006","0")
	obj.extend("continuum002","3","motor001","0")


