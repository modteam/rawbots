import library

with open('../../Resources/Bots/potatosub.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addBody_cvr_tlt_gen01_05("bdy_cvr_tlt_gen01_050","-3,1,0","0,270,0")
	obj.addBody_gen02_1("body_gen02_11","6,0,-3","0,270,270")
	obj.addBody_gen02_1("body_gen02_12","6,0,3","0,270,90")
	obj.addContinuum("continuum3","0,0,-2","0,0,0")
	obj.addContinuum("continuum4","0,2,-2","0,0,0")
	obj.addContinuum("continuum5","10,0,0","0,0,0")
	obj.addContinuum("continuum6","0,0,0","0,0,0")
	obj.addContinuum("continuum7","0,0,2","0,0,0")
	obj.addContinuum("continuum8","8,0,0","0,0,0")
	obj.addContinuum("continuum9","0,0,-6","0,0,0")
	obj.addContinuum("continuum10","-2,0,0","0,0,0")
	obj.addContinuum("continuum11","0,0,-4","0,0,0")
	obj.addContinuum("continuum12","4,0,0","0,0,0")
	obj.addContinuum("continuum13","6,0,0","0,0,0")
	obj.addContinuum("continuum14","0,0,4","0,0,0")
	obj.addContinuum("continuum15","0,2,2","0,0,0")
	obj.addContinuum("continuum16","2,0,0","0,0,0")
	obj.addContinuum("continuum17","6,0,2","0,0,0")
	obj.addContinuum("continuum18","6,0,-2","0,0,0")
	obj.addContinuum("continuum19","0,0,6","0,0,0")
	obj.addFin("fin20","0,0,-10","0,90,0")
	obj.addFin("fin21","0,0,10","0,270,0")
	obj.addFluxCapacitor("fluxcapacitor22","0,2,6","0,0,0")
	obj.addFluxCapacitor("fluxcapacitor23","0,2,-6","0,0,0")
	obj.addHydroJet("hydrojet24","10,0,2","90,270,0")
	obj.addHydroJet("hydrojet25","10,0,-2","90,270,0")
	obj.addMotor("motor26","0,0,8","90,0,0")
	obj.addMotor("motor27","0,0,-8","270,0,0")
	obj.addPlasmaCannon("plasmacannon28","-3,0,-6","0,0,90")
	obj.addPlasmaCannon("plasmacannon29","-3,0,6","0,0,90")
	obj.addSpotLight("spotlight30","-2,2,2","0,0,90")
	obj.addSpotLight("spotlight31","-2,2,-2","0,0,90")

	obj.extend("continuum5","0","continuum8","3")
	obj.extend("continuum8","0","continuum13","3")
	obj.extend("continuum13","0","continuum12","3")
	obj.extend("continuum12","0","continuum16","3")
	obj.extend("continuum16","0","continuum6","3")
	obj.extend("continuum6","0","continuum10","3")
	obj.extend("continuum10","0","bdy_cvr_tlt_gen01_050","0")

	obj.extend("body_gen02_11","0","continuum18","2")
	obj.extend("continuum18","5","continuum13","2")
	obj.extend("continuum13","5","continuum17","2")
	obj.extend("continuum17","5","body_gen02_12","0")
	obj.extend("continuum5","2","hydrojet24","0")
	obj.extend("continuum5","5","hydrojet25","1")

	obj.extend("fin21","0","motor26","0")
	obj.extend("motor26","1","continuum19","2")
	obj.extend("continuum19","5","continuum14","2")
	obj.extend("continuum14","5","continuum7","2")
	obj.extend("continuum7","5","continuum6","2")
	obj.extend("continuum6","5","continuum3","2")
	obj.extend("continuum3","5","continuum11","2")
	obj.extend("continuum11","5","continuum9","2")
	obj.extend("continuum9","5","motor27","1")
	obj.extend("motor27","0","fin20","0")

	obj.extend("continuum9","1","fluxcapacitor23","0")
	obj.extend("continuum19","1","fluxcapacitor22","0")
	obj.extend("continuum3","1","continuum4","4")
	obj.extend("continuum7","1","continuum15","4")
	obj.extend("continuum4","3","spotlight31","0")
	obj.extend("continuum15","3","spotlight30","0")
	obj.extend("continuum9","3","plasmacannon28","0")
	obj.extend("continuum19","3","plasmacannon29","0")

	obj.consume("plasmacannon28", "feed","fluxcapacitor22","part")
	obj.consume("plasmacannon29", "feed","fluxcapacitor23","part")

	obj.addGraphChunk("Input_Stuff","""
#keyboard
kb01 as keyboard_sensor

kb01_ev output_for kb01
kb01_ev as keyboard_event

#inputmanager
input001 as input_manager

input001_event input_for input001
input001_event as input_event

input001_subs input_for input001
input001_subs as input_subscription

############################################################
#input setup
############################################################

forward_motion as input_sampler
forward_motion . name forward_motion
forward_motion . attack 5
forward_motion . release 0
forward_motion . positive W
forward_motion . negative S

forward_motion_sample output_for forward_motion
forward_motion_sample as input_sampler_sample

forward_mapper as sample_mapper
forward_mapper . sample_min -1
forward_mapper . sample_max 1
forward_mapper . mapped_min 0
forward_mapper . mapped_max 1000
forward_mapper . invert False

forward_mapper_sample input_for forward_mapper
forward_mapper_sample as sample_mapper_sample

forward_mapper_mapped output_for forward_mapper
forward_mapper_mapped as sample_mapper_mapped

forward_mapper_inverted_mapped output_for forward_mapper
forward_mapper_inverted_mapped as sample_mapper_mapped_inverted

forward_mapper_sample consumes forward_motion_sample

left_mapper as sample_mapper
left_mapper . sample_min -1
left_mapper . sample_max 1
left_mapper . mapped_min 0
left_mapper . mapped_max -1000
left_mapper . invert False

left_mapper_sample input_for left_mapper
left_mapper_sample as sample_mapper_sample

left_mapper_mapped output_for left_mapper
left_mapper_mapped as sample_mapper_mapped

left_mapper_inverted_mapped output_for left_mapper
left_mapper_inverted_mapped as sample_mapper_mapped_inverted

left_mapper_sample consumes forward_motion_sample

############################################################
#input setup
############################################################

side_motion as input_sampler
side_motion . name lift
side_motion . attack 5
side_motion . release 0
side_motion . positive A
side_motion . negative D

side_motion_sample output_for side_motion
side_motion_sample as input_sampler_sample

right_mapper as sample_mapper
right_mapper . sample_min -1
right_mapper . sample_max 1
right_mapper . mapped_min 0
right_mapper . mapped_max -1000
right_mapper . invert False

right_mapper_sample input_for right_mapper
right_mapper_sample as sample_mapper_sample

right_mapper_mapped output_for right_mapper
right_mapper_mapped as sample_mapper_mapped

right_mapper_inverted_mapped output_for right_mapper
right_mapper_inverted_mapped as sample_mapper_mapped_inverted

right_mapper_sample consumes side_motion_sample

############################################################

input001_event consumes kb01_ev
input001_subs consumes forward_motion_subs
input001_subs consumes side_motion_subs

motor27_angle consumes right_mapper_mapped
motor26_angle consumes left_mapper_mapped

############################################################
#jet thrust
############################################################

jet_thrust_control as input_sampler
jet_thrust_control . name jet_thrust_control
jet_thrust_control . attack 4
jet_thrust_control . release 0
jet_thrust_control . positive E
jet_thrust_control . negative Q

jet_thrust_control_sample output_for jet_thrust_control
jet_thrust_control_sample as input_sampler_sample

jet_thrust_mapper as sample_mapper
jet_thrust_mapper . sample_min -1
jet_thrust_mapper . sample_max 1
jet_thrust_mapper . mapped_min 0
jet_thrust_mapper . mapped_max 2000
jet_thrust_mapper . invert False

jet_thrust_mapper_sample input_for jet_thrust_mapper
jet_thrust_mapper_sample as sample_mapper_sample

jet_thrust_mapper_mapped output_for jet_thrust_mapper
jet_thrust_mapper_mapped as sample_mapper_mapped

jet_thrust_mapper_inverted_mapped output_for jet_thrust_mapper
jet_thrust_mapper_inverted_mapped as sample_mapper_mapped_inverted

jet_thrust_mapper_sample consumes jet_thrust_control_sample

hydrojet24_thrust consumes jet_thrust_mapper_mapped
hydrojet25_thrust consumes jet_thrust_mapper_mapped

############################################################
#fire control
############################################################

firecontrol as input_sampler
firecontrol . positive Space
firecontrol . attack 10
firecontrol . release 10

firecontrol_sample output_for firecontrol
firecontrol_sample as input_sampler_sample

firecontrol_mapper as sample_mapper
firecontrol_mapper . sample_min 0
firecontrol_mapper . sample_max 1
firecontrol_mapper . mapped_min 0
firecontrol_mapper . mapped_max 1
firecontrol_mapper . invert False

firecontrol_mapper_sample input_for firecontrol_mapper
firecontrol_mapper_sample as sample_mapper_sample

firecontrol_mapper_mapped output_for firecontrol_mapper
firecontrol_mapper_mapped as sample_mapper_mapped

firecontrol_mapper_inverted_mapped output_for firecontrol_mapper
firecontrol_mapper_inverted_mapped as sample_mapper_mapped_inverted

firecontrol_mapper_sample consumes firecontrol_sample

plasmacannon28_shoot consumes fire_control_mapper_mapped
plasmacannon29_shoot consumes fire_control_mapper_mapped

flux01_level . value 2
flux02_level . value 2
flux03_level . value 2

""")