import library

with open('../../BotDefinitions/flying.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()

    obj.addCore("core001","0,0,0","0,0,0")
    obj.addContinuum("box0","2,0,0","0,0,0")
    obj.extend("box0","3","core001","0")
    
    obj.addContinuum("box1","-2,0,0","0,0,0")
    obj.extend("box1","0","core001","3")

    obj.extend("box1","3","box2","0")

    obj.addMotor("motor1","0,2,2","0,0,0")
    obj.extend("motor1","1","box5","1")

    obj.addMotor("motor2","-5,2,0","0,0,0")
    obj.addContinuum("box2","-5,0,0","0,0,0")
    obj.extend("box2","1","motor2","1")

    obj.addMotor("motor3","-5,0,2","90,0,0")
    obj.extend("motor3","1","box2","2")

    obj.addMotor("motor4","-5,0,-2","270,0,0")
    obj.extend("motor4","1","box2","5")
    
    obj.addContinuum("box3","-5,0,-4","0,0,0")
    obj.extend("box3","2","motor4","0")

    obj.addMotor("motor5","-5,-2,-4","180,180,0")
    obj.extend("motor5","1","box3","4")

    obj.addContinuum("box4","2,0,2","0,0,0")
    obj.extend("box4","5","box0","2")

    obj.addContinuum("box5","0,0,2","0,0,0")
    obj.extend("box5","5","core001","2")

    obj.addContinuum("box6","-2,0,2","0,0,0")
    obj.extend("box6","5","box1","2")

    obj.addSensor("sensor1","0,4,2","0,270,0")
    obj.extend("sensor1","0","motor1","0")

    obj.addJet("left_jet","-5,-4,-2","270,360,0")
    obj.extend("left_jet","0","motor5","0")

    obj.addPistol("pistol1","-5,4,-1","0,180,0")
    obj.extend("pistol1","1","motor2","0")

    obj.addJet("left_back_jet","-5,-2,4","0,180,360")
    obj.extend("left_back_jet","0","motor3","0")

    #---part2

    obj.addContinuum("box7","5,0,0","0,0,0")
    obj.addMotor("motor6","5,0,-2","270,0,0")
    obj.extend("motor6","1","box7","5")

    obj.extend("box0","0","box7","3")

    obj.addContinuum("box8","5,0,-4","0,0,0")
    obj.extend("box8","2","motor6","0")

    obj.addMotor("motor7","5,-2,-4","180,180,0")
    obj.extend("motor7","1","box8","4")
    
    obj.addJet("right_jet","5,-4,-2","270,360,0")
    obj.extend("right_jet","0","motor7","0")

    obj.addMotor("motor8","5,0,2","90,0,0")
    obj.extend("motor8","1","box7","2")
    
    obj.addJet("right_back_jet","5,-2,4","0,180,360")
    obj.extend("right_back_jet","0","motor8","0")

    obj.addMotor("motor9","5,2,0","0,0,0")
    obj.extend("motor9","1","box7","1")
    
    obj.addPistol("pistol2","5,4,-1","0,180,0")
    obj.extend("pistol2","1","motor9","0")

    # Shields

    obj.addBody_gen03_1("shield1","-5,0,-5.30","270,180,0")
    obj.extend("shield1","0","box3","5")
    
    obj.addBody_gen03_1("shield2","5,0,-5.30","270,180,0")
    obj.extend("shield2","0","box8","5")

    obj.addBody_gen03_4("shield3","0,0,-1.3","270,270,0")
    obj.extend("shield3","0","core001","5")

    obj.addBody_gen03_3("shield4","0,0,3.30","270,0,0")
    obj.extend("shield4","0","box5","2")
    

    obj.addBody_gen03_1("shield5","6.30,0,0","360,360,90")
    obj.extend("shield5","0","box7","0")

    obj.addBody_gen03_1("shield6","-6.30,0,0","0,180,90")
    obj.extend("shield6","0","box2","3")
   
