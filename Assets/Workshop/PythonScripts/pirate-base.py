import library

with open('../../Resources/Bots/pirate-base.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addBody_gen03_2("body_gen03_20","0,0,10","0,0,90")
	obj.addBody_gen03_2("body_gen03_21","0,0,-10","0,180,90")
	obj.addBody_gen03_2("body_gen03_22","-10,0,0","0,270,90")
	obj.addBody_gen03_2("body_gen03_23","10,0,0","0,90,90")
	obj.addContinuum("continuum4","0,0,0","0,0,0")

	obj.addElbow("elbow8","0,0,-2","270,270,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow9","-2,0,0","270,0,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow11","0,0,2","270,90,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow12","2,0,0","270,180,0",initial_values=[["angle","45"]])

	obj.addElbow("elbow5","0,0,6","270,90,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow6","-6,0,0","270,0,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow7","6,0,0","270,180,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow10","0,0,-6","270,270,0",initial_values=[["angle","-90"]])

	obj.extend("continuum4","0","elbow12","0")
	obj.extend("continuum4","2","elbow11","0")
	obj.extend("continuum4","3","elbow9","0")
	obj.extend("continuum4","5","elbow8","0")

	obj.extend("elbow12","1","elbow7","0")
	obj.extend("elbow11","1","elbow5","0")
	obj.extend("elbow9","1","elbow6","0")
	obj.extend("elbow8","1","elbow10","0")

	obj.extend("elbow7","1","body_gen03_23","0")
	obj.extend("elbow5","1","body_gen03_20","0")
	obj.extend("elbow6","1","body_gen03_22","0")
	obj.extend("elbow10","1","body_gen03_21","0")
