import library

with open('../../Resources/Bots/saucerflier.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core01","0,0,0","0,0,0")
	obj.addContinuum("cont01","6,0,0","0,0,0")
	obj.addContinuum("cont02","4,0,4","0,45,0")
	obj.addContinuum("cont03","0,0,6","0,0,0")
	obj.addContinuum("cont04","-4,0,4","0,45,0")
	obj.addContinuum("cont05","-6,0,0","0,0,0")
	obj.addContinuum("cont06","-4,0,-4","0,45,0")
	obj.addContinuum("cont07","0,0,-6","0,0,0")
	obj.addContinuum("cont08","4,0,-4","0,45,0")

	#obj.extend("core01","2","cont03","5")
	#obj.extend("core01","3","cont05","0")
	#obj.extend("core01","5","cont07","2")
	#obj.extend("core01","0","cont01","3")

	obj.extend("cont01","2","cont02","0")
	obj.extend("cont02","3","cont03","0")

	obj.extend("cont07","3","cont06","0")
	obj.extend("cont05","5","cont06","3")

	obj.extend("cont04","2","cont03","3")
	obj.extend("cont04","5","cont05","2")

	obj.extend("cont08","2","cont01","5")
	obj.extend("cont08","5","cont07","0")

	obj.addJet("toprightjet","4,0,7","0,45,-90")
	obj.addJet("bottomrightjet","4,0,-7","0,135,90")
	obj.addJet("topleftjet","-4,0,7","0,135,270")
	obj.addJet("bottomleftjet","-4,0,-7","0,45,90")

	obj.extend("toprightjet","1","cont02","2")
	obj.extend("topleftjet","0","cont04","3")
	obj.extend("bottomrightjet","1","cont08","0")
	obj.extend("bottomleftjet","0","cont06","5")

	#obj.addJet("topjet","0,-2,4","0,0,0")
	obj.addHover("tophover","0,0,4","0,-90,0")
	#obj.addJet("bottomjet","0,-2,-4","0,0,0")
	obj.addHover("bottomhover","0,0,-4","0,90,0")
	#obj.addJet("leftjet","-4,-2,0","0,90,0")
	obj.addHover("lefthover","-4,0,0","0,0,0")
	#obj.addJet("rightjet","4,-2,0","0,90,0")
	obj.addHover("righthover","4,0,0","0,0,0")

	#obj.extend("topjet","0","cont03","5")
	obj.extend("tophover","0","cont03","5")
	obj.extend("tophover","1","core01","2")
	#obj.extend("bottomjet","1","cont07","2")
	obj.extend("bottomhover","0","cont07","2")
	obj.extend("bottomhover","1","core01","5")
	#obj.extend("leftjet","1","cont05","0")
	obj.extend("lefthover","1","cont05","0")
	obj.extend("lefthover","0","core01","3")
	#obj.extend("rightjet","0","cont01","3")
	obj.extend("righthover","0","cont01","3")
	obj.extend("righthover","1","core01","0")



	#obj.addFin("finc1","2.5,0,2.5","0,135,0")
	#obj.extend("finc1","0","cont02","5")
#
	#obj.addFin("finc3","-2.5,0,-2.5","0,135,180")
	#obj.extend("finc3","0","cont06","2")

	obj.addSensor("cam01","7,0,0","0,90,0")
	obj.extend("cam01","0","cont01","0")
	obj.addSensor("cam02","-7,0,0","0,-90,0")
	obj.extend("cam02","0","cont05","3")

	obj.addLaser("ls01","0,0,8.5","0,90,90","2")
	obj.extend("ls01","1","con03","2")

	obj.addGraphChunk("input definition", """

		#keyboard
kb01 as keyboard_sensor

kb01_ev output_for kb01
kb01_ev as keyboard_event

#inputmanager
input001 as input_manager

input001_event input_for input001
input001_event as input_event

input001_subs input_for input001
input001_subs as input_subscription

		#input setup
jets_thrust as input_setup
jets_thrust . positive W
jets_thrust . negative S
jets_thrust . sensitivity 3
jets_thrust . gravity 0
jets_thrust . axis True
jets_thrust_subs output_for jets_thrust
jets_thrust_subs as setup_subscriptor
jets_thrust_action output_for jets_thrust
jets_thrust_action as setup_action
		#input setup2
jetsleftthrust as input_setup
jetsleftthrust . positive Q
jetsleftthrust . sensitivity 0
jetsleftthrust . gravity 1
jetsleftthrust . axis False
jetsleftthrust_subs output_for jetsleftthrust
jetsleftthrust_subs as setup_subscriptor
jetsleftthrust_action output_for jetsleftthrust
jetsleftthrust_action as setup_action
		#input setup3
jetsrightthrust as input_setup
jetsrightthrust . positive E
jetsrightthrust . sensitivity 0
jetsrightthrust . gravity 1
jetsrightthrust . axis False
jetsrightthrust_subs output_for jetsrightthrust
jetsrightthrust_subs as setup_subscriptor
jetsrightthrust_action output_for jetsrightthrust
jetsrightthrust_action as setup_action
		#input setup4
jetssideright as input_setup
jetssideright . positive D
jetssideright . sensitivity 0
jetssideright . gravity 1
jetssideright . axis False
jetssideright_subs output_for jetssideright
jetssideright_subs as setup_subscriptor
jetssideright_action output_for jetssideright
jetssideright_action as setup_action
		#input setup5
jetssideleft as input_setup
jetssideleft . positive A
jetssideleft . sensitivity 0
jetssideleft . gravity 1
jetssideleft . axis False
jetssideleft_subs output_for jetssideleft
jetssideleft_subs as setup_subscriptor
jetssideleft_action output_for jetssideleft
jetssideleft_action as setup_action
		#input setup6
jetsfullthrust as input_setup
jetsfullthrust . positive Space
jetsfullthrust . sensitivity 0
jetsfullthrust . gravity 1
jetsfullthrust . axis False
jetsfullthrust_subs output_for jetsfullthrust
jetsfullthrust_subs as setup_subscriptor
jetsfullthrust_action output_for jetsfullthrust
jetsfullthrust_action as setup_action
		#input linear_mapper
jets_thrust_map as linear_mapper
jets_thrust_map . map [0,500,1]

jets_raw input_for jets_thrust_map
jets_raw as linear_mapper_raw

jets_thrust_mapped output_for jets_thrust_map
jets_thrust_mapped as linear_mapper_mapped

jets_raw consumes jets_thrust_action

		#input linear_mapper2
jets_side_right_map as linear_mapper
jets_side_right_map . map [0,500,1]

jets_side_right_raw input_for jets_side_right_map
jets_side_right_raw as linear_mapper_raw

jets_side_right_mapped output_for jets_side_right_map
jets_side_right_mapped as linear_mapper_mapped

jets_side_right_raw consumes jetssideright_action

		#input linear_mapper3
jets_side_left_map as linear_mapper
jets_side_left_map . map [0,500,1]

jets_side_left_raw input_for jets_side_left_map
jets_side_left_raw as linear_mapper_raw

jets_side_left_mapped output_for jets_side_left_map
jets_side_left_mapped as linear_mapper_mapped

jets_side_left_raw consumes jetssideleft_action

		#input linear_mapper4
jetsleft_map as linear_mapper
jetsleft_map . map [0,500,1]

jetsleft_map_raw input_for jetsleft_map
jetsleft_map_raw as linear_mapper_raw

jetsleft_map_mapped output_for jetsleft_map
jetsleft_map_mapped as linear_mapper_mapped

jetsleft_map_raw consumes jetsleftthrust_action

		#input linear_mapper5
jetsright_map as linear_mapper
jetsright_map . map [0,500,1]

jetsright_map_raw input_for jetsright_map
jetsright_map_raw as linear_mapper_raw

jetsright_map_mapped output_for jetsright_map
jetsright_map_mapped as linear_mapper_mapped

jetsright_map_raw consumes jetsrightthrust_action

		#input linear_mapper6
jetsfullthrust_map as linear_mapper
jetsfullthrust_map . map [0,500,1]

jetsfullthrust_map_raw input_for jetsfullthrust_map
jetsfullthrust_map_raw as linear_mapper_raw

jetsfullthrust_map_mapped output_for jetsfullthrust_map
jetsfullthrust_map_mapped as linear_mapper_mapped

jetsfullthrust_map_raw consumes jetsfullthrust_action

#consumes
input001_event consumes kb01_ev
input001_subs consumes jets_thrust_subs
input001_subs consumes jetsleftthrust_subs
input001_subs consumes jetsrightthrust_subs
input001_subs consumes jetssideright_subs
input001_subs consumes jetssideleft_subs
input001_subs consumes jetsfullthrust_subs

topjet_thrust consumes jets_thrust_mapped
bottomjet_thrust consumes jets_thrust_mapped
leftjet_thrust consumes jets_thrust_mapped
rightjet_thrust consumes jets_thrust_mapped

#topleftjet_thrust consumes jets_thrust_mapped
#bottomrightjet_thrust consumes jets_thrust_mapped

topleftjet_thrust consumes jetsleft_map_mapped
bottomrightjet_thrust consumes jetsleft_map_mapped

toprightjet_thrust consumes jetsright_map_mapped
bottomleftjet_thrust consumes jetsright_map_mapped

bottomleftjet_thrust consumes jets_side_right_mapped
bottomrightjet_thrust consumes jets_side_right_mapped

topleftjet_thrust consumes jets_side_left_mapped
toprightjet_thrust consumes jets_side_left_mapped

topjet_thrust consumes jetsfullthrust_map_mapped
bottomjet_thrust consumes jetsfullthrust_map_mapped
leftjet_thrust consumes jetsfullthrust_map_mapped
rightjet_thrust consumes jetsfullthrust_map_mapped

	""")