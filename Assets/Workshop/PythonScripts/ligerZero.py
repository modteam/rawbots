import library

with open('../ligerZero.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("master","-1,0,0","0,0,0")
	obj.addContinuum("continuum001","2,0,0","0,0,0")
	obj.extend("continuum001","3","master","0")
	obj.addContinuum("continuum002","5,0,0","0,0,0")
	obj.extend("continuum002","3","continuum001","0")
	obj.addContinuum("continuum003","8,0,0","0,0,0")
	obj.extend("continuum003","3","continuum002","0")
	obj.addContinuum("continuum004","11,0,0","0,0,0")
	obj.extend("continuum004","3","continuum003","0")
	obj.addContinuum("continuum005","14,0,0","0,0,0")
	obj.extend("continuum005","3","continuum004","0")
	obj.addContinuum("continuum006","17,2,0","0,0,0")
	obj.extend("continuum006","3","continuum005","0")

	obj.addMotor("bm1","-1,0,3","90,0,0")
	obj.extend("bm1","1","master","2")
	obj.addMotor("bm2","-1,0,-3","-90,0,0")
	obj.extend("bm2","1","master","5")
	obj.addMotor("bm3","12,0,3","90,0,0")
	obj.extend("bm3","1","continuum004","2")
	obj.addMotor("bm4","12,0,-3","-90,0,0")
	obj.extend("bm4","1","continuum004","5")

	obj.addContinuum("rightleg","-1,0,-6","0,0,0")
	obj.extend("rightleg","2","bm2","0")
	obj.addContinuum("rightleg_1","-1,-5,-8","0,0,0")
	obj.extend("rightleg_1","1","rightleg","4")
	obj.addMotor("rightleg_1_bm5","-1,-5,-5","90,0,0")
	obj.extend("rightleg_1_bm5","1","rightleg_1","2")
	obj.addContinuum("rightleg_2","-1,-5,-2","0,0,0")
	obj.extend("rightleg_2","5","rightleg_1_bm5","0")


	obj.addContinuum("rightfoot","-1,-8,-2","0,0,0")
	obj.extend("rightfoot","4","rightleg_2","1")
	obj.addContinuum("rightfoot_1","-1,-10,-2","0,0,0")
	obj.extend("rightfoot_1","4","rightfoot","1")
	obj.addContinuum("rightfoot_2","-1,-10,-4","0,0,0")
	obj.extend("rightfoot_2","5","rightfoot_1","2")

	obj.addContinuum("leftleg","-1,0,6","0,0,0")
	obj.extend("leftleg","5","bm1","0")
	obj.addContinuum("leftleg_1","-1,-5,8","0,0,0")
	obj.extend("leftleg_1","1","leftleg","4")
	obj.addMotor("leftleg_1_bm5","-1,-5,5","-90,0,0")
	obj.extend("leftleg_1_bm5","1","leftleg_1","5")
	obj.addContinuum("leftleg_2","-1,-5,2","0,0,0")
	obj.extend("leftleg_2","2","leftleg_1_bm5","0")

	obj.addContinuum("leftfoot","-1,-8,2","0,0,0")
	obj.extend("leftfoot","4","leftleg_2","1")
	obj.addContinuum("leftfoot_1","-1,-10,2","0,0,0")
	obj.extend("leftfoot_1","4","leftfoot","1")
	obj.addContinuum("leftfoot_2","-1,-10,4","0,0,0")
	obj.extend("leftfoot_2","5","leftfoot_1","2")




	obj.addContinuum("rightarm","12,0,-6","0,0,0")
	obj.extend("rightarm","2","bm4","0")
	obj.addContinuum("rightarm_1","12,-5,-8","0,0,0")
	obj.extend("rightarm_1","1","rightarm","4")
	obj.addMotor("rightarm_1_bm5","12,-5,-5","90,0,0")
	obj.extend("rightarm_1_bm5","1","rightarm_1","2")
	obj.addContinuum("rightarm_2","12,-5,-2","0,0,0")
	obj.extend("rightarm_2","5","rightarm_1_bm5","0")


	obj.addContinuum("righthand","12,-8,-2","0,0,0")
	obj.extend("righthand","4","rightarm_2","1")
	obj.addContinuum("righthand_1","12,-10,-2","0,0,0")
	obj.extend("righthand_1","4","righthand","1")
	obj.addContinuum("righthand_2","12,-10,-4","0,0,0")
	obj.extend("righthand_2","5","righthand_1","2")

	obj.addContinuum("leftarm","12,0,6","0,0,0")
	obj.extend("leftarm","5","bm3","0")
	obj.addContinuum("leftarm_1","12,-5,8","0,0,0")
	obj.extend("leftarm_1","1","leftarm","4")
	obj.addMotor("leftarm_1_bm5","12,-5,5","-90,0,0")
	obj.extend("leftarm_1_bm5","1","leftarm_1","5")
	obj.addContinuum("leftarm_2","12,-5,2","0,0,0")
	obj.extend("leftarm_2","2","leftarm_1_bm5","0")


	obj.addContinuum("lefthand","12,-8,2","0,0,0")
	obj.extend("lefthand","4","leftarm_2","1")
	obj.addContinuum("lefthand_1","12,-10,2","0,0,0")
	obj.extend("lefthand_1","4","lefthand","1")
	obj.addContinuum("lefthand_2","12,-10,4","0,0,0")
	obj.extend("lefthand_2","5","lefthand_1","2")


	obj.addOscillator(name = "osc1",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Triangle",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.2",
		amplitude_range = "0,45,1",
		amplitude_value = "0",
		offset_range= "0,100,1",
		offset_value = "0",
		phase_range = "0,10,0.1")
	obj.addOscillator(name = "osc2",
		pos = "0,10,0",
		rot= "0,0,0",
		type="Triangle",
		invert= "True",
		frequency_range = "0,10,0.1",
		frequency_value = "0.2",
		amplitude_range = "0,45,1",
		amplitude_value = "0",
		offset_range= "0,100,1",
		offset_value = "0",
		phase_range = "0,10,0.1")

	obj.consume("bm1","angle","osc1","sample")
	obj.consume("bm2","angle","osc1","sample")
	obj.consume("bm3","angle","osc1","sample")
	obj.consume("bm4","angle","osc1","sample")

	obj.consume("rightleg_1_bm5","angle","osc2","sample")
	obj.consume("rightarm_1_bm5","angle","osc2","sample")
	obj.consume("leftleg_1_bm5","angle","osc2","sample")
	obj.consume("leftarm_1_bm5","angle","osc2","sample")
#rightleg_1_bm5



