import library

with open('../../Resources/Bots/pirate-turret.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addBody_gen03_2("body_gen03_20","0,0,10","0,0,90")
	obj.addBody_gen03_2("body_gen03_21","0,0,-10","0,180,90")
	obj.addBody_gen03_2("body_gen03_22","-10,0,0","0,270,90")
	obj.addBody_gen03_2("body_gen03_23","10,0,0","0,90,90")
	obj.addContinuum("continuum4","0,0,0","0,0,0")

	obj.addElbow("elbow8","0,0,-2","270,270,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow9","-2,0,0","270,0,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow11","0,0,2","270,90,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow12","2,0,0","270,180,0",initial_values=[["angle","45"]])

	obj.addElbow("elbow5","0,0,6","270,90,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow6","-6,0,0","270,0,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow7","6,0,0","270,180,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow10","0,0,-6","270,270,0",initial_values=[["angle","-90"]])

	obj.extend("continuum4","0","elbow12","0")
	obj.extend("continuum4","2","elbow11","0")
	obj.extend("continuum4","3","elbow9","0")
	obj.extend("continuum4","5","elbow8","0")

	obj.extend("elbow12","1","elbow7","0")
	obj.extend("elbow11","1","elbow5","0")
	obj.extend("elbow9","1","elbow6","0")
	obj.extend("elbow8","1","elbow10","0")

	obj.extend("elbow7","1","body_gen03_23","0")
	obj.extend("elbow5","1","body_gen03_20","0")
	obj.extend("elbow6","1","body_gen03_22","0")
	obj.extend("elbow10","1","body_gen03_21","0")

	#Turrets
	obj.addBody_gen01_2("body_gen01_20","0,4,-2","90,0,0")
	obj.addBody_gen03_1("body_gen03_11","0,8,1","0,90,270")
	obj.addContinuum("continuum05","-2,8,0","0,0,0")
	obj.addContinuum("continuum06","0,4,0","0,0,0")
	obj.addContinuum("continuum07","2,8,0","0,0,0")
	obj.addContinuum("continuum08","0,8,0","0,0,0")
	obj.addElbow("elbow09","0,6,0","0,0,90")
	obj.addMotor("motor10","0,2,0","0,0,0")
	obj.addPlasmaCannon("plasmacannon01","0,8,-3","270,0,0")
	obj.addPlasmaCannon("plasmacannon02","-2,8,-3","270,0,0")
	obj.addPlasmaCannon("plasmacannon03","2,8,-3","270,0,0")

	obj.extend("motor10","0","continuum4","1")
	obj.extend("motor10","1","continuum06","4")
	obj.extend("continuum06","1","elbow09","0")
	obj.extend("elbow09","1","continuum08","4")
	obj.extend("continuum08","0","continuum07","3")
	obj.extend("continuum08","3","continuum05","0")

	obj.extend("plasmacannon01","0","continuum08","5")
	obj.extend("plasmacannon02","0","continuum05","5")
	obj.extend("plasmacannon03","0","continuum07","5")
	obj.extend("continuum08","2","body_gen03_11","0")
	obj.extend("continuum06","5","body_gen01_20","0")


