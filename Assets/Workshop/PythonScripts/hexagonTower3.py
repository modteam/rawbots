import library

with open('../../BotDefinitions/hexagonTower3.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	#Creating platforms
	obj.addHexPlat("platform000","0,0,0","0,0,0")
	obj.addHexPlat("platform001","0,42,0","0,0,0")

	#creating platforms
	obj.addHexPlat("platform002","0,84,0","0,0,0")
	obj.addHexPlat("platform003","0,126,0","0,0,0")

	#Creating columns
	obj.addHexColumn("column000","47,0,0","270,0,0")
	obj.addHexColumn("column001","-47,0,0","270,180,0")
	obj.addHexColumn("column002","-23.4,0,-40.7","270,120,0")
	obj.addHexColumn("column003","23.4,0,-40.7","270,60,0")
	obj.addHexColumn("column004","23.4,0,40.7","270,300,0")
	obj.addHexColumn("column005","-23.4,0,40.7","270,240,0")

	#Creating columns
	obj.addHexColumn("column006","47,42,0","270,0,0")
	obj.addHexColumn("column007","-47,42,0","270,180,0")
	obj.addHexColumn("column008","-23.4,42,-40.7","270,120,0")
	obj.addHexColumn("column009","23.4,42,-40.7","270,60,0")
	obj.addHexColumn("column010","23.4,42,40.7","270,300,0")
	obj.addHexColumn("column011","-23.4,42,40.7","270,240,0")

	#Creating columns
	obj.addHexColumn("column012","47,84,0","270,0,0")
	obj.addHexColumn("column013","-47,84,0","270,180,0")
	obj.addHexColumn("column014","-23.4,84,-40.7","270,120,0")
	obj.addHexColumn("column015","23.4,84,-40.7","270,60,0")
	obj.addHexColumn("column016","23.4,84,40.7","270,300,0")
	obj.addHexColumn("column017","-23.4,84,40.7","270,240,0")

	#Connecting platform000 to columns
	obj.extend("platform000","01","column000","01")
	obj.extend("platform000","02","column001","01")
	obj.extend("platform000","03","column002","01")
	obj.extend("platform000","04","column003","01")
	obj.extend("platform000","05","column004","01")
	obj.extend("platform000","06","column005","01")

	#Connecting platform001 to columns
	obj.extend("platform001","20","column000","00")
	obj.extend("platform001","21","column001","00")
	obj.extend("platform001","22","column002","00")
	obj.extend("platform001","23","column003","00")
	obj.extend("platform001","24","column004","00")
	obj.extend("platform001","25","column005","00")

	#Connecting platform001 to columns
	obj.extend("platform001","01","column006","01")
	obj.extend("platform001","02","column007","01")
	obj.extend("platform001","03","column008","01")
	obj.extend("platform001","04","column009","01")
	obj.extend("platform001","05","column010","01")
	obj.extend("platform001","06","column011","01")

	#Connecting platform002 to columns
	obj.extend("platform002","20","column006","00")
	obj.extend("platform002","21","column007","00")
	obj.extend("platform002","22","column008","00")
	obj.extend("platform002","23","column009","00")
	obj.extend("platform002","24","column010","00")
	obj.extend("platform002","25","column011","00")

	#Connecting platform002 to columns
	obj.extend("platform002","01","column012","01")
	obj.extend("platform002","02","column013","01")
	obj.extend("platform002","03","column014","01")
	obj.extend("platform002","04","column015","01")
	obj.extend("platform002","05","column016","01")
	obj.extend("platform002","06","column017","01")

	#Connecting platform003 to columns
	obj.extend("platform003","20","column012","00")
	obj.extend("platform003","21","column013","00")
	obj.extend("platform003","22","column014","00")
	obj.extend("platform003","23","column015","00")
	obj.extend("platform003","24","column016","00")
	obj.extend("platform003","25","column017","00")
