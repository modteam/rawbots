import library

with open('../../Resources/Bots/PanaElevator.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addContinuum("continuum0","0,1,41.63849","0,0,0")
	obj.addContinuum("continuum1","0,5,41.63849","0,0,0")
	obj.addContinuum("continuum2","0,3,41.63849","0,0,0")
	obj.addContinuum("continuum3","0,5,-41.63849","0,0,0")
	obj.addContinuum("continuum4","0,1,-41.63849","0,0,0")
	obj.addContinuum("continuum5","0,5,-39.63849","0,0,0")
	obj.addContinuum("continuum6","0,5,39.63849","0,0,0")
	obj.addContinuum("continuum7","0,3,-41.63849","0,0,0")
	obj.addHexPlat("hexplat8","0,0,0","0,0,0")
	obj.addPiston("piston9","0,3,-39.5","0,0,180")
	obj.addPiston("piston10","0,3,39.5","0,0,180")
	obj.addHexEarth("hexearth01","0,-21,-81.4","0,0,0")
	obj.addHexEarth("hexearth02","0,-21,81.4","0,0,0")

	obj.extend("continuum4","1","continuum7","4")
	obj.extend("continuum7","1","continuum3","4")
	obj.extend("continuum3","2","continuum5","5")

	obj.extend("continuum5","4","piston9","1")
	obj.extend("piston9","0","hexplat8","26")

	obj.extend("continuum0","1","continuum2","4")
	obj.extend("continuum2","1","continuum1","4")
	obj.extend("continuum1","2","continuum6","5")

	obj.extend("continuum6","4","piston10","1")
	obj.extend("piston10","0","hexplat8","32")

	obj.extend("continuum4","4","hexearth01","40")

	obj.extend("continuum0","4","hexearth02","34")

	obj.addOscillator(name = "osc_elev",
		type="Sine",
		invert= "False",
		frequency_value = "0.20",
		amplitude_value = "0.5",
		offset_value = "0.5",
		phase_value = "0")

	obj.consume("piston9","position","osc_elev","sample")
	obj.consume("piston10","position","osc_elev","sample")
