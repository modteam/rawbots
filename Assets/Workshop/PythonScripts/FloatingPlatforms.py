import library

with open('../../BotDefinitions/FloatingPlatforms.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	#Creating terrain
	obj.addHexPlat("platform000","0,0,0","0,0,0")
	obj.addHexPlat("platform001","70,0,40","0,0,0")
	obj.addHexPlat("platform002","0,0,81","0,0,0")

	#Creating hover
	obj.addHover("hover000","-22,-0.7,-42","0,270,0")
	obj.addHover("hover001","22,-0.7,-42","0,270,0")
	obj.addHover("hover002","-25,-0.7,-41","0,330,0")
	obj.addHover("hover003","-48,-0.7,-2","0,330,0")
	obj.addHover("hover004","-22,-0.7,123","0,90,0")
	obj.addHover("hover005","22,-0.7,123","0,90,0")
	obj.addHover("hover006","-25,-0.7,122","0,30,0")
	obj.addHover("hover007","-48,-0.7,83","0,30,0")
	obj.addHover("hover008","95,-0.7,-1","0,210,0")
	obj.addHover("hover009","118,-0.7,38","0,210,0")
	obj.addHover("hover010","118,-0.7,42","0,150,0")
	obj.addHover("hover011","95,-0.7,81","0,150,0")

	#Connecting platform000 to platform002
	obj.extend("platform000","08","platform002","09")
	obj.extend("platform000","07","platform002","10")

	#Connecting platform000 to platform001
	obj.extend("platform000","16","platform001","18")
	obj.extend("platform000","15","platform001","17")

	#Connecting platform001 to platform002
	obj.extend("platform001","13","platform002","11")
	obj.extend("platform001","14","platform002","12")

	#Connecting platform000 to hovers
	obj.extend("platform000","09","hover000","0")
	obj.extend("platform000","10","hover001","0")
	obj.extend("platform000","17","hover002","0")
	obj.extend("platform000","18","hover003","0")

	#Connecting platform002 to hovers
	obj.extend("platform002","08","hover004","0")
	obj.extend("platform002","07","hover005","0")
	obj.extend("platform002","14","hover006","0")
	obj.extend("platform002","13","hover007","0")

	#Connecting platform001 to hovers
	obj.extend("platform001","11","hover008","0")
	obj.extend("platform001","12","hover009","0")
	obj.extend("platform001","15","hover010","0")
	obj.extend("platform001","16","hover011","0")

