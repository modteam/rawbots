import library

with open('../../BotDefinitions/helicopter001.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()

    obj.addCore("core001","0,0,0","0,0,0")
    obj.addContinuum("box01","0,0,2","0,0,0")
    obj.extend("box01","5","core001","2")

    obj.addContinuum("box02","0,0,4","0,0,0")
    obj.extend("box02","5","box01","2")

    obj.addContinuum("box03","0,0,6","0,0,0")
    obj.extend("box03","5","box02","2")

    obj.addContinuum("box04","0,0,-2","0,0,0")
    obj.extend("box04","2","core001","5")

    obj.addContinuum("box05","0,0,-4","0,0,0")
    obj.extend("box05","2","box04","5")

    obj.addContinuum("box06","0,0,-6","0,0,0")
    obj.extend("box06","2","box05","5")

    obj.addContinuum("box07","2,0,0","0,0,0")
    obj.extend("box07","3","core001","0")

    obj.addContinuum("box08","-2,0,0","0,0,0")
    obj.extend("box08","0","core001","3")

    obj.addMotor("motor01","4,0,0","90,90,0")
    obj.extend("motor01","1","box07","0")

    obj.addMotor("motor02","-4,0,0","90,270,0")
    obj.extend("motor02","1","box08","3")

    obj.addCannon("cannon01","-7,0,-1","0,180,270")
    obj.extend("cannon01","0","motor02","0")

    obj.addCannon("cannon02","7,0,-1","0,180,90")
    obj.extend("cannon02","0","motor01","0")

    obj.addContinuum("box10","0,-4,6","0,0,0")
    obj.extend("box10","1","motor08","1")

    obj.addMotor("motor03","-2,-4,6","90,270,0")
    obj.extend("motor03","1","box10","3")

    obj.addMotor("motor04","2,-4,6","90,90,0")
    obj.extend("motor04","1","box10","0")

    obj.addMotor("motor07","0,-2,-6","0,90,0")
    obj.extend("motor07","0","box06","4")

    obj.addContinuum("box09","0,-4,-6","0,0,0")
    obj.extend("box09","1","motor07","1")
    
    obj.addMotor("motor05","-2,-4,-6","90,270,0")
    obj.extend("motor05","1","box09","3")

    obj.addMotor("motor06","2,-4,-6","90,90,0")
    obj.extend("motor06","1","box09","0")

    obj.addMotor("motor08","0,-2,6","0,90,0")
    obj.extend("motor08","0","box03","4")

    obj.addMotor("motor09","0,2,0","0,90,0")
    obj.extend("motor09","1","core001","1")

    obj.addTripleHelix("helix01","0,4,0","0,90,0")
    obj.extend("helix01","0","motor09","0")

    obj.addJet("jet01","-4,-4,8","0,90,270")
    obj.extend("jet01","0","motor03","0")

    obj.addJet("jet02","4,-4,8","0,90,270")
    obj.extend("jet02","1","motor04","0")

    obj.addSensor("sensor01","4,-4,-6","0,180,90")
    obj.extend("sensor01","0","motor06","0")

    obj.addSensor("sensor02","-4,-4,-6","0,180,270")
    obj.extend("sensor02","0","motor05","0")


    # --- Shields

    obj.addBody_gen03_1("shield1","0,1,4","0,0,180")
    obj.extend("shield1","0","box02","1")

    obj.addBody_gen03_1("shield2","0,1,-4","0,0,180")
    obj.extend("shield2","0","box05","1")

    obj.addBody_gen03_1("shield3","0,-4,7","0,270,90")
    obj.extend("shield3","0","box10","2")

    obj.addBody_gen03_4("shield4","0,-4,-7","270,270,0")
    obj.extend("shield4","0","box09","5")

    
