import library

with open('../../Resources/Bots/pirate-evolve-turret.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addBody_gen03_2("body_gen03_20","0,2,17","270,90,0")
	obj.addBody_gen03_2("body_gen03_21","0,2,-17","270,270,0")
	obj.addBody_gen03_2("body_gen03_22","-17,2,0","270,0,0")
	obj.addBody_gen03_2("body_gen03_23","17,2,0","270,180,0")

	obj.addContinuum("continuum0","0,0,0","0,0,0")

	obj.addElbow("motorelbow01","2,0,0","360,0,180")
	obj.addElbow("motorelbow02","-2,0,0","0,0,0")
	obj.addElbow("motorelbow03","0,0,2","0,90,0")
	obj.addElbow("motorelbow04","0,0,-2","0,270,0")

	obj.extend("continuum0","5","motorelbow01","0")
	obj.extend("continuum0","2","motorelbow02","0")
	obj.extend("continuum0","0","motorelbow03","0")
	obj.extend("continuum0","3","motorelbow04","0")


	obj.addElbow("elbow8","0,0,-4","270,270,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow9","-4,0,0","270,0,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow11","0,0,4","270,90,0",initial_values=[["angle","45"]])
	obj.addElbow("elbow12","4,0,0","270,180,0",initial_values=[["angle","45"]])

	obj.addElbow("elbow10","0,0,-8","270,270,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow5","0,0,8","270,90,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow6","-8,0,0","270,0,0",initial_values=[["angle","-90"]])
	obj.addElbow("elbow7","8,0,0","270,180,0",initial_values=[["angle","-90"]])

	obj.extend("motorelbow01","1","elbow12","0")
	obj.extend("motorelbow02","1","elbow9","0")
	obj.extend("motorelbow04","1","elbow8","0")
	obj.extend("motorelbow03","1","elbow11","0")

	obj.extend("elbow12","1","elbow7","0")
	obj.extend("elbow11","1","elbow5","0")
	obj.extend("elbow9","1","elbow6","0")
	obj.extend("elbow8","1","elbow10","0")

	obj.addContinuum("continuum1","-16,0,0","0,0,0")
	obj.addContinuum("continuum2","0,0,16","0,0,0")
	obj.addContinuum("continuum3","16,0,0","0,0,0")
	obj.addContinuum("continuum4","0,0,-16","0,0,0")

	obj.extend("continuum3","1","body_gen03_23","0")
	obj.extend("continuum2","1","body_gen03_20","0")
	obj.extend("continuum1","1","body_gen03_22","0")
	obj.extend("continuum4","1","body_gen03_21","0")

	obj.addElbow("elbow13","0,0,12","90,180,90",initial_values=[["angle","0"]])
	obj.addElbow("elbow14","-12,0,0","90,90,90",initial_values=[["angle","0"]])
	obj.addElbow("elbow15","12,0,0","90,270,90",initial_values=[["angle","0"]])
	obj.addElbow("elbow16","0,0,-12","90,0,90",initial_values=[["angle","0"]])

	obj.extend("elbow7","1","elbow15","0")
	obj.extend("elbow5","1","elbow13","0")
	obj.extend("elbow6","1","elbow14","0")
	obj.extend("elbow10","1","elbow16","0")

	obj.extend("continuum3","3","elbow15","1")
	obj.extend("continuum2","5","elbow13","1")
	obj.extend("continuum1","0","elbow14","1")
	obj.extend("continuum4","2","elbow16","1")

	obj.addHook("hook01","-19,0,0","0,0,270")
	obj.addHook("hook02","0,0,17","270,0,0")
	obj.addHook("hook03","19,0,0","0,0,90")
	obj.addHook("hook04","0,0,-17","90,0,0")

	obj.extend("continuum1","3","hook01","0")
	obj.extend("continuum2","2","hook02","0")
	obj.extend("continuum3","0","hook03","0")
	obj.extend("continuum4","5","hook04","0")


	# #Turret
	# obj.addBody_gen01_2("body_gen01_turret","0,4,-2","90,0,0")
	# obj.addBody_gen03_1("body_gen03_turret","0,8,1","0,90,270")
	# obj.addContinuum("continuum_t1","-2,8,0","0,0,0")
	# obj.addContinuum("continuum_t2","0,4,0","0,0,0")
	# obj.addContinuum("continuum_t3","2,8,0","0,0,0")
	# obj.addContinuum("continuum_t4","0,8,0","0,0,0")
	# obj.addElbow("elbowturret","0,6,0","0,0,90")
	# obj.addMotor("motorturret","0,2,0","0,0,0")
	# obj.addPlasmaCannon("plasmacannon01","0,8,-3","270,0,0")
	# obj.addPlasmaCannon("plasmacannon02","-2,8,-3","270,0,0")
	# obj.addPlasmaCannon("plasmacannon03","2,8,-3","270,0,0")

	# obj.extend("motorturret","0","continuum0","1")
	# obj.extend("motorturret","1","continuum_t2","4")
	# obj.extend("continuum_t2","1","elbowturret","0")
	# obj.extend("elbowturret","1","continuum_t4","4")
	# obj.extend("continuum_t4","0","continuum_t3","3")
	# obj.extend("continuum_t4","3","continuum_t1","0")

	# obj.extend("plasmacannon01","0","continuum_t4","5")
	# obj.extend("plasmacannon02","0","continuum_t1","5")
	# obj.extend("plasmacannon03","0","continuum_t3","5")
	# obj.extend("continuum_t4","2","body_gen03_turret","0")
	# obj.extend("continuum_t2","5","body_gen01_turret","0")

	# obj.addFluxCapacitor("flux01","-2,10,0","0,0,0")
	# obj.addFluxCapacitor("flux02","0,10,0","0,0,0")
	# obj.addFluxCapacitor("flux03","2,10,0","0,0,0")

	# obj.extend("flux01","0","continuum_t1","1")
	# obj.extend("flux02","0","continuum_t4","1")
	# obj.extend("flux03","0","continuum_t3","1")

	# obj.consume("plasmacannon01", "feed","flux01","part")
	# obj.consume("plasmacannon02", "feed","flux02","part")
	# obj.consume("plasmacannon03", "feed","flux03","part")

	obj.addOscillator(name = "leg01",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "50",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "0.5",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "leg02",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "50",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "1",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "leg03",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "50",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "1.5",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "leg04",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "50",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "0",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "rotor01",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "5",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "0.5",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "rotor02",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "5",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "1.5",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "rotor03",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "-5",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "1",
		phase_range = "0,10,0.1")

	obj.addOscillator(name = "rotor04",
		pos = "0,4,5",
		rot= "0,0,0",
		type="Sine",
		invert= "False",
		frequency_range = "0,10,0.1",
		frequency_value = "0.71",
		amplitude_range = "0,10,1",
		amplitude_value = "5",
		offset_range= "-1500,1500,1",
		offset_value = "10",
		phase_value = "0",
		phase_range = "0,10,0.1")

	obj.consume("elbow12","angle","leg01","sample")
	obj.consume("elbow9","angle","leg02","sample")
	obj.consume("elbow8","angle","leg03","sample")
	obj.consume("elbow11","angle","leg04","sample")

	#obj.consume("motorelbow01","angle","rotor01","sample")
	#obj.consume("motorelbow02","angle","rotor02","sample")
	#obj.consume("motorelbow03","angle","rotor03","sample")
	#obj.consume("motorelbow04","angle","rotor04","sample")

	obj.addGraphChunk("input definition", """
		#keyboard
kb01 as keyboard_sensor

kb01_ev output_for kb01
kb01_ev as keyboard_event

#inputmanager
input001 as input_manager

input001_event input_for input001
input001_event as input_event

input001_subs input_for input001
input001_subs as input_subscription

input001_event consumes kb01_ev

######################################################

fire_setup as input_setup

fire_setup . name fire_setup
fire_setup . positive Space
fire_setup . sensitivity 0
fire_setup . gravity 1
fire_setup . axis False

fire_setup_subscriptor output_for fire_setup
fire_setup_subscriptor as setup_subscriptor

fire_setup_action output_for fire_setup
fire_setup_action as setup_action

input001_subs consumes fire_setup_subscriptor

plasmacannon01_shoot consumes fire_setup_action
plasmacannon02_shoot consumes fire_setup_action
plasmacannon03_shoot consumes fire_setup_action

flux01_level . value 2
flux02_level . value 2
flux03_level . value 2

""")