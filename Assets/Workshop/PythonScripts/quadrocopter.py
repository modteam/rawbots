import library

with open('../quadrocopter.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core001","0,0,0","0,0,0")

	obj.addContinuum("holder1a","2,0,0","0,0,0")
	obj.addContinuum("holder1e","4,0,0","0,0,0")
	obj.addContinuum("holder1i","6,0,0","0,0,0")
	obj.addContinuum("holder1","10,0,0","0,0,0")

	obj.extend("holder1a","3","core001","0")
	obj.extend("holder1e","3","holder1a","0")
	obj.extend("holder1i","3","holder1e","0")
	obj.extend("holder1","3","holder1i","0")

	obj.addContinuum("holder2","-10,0,0","0,0,0")
	obj.addContinuum("holder2a","-6,0,0","0,0,0")
	obj.addContinuum("holder2e","-4,0,0","0,0,0")
	obj.addContinuum("holder2i","-2,0,0","0,0,0")

	obj.extend("holder2i","0","core001","3")
	obj.extend("holder2e","0","holder2i","3")
	obj.extend("holder2a","0","holder2e","3")
	obj.extend("holder2","0","holder2a","3")


#	obj.extend("holder2","0","core001","3")

	obj.addHelix("helix1","12,0,12","0,90,0")
	obj.addMotor("motor1","12,-2,12","0,90,0",initial_values =[["velocity","500"]])
	obj.extend("helix1","0","motor1","0")
	obj.addContinuum("base1","12,-4,12","0,0,0")
	obj.extend("motor1","1","base1","1")
	obj.extend("base1","5","holder1","2")


	obj.addHelix("helix2","12,0,-12","0,90,0")
	obj.addMotor("motor2","12,-2,-12","0,90,0",initial_values =[["velocity","500"]])
	obj.extend("helix2","0","motor2","0")
	obj.addContinuum("base2","12,-4,-12","0,0,0")
	obj.extend("motor2","1","base2","1")
	obj.extend("base2","2","holder1","5")


	obj.addHelix("helix3","-12,0,12","0,90,0")
	obj.addMotor("motor3","-12,-2,12","0,90,0",initial_values =[["velocity","500"]])
	obj.extend("helix3","0","motor3","0")
	obj.addContinuum("base3","-12,-4,12","0,0,0")
	obj.extend("motor3","1","base3","1")
	obj.extend("base3","5","holder2","2")

	obj.addHelix("helix4","-12,0,-12","0,90,0")
	obj.addMotor("motor4","-12,-2,-12","0,90,0",initial_values =[["velocity","500"]])
	obj.extend("helix4","0","motor4","0")
	obj.addContinuum("base4","-12,-4,-12","0,0,0")
	obj.extend("motor4","1","base4","1")
	obj.extend("base4","2","holder2","5")