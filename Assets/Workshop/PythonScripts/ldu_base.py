import library

with open('../../BotDefinitions/ldu-base.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core00","0,0,0","0,0,0")
	
	obj.addMotor("center_right_motor","2,0,0","0,0,270")
	obj.addWideWheelSmall("center_right_wheel","4,0,0","0,90,0")
	obj.extend("center_right_motor","1","core00","0")
	obj.extend("center_right_wheel","1","center_right_motor","0")

	obj.addMotor("center_left_motor","-2,0,0","0,0,90")
	obj.addWideWheelSmall("center_left_wheel","-4,0,0","0,90,0")
	obj.extend("center_left_motor","1","core00","3")
	obj.extend("center_left_wheel","0","center_left_motor","0")

	obj.addContinuum("back_wheel_support","0,0,-4","0,0,0")
	obj.extend("back_wheel_support","2","core00","5")

	obj.addMotor("back_right_motor","2,0,-4","0,0,270")
	obj.addWideWheelSmall("back_right_wheel","4,0,-4","0,90,0")
	obj.extend("back_right_motor","1","back_wheel_support","0")
	obj.extend("back_right_wheel","1","back_right_motor","0")

	obj.addMotor("back_left_motor","-2,0,-4","0,0,90")
	obj.addWideWheelSmall("back_left_wheel","-4,0,-4","0,90,0")
	obj.extend("back_left_motor","1","back_wheel_support","3")
	obj.extend("back_left_wheel","0","back_left_motor","0")

	obj.addContinuum("front_wheel_support","0,0,4","0,0,0")
	obj.extend("front_wheel_support","5","front_support","2")

	obj.addMotor("front_right_motor","2,0,4","0,0,270")
	obj.addWideWheelSmall("front_right_wheel","4,0,4","0,90,0")
	obj.extend("front_right_motor","1","front_wheel_support","0")
	obj.extend("front_right_wheel","1","front_right_motor","0")

	obj.addMotor("front_left_motor","-2,0,4","0,0,90")
	obj.addWideWheelSmall("front_left_wheel","-4,0,4","0,90,0")
	obj.extend("front_left_motor","1","front_wheel_support","3")
	obj.extend("front_left_wheel","0","front_left_motor","0")

	obj.addContinuum("front_support","0,0,2","0,0,0")
	obj.extend("front_support","2","front_wheel_support","5")
	obj.extend("core00","2","front_support","5")

	obj.addContinuum("hip","0,2,2","0,0,0")
	obj.addContinuum("chest","0,4,2","0,0,0")
	obj.addSensor("head","0,6,2","0,0,0")
	obj.extend("hip","4","front_support","1")
	obj.extend("chest","4","hip","1")
	obj.extend("head","0","chest","1")

	obj.addContinuum("shield_support","0,2,4","0,0,0")
	obj.extend("shield_support","4","front_wheel_support","1")

	obj.addElbow("right_light_holder","2,4,2","0,0,0")
	obj.extend("right_light_holder","1","chest","0")
