import library

with open('../dog.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("core00","0,0,0","0,0,0")
	obj.addMotor("rightshoulder","2,2,2","0,0,270")
	obj.addMotor("leftshoulder","-2,2,2","0,0,90")
	obj.extend("rightshoulder","1","core00","0")
	obj.extend("leftshoulder","1","core00","3")

	obj.addElbow("rightarmpit","4,2,2","90,180,0",initial_values=[["angle","90"]])
	obj.extend("rightarmpit","0","rightshoulder","0")

	obj.addElbow("rightelbow","7,2,2","0,180,0",initial_values=[["angle","30"]])
	obj.extend("rightelbow","0","rightarmpit","1")
	obj.addFoot("rightpad","10,2,2","0,0,90")
	obj.extend("rightpad","0","rightelbow","1")

	obj.addElbow("leftarmpit","-4,2,2","90,180,0",initial_values=[["angle","90"]])
	obj.extend("leftarmpit","1","leftshoulder","0")

	obj.addElbow("leftelbow","-7,2,2","0,180,0",initial_values=[["angle","30"]])
	obj.extend("leftelbow","1","leftarmpit","0")
	obj.addFoot("leftpad","-10,2,2","0,0,270")
	obj.extend("leftpad","0","leftelbow","0")

	obj.addElbow("vertebrae00","0,0,-2","90,270,0",initial_values=[["angle","-45"]])
	obj.extend("vertebrae00","0","core00","5")
	obj.addPiston("vertebrae","0,0,-4","270,0,0",initial_values=[["position","0"]])
	obj.extend("vertebrae","1","vertebrae00","1")
	obj.addContinuum("back","0,0,-8","0,0,0")
	obj.extend("back","2","vertebrae","0")
	obj.addMotor("righthip","2,2,-8","0,0,270")
	obj.addMotor("lefthip","-2,2,-8","0,0,90")
	obj.extend("righthip","1","back","0")
	obj.extend("lefthip","1","back","3")
	
	obj.addElbow("rightgroin","4,2,-8","90,180,0",initial_values=[["angle","90"]])
	obj.extend("rightgroin","0","righthip","0")
	obj.addElbow("rightknee01","6,2,-8","0,180,0",initial_values=[["angle","90"]])
	obj.extend("rightknee01","0","rightgroin","1")

	obj.addElbow("rightknee02","10,2,-8","0,180,0",initial_values=[["angle","270"]])
	obj.extend("rightknee02","0","rightknee01","1")
	obj.addPiston("rightleg","12,2,-8","0,0,270",initial_values=[["position","0.25"]])
	obj.extend("rightleg","1","rightknee02","1")
	obj.addElbow("rightankle","16,2,-8","180,180,0",initial_values=[["angle","45"]])
	obj.extend("rightankle","0","rightleg","0")
	obj.addFoot("rightfoot","18,2,-8","0,0,90")
	obj.extend("rightfoot","0","rightankle","1")

	obj.addElbow("leftgroin","-4,2,-8","90,0,0",initial_values=[["angle","90"]])
	obj.extend("leftgroin","0","lefthip","0")
	obj.addElbow("leftknee01","-6,2,-8","0,0,0",initial_values=[["angle","270"]])
	obj.extend("leftknee01","0","leftgroin","1")

	obj.addElbow("leftknee02","-10,2,-8","0,0,0",initial_values=[["angle","90"]])
	obj.extend("leftknee02","0","leftknee01","1")
	obj.addPiston("leftleg","-12,2,-8","0,0,90",initial_values=[["position","0.25"]])
	obj.extend("leftleg","1","leftknee02","1")
	obj.addElbow("leftankle","-16,2,-8","0,0,0",initial_values=[["angle","45"]])
	obj.extend("leftankle","0","leftleg","0")
	obj.addFoot("leftfoot","-18,2,-8","0,0,270")
	obj.extend("leftfoot","0","leftankle","1")