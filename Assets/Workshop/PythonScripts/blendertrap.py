import library

with open('../../Resources/Bots/blendertrap.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()
    obj.addHexPlat("trapbase","0,0,0","0,0,0")
    obj.addContinuum("cont1","0,3,0","0,0,0")
    obj.addMotor("motor01","0,1,0","0,0,0")
    obj.extend("trapbase","00","motor01","1")
    obj.extend("cont1","4","motor01","0") 

    obj.addLaser("laser01","2,3,0","0,0,-90","0.75")
    obj.addLaser("laser02","0,3,2","0,90,90","0.75")
    obj.addLaser("laser03","-2,3,0","0,0,90","0.75")
    obj.addLaser("laser04","0,3,-2","0,-90,90","0.75")

    obj.extend("laser01","0","cont1","5")
    obj.extend("laser02","0","cont1","0")
    obj.extend("laser03","0","cont1","2")
    obj.extend("laser04","0","cont1","3")

    obj.addOscillator(name = "trap_osc",
    pos = "0,1,0",
    rot= "0,0,0",
    type="Sine",
    invert= "False",
    frequency_range = "0,10,0.1",
    frequency_value = "1",
    amplitude_range = "0,10,1",
    amplitude_value = "180",
    offset_range= "-1500,1500,1",
    offset_value = "0",
    phase_range = "0,10,0.1")
    obj.consume("motor01","angle","trap_osc","sample")

    obj.addGraphChunk("laser lengths","""
laser01 . length 1
laser02 . length 1
laser03 . length 1
laser04 . length 1
    	""")