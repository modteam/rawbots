import library

with open('../../Resources/Bots/bigturret.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("mainc","0,2,0","0,45,0")
	obj.addFoot("foottr","2.75,0,2.75","0,45,0")
	obj.addFoot("foottl","-2.75,0,2.75","0,315,0")
	obj.addFoot("footbr","2.75,0,-2.75","0,135,0")
	obj.addFoot("footbl","-2.75,0,-2.75","0,225,0")

	obj.extend("mainc","2","foottr","0")
	obj.extend("mainc","3","foottl","0")
	obj.extend("mainc","0","footbr","0")
	obj.extend("mainc","5","footbl","0")

	#continuum rear
	#continuum axis
	#continuum right
	#continuum left

	obj.addMotor("motor01", "0,4,0", "0,0,0")
	obj.extend("mainc", "1", "motor01","1")

	obj.addContinuum("contleft","0,6,-2","0,0,0")
	obj.addContinuum("contaxis","0,6,0","0,0,0")
	obj.addContinuum("contrear","2,6,0","0,0,0")
	obj.addContinuum("contright","0,6,2","0,0,0")

	obj.extend("motor01", "0", "contaxis", "4")

	obj.extend("contleft","0","contaxis","3")
	obj.extend("contright","3","contaxis","0")
	obj.extend("contrear","2","contaxis","5")

	obj.addElbow("rightgunelbow","-2,6,2","-90,0,0")
	obj.addElbow("centergunelbow","-2,6,0","-90,0,0")
	obj.addElbow("leftgunelbow","-2,6,-2","-90,0,0")

	obj.extend("leftgunelbow","0","contleft","2")
	obj.extend("centergunelbow","0","contaxis","2")
	obj.extend("rightgunelbow","0","contright","2")

	obj.addPlasmaCannon("plasmaleft","-5,6,-2","90,0,90")
	obj.addPlasmaCannon("plasmacenter","-5,6,0","90,0,90")
	obj.addPlasmaCannon("plasmaright","-5,6,2","90,0,90")

	obj.extend("plasmaleft","0","leftgunelbow","1")
	obj.extend("plasmacenter","0","centergunelbow","1")
	obj.extend("plasmaright","0","rightgunelbow","1")

	obj.addFluxCapacitor("fluxright","2,6,2","90,0,0")
	obj.addFluxCapacitor("fluxcenter","4,6,0","0,0,-90")
	obj.addFluxCapacitor("fluxleft","2,6,-2","-90,0,0")

	obj.extend("fluxleft","0","contrear","0")
	obj.extend("fluxcenter","0","contrear","5")
	obj.extend("fluxright","0","contrear","3")

	obj.consume("plasmaleft", "feed","fluxleft","part")
	obj.consume("plasmacenter", "feed","fluxcenter","part")
	obj.consume("plasmaright", "feed","fluxright","part")

	obj.addContinuum("camcont", "2,8,0", "0,0,0")
	obj.extend("camcont","4","contrear","1")
	obj.addElbow("camelbow","0,8,0","270,180,0")
	obj.extend("camelbow","1","camcont","2")

	obj.addSensor("sensor1","-1,8,0","0,270,0")
	obj.extend("sensor1", "0","camelbow","0")

	obj.addBody_gen01_3("leftshield","0,6,-3.5","0,90,-270")
	obj.addBody_gen01_3("rightshield","0,6,3.5","0,90,270")
	obj.addBody_gen01_3("rearshield","4.25,7,0","90,-90,0")

	obj.extend("leftshield","0","contleft","3")
	obj.extend("rightshield","0","contright","0")
	obj.extend("rearshield","0","contrear","5")


	obj.addGraphChunk("input definition", """
		#keyboard
kb01 as keyboard_sensor

kb01_ev output_for kb01
kb01_ev as keyboard_event

#inputmanager
input001 as input_manager

input001_event input_for input001
input001_event as input_event

input001_subs input_for input001
input001_subs as input_subscription

#input setup
side_motion as input_setup
side_motion . positive A
side_motion . negative D
side_motion . sensitivity 2
side_motion . gravity 0
side_motion . axis True

side_motion_subs output_for side_motion
side_motion_subs as setup_subscriptor

side_motion_action output_for side_motion
side_motion_action as setup_action

#input linear_mapper
right_side_map as linear_mapper
right_side_map . map [0,180,1]
right_side_raw input_for right_side_map
right_side_raw as linear_mapper_raw
right_side_mapped output_for right_side_map
right_side_mapped as linear_mapper_mapped
right_side_raw consumes side_motion_action

input001_event consumes kb01_ev
input001_subs consumes side_motion_subs

motor01_angle consumes right_side_mapped

#input setup elbowanglecontrol
elbow_angle_input as input_setup
elbow_angle_input . positive W
elbow_angle_input . negative S
elbow_angle_input . sensitivity 2
elbow_angle_input . gravity 0
elbow_angle_input . axis True

elbow_angle_input_subs output_for elbow_angle_input
elbow_angle_input_subs as setup_subscriptor

elbow_angle_input_action output_for elbow_angle_input
elbow_angle_input_action as setup_action


#elbowInputMapper
elbow_angle_map as linear_mapper
elbow_angle_map . map [45,90,1]

elbow_angle_raw input_for elbow_angle_map
elbow_angle_raw as linear_mapper_raw

elbow_angle_mapped output_for elbow_angle_map
elbow_angle_mapped as linear_mapper_mapped

elbow_angle_raw consumes elbow_angle_input_action

input001_subs consumes elbow_angle_input_subs

rightgunelbow_angle consumes elbow_angle_mapped
leftgunelbow_angle consumes elbow_angle_mapped
centergunelbow_angle consumes elbow_angle_mapped
camelbow_angle consumes elbow_angle_mapped

rightgunelbow_angle . value 45
leftgunelbow_angle . value 45
centergunelbow_angle . value 45
camelbow_angle . value 45


######################################################

fire_setup as input_setup

fire_setup . name fire_setup
fire_setup . positive Space
fire_setup . sensitivity 0
fire_setup . gravity 1
fire_setup . axis False

fire_setup_subscriptor output_for fire_setup
fire_setup_subscriptor as setup_subscriptor

fire_setup_action output_for fire_setup
fire_setup_action as setup_action

input001_subs consumes fire_setup_subscriptor

plasmaleft_shoot consumes fire_setup_action
plasmaright_shoot consumes fire_setup_action
plasmacenter_shoot consumes fire_setup_action

fluxright_level . value 3
fluxcenter_level . value 2
fluxleft_level . value 4

""")