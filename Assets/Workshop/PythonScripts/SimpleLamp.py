import library

with open('../../BotDefinitions/Light.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addCore("master","0,0,0","0,0,0")

	obj.addHook("hook001","0,-3,0","0,0,0")

	obj.extend("hook001","0","master","4")

	obj.addElbow("elbow001","0,2,0","0,0,90",initial_values = [["angle","90"]])
	obj.extend("elbow001","1","master","1")
	obj.addSpotLight("light001","0,4,0","0,0,0")

	obj.extend("light001","0","elbow001","0")
