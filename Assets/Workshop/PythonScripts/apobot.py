import library

with open('../../Resources/Bots/apobot.txt', 'w') as f:
	obj = library.Workshop(f)
	obj.libs()

	obj.addBody_cvr_tlt_gen01_05("bdy_cvr_tlt_gen01_050","4,12,8.010032","0,0,0")
	obj.addBody_cvr_tlt_gen01_05("bdy_cvr_tlt_gen01_051","-4,13,0","0,270,180")
	obj.addBody_cvr_tlt_gen01_05("bdy_cvr_tlt_gen01_052","6,11,0","0,90,0")
	obj.addBody_cvr_tlt_gen01_05("bdy_cvr_tlt_gen01_053","4,12,-8","0,180,0")
	obj.addBody_gen02_3("body_gen02_34","12,6,-7","0,270,270")
	obj.addBody_gen02_3("body_gen02_35","12,6,7","0,270,90")
	obj.addBody_gen03_1("body_gen03_16","5,6,8","90,270,0")
	obj.addBody_gen03_1("body_gen03_17","5,6,-8","90,270,0")
	obj.addBody_gen03_1("body_gen03_18","5,2,-4","90,90,0")
	obj.addBody_gen03_1("body_gen03_19","5,2,4","90,90,0")
	obj.addBody_gen03_4("body_gen03_410","0,8,0","90,0,0")
	obj.addBody_gen03_4("body_gen03_411","4,14,0","0,90,270")
	obj.addContinuum("continuum12","2,10,0","0,0,0")
	obj.addContinuum("continuum13","4,12,-2","0,0,0")
	obj.addContinuum("continuum14","4,10,0","0,0,0")
	obj.addContinuum("continuum15","2,2,-8","0,0,0")
	obj.addContinuum("continuum16","-2,12,2","0,0,0")
	obj.addContinuum("continuum17","4,12,0","0,0,0")
	obj.addContinuum("continuum18","-2,12,-2","0,0,0")
	obj.addContinuum("continuum19","4,2,-8","0,0,0")
	obj.addContinuum("continuum20","4,10,8","0,0,0")
	obj.addContinuum("continuum21","4,12,-8","0,0,0")
	obj.addContinuum("continuum22","2,6,0","0,0,0")
	obj.addContinuum("continuum23","-2,12,0","0,0,0")
	obj.addContinuum("continuum24","4,8,0","0,0,0")
	obj.addContinuum("continuum25","4,12,2","0,0,0")
	obj.addContinuum("continuum26","6,2,-8","0,0,0")
	obj.addContinuum("continuum27","4,10,-8","0,0,0")
	obj.addContinuum("continuum28","4,2,-6","0,0,0")
	obj.addContinuum("continuum29","4,8,8","0,0,0")
	obj.addContinuum("continuum30","4,8,-8","0,0,0")
	obj.addContinuum("continuum31","4,12,8","0,0,0")
	obj.addContinuum("continuum32","4,2,-10","0,0,0")
	obj.addContinuum("continuum33","6,6,-4","0,0,1")
	obj.addContinuum("continuum34","6,8,-4","0,0,1")
	obj.addContinuum("continuum35","6,0,-4","0,0,0")
	obj.addContinuum("continuum36","6,2,-4","0,0,0")
	obj.addContinuum("continuum37","6,4,-4","0,0,0")
	obj.addContinuum("continuum38","6,-2,-4","0,0,0")
	obj.addContinuum("continuum39","4,6,0","0,0,0")
	obj.addContinuum("continuum40","6,8,0","0,0,0")
	obj.addContinuum("continuum41","8,6,0","0,0,0")
	obj.addContinuum("continuum42","10,6,0","0,0,0")
	obj.addContinuum("continuum43","12,6,0","0,0,0")
	obj.addContinuum("continuum44","6,8,4","0,0,1")
	obj.addContinuum("continuum45","6,6,4","0,0,1")
	obj.addContinuum("continuum46","6,2,4","0,0,0")
	obj.addContinuum("continuum47","6,0,4","0,0,0")
	obj.addContinuum("continuum48","6,4,4","0,0,0")
	obj.addContinuum("continuum49","6,-2,4","0,0,0")
	obj.addContinuum("continuum50","16,6,0","0,0,0")
	obj.addContinuum("continuum51","12,6,4","0,0,0")
	obj.addContinuum("continuum52","12,6,2","0,0,0")
	obj.addContinuum("continuum53","12,6,-2","0,0,0")
	obj.addContinuum("continuum54","12,6,-4","0,0,0")
	obj.addContinuum("continuum55","14,6,0","0,0,0")
	obj.addCore("core56","2,8,0","0,0,0")
	obj.addElbow("elbow57","4,12,-6","0,90,0")
	obj.addElbow("elbow58","0,12,0","90,0,0")
	obj.addElbow("elbow59","4,12,6","0,90,0")
	obj.addElbow("elbow60","4,6,8","0,90,90",initial_values=[["angle","-90"]])
	obj.addElbow("elbow61","4,6,-8","0,90,90",initial_values=[["angle","-90"]])
	obj.addElbow("elbow62","2,12,0","0,0,0")
	obj.addElbow("elbow63","6,6,0","270,0,0")
	obj.addFist("fist64","4,-3,8","90,-90,0")
	obj.addFluxCapacitor("fluxcapacitor65","-2,12,4","90,0,0")
	obj.addFluxCapacitor("fluxcapacitor66","-2,12,-4","270,0,0")
	obj.addLaser("laser67","4,0,-6","0,0,180")
	obj.addLaser("laser68","6,0,-8","0,0,180")
	obj.addLaser("laser69","2,0,-8","0,0,180")
	obj.addLaser("laser70","4,0,-10","0,0,180")
	obj.addMotor("motor71","4,4,8","0,0,180")
	obj.addMotor("motor72","4,4,-8","0,0,180",initial_values=[["velocity","0"]])
	obj.addMotor("motor73","4,12,4","90,0,0")
	obj.addMotor("motor74","4,12,-4","90,180,0")
	obj.addMotor("motor75","6,8,-2","0,270,90")
	obj.addMotor("motor76","16,6,-2","0,270,90",initial_values=[["velocity","-50"]])#Wheelmotor
	obj.addMotor("motor77","6,8,2","0,90,90")
	obj.addMotor("motor78","6,-2,-2","0,90,90",initial_values=[["velocity","-50"]])#Wheelmotorfront
	obj.addMotor("motor79","6,-2,2","0,270,90",initial_values=[["velocity","50"]])#Wheelmotorfront
	obj.addMotor("motor80","16,6,2","0,90,90",initial_values=[["velocity","50"]])#Wheelmotor
	obj.addPiston("piston81","4,2,8","0,0,180")
	obj.addPlasmaCannon("plasmacannon82","-5,12,2","270,90,0")
	#obj.addPlasmaCannon("plasmacannon83","-3,6,-8","270,90,0")
	obj.addPlasmaCannon("plasmacannon84","-5,12,-2","270,90,0")
	obj.addSpotLight("spotlight85","0,6,0","0,0,91")
	obj.addWideWheel("widewheel86","6,-2,0","0,0,0")
	obj.addWideWheel("widewheelsmall87","16,6,4","0,0,0")
	obj.addWideWheel("widewheelsmall88","16,6,-4","0,0,0")

	obj.extend("continuum38","1","continuum35","4")
	obj.extend("continuum35","1","continuum36","4")
	obj.extend("continuum36","1","continuum37","4")
	obj.extend("continuum37","1","continuum33","4")
	obj.extend("continuum33","1","continuum34","4")
	obj.extend("continuum49","1","continuum47","4")
	obj.extend("continuum47","1","continuum46","4")
	obj.extend("continuum46","1","continuum48","4")
	obj.extend("continuum48","1","continuum45","4")
	obj.extend("continuum45","1","continuum44","4")
	obj.extend("continuum50","3","continuum55","0")
	obj.extend("continuum55","3","continuum43","0")
	obj.extend("continuum43","3","continuum42","0")
	obj.extend("continuum42","3","continuum41","0")
	obj.extend("continuum41","3","elbow63","0")
	obj.extend("elbow63","1","continuum39","0")
	obj.extend("continuum39","3","continuum22","0")
	obj.extend("continuum22","3","spotlight85","0")
	obj.extend("body_gen02_34","0","continuum54","5")
	obj.extend("continuum54","2","continuum53","5")
	obj.extend("continuum53","2","continuum43","5")
	obj.extend("continuum43","2","continuum52","5")
	obj.extend("continuum52","2","continuum51","5")
	obj.extend("continuum51","2","body_gen02_35","0")
	obj.extend("continuum50","5","motor76","1")
	obj.extend("continuum50","2","motor80","1")
	obj.extend("motor76","0","widewheelsmall88","0")
	obj.extend("motor80","0","widewheelsmall87","1")
	obj.extend("continuum38","2","motor78","0")
	obj.extend("motor78","1","widewheel86","1")
	obj.extend("widewheel86","0","motor79","1")
	obj.extend("motor79","0","continuum49","5")
	obj.extend("continuum34","2","motor75","0")
	obj.extend("motor75","1","continuum40","5")
	obj.extend("continuum40","2","motor77","1")
	obj.extend("motor77","0","continuum44","5")
	obj.extend("continuum39","1","continuum24","4")
	obj.extend("continuum24","1","continuum14","4")
	obj.extend("continuum14","1","continuum17","4")
	obj.extend("continuum17","1","body_gen03_411","0")
	obj.extend("continuum22","1","core56","4")
	obj.extend("core56","1","continuum12","4")
	obj.extend("core56","3","body_gen03_410","0")
	obj.extend("core56","0","continuum24","3")
	obj.extend("continuum24","0","continuum40","3")
	obj.extend("continuum17","3","elbow62","0")
	obj.extend("elbow62","1","elbow58","0")
	obj.extend("elbow58","1","continuum23","0")
	obj.extend("continuum23","3","bdy_cvr_tlt_gen01_051","0")
	obj.extend("continuum23","2","continuum16","5")
	obj.extend("continuum16","2","fluxcapacitor65","0")
	obj.extend("continuum18","2","continuum23","5")
	obj.extend("continuum18","5","fluxcapacitor66","0")
	obj.extend("continuum18","3","plasmacannon84","0")
	obj.extend("continuum16","3","plasmacannon82","0")
	obj.extend("body_gen02_34","0","continuum54","5")
	obj.extend("body_gen02_35","0","continuum51","2")
	obj.extend("continuum46","3","body_gen03_19","0")
	obj.extend("continuum36","3","body_gen03_18","0")
	obj.extend("continuum14","0","bdy_cvr_tlt_gen01_052","0")
	obj.extend("bdy_cvr_tlt_gen01_053","0","continuum21","5")
	obj.extend("continuum21","2","elbow57","0")
	obj.extend("elbow57","1","motor74","0")
	obj.extend("motor74","1","continuum13","5")
	obj.extend("continuum13","2","continuum17","5")
	obj.extend("continuum17","2","continuum25","5")
	obj.extend("continuum25","2","motor73","1")
	obj.extend("motor73","0","elbow59","0")
	obj.extend("elbow59","1","continuum31","5")
	obj.extend("continuum31","2","bdy_cvr_tlt_gen01_050","0")
	obj.extend("continuum31","4","continuum20","1")
	obj.extend("continuum20","4","continuum29","1")
	obj.extend("continuum29","4","elbow60","0")
	obj.extend("elbow60","1","motor71","1")
	obj.extend("continuum29","0","body_gen03_16","0")
	obj.extend("motor71","0","piston81","1")
	obj.extend("piston81","0","fist64","0")
	obj.extend("continuum21","4","continuum27","1")
	obj.extend("continuum27","4","continuum30","1")
	obj.extend("continuum30","4","elbow61","0")
	obj.extend("elbow61","1","motor72","1")
	obj.extend("motor72","0","continuum19","1")
	obj.extend("continuum30","0","body_gen03_17","0")
	obj.extend("continuum26","3","continuum19","0")
	obj.extend("continuum19","3","continuum15","0")
	obj.extend("continuum32","2","continuum19","5")
	obj.extend("continuum19","2","continuum28","5")
	obj.extend("continuum15","4","laser69","0")
	obj.extend("continuum32","4","laser70","0")
	obj.extend("continuum28","4","laser67","0")
	obj.extend("continuum26","4","laser68","0")

	obj.consume("plasmacannon82", "feed","fluxcapacitor65","part")
	obj.consume("plasmacannon84", "feed","fluxcapacitor66","part")

	obj.addGraphChunk("input definition", """
		#keyboard
kb01 as keyboard_sensor

kb01_ev output_for kb01
kb01_ev as keyboard_event

#inputmanager
input001 as input_manager

input001_event input_for input001
input001_event as input_event

input001_subs input_for input001
input001_subs as input_subscription

input001_event consumes kb01_ev

######################################################

fire_setup as input_setup

fire_setup . name fire_setup
fire_setup . positive Space
fire_setup . sensitivity 0
fire_setup . gravity 1
fire_setup . axis False

fire_setup_subscriptor output_for fire_setup
fire_setup_subscriptor as setup_subscriptor

fire_setup_action output_for fire_setup
fire_setup_action as setup_action

input001_subs consumes fire_setup_subscriptor

plasmacannon82_shoot consumes fire_setup_action
plasmacannon84_shoot consumes fire_setup_action

fluxcapacitor65_level . value 2
fluxcapacitor66_level . value 3

""")
