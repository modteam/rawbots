import library

with open('../../BotDefinitions/buoyant.txt', 'w') as f:
    obj = library.Workshop(f)
    obj.libs()

    obj.addCore("core1","0,2,-2.5", "0,0,0")
    obj.addFloatingDevice("fltop","0,0,0", "0,90,0")
    obj.addFloatingDevice("flleft","-2.5,0,-4", "0,-30,0")
    obj.addFloatingDevice("flright","2.5,0,-4", "0,30,0")
    obj.addMotor("motor1","0,4,-2.5", "0,0,0")
    obj.addContinuum("cont1","0,6,-2.5", "0,0,0")
    obj.addSpotLight("topspotlight","0,6,-4.5", "270,0,0")
    obj.addSpotLight("bottomspotlight","0,6,-0.5", "90,0,0")

    obj.extend("core1","2", "fltop","0")
    obj.extend("core1","3", "flleft","0")
    obj.extend("core1","0", "flright","0")
    obj.extend("core1","1","motor1","1")
    obj.extend("motor1","0","cont1","4")
    obj.extend("cont1","5","topspotlight","0")
    obj.extend("cont1","2","bottomspotlight","0")

    obj.addOscillator(name = "light_osc",
    pos = "0,4,5",
    rot= "0,0,0",
    type="Sine",
    invert= "False",
    frequency_range = "0,10,0.1",
    frequency_value = "0.05",
    amplitude_range = "0,10,1",
    amplitude_value = "180",
    offset_range= "-1500,1500,1",
    offset_value = "0",
    phase_range = "0,10,0.1")

    obj.consume("motor1","angle","light_osc","sample")