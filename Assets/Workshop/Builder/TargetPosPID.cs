using UnityEngine;
using System.Collections;

public class TargetPosPID : MonoBehaviour {


    public Oracle.PID pid ;
    private Transform target;
    public System.Action PrepareFunc = () => {};
    public System.Action GoalFunc = () => {};
    public System.Action FailFunc = () => {};
    public System.Func< Vector3 > targetFunc = () => Vector3.zero;
    public System.Func< Quaternion > targetRotationFunc = () => Quaternion.identity;

    public BulletRigidBody bulletRigidbody;

    public bool breakPid = false;
    
    float timer;

    public bool useOldConnectMode; //use a force to place the assembly to connect, instead of just teleporting it into place.
    //used in scenarios where teleportation could possibly bend joints in an impossible manner -z26

    public void Initialize ( System.Func< Vector3 > targetFunc, System.Func< Quaternion > targetRotation ) {
        pid = new Oracle.PID();
        pid.Reset( transform.position );
        pid.setpoint = targetFunc();
        this.targetFunc = targetFunc;

        targetRotationFunc = targetRotation;
        pid.kp = 50.0f;
        pid.ki = 1f;
        pid.kd = 8f;

        timer = Time.time;

        bulletRigidbody = GetComponent<BulletRigidBody>();
        StartCoroutine( MoveToTarget() );
    }


    IEnumerator MoveToTarget () { //an action like that is an IEnumerator? I need to read up on that. -z26
        while ( true ) {
            if ( Time.time - timer > 2f  || breakPid) {
                FailFunc();
                break;
            }
            pid.setpoint = targetFunc();
            var force = pid.Compute( transform.position, Time.deltaTime );
            //Unless the legacy connection mode is enabed, teleport the part into place regardless if its close or not to its target. -z26
            if ( Vector3.Distance( targetFunc(), transform.position ) < 0.5f || !useOldConnectMode) {
                GoalFunc();
                break;
            }

            force = Vector3.ClampMagnitude(force,1200);
            bulletRigidbody.AddForce(force,Vector3.zero);
            var rotation = Quaternion.Lerp(transform.rotation, targetRotationFunc(),0.4f);
            bulletRigidbody.SetRotation(rotation);

            yield return null;

        }

    }

    public void Destroy () {
        Destroy( this );
    }
}
