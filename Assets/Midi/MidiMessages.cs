using System;

public class MidiMessages {
    public interface IMidiMessage {
        int DataBytesCount
     { get; }
     
        int Channel
     { get; set; }

        int Timestamp
        { get; set; }

        void AddData ( byte value, int index );
    }
 
    public class NoteOff : IMidiMessage {
     
        public int DataBytesCount
     { get { return 2; } }
     
        public int Channel
     { get; set; }
     
        public int Note
     { get; set; }
     
        public int Velocity
     { get; set; }

        public int Timestamp
        { get; set; }

        public void AddData ( byte value, int index ) {
            if ( index == 0 ) {
                Note = ( int )value;
            }
         
            if ( index == 1 ) {
                Velocity = ( int )value;
            }
        }
     
        public override string ToString () {
            return string.Format( "[NoteOff] Channel:{0}, Note:{1}, Velocity:{2}", Channel, Note, Velocity );
        }
    }
 
    public class NoteOn : IMidiMessage {
     
        public int DataBytesCount
     { get { return 2; } }
     
        public int Channel
     { get; set; }
     
        public int Note
     { get; set; }
     
        public int Velocity
     { get; set; }

        public int Timestamp
        { get; set; }

        public void AddData ( byte value, int index ) {
            if ( index == 0 ) {
                Note = ( int )value;
            }
         
            if ( index == 1 ) {
                Velocity = ( int )value;
            }
        }
     
        public override string ToString () {
            return string.Format( "[NoteOn] Channel:{0}, Note:{1}, Velocity:{2}", Channel, Note, Velocity );
        }
    }
 
    public class NoteAfterTouch : IMidiMessage {
     
        public int DataBytesCount
     { get { return 2; } }
     
        public int Channel
     { get; set; }
     
        public int Note
     { get; set; }
     
        public int Velocity
     { get; set; }

        public int Timestamp
        { get; set; }

        public void AddData ( byte value, int index ) {
            if ( index == 0 ) {
                Note = ( int )value;
            }
         
            if ( index == 1 ) {
                Velocity = ( int )value;
            }
        }
     
        public override string ToString () {
            return string.Format( "[AfterTouch] Channel:{0}, Note:{1}, Velocity:{2}", Channel, Note, Velocity );
        }
     
    }
 
    public class ControlChange : IMidiMessage {
     
        public int DataBytesCount
     { get { return 2; } }
     
        public int Channel
     { get; set; }
     
        public int Control
     { get; set; }
     
        public int Value
     { get; set; }

        public int Timestamp
        { get; set; }

        public void AddData ( byte value, int index ) {
            if ( index == 0 ) {
                Control = ( int )value;
            }
         
            if ( index == 1 ) {
                Value = ( int )value;
            }
        }
     
        public override string ToString () {
            return string.Format( "[ControlChange] Channel:{0}, Controller:{1}, Value:{2}", Channel, Control, Value );
        }
    }
 
    public class ProgramChange : IMidiMessage {
     
        public int DataBytesCount
     { get { return 1; } }
     
        public int Channel
     { get; set; }
     
        public int Program
     { get; set; }

        public int Timestamp
        { get; set; }

        public void AddData ( byte value, int index ) {
            if ( index == 0 ) {
                Program = ( int )value;
            }
        }
     
        public override string ToString () {
            return string.Format( "[ProgramChange] Channel:{0}, Program:{1}", Channel, Program );
        }
    }
 
    public class ChannelPressure : IMidiMessage {
     
        public int DataBytesCount
     { get { return 1; } }
     
        public int Channel
     { get; set; }
     
        public int PressureAmount
     { get; set; }

        public int Timestamp
        { get; set; }

        public void AddData ( byte value, int index ) {
            if ( index == 0 ) {
                PressureAmount = ( int )value;
            }
        }
     
        public override string ToString () {
            return string.Format( "[ChannelPressure] Channel:{0}, PressureAmount:{1}", Channel, PressureAmount );
        }
    }
 
    public class PitchWheel : IMidiMessage {
     
        public int DataBytesCount
     { get { return 1; } }
     
        public int Channel
     { get; set; }
     
        public int Value
     { get; set; }

        public int Timestamp
        { get; set; }

        public void AddData ( byte value, int index ) {
            if ( index == 0 ) {
                Value = ( int )value;
            }
        }
     
        public override string ToString () {
            return string.Format( "[PitchWheel] Channel:{0}, Value:{1}", Channel, Value );
        }
    }
}