#include "btBulletDynamicsCommon.h"
#include <list>
using namespace std;


struct Vector3
{
    float x;
    float y;
    float z;
    
    Vector3& operator=(const btVector3& rhs){
        x = rhs.x();
        y = rhs.y();
        z = rhs.z();
        return *this;
    }
    
    btVector3 TobtVector3(){
        return btVector3(btScalar(x),btScalar(y),btScalar(z));
    }
    
    };

class PIDScalar {
    public :
    float setpoint;
    float kp, ki, kd;
    float integral;
    float lastFeedback;
    
    void Reset ( float feedback );
    float Compute ( float feedback, float dt );
};

class BulletMotor{
    
public:
    virtual ~BulletMotor(){}
    virtual void SetVelocity(float velocity) = 0;
    virtual void SetServoVelocity(float velocity) = 0;
    virtual void SetTarget(float target,float scaling) = 0;
    virtual void StopMotor() = 0;
    virtual void Reset() = 0;
    virtual float Compute(float dt) = 0;
    virtual void Destroy() = 0;
    virtual bool IsActive() = 0;
    virtual bool InServoMode() = 0;
    virtual Vector3 getDebug() = 0;
    
};

class BulletRotationalMotor : public BulletMotor{
    
public :
    int id;
    PIDScalar* controller;
    btGeneric6DofConstraint* constraint6Dof;
    int axisIndex;
    btRotationalLimitMotor* motor;
    bool servoMode;
    bool isActive;
    int motorType; // 0 means angular, 1 means linear
    
    virtual void SetVelocity(float velocity);
    virtual void SetServoVelocity(float velocity);
    virtual void SetTarget(float target,float scaling);
    virtual void StopMotor();
    virtual void Reset();
    virtual float Compute(float dt);
    virtual void Destroy();
    virtual bool IsActive();
    virtual bool InServoMode();
    virtual Vector3 getDebug();
};

class BulletTranslationalMotor : public BulletMotor{
public:
    int id;
    PIDScalar* controller;
    btGeneric6DofConstraint* constraint6Dof;
    int axisIndex;
    btTranslationalLimitMotor* motor;
    bool servoMode;
    bool isActive;
    int motorType; // 0 means angular, 1 means linear
    
    virtual void SetVelocity(float velocity);
    virtual void SetServoVelocity(float velocity);
    virtual void SetTarget(float target,float scaling);
    virtual void StopMotor();
    virtual void Reset();
    virtual float Compute(float dt);
    virtual void Destroy();
    virtual bool IsActive();
    virtual bool InServoMode();
    virtual Vector3 getDebug();
};

class BulletConstraint  {
public:
    int id;
    bool usesMotor;
    BulletMotor* servo;
};


    
struct Quaternion
{
    float x;
    float y;
    float z;
    float w;

    Quaternion& operator=( const btQuaternion& rhs ) {
        x = rhs.x();
        y = rhs.y();
        z = rhs.z();
        w = rhs.w();
        return  *this;
    }

    btQuaternion TobtQuaternion( ) {
        return btQuaternion(btScalar(x),btScalar(y),btScalar(z),btScalar(w));
    }

};



struct BulletStatus
{
    float rigidBodyCount;
    float objectsBatchAddedId;
    float actionsBatchAddedId;
    int collisionCount;
    float debugValue;
    float debugValue2;
    float debugValue3;
};


struct Head
{
    int id;
    int bulletObjectType;
};

struct Transform
{
    int id;
    Vector3 position;
    Quaternion rotation;
    Vector3 velocity;
    Vector3 angularVelocity;
};

struct Shape{
    int id;
    int type;
    int vertexCount;
    Vector3 scale;
    Quaternion localRotation;
    Vector3 localPosition;
};

struct Rigidbody{
    int id;
    short mask;
    short collisionGroup;
    Vector3 position;
    Quaternion rotation;
    float linearDamping;
    float angularDamping;
    float restitution;
    float friction;
	float rollingFriction;
    float mass;
    int isTrigger;
    int alwaysActive;
    int compoundShapeId;
    Vector3 compoundShapeScale;
    int shapeCount;
    Vector3 inertiaPoint;
};

struct Constraint{
    int id;
    int useLinearReferenceFrameA;
    int constraintToWorld;
    Vector3 forward;
    Vector3 right;
    Vector3 pivot;
    Vector3 lowerLimit;
    Vector3 upperLimit;
    Vector3 lowerLinearLimit;
    Vector3 upperLinearLimit;

    Vector3 linearCFMValue;
    Vector3 angularCFMValue;
    Vector3 linearERPValue;
    Vector3 angularERPValue;
    
    Vector3 rbAPos;
    Vector3 rbBPos;
    
    Quaternion rbARot;
    Quaternion rbBRot;

    int enableSpring;
    int springAxis;
    float damping;
    float stiffness;

    int disableCollisionBetweenBodies;
    int solverIterations;
};

struct Motor{
    int id;
    int constraintId;
    int motorType;
    int axis;
    float kp;
    float ki;
    float kd;
};

struct CollisionAOnB{
    int idA;
    int idB;
    Vector3 ptA;
    Vector3 ptB;
    Vector3 normalOnB;
};

struct RaycastResult{
    int id;
    int hit;
    Vector3 hitPoint;
    Vector3 normal;
};

struct UnityRigidbodyData{
    int id;
    btVector3 centerOfMass;
};

struct broadPhaseAABBCallback : public btBroadphaseAabbCallback {
    list<btCollisionObject*>& collisionObjectArray;
    short int collisionGroup, mask;
    btCollisionObject * me;
    broadPhaseAABBCallback(btCollisionObject * caller, list < btCollisionObject* >& array,
                           short int group=btBroadphaseProxy::DefaultFilter,
                           short int collisionMask=btBroadphaseProxy::AllFilter) :
    me(caller), collisionObjectArray(array), collisionGroup(group), mask(collisionMask){
        collisionObjectArray.clear();
    }
    
    SIMD_FORCE_INLINE bool Colides(const btBroadphaseProxy* proxy) const {
        bool collides = (proxy->m_collisionFilterGroup & mask) != 0;
        collides = collides && (collisionGroup & proxy->m_collisionFilterMask);
        return collides;
    }
    
    virtual bool process (const btBroadphaseProxy *proxy) {
        btCollisionObject * collisionObject = ( btCollisionObject * )proxy->m_clientObject;
        if (Colides(proxy) && collisionObject != me){
            collisionObjectArray.push_back(collisionObject);
        }
        return true;
    }
};
