#include "BulletServiceSupport.h"
#include "BulletCollision/NarrowPhaseCollision/btRaycastCallback.h"
#include "BulletCollision/BroadphaseCollision/btBroadphaseInterface.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "BulletWorldImporter/btBulletWorldImporter.h"
#include <stdio.h>
#include <map>
using namespace std;

typedef map< int, BulletMotor* > BulletServoMap;
typedef map< int, btVector3 > ForcesMap;// unity will use this one.

#if defined _WIN32
#define EXPORT __declspec(dllexport)
typedef  void  (__stdcall *importerCallback)(int bodyId, int shapeId);
typedef void (__stdcall *callback)(CollisionAOnB collision);
#else
#define EXPORT
typedef  void  (*importerCallback)(int bodyId, int shapeId);
typedef void  (*callback)(CollisionAOnB collision);
#endif

static callback collision_callback;
static importerCallback file_callback;

class UnitySimulation {

private:
    //TODO: this is unneccesary
    map<int, btCompoundShape*  > collisionShapesMap;
    
    /* Constraints list */
    map< int, btGeneric6DofConstraint* > constraintsMap;// unity will use this one.
    
    ForcesMap constantForcesMap;// unity will use this one.
    
    BulletServoMap servosMap;
    btClock debug_clock;
    btClock m_clock;
    btClock communication_clock;
    btDefaultCollisionConfiguration* collisionConfiguration;
    btCollisionDispatcher* dispatcher;
    btBroadphaseInterface* overlappingPairCache;
    btSequentialImpulseConstraintSolver* solver;
    btDiscreteDynamicsWorld* dynamicsWorld;
    btAlignedObjectArray<btCollisionShape*> collisionShapes;
        
    unsigned char* headBuffer;
    unsigned char* bodyBuffer;
    unsigned char* motorBuffer;
    unsigned char* constraintBuffer;
    unsigned char* shapeBuffer;
    unsigned char* vector3Buffer;
    
    unsigned char* buffer;
    float simulationScaling;
public:
    char* dataFile;
    int dataLength;
    
    UnitySimulation();
    ~UnitySimulation();
    void Simulate(float step, float maxSubSteps, float fixedTimeStep);
    void SetForceUpdateAllAabbs(int active);
	void SetWorldScale(float scale);
    void SetWorldIterations(int iterations);
    void ResolveCollisions();
    void ApplyGravity();
    Transform GetTransform(btRigidBody* body);
    btRigidBody* AddRigidbody(unsigned char * newObject);
	btCompoundShape* CreateCompoundShape();
	btCompoundShape* GetCompoundShape(int compoundShapeId);
	int IsCompoundShapeLoaded(int compoundShapeId);
	void AddChildrenToCompoundShape(btCompoundShape* compoundShape, btCollisionShape* shape, Vector3 localPosition,Quaternion localRotation, Vector3 centerOfMass);
	btCollisionShape* CreateBoxShape(Vector3 scale);
	btCollisionShape* CreateSphereShape(float radio);
	btCollisionShape* CreateCylinderShape(Vector3 scale);
	btCollisionShape* CreateConvexHullShape(Vector3* vertices, int size);
	btCollisionShape* CreateTriangleMeshShape(int id, int j);
	btRigidBody* AddBulletRigidbody(Rigidbody unityRigidbody, btCompoundShape* compoundShape);
    void AddMotor(Motor motor);
    void AddConstraint(Constraint constraint,btRigidBody* bodyA, btRigidBody* bodyB);
    void ResolvePIDs();
    void AddForce(btRigidBody* body, Vector3 force, Vector3 offset);
    void SetCollisionShapeScale(btRigidBody* body, Vector3 scale);
    void SetVelocity(btRigidBody* body, Vector3 velocity);
    void SetAngularVelocity(btRigidBody* body, Vector3 angularVelocity);
    void RemoveRidigbody(btRigidBody* body);
    void RemoveMotor(int id);
    void SetServoAngle(int id, float angle );
	void SetBodyMass(btRigidBody* body, float mass,Vector3 inertiaPoint );
    void SetServoVelocity(int id,float velocity);
    void StopServo(int id);
    void ChangeLayer( btRigidBody* body, short mask, short group );
	void SetWorldGravity(Vector3 gravity);
    RaycastResult Raycast(short mask, short group,Vector3 from, Vector3 to );
    int RaycastAll(short mask, short group,Vector3 from, Vector3 to ,unsigned char * hits);
    int OverlapSphere(btRigidBody* body, short mask, short group, Vector3 center, float radius, unsigned char * hits);
    void SetRigidbodyPosition(btRigidBody* body, Vector3 position);
    void SetRigidbodyRotation(btRigidBody* body, Quaternion rotation);
    void SetDamping(btRigidBody* body, float linearDamping, float angularDamping);
    void SetGravity(btRigidBody* body, Vector3 g);
    void RemoveContraint(int id);
    void Tick(btScalar timeStep);
    static void CallBack(btDynamicsWorld *world, btScalar timeStep);
};


EXPORT UnitySimulation* simulation;
extern "C" EXPORT float FastInvSqrt(float x);
extern "C" EXPORT Transform GetTransform(btRigidBody* body);
extern "C" EXPORT void SetWorldGravity(Vector3 gravity);
extern "C" EXPORT void SimulateI();
extern "C" EXPORT void SimulateStep(float step, float maxSubSteps, float fixedTimeStep );
extern "C" EXPORT void EndSimulation();
extern "C" EXPORT void AddForce(btRigidBody* body, Vector3 force, Vector3 offset );
extern "C" EXPORT void SetCollisionShapeScale(btRigidBody* body, Vector3 scale);
extern "C" EXPORT void SetVelocity(btRigidBody* body, Vector3 velocity);
extern "C" EXPORT void SetAngularVelocity(btRigidBody* body, Vector3 angularVelocity);
extern "C" EXPORT void SetMotorAngle( int id, float angle );
extern "C" EXPORT void SetMotorVelocity( int id, float velocity );
extern "C" EXPORT void StopMotor( int id );
extern "C" EXPORT void ChangeLayer( btRigidBody* body, short mask, short group );
extern "C" EXPORT void SetRigidbodyPosition(btRigidBody* body, Vector3 position);
extern "C" EXPORT void SetRigidbodyRotation(btRigidBody* body, Quaternion rotation);
extern "C" EXPORT void SetGravity(btRigidBody* body, Vector3 g);
extern "C" EXPORT btRigidBody* AddRigidbody(unsigned char* newObjectsList);

extern "C" EXPORT btCompoundShape* CreateCompoundShape();
extern "C" EXPORT btCompoundShape* GetCompoundShape(int compoundShapeId);
extern "C" EXPORT int IsCompoundShapeLoaded(int compoundShapeid);
extern "C" EXPORT void AddChildrenToCompoundShape(btCompoundShape* compoundShape, btCollisionShape* shape, Vector3 localPosition,Quaternion localRotation, Vector3 centerOfMass);
extern "C" EXPORT btCollisionShape* CreateBoxShape(Vector3 scale);
extern "C" EXPORT btCollisionShape* CreateSphereShape(float radio);
extern "C" EXPORT btCollisionShape* CreateCylinderShape(Vector3 scale);
extern "C" EXPORT btCollisionShape* CreateConvexHullShape(Vector3* vertices, int size);
extern "C" EXPORT btCollisionShape* CreateTriangleMeshShape(int id, int j);
extern "C" EXPORT btRigidBody* AddBulletRigidbody(Rigidbody unityRigidbody, btCompoundShape* compoundShape);



extern "C" EXPORT void AddConstraint (Constraint constraint, btRigidBody* bodyA, btRigidBody* bodyB);
extern "C" EXPORT void AddMotor (Motor motor);
extern "C" EXPORT void RemoveRidigbody (btRigidBody* body);
extern "C" EXPORT void RemoveContraint(int id);
extern "C" EXPORT void RemoveMotor (int id);
extern "C" EXPORT RaycastResult BulletRaycast (short mask, short group,Vector3 from, Vector3 to);
extern "C" EXPORT int BulletRaycastAll (short mask, short group,Vector3 from, Vector3 to, unsigned char* hits);
extern "C" EXPORT int BulletOverlapSphere(btRigidBody* body, short mask, short group, Vector3 center, float radius, unsigned char * hits);
extern "C" EXPORT void SetDamping(btRigidBody* body, float linearDamping, float angularDamping);
extern "C" EXPORT void SetBodyMass(btRigidBody* body, float mass, Vector3 inertiaPoint);
extern "C" EXPORT void RegisterCallback (callback cb);
extern "C" EXPORT void RegisterImporterCallback (importerCallback cb);
extern "C" EXPORT void SerializeShape(char* path, int numVertex,int numTriangles,Vector3 * vertex, int * triangleIndex);
extern "C" EXPORT void SetLoadedData(char* data, int length);
extern "C" EXPORT void SetWorldIterations(int iterations);
extern "C" EXPORT void SetForceUpdateAllAabbs(int active);
extern "C" EXPORT void SetWorldScaling(float scale);