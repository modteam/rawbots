//Uncomment if using visualstudio -z26
//#if defined _WIN32
//#include "stdafx.h"
//#endif

#include "BulletService.h"

UnitySimulation::UnitySimulation(){
		simulationScaling = 0.1f;
		dataLength = 0;
		dataFile = NULL;
    
		collisionConfiguration = new btDefaultCollisionConfiguration();
		dispatcher = new    btCollisionDispatcher(collisionConfiguration);

		overlappingPairCache = new btDbvtBroadphase();
		solver = new btSequentialImpulseConstraintSolver;
		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,overlappingPairCache,solver,collisionConfiguration);

		bodyBuffer =new unsigned char[sizeof(Rigidbody)];
		constraintBuffer =new unsigned char[sizeof(Constraint)];
		motorBuffer = new unsigned char[sizeof(Motor)];
		buffer =new unsigned char[sizeof(Transform)];
		headBuffer = new unsigned char[sizeof(Head)];
		shapeBuffer = new unsigned char[sizeof(Shape)];
		vector3Buffer = new unsigned char[sizeof(Vector3)];
		
		dynamicsWorld->setGravity(btVector3(0.0f,0.0f,0.0f));

		btContactSolverInfo& info = dynamicsWorld->getSolverInfo();
		info.m_numIterations = 20;
        info.m_solverMode = SOLVER_USE_WARMSTARTING | SOLVER_SIMD; 
		//info.m_splitImpulse = 1; //enable split impulse feature

		dynamicsWorld->setInternalTickCallback(&CallBack,static_cast<void *>(this),false);
}

UnitySimulation::~UnitySimulation(){
    
    for (int i=dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		dynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}
    
	for (int j=0;j<collisionShapes.size();j++)
	{
		delete collisionShapes[j];
	}
    
	delete dynamicsWorld;
	delete solver;
	delete overlappingPairCache;
	delete dispatcher;
	delete collisionConfiguration;
    
    delete[] headBuffer;
    delete[] bodyBuffer;
    delete[] motorBuffer;
    delete[] constraintBuffer;
    delete[] shapeBuffer;
    delete[] vector3Buffer;

    delete[] buffer;
}

void UnitySimulation::Simulate(float step, float maxSubSteps, float fixedTimeStep){ 
	dynamicsWorld->stepSimulation(step,maxSubSteps,fixedTimeStep);
}
void UnitySimulation::SetForceUpdateAllAabbs(int active){ 
	dynamicsWorld->setForceUpdateAllAabbs((active==1)?true:false);
}
void UnitySimulation::SetWorldScale(float scale){ 
	simulationScaling = scale;
}

void UnitySimulation::SetWorldIterations(int iterations){ 
	dynamicsWorld->getSolverInfo().m_numIterations = iterations;
}

void UnitySimulation::ResolveCollisions() {
	int numManifolds = dynamicsWorld ->getDispatcher()->getNumManifolds();
	int count = 0;
	
	for( int i = 0 ; i < numManifolds; ++i ) {
		btPersistentManifold* contactManifold = dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
		btCollisionObject* objA = const_cast<btCollisionObject*>(contactManifold->getBody0());
		btCollisionObject* objB = const_cast<btCollisionObject*>(contactManifold->getBody1());

		int numContacts = contactManifold->getNumContacts();


		for( int j = 0 ; j < numContacts ; ++j ) {
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			if( pt.getDistance() < 1.f ) {
				const btVector3& ptA = pt.getPositionWorldOnA();
				const btVector3& ptB = pt.getPositionWorldOnB();
				const btVector3& normalOnB = pt.m_normalWorldOnB;

				btRigidBody *rigidBodyA = btRigidBody::upcast(objA);
				btRigidBody *rigidBodyB = btRigidBody::upcast(objB);
				int bodyAId =((UnityRigidbodyData*) rigidBodyA->getUserPointer())->id;// rigidbodiesInverseMap[rigidBodyA];
				int bodyBId =((UnityRigidbodyData*) rigidBodyB->getUserPointer())->id;// rigidbodiesInverseMap[rigidBodyB];
				CollisionAOnB collision;
				collision.idA = bodyAId;
				collision.idB = bodyBId;
				collision.ptA = ptA*(1/simulationScaling);
				collision.ptB = ptB*(1/simulationScaling);
				collision.normalOnB = normalOnB;
                
				if(collision_callback != NULL){
					 (*collision_callback)(collision);
				}
                break;
				
			}
		}
		++count;
	}
}

void UnitySimulation::ApplyGravity(){
	btCollisionObjectArray objects = dynamicsWorld ->getCollisionObjectArray();
	for (int i = 0; i < objects.size(); i++) {
		btRigidBody *rigidBody = btRigidBody::upcast(objects[i]);
		if (!rigidBody) {
			continue;
		}
		rigidBody->applyGravity();
	}
}
btCompoundShape* UnitySimulation::CreateCompoundShape(){
	return new btCompoundShape();
}

btCompoundShape* UnitySimulation::GetCompoundShape(int compoundShapeId){
	map<int,btCompoundShape*>::const_iterator got = collisionShapesMap.find (compoundShapeId);
		if(got == collisionShapesMap.end()){
			return 0;
		}else{
			return got->second;
		}
}

int UnitySimulation::IsCompoundShapeLoaded(int compoundShapeId){
	bool reuseShape = compoundShapeId > -1;
	if(reuseShape){
		map<int,btCompoundShape*>::const_iterator got = collisionShapesMap.find (compoundShapeId);
		if(got == collisionShapesMap.end()){
			return -1;
		}else{
			return 1;
		}
	}
	else{
		return -1;
	}

}


btCollisionShape* UnitySimulation::CreateBoxShape(Vector3 scale){
	btCollisionShape* boxShape = new btBoxShape(scale.TobtVector3()*simulationScaling);
	return boxShape;
}

//btCollisionShape* sphereShape = new btSphereShape(btScalar(shapeFromChar->scale.x)*simulationScaling);

btCollisionShape* UnitySimulation::CreateSphereShape(float radio){
	btCollisionShape* sphereShape = new btSphereShape(btScalar(radio)*simulationScaling);
	return sphereShape;
}

btCollisionShape* UnitySimulation::CreateCylinderShape(Vector3 scale){
	btCollisionShape* cylinderShape = new btCylinderShape(scale.TobtVector3()*simulationScaling);
	return cylinderShape;
}

btCollisionShape* UnitySimulation::CreateConvexHullShape(Vector3* vertices, int size){

	btVector3* points = new btVector3[size];
	for(int k = 0; k< size; k++){
		points[k] =vertices[k].TobtVector3()*simulationScaling;
	}
	btConvexShape* childShape = new btConvexHullShape(&(points[0].getX()),size);
	return childShape;
}



btCollisionShape* UnitySimulation::CreateTriangleMeshShape(int id, int shapeId){
			if(file_callback == NULL){
               fprintf(stderr, "CALLBACK NOT FOUND\n");
                return 0;
            }
           
            (*file_callback)(id,shapeId);
            btBvhTriangleMeshShape * triangleMeshShape = 0;
            btBulletWorldImporter import(0);
            if(import.loadFileFromMemory(dataFile, dataLength)){
                int numShape = import.getNumCollisionShapes();
                if (numShape){
                    triangleMeshShape = (btBvhTriangleMeshShape*)import.getCollisionShapeByIndex(0);
                    triangleMeshShape->setLocalScaling( btVector3(1, 1, 1) * simulationScaling );
                }
            }
			return triangleMeshShape;
}

void UnitySimulation::AddChildrenToCompoundShape(btCompoundShape* compoundShape, btCollisionShape* shape, Vector3 localPosition,Quaternion localRotation, Vector3 centerOfMass){

	btTransform colliderTransform;
	colliderTransform.setIdentity();
	colliderTransform.setOrigin(localPosition.TobtVector3()*simulationScaling - centerOfMass.TobtVector3() * simulationScaling);
	colliderTransform.setRotation(localRotation.TobtQuaternion());

	compoundShape->addChildShape(colliderTransform,shape);
}

btRigidBody* UnitySimulation::AddBulletRigidbody(Rigidbody unityRigidbody, btCompoundShape* compoundShape){

	UnityRigidbodyData* userData = new UnityRigidbodyData();
	userData -> id= unityRigidbody.id;
	userData -> centerOfMass = unityRigidbody.inertiaPoint.TobtVector3();

	bool reuseShape = unityRigidbody.compoundShapeId > -1;
	
	bool isLoaded = IsCompoundShapeLoaded(unityRigidbody.compoundShapeId);

	btTransform centerOfMassTransform;
	centerOfMassTransform.setIdentity();
	centerOfMassTransform.setOrigin(userData -> centerOfMass*simulationScaling);

	btTransform unityGlobalTransform;
	unityGlobalTransform.setIdentity();
	unityGlobalTransform.setOrigin(unityRigidbody.position.TobtVector3()*simulationScaling);
	unityGlobalTransform.setRotation(unityRigidbody.rotation.TobtQuaternion());

	btTransform rigidBodyTransform = unityGlobalTransform * centerOfMassTransform;

	btRigidBody* body = 0;
	btScalar mass(0.0f);
	bool isDynamic = false;
	
	
	if(unityRigidbody.mass > 0.01f){
		mass = unityRigidbody.mass;
		isDynamic = true;
	}

	btVector3 localInertia(0,0,0);
	if( (isLoaded < 0)){
		collisionShapes.push_back(compoundShape);
		pair<int,btCompoundShape*> collisionShapeKeyPair (unityRigidbody.compoundShapeId ,compoundShape);
		collisionShapesMap.insert(collisionShapeKeyPair);
	}

	if(isDynamic){
		compoundShape->calculateLocalInertia(mass,localInertia);


		btDefaultMotionState* myMotionState = new btDefaultMotionState(rigidBodyTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,compoundShape, localInertia);
		rbInfo.m_restitution = unityRigidbody.restitution;
		rbInfo.m_friction = unityRigidbody.friction;
		rbInfo.m_rollingFriction = unityRigidbody.rollingFriction;// btScalar(10);
        rbInfo.m_additionalDamping = true;		
		body = new btRigidBody(rbInfo);

	}else{


		btDefaultMotionState* myMotionState = new btDefaultMotionState(rigidBodyTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,compoundShape, localInertia);
		rbInfo.m_restitution = unityRigidbody.restitution;
		rbInfo.m_friction = unityRigidbody.friction;
		rbInfo.m_rollingFriction = unityRigidbody.rollingFriction;


		body = new btRigidBody(rbInfo);
		body ->setCollisionFlags(body ->getCollisionFlags() |  btCollisionObject::CF_STATIC_OBJECT);
		body->setActivationState(DISABLE_SIMULATION);
	}


	if(unityRigidbody.alwaysActive== 1){
		body->setActivationState(DISABLE_DEACTIVATION);
	}

	if(unityRigidbody.isTrigger == 1){
		body ->setCollisionFlags(body ->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
	}
	body->setUserPointer(userData);

	dynamicsWorld->addRigidBody(body,unityRigidbody.collisionGroup,unityRigidbody.mask);
    return body;
}


btRigidBody* UnitySimulation::AddRigidbody(unsigned char * newObject){
	int offset = 0;
	memcpy ( bodyBuffer, newObject, sizeof(Rigidbody));
	offset+= sizeof(Rigidbody);
	Rigidbody* objectFromChar = reinterpret_cast<Rigidbody*>(bodyBuffer);
	Rigidbody* current = objectFromChar;

	UnityRigidbodyData* userData = new UnityRigidbodyData();
	userData -> id= current->id;
	userData -> centerOfMass = current->inertiaPoint.TobtVector3();


	btCompoundShape* compoundShape = new btCompoundShape();
	btVector3 centerOfMass = current->inertiaPoint.TobtVector3();
	bool reuseShape = current->compoundShapeId > -1;
	
	if(reuseShape){
		map<int,btCompoundShape*>::const_iterator got = collisionShapesMap.find (current->compoundShapeId);
		if(got == collisionShapesMap.end()){
			compoundShape = new btCompoundShape();
			reuseShape = false;
		}else{
			compoundShape = got->second;
		}
	}
	else{
		compoundShape = new btCompoundShape();
	}
	
	if(!reuseShape){
	for(int j = 0; j< current->shapeCount; j++){
		memcpy ( shapeBuffer, newObject+ offset, sizeof(Shape));
		offset+= sizeof(Shape);
		Shape* shapeFromChar = reinterpret_cast<Shape*>(shapeBuffer);

		if(shapeFromChar->type == 0){
			btCollisionShape* boxShape = new btBoxShape(shapeFromChar->scale.TobtVector3()*simulationScaling);
			btTransform colliderTransform;
			colliderTransform.setIdentity();
			colliderTransform.setOrigin(shapeFromChar->localPosition.TobtVector3()*simulationScaling - centerOfMass * simulationScaling);
			colliderTransform.setRotation(shapeFromChar->localRotation.TobtQuaternion());
			compoundShape->addChildShape(colliderTransform,boxShape);
		}else if(shapeFromChar ->type == 1){
			btCollisionShape* sphereShape = new btSphereShape(btScalar(shapeFromChar->scale.x)*simulationScaling);
			btTransform colliderTransform;
			colliderTransform.setIdentity();

			colliderTransform.setOrigin(shapeFromChar->localPosition.TobtVector3()*simulationScaling - centerOfMass*simulationScaling);
            
			colliderTransform.setRotation(shapeFromChar->localRotation.TobtQuaternion());

			compoundShape->addChildShape(colliderTransform,sphereShape);
		}else if(shapeFromChar ->type == 2){

			int size =shapeFromChar ->vertexCount;
			btVector3* points = new btVector3[size];
			for(int k = 0; k< shapeFromChar ->vertexCount; k++){
				memcpy ( vector3Buffer, newObject+ offset, sizeof(Vector3));
				offset+= sizeof(Vector3);
				Vector3* vertexFromChar = reinterpret_cast<Vector3*>(vector3Buffer);
				points[k] = vertexFromChar->TobtVector3()*simulationScaling;
				
			}

			btConvexShape* childShape1 = new btConvexHullShape(&(points[0].getX()),size);

			btTransform colliderTransform;
			colliderTransform.setIdentity();

			colliderTransform.setOrigin(shapeFromChar->localPosition.TobtVector3()*simulationScaling - centerOfMass*simulationScaling);

			colliderTransform.setRotation(shapeFromChar->localRotation.TobtQuaternion());

			compoundShape->addChildShape(colliderTransform,childShape1);


		}else if(shapeFromChar ->type == 3){//cylinder D:
			btCollisionShape* cylinderShape = new btCylinderShape(shapeFromChar->scale.TobtVector3()*simulationScaling);
			btTransform colliderTransform;
			colliderTransform.setIdentity();

			colliderTransform.setOrigin(shapeFromChar->localPosition.TobtVector3()*simulationScaling - centerOfMass*simulationScaling);
				

			colliderTransform.setRotation(shapeFromChar->localRotation.TobtQuaternion());

			compoundShape->addChildShape(colliderTransform,cylinderShape);
		} else if(shapeFromChar ->type == 4){ //TriangleMesh Shape FOR DESERIALIZATION ONLY
            
            if(file_callback == NULL){
               fprintf(stderr, "CALLBACK NOT FOUND\n");
                continue;
            }
           
            (*file_callback)(objectFromChar->id,j);
            btBvhTriangleMeshShape * triangleMeshShape = 0;
            btBulletWorldImporter import(0);
            if(import.loadFileFromMemory(dataFile, dataLength)){
//               fprintf(stderr, "LOADING DATA\n");
                int numShape = import.getNumCollisionShapes();
                if (numShape){
//                    fprintf(stderr, "SHAPE OK\n");
                    triangleMeshShape = (btBvhTriangleMeshShape*)import.getCollisionShapeByIndex(0);
                    triangleMeshShape->setLocalScaling( btVector3(1, 1, 1) * simulationScaling );
                }
            }
            
            btTransform colliderTransform;
			colliderTransform.setIdentity();
            
			colliderTransform.setOrigin( shapeFromChar->localPosition.TobtVector3()*simulationScaling - centerOfMass*simulationScaling);
            
			colliderTransform.setRotation(shapeFromChar->localRotation.TobtQuaternion());
            
			compoundShape->addChildShape(colliderTransform,triangleMeshShape);
		}
	}
	
	}

	
	btTransform centerOfMassTransform;
	centerOfMassTransform.setIdentity();
	centerOfMassTransform.setOrigin(centerOfMass*simulationScaling);

	btTransform unityGlobalTransform;
	unityGlobalTransform.setIdentity();
	unityGlobalTransform.setOrigin(current -> position.TobtVector3()*simulationScaling);
	unityGlobalTransform.setRotation(current -> rotation.TobtQuaternion());

	btTransform rigidBodyTransform = unityGlobalTransform * centerOfMassTransform;

	btRigidBody* body = 0;
	btScalar mass(0.0f);
	bool isDynamic = false;
	
	
	if(current -> mass > 0.1f){
		mass = current -> mass;
		isDynamic = true;
	}
	btVector3 localInertia(0,0,0);
	if(isDynamic){
		compoundShape->calculateLocalInertia(mass,localInertia);
		if(!reuseShape){
			
			collisionShapes.push_back(compoundShape);
			pair<int,btCompoundShape*> collisionShapeKeyPair (current->compoundShapeId ,compoundShape);
			collisionShapesMap.insert(collisionShapeKeyPair);

		}
		btDefaultMotionState* myMotionState = new btDefaultMotionState(rigidBodyTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,compoundShape, localInertia);
		rbInfo.m_restitution = current ->restitution;
		rbInfo.m_friction = current ->friction;
		rbInfo.m_rollingFriction = current ->rollingFriction;// btScalar(10);
        rbInfo.m_additionalDamping = true;

		
		body = new btRigidBody(rbInfo);
		//body->setActivationState(DISABLE_DEACTIVATION);
	}else{
		if(!reuseShape){
			collisionShapes.push_back(compoundShape);
			pair<int,btCompoundShape*> collisionShapeKeyPair (current->compoundShapeId ,compoundShape);
			collisionShapesMap.insert(collisionShapeKeyPair);
		}

		btDefaultMotionState* myMotionState = new btDefaultMotionState(rigidBodyTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState,compoundShape, localInertia);
		rbInfo.m_restitution = current ->restitution;
		rbInfo.m_friction = current ->friction;
		rbInfo.m_rollingFriction = current ->rollingFriction;// btScalar(10);
        //rbInfo.m_additionalDamping = true;
        //rbInfo.m_additionalLinearDampingThresholdSqr = btScalar(0.1);

		body = new btRigidBody(rbInfo);
		body ->setCollisionFlags(body ->getCollisionFlags() |  btCollisionObject::CF_STATIC_OBJECT);
		body->setActivationState(DISABLE_SIMULATION);
	}


	if(current ->alwaysActive== 1){
		body->setActivationState(DISABLE_DEACTIVATION);
	}

		if(current ->isTrigger == 1){
			body ->setCollisionFlags(body ->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
		}
//		body ->setDamping(current->linearDamping,current->angularDamping);
//		body ->setSleepingThresholds(0.1f,0.1f);
		body->setUserPointer(userData);


		dynamicsWorld->addRigidBody(body,current->collisionGroup,current->mask);
        return body;
}

void UnitySimulation::AddMotor(Motor motor){

	btGeneric6DofConstraint* constraint = constraintsMap[motor.constraintId];

	if(motor.motorType == 0){
		constraint->getRotationalLimitMotor(motor.axis)->m_maxLimitForce = SIMD_INFINITY;
		constraint->getRotationalLimitMotor(motor.axis)->m_limitSoftness = btScalar(1.0);
		constraint->getRotationalLimitMotor(motor.axis)->m_maxMotorForce =  SIMD_INFINITY;
	}else{
		constraint->getTranslationalLimitMotor()->m_limitSoftness = btScalar(1.0);
		constraint->getTranslationalLimitMotor()->m_maxMotorForce[motor.axis] =  SIMD_INFINITY;
	}


	PIDScalar* controller = new PIDScalar();
	controller ->kp = motor.kp;
	controller ->ki =  motor.ki;
	controller ->kd =  motor.kd;
	controller ->setpoint = 0;

	if(motor.motorType == 0){

		BulletRotationalMotor* servo = new BulletRotationalMotor();
		servo ->axisIndex = motor.axis;
		servo ->constraint6Dof = constraint;
		servo ->motor = constraint-> getRotationalLimitMotor(motor.axis);
		servo ->id = motor.id;
		servo ->isActive = false;
		servo ->controller = controller;
		pair<int,BulletMotor*> servoMotorPair (motor.constraintId,servo);
		servosMap.insert(servoMotorPair);


	}else{
		BulletTranslationalMotor* servo = new BulletTranslationalMotor();
		servo ->axisIndex =  motor.axis;
		servo ->constraint6Dof = constraint;
		servo ->motor = constraint->getTranslationalLimitMotor();
		servo ->id = motor.id;
		servo ->isActive = false;
		servo ->controller = controller;
		pair<int,BulletMotor*> servoMotorPair (motor.constraintId,servo);
		servosMap.insert(servoMotorPair);
	}

}

void UnitySimulation::AddConstraint(Constraint constraint, btRigidBody* bodyA, btRigidBody* bodyB){
    
	btTransform trans = bodyA->getWorldTransform();
	bodyA ->activate(true);


	Vector3 position = constraint.rbAPos;
	Quaternion rotation = constraint.rbARot;

	UnityRigidbodyData* userData = (UnityRigidbodyData *)bodyA -> getUserPointer();

	btTransform localUnityTransform;
	localUnityTransform.setIdentity();
	localUnityTransform.setOrigin(userData->centerOfMass*simulationScaling*-1);

	btTransform globalUnityTransform = trans * localUnityTransform;
	globalUnityTransform.setOrigin(position.TobtVector3()*simulationScaling);
	globalUnityTransform.setRotation(rotation.TobtQuaternion());

	btTransform localCenterOfMass;
	localCenterOfMass.setIdentity();
	localCenterOfMass.setOrigin(userData->centerOfMass*simulationScaling);
	

	btTransform globalCenterOfMass = globalUnityTransform*localCenterOfMass;


	trans.setOrigin(globalCenterOfMass.getOrigin());
	trans.setRotation(globalCenterOfMass.getRotation());


	bodyA->setWorldTransform(trans);
	bodyA->clearForces();

	if(constraint.constraintToWorld == 0) {

		btTransform transB = bodyB ->getWorldTransform();
		bodyB ->activate(true);

		Vector3 positionB = constraint.rbBPos;
		Quaternion rotationB = constraint.rbBRot;

		UnityRigidbodyData* userData2 = (UnityRigidbodyData *)bodyB -> getUserPointer();

		btTransform localUnityTransform;
		localUnityTransform.setIdentity();
		localUnityTransform.setOrigin(userData2->centerOfMass*simulationScaling*-1);

		btTransform globalUnityTransform = transB * localUnityTransform;
		globalUnityTransform.setOrigin(positionB.TobtVector3()*simulationScaling);
		globalUnityTransform.setRotation(rotationB.TobtQuaternion());

		btTransform localCenterOfMass;
		localCenterOfMass.setIdentity();
		localCenterOfMass.setOrigin(userData2->centerOfMass*simulationScaling);


		btTransform globalCenterOfMass = globalUnityTransform*localCenterOfMass;

		transB.setOrigin(globalCenterOfMass.getOrigin());
		transB.setRotation(globalCenterOfMass.getRotation());

		bodyB->setWorldTransform(transB);
		bodyB->clearForces();
	}

	btVector3 zAxis = constraint.forward.TobtVector3();
	btVector3 xAxis = constraint.right.TobtVector3();
	btVector3 yAxis = zAxis.cross(xAxis); // we want right coordinate system
    
	xAxis = xAxis.normalize();
	yAxis = yAxis.normalize();
	zAxis = zAxis.normalize();
	btTransform frameInW;
	frameInW.setIdentity();
	frameInW.getBasis().setValue(   xAxis[0], yAxis[0], zAxis[0],
		xAxis[1], yAxis[1], zAxis[1],
		xAxis[2], yAxis[2], zAxis[2]);
	frameInW.setOrigin(constraint.pivot.TobtVector3()*simulationScaling);

	btTransform frameInA = bodyA->getCenterOfMassTransform().inverse() * frameInW;

	btGeneric6DofSpringConstraint* constraint6Dof = 0;

	if(constraint.constraintToWorld == 1){
		constraint6Dof = new btGeneric6DofSpringConstraint (*bodyA,frameInA,(constraint.useLinearReferenceFrameA == 1));

		bodyA ->addConstraintRef(constraint6Dof);

	}else{

		btTransform frameInB = bodyB->getCenterOfMassTransform().inverse() * frameInW;
		constraint6Dof = new btGeneric6DofSpringConstraint (*bodyA,*bodyB,frameInA,frameInB,(constraint.useLinearReferenceFrameA == 1));

		bodyA ->addConstraintRef(constraint6Dof);
		bodyB ->addConstraintRef(constraint6Dof);
	}
	constraint6Dof ->setLinearLowerLimit(constraint.lowerLinearLimit.TobtVector3()*simulationScaling);
	constraint6Dof ->setLinearUpperLimit(constraint.upperLinearLimit.TobtVector3()*simulationScaling);

	constraint6Dof->setAngularLowerLimit(constraint.lowerLimit.TobtVector3());
	constraint6Dof->setAngularUpperLimit(constraint.upperLimit.TobtVector3());

	
	//linear
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_CFM, constraint.linearCFMValue.x, 0);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_CFM, constraint.linearCFMValue.y, 1);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_CFM, constraint.linearCFMValue.z, 2);
	//angular
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_CFM, constraint.angularCFMValue.x, 3);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_CFM, constraint.angularCFMValue.y, 4);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_CFM, constraint.angularCFMValue.z, 5);

	//linear

	constraint6Dof->setParam(BT_CONSTRAINT_STOP_ERP, constraint.linearERPValue.x, 0);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_ERP, constraint.linearERPValue.y, 1);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_ERP, constraint.linearERPValue.z, 2);
	//angular
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_ERP, constraint.angularERPValue.x, 3);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_ERP, constraint.angularERPValue.y, 4);
	constraint6Dof->setParam(BT_CONSTRAINT_STOP_ERP, constraint.angularERPValue.z, 5);
	
	


	if( constraint.springAxis == 1 ){
		constraint6Dof->enableSpring(constraint.springAxis,true);
		constraint6Dof->setDamping(constraint.springAxis,constraint.damping);
		constraint6Dof->setStiffness(constraint.springAxis,constraint.stiffness);
		constraint6Dof ->setEquilibriumPoint();

	}
	constraint6Dof ->setOverrideNumSolverIterations(constraint.solverIterations);


	pair<int,btGeneric6DofConstraint*> constraintPair (constraint.id,constraint6Dof);
	constraintsMap.insert(constraintPair);
	constraint6Dof->setUserConstraintId( constraint.id);

	dynamicsWorld->addConstraint(constraint6Dof,(constraint.disableCollisionBetweenBodies == 1));

}

void UnitySimulation::ResolvePIDs(){
	for (BulletServoMap::iterator it = servosMap.begin(); it != servosMap.end(); ++it){
		BulletMotor* currentMotor =  it->second;

		if(currentMotor ->IsActive() && currentMotor ->InServoMode()){
			float sample = currentMotor->Compute(0.02f);

//			Vector3 data =  currentMotor->getDebug();
			


			currentMotor->SetServoVelocity(sample);


		}
	}
}

void UnitySimulation::AddForce(btRigidBody* body, Vector3 force, Vector3 offset){
	body ->activate(true);
	body->clearForces();
	body ->applyForce(force.TobtVector3()*simulationScaling, offset.TobtVector3()*simulationScaling);
}

void UnitySimulation::SetCollisionShapeScale(btRigidBody* body, Vector3 scale){    
	btCompoundShape* shape = (btCompoundShape*) body ->getCollisionShape();
	shape ->setLocalScaling(scale.TobtVector3());
	dynamicsWorld->updateSingleAabb(body);
}

void UnitySimulation::SetVelocity(btRigidBody* body, Vector3 velocity) {
	body ->activate(true);
	body ->setLinearVelocity(velocity.TobtVector3()*simulationScaling);
}

void UnitySimulation::SetAngularVelocity(btRigidBody* body, Vector3 angularVelocity) {
	body ->activate(true);
	body ->setAngularVelocity(angularVelocity.TobtVector3());
}

void UnitySimulation::RemoveRidigbody(btRigidBody* body) {
	
	while (body->getNumConstraintRefs())
   {
	   
      btTypedConstraint* constraint = body->getConstraintRef(0);
	  int id = constraint->getUserConstraintId();
	  constraintsMap.erase(id);
	  /*
	  map<int,BulletMotor*>::const_iterator got = servosMap.find (id);
		if(got == servosMap.end()){


		}else{
				//servosMap.erase(id);
				got ->second->StopMotor();
				delete got ->second;
		}
		*/
      dynamicsWorld->removeConstraint(constraint);
   }
   
	if (body && body->getMotionState())
	{
		delete body -> getMotionState();
		delete (UnityRigidbodyData *)body -> getUserPointer();
	}
	
	dynamicsWorld ->removeRigidBody(body);
	delete body;

}


void UnitySimulation::RemoveMotor(int id){

	map<int,BulletMotor*>::const_iterator got = servosMap.find (id);
	if(got == servosMap.end()){

	}else{
		servosMap.erase(id);
		got ->second->StopMotor();
		delete got ->second;
	}
}


void UnitySimulation::SetBodyMass(btRigidBody* body,float mass,Vector3 inertiaPoint){		
	btCompoundShape* shape = (btCompoundShape*) body ->getCollisionShape();
	btVector3 inertia = inertiaPoint.TobtVector3()*simulationScaling;
	shape -> calculateLocalInertia(mass,inertia);
	body-> setMassProps(mass,inertia);
	
	body->updateInertiaTensor();
}



void UnitySimulation::SetServoAngle(int id, float angle ) {
	BulletMotor* servo = servosMap[id];
	servo ->SetTarget(angle,simulationScaling);
}

void UnitySimulation::SetServoVelocity(int id,float velocity){
	BulletMotor* servo = servosMap[id];
	servo ->SetVelocity(velocity);
}

void UnitySimulation::StopServo(int id){
	BulletMotor* servo = servosMap[id];
	servo ->StopMotor();
}

void UnitySimulation::ChangeLayer(btRigidBody* body, short mask, short group ) {
	dynamicsWorld ->removeRigidBody(body);
	dynamicsWorld ->addRigidBody(body,group,mask);
}


int UnitySimulation::RaycastAll( short mask, short group, Vector3 origin, Vector3 direction, unsigned char * resultArray ) {

	btCollisionWorld::AllHitsRayResultCallback allResults(origin.TobtVector3()*simulationScaling,direction.TobtVector3()*simulationScaling);
	allResults.m_flags |= btTriangleRaycastCallback::kF_KeepUnflippedNormal;
	dynamicsWorld->rayTest(origin.TobtVector3()*simulationScaling,direction.TobtVector3()*simulationScaling,allResults);

	int hitCount =  allResults.m_hitFractions.size();
	for (int i=0;i<hitCount;i++)
	{
		btVector3 p = (origin.TobtVector3()*simulationScaling).lerp(direction.TobtVector3()*simulationScaling,allResults.m_hitFractions[i])*(1.0f/simulationScaling);

		RaycastResult result;
		Vector3 hit;
		Vector3 normal;		

		const btRigidBody* constBody = btRigidBody::upcast(allResults.m_collisionObjects[i]);
		btRigidBody* body = const_cast<btRigidBody*>(constBody);
		int id = ((UnityRigidbodyData*) body->getUserPointer())->id;// rigidbodiesInverseMap[body];

		hit.x = p.getX();
		hit.y = p.getY();
		hit.z = p.getZ();



		btVector3 hitNormal =  allResults.m_hitNormalWorld[i];

		normal.x = hitNormal.getX();
		normal.y = hitNormal.getY();
		normal.z = hitNormal.getZ();

		result.hitPoint = hit;
		result.normal = normal;
		result.id = id;
		result.hit = 1;

		memcpy(resultArray+i*sizeof(RaycastResult),&result,sizeof(RaycastResult));
    }
	return hitCount;
}


int UnitySimulation::OverlapSphere(btRigidBody* body, short mask, short group, Vector3 center, float radius, unsigned char *hits){
    
	btVector3 min = center.TobtVector3()*simulationScaling - (btVector3(1.0f,1.0f,1.0f) * radius*simulationScaling);
	btVector3 max = center.TobtVector3()*simulationScaling + (btVector3(1.0f,1.0f,1.0f) * radius*simulationScaling);
    
    btCollisionObject * caller = btRigidBody::upcast(body);
    list<btCollisionObject*> collisionList;
    broadPhaseAABBCallback callback(caller,collisionList, group, mask);
    dynamicsWorld->getBroadphase()->aabbTest(min, max, callback);
    
    int count = 0;
    
    for(list<btCollisionObject*>::iterator it = collisionList.begin() ; it != collisionList.end() ; ++it){
        btCollisionObject* otherCollider = *it;
        btRigidBody* other = btRigidBody::upcast(otherCollider);
        int id = ((UnityRigidbodyData*) other->getUserPointer())->id;
        memcpy(hits+count*sizeof(int),&id,sizeof(int));
        count ++;
    }
        
    return count;
    
}

void UnitySimulation::SetWorldGravity(Vector3 gravity){
	dynamicsWorld->setGravity(gravity.TobtVector3());

}

RaycastResult UnitySimulation::Raycast( short mask, short group, Vector3 origin, Vector3 direction ) {
	//btRigidBody* body = rigidbodiesMap[id];
	btVector3 from = origin.TobtVector3()*simulationScaling;
	btVector3 to = direction.TobtVector3()*simulationScaling;

	btCollisionWorld::ClosestRayResultCallback	closestResults(from,to);
	closestResults.m_collisionFilterGroup = group;
	closestResults.m_collisionFilterMask = mask;
	closestResults.m_flags |= btTriangleRaycastCallback::kF_FilterBackfaces;
	dynamicsWorld->rayTest(from,to,closestResults);

	RaycastResult result;
	Vector3 hit;
	Vector3 normal;
	if (closestResults.hasHit())
	{
		btVector3 p = (from).lerp(to,closestResults.m_closestHitFraction)*(1.0f/simulationScaling);
		
		const btRigidBody* constBody = btRigidBody::upcast(closestResults.m_collisionObject);
		btRigidBody* body = const_cast<btRigidBody*>(constBody);
		int id = ((UnityRigidbodyData*) body->getUserPointer())->id;// rigidbodiesInverseMap[body];

		hit.x = p.getX();
		hit.y = p.getY();
		hit.z = p.getZ();

		btVector3 hitNormal =  closestResults.m_hitNormalWorld;

		normal.x = hitNormal.getX();
		normal.y = hitNormal.getY();
		normal.z = hitNormal.getZ();

		result.hitPoint = hit;
		result.normal = normal;
		result.id = id;
		result.hit = 1;

		return result;
	}
	else{
		hit.x = 0;
		hit.y = 0;
		hit.z = 0;
		result.hit = 0;
		return result;
	}


}


void UnitySimulation::SetRigidbodyPosition(btRigidBody* body, Vector3 position){

	btTransform trans;
	trans=body->getWorldTransform();



    UnityRigidbodyData* userData = (UnityRigidbodyData *)body -> getUserPointer();

	btTransform localUnityTransform;
	localUnityTransform.setIdentity();
	localUnityTransform.setOrigin(userData->centerOfMass*simulationScaling*-1);

	btTransform globalUnityTransform = trans * localUnityTransform;
	globalUnityTransform.setOrigin(position.TobtVector3()*simulationScaling);


	btTransform localCenterOfMass;
	localCenterOfMass.setIdentity();
	localCenterOfMass.setOrigin(userData->centerOfMass*simulationScaling);
	

	btTransform globalCenterOfMass = globalUnityTransform*localCenterOfMass;


	trans.setOrigin(globalCenterOfMass.getOrigin());
	trans.setRotation(globalCenterOfMass.getRotation());

	body->setWorldTransform(trans);
	

	body ->getMotionState()->getWorldTransform(trans);

	trans.setOrigin(globalCenterOfMass.getOrigin());
	trans.setRotation(globalCenterOfMass.getRotation());

	body->getMotionState() ->setWorldTransform(trans);

}

void UnitySimulation::SetRigidbodyRotation(btRigidBody* body, Quaternion rotation) {
	btTransform trans;

	trans=body->getWorldTransform();



	UnityRigidbodyData* userData = (UnityRigidbodyData *)body -> getUserPointer();

	btTransform localUnityTransform;
	localUnityTransform.setIdentity();
	localUnityTransform.setOrigin(userData->centerOfMass*simulationScaling*-1);

	btTransform globalUnityTransform = trans * localUnityTransform;
	globalUnityTransform.setRotation(rotation.TobtQuaternion());


	btTransform localCenterOfMass;
	localCenterOfMass.setIdentity();
	localCenterOfMass.setOrigin(userData->centerOfMass*simulationScaling);
	

	btTransform globalCenterOfMass = globalUnityTransform*localCenterOfMass;




	trans.setOrigin(globalCenterOfMass.getOrigin());
	trans.setRotation(globalCenterOfMass.getRotation());
	body->setWorldTransform(trans);


	body->getMotionState()->getWorldTransform(trans);
	trans.setOrigin(globalCenterOfMass.getOrigin());
	trans.setRotation(globalCenterOfMass.getRotation());
	body->getMotionState()->setWorldTransform(trans);
}


Transform UnitySimulation::GetTransform(btRigidBody* body){
    Transform transform;
    if (body && body->getMotionState()/* && body->isActive()*/){
        btTransform trans;
        body->getMotionState()->getWorldTransform(trans);
        
        UnityRigidbodyData* userData = (UnityRigidbodyData *)body -> getUserPointer();
        
        btTransform localUnityTransform;
        localUnityTransform.setIdentity();
        localUnityTransform.setOrigin(userData->centerOfMass*-1*simulationScaling);
        
        btTransform globalUnityTransform = trans * localUnityTransform;
        
        btVector3 velocity = body->getLinearVelocity()*(1/simulationScaling);
        btVector3 angularVelocity = body ->getAngularVelocity();
        
        btVector3 position = globalUnityTransform.getOrigin()*(1/simulationScaling);
        btQuaternion rotation = globalUnityTransform.getRotation();
        
        transform.position.x = float(position.getX());
        transform.position.y = float(position.getY());
        transform.position.z = float(position.getZ());
        
        transform.rotation.x = rotation.getX();
        transform.rotation.y = rotation.getY();
        transform.rotation.z = rotation.getZ();
        transform.rotation.w = rotation.getW();
        
        transform.velocity.x = velocity.x();
        transform.velocity.y = velocity.y();
        transform.velocity.z = velocity.z();
        
        
        transform.angularVelocity.x = angularVelocity.getX();
        transform.angularVelocity.y = angularVelocity.getY();
        transform.angularVelocity.z = angularVelocity.getZ();
        
        transform.id = 0;
        if(body->isActive()){
            transform.id = 1;
        }
        
        
    }
    
    return transform;
    
}


void UnitySimulation::SetGravity(btRigidBody* body, Vector3 g){
	body->setGravity(g.TobtVector3());
}

void UnitySimulation::RemoveContraint(int id){
	btGeneric6DofConstraint* constraint =  constraintsMap[id];
	constraintsMap.erase(id);

	map<int,BulletMotor*>::const_iterator got = servosMap.find (id);
	if(got == servosMap.end()){


	}else{
			servosMap.erase(id);
			delete got ->second;
	}

	dynamicsWorld->removeConstraint(constraint);
}


void UnitySimulation::Tick(btScalar timeStep){
	ResolveCollisions();
	ResolvePIDs();
}

void UnitySimulation::CallBack(btDynamicsWorld *world, btScalar timeStep) {
	UnitySimulation *w = static_cast<UnitySimulation *>(world->getWorldUserInfo());
	w ->dynamicsWorld-> clearForces();
	w->Tick(timeStep);
	
}

float FastInvSqrt(float x){

	float xhalf = 0.5f*x;
	int i = *(int*)&x;
	i = 0x5f375a86 - (i>>1);
	x = *(float*)&i;
	x = x*(1.5f - xhalf*x*x);
	return x;
}




void SetWorldGravity(Vector3 gravity){
	if(simulation){
		simulation->SetWorldGravity(gravity);
	}
}

RaycastResult BulletRaycast(short mask, short group,Vector3 from, Vector3 to){
	if(simulation){
		return simulation->Raycast(mask,group,from,to);
	}
	else{
		RaycastResult result;
		return result;
	}
}
int BulletRaycastAll(short mask, short group,Vector3 from, Vector3 to,unsigned char* hits){
	if(simulation){
		return simulation->RaycastAll(mask,group,from,to,hits);
	}else{
		return 0;
	}
}

int BulletOverlapSphere(btRigidBody* body, short mask, short group, Vector3 center, float radius, unsigned char * hits){
    return simulation->OverlapSphere(body, mask, group, center, radius, hits);
}

void SimulateI() {
	simulation = new UnitySimulation();
}

void EndSimulation(){
    delete simulation;
}

void SimulateStep(float step, float maxSubSteps, float fixedTimeStep ){
	simulation->Simulate(step , maxSubSteps, fixedTimeStep);
}

void UnitySimulation::SetDamping( btRigidBody* body, float linearDamping, float angularDamping) {
    body->setDamping(linearDamping, angularDamping);
}

void AddForce( btRigidBody* body, Vector3 force, Vector3 offset ){
	if(simulation){
		simulation->AddForce(body,force,offset);
	}
}
void SetCollisionShapeScale( btRigidBody* body,Vector3 scale){
	if(simulation){
		simulation->SetCollisionShapeScale(body,scale);
	}
}

void SetVelocity( btRigidBody* body, Vector3 velocity){
	if(simulation){
		simulation->SetVelocity(body, velocity);
	}
}

void SetAngularVelocity( btRigidBody* body, Vector3 angularVelocity){
	if(simulation){
		simulation->SetAngularVelocity(body,angularVelocity);
	}
}

void SetMotorAngle( int id, float angle ) {
	if(simulation){
		simulation->SetServoAngle(id,angle);
	}
}

void SetMotorVelocity( int id, float velocity ) {
	if(simulation){
		simulation->SetServoVelocity(id, velocity);
	}
}

void StopMotor( int id ) {
	if(simulation){
		simulation->StopServo(id);
	}
}

void ChangeLayer( btRigidBody* body, short mask, short group ) {
	if(simulation){
		simulation->ChangeLayer(body, mask, group);
	}
}

void SetRigidbodyPosition( btRigidBody* body, Vector3 position){
	if(simulation){
		simulation->SetRigidbodyPosition(body, position);
	}
}

void SetRigidbodyRotation( btRigidBody* body, Quaternion rotation) {
	if(simulation){
		simulation->SetRigidbodyRotation(body, rotation);
	}
}

void SetGravity( btRigidBody* body, Vector3 g){
	if(simulation){
		simulation->SetGravity(body, g);
	}
}

btRigidBody* AddRigidbody(unsigned char* newObjectsList){
	if(simulation){
		return ( simulation->AddRigidbody(newObjectsList) );
	}else{
		return 0;
	}
}

btCompoundShape* CreateCompoundShape(){
	if(simulation){
		return ( simulation->CreateCompoundShape() );
	}else{
		return 0;
	}
}

btCompoundShape* GetCompoundShape(int compoundShapeId){
	if(simulation){
		return ( simulation->GetCompoundShape(compoundShapeId) );
	}else{
		return 0;
	}
}

int IsCompoundShapeLoaded(int compoundShapeId){
	if(simulation){
		return ( simulation->IsCompoundShapeLoaded(compoundShapeId) );
	}else{
		return -1;
	}
}
void AddChildrenToCompoundShape(btCompoundShape* compoundShape, btCollisionShape* shape, Vector3 localPosition,Quaternion localRotation, Vector3 centerOfMass){
	if(simulation){
		simulation->AddChildrenToCompoundShape( compoundShape,  shape,  localPosition, localRotation,  centerOfMass) ;
	}
}
btCollisionShape* CreateBoxShape(Vector3 scale){
	if(simulation){
		return ( simulation->CreateBoxShape(scale) );
	}else{
		return 0;
	}
}

btCollisionShape* CreateSphereShape(float radio){
	if(simulation){
		return ( simulation->CreateSphereShape(radio) );
	}else{
		return 0;
	}
}


btCollisionShape* CreateCylinderShape(Vector3 scale){
	if(simulation){
		return ( simulation->CreateCylinderShape(scale) );
	}else{
		return 0;
	}
}
btCollisionShape* CreateConvexHullShape(Vector3* vertices, int size){
	if(simulation){
		return ( simulation->CreateConvexHullShape(vertices,size) );
	}else{
		return 0;
	}
}
btCollisionShape* CreateTriangleMeshShape(int id, int shapeId){
	if(simulation){
		return ( simulation->CreateTriangleMeshShape(id,shapeId) );
	}else{
		return 0;
	}
}
btRigidBody* AddBulletRigidbody(Rigidbody unityRigidbody, btCompoundShape* compoundShape){
	if(simulation){
		return ( simulation->AddBulletRigidbody(unityRigidbody,compoundShape) );
	}else{
		return 0;
	}
}

void AddConstraint (Constraint constraint, btRigidBody* bodyA, btRigidBody* bodyB){
	if(simulation){
		simulation->AddConstraint(constraint,bodyA,bodyB);
	}
}

void AddMotor (Motor motor){
	if(simulation){
		simulation->AddMotor(motor);
	}
}

void RemoveRidigbody ( btRigidBody* body ){
	if(simulation){
		simulation-> RemoveRidigbody(body);
	}
}

void RemoveContraint(int id){
	if(simulation){
		simulation->RemoveContraint(id);
	}
}

void RemoveMotor (int id){
	if(simulation){
		simulation->RemoveMotor(id);
	}
}

void SetDamping (btRigidBody* body, float linearDamping, float angularDamping ){
	if(simulation){
		simulation->SetDamping(body, linearDamping, angularDamping);
	}
}

void RegisterCallback (callback cb) {
    collision_callback = cb;
}

void RegisterImporterCallback (importerCallback cb){
    file_callback = cb;
}

void SetBodyMass ( btRigidBody* body,float mass, Vector3 inertiaPoint){
	if(simulation){
		simulation->SetBodyMass(body,mass,inertiaPoint);
	}
}

void SetLoadedData(char* data, int length){
	if(simulation){
		simulation->dataLength = length;
		simulation->dataFile = data;
	}
}

Transform GetTransform(btRigidBody* body){
	if(simulation){
		return simulation->GetTransform(body);
	}else{
        Transform empty;
        return empty;
    }
}
void SetForceUpdateAllAabbs(int active){
	if(simulation){
		simulation->SetForceUpdateAllAabbs(active);
	}
}

void SetWorldIterations(int iterations){
	if(simulation){
		simulation->SetWorldIterations(iterations);
	}
}
void SetWorldScaling(float scale){
	if(simulation){
		simulation->SetWorldScale(scale);
	}
}

void SerializeShape(char * path, int numVertex,int numTriangles,Vector3 * vertex, int * triangleIndex){
    fprintf(stderr,"Serializing... path %s\n", path);
    
    btBvhTriangleMeshShape * triangleMeshShape = 0;
    
    btVector3* points = 0;
    points = new btVector3[numVertex];
    
    btVector3 aabbMin, aabbMax;
    aabbMin.setValue(0, 0, 0);
    aabbMax.setValue(0, 0, 0);
    
    for(int i = 0; i < numVertex ; ++i) {
        btVector3  point = vertex[i].TobtVector3();
        points[i] = point;
        aabbMin.setX(min(aabbMin.x(),points[i].x()));
        aabbMin.setY(min(aabbMin.y(),points[i].y()));
        aabbMin.setZ(min(aabbMin.z(),points[i].z()));
        aabbMax.setX(max(aabbMax.x(),points[i].x()));
        aabbMax.setY(max(aabbMax.y(),points[i].y()));
        aabbMax.setZ(max(aabbMax.z(),points[i].z()));
    }
        
    int triangleStride = 3 * sizeof(int);
    int vertexStride = sizeof(btVector3);
    bool useQuantizedAabbCompression = true;
    btTriangleIndexVertexArray * indexedVertexArray = 0;
    
    indexedVertexArray = new btTriangleIndexVertexArray(numTriangles,
                                                        triangleIndex,
                                                        triangleStride,
                                                        numVertex,
                                                        (btScalar*) &points[0].x(),
                                                        vertexStride);
    
    triangleMeshShape = new btBvhTriangleMeshShape(indexedVertexArray,useQuantizedAabbCompression,aabbMin,aabbMax);
    int maxSerializeBufferSize = 1024*1024*5;
    btDefaultSerializer* serializer = new btDefaultSerializer(maxSerializeBufferSize);
    serializer->startSerialization();
    triangleMeshShape->serializeSingleShape(serializer);
    serializer->finishSerialization();
    FILE* file = fopen(path, "wb");
    fwrite(serializer->getBufferPointer(), serializer->getCurrentBufferSize(), 1, file);
    fclose(file);
    
    fprintf(stderr,"Serializing... DONE!\n");
}
